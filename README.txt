Mbuddies is a music social network app for android systems, the app implements a music player, a chat, 
friendship manager, geolocation and many more features . 
The app has been developed as the final work for the cycle of programming in Java. 
The app needs to work with the server running, unfortunately  the server is not online right now, 
but you can see a video demostration with the app working with the server running.

Technologies :

	Android
	db4o
	websockets
	gson
	Threads
	

Please remember to import the project libreries :

	android-support-v7-appcompat
	android-websockets-master

to your eclipse workspace and reference that projects as libraries to the Mbuddies project, 
you can find the project libraries in libs folder.

Additionally , you can view the project documentation and the video demonstration in the docs folder.

To download the Mbuddies server code, please visit:
https://gitlab.com/Amado/MbuddiesServer/tree/master
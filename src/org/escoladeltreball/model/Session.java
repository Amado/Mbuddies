package org.escoladeltreball.model;

import java.util.List;

import org.escoladeltreball.activities.ChatActivity;
import org.escoladeltreball.db4o.Db4oDAOUser;
import org.escoladeltreball.db4o.Db4oHelper;
import org.escoladeltreball.db4o.User;
import org.escoladeltreball.model.server.persistence.ModelConverter;
import org.escoladeltreball.model.server.persistence.UsersServ;
import org.escoladeltreball.support.Config;
import org.escoladeltreball.support.SerializableJson;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.db4o.ObjectContainer;
import com.google.gson.annotations.Expose;

/**
 * almacena datos de sesion y chache del usuario en shared preferences para
 * recuperarlos al reiniciar la aplicacion o entre activities el identificador
 * de las listas que al eliminarse no se han podido sincronizar con el servidor
 */
public class Session extends SerializableJson<Session> {

	@Expose
	private UsersServ user; // to serialize couldn't be static

	/**
	 * cuando un lista no se puede eliminar o modificar su contenido en servidor
	 * por cualquier motivo, se ha de agregar el favorite.identification a esta
	 * lista para eliminar o modificar su contenido cuando pueda volver a
	 * conectarse al servidor lo mismo pasa con el perfil de usuario
	 * 
	 */
	@Expose
	private static List<String> favoriteListToRemove;

	@Expose
	private static List<String> favoriteListToUpdate;

	/**
	 * se guardan las peticiones de amistad realizadas para evitar que se envien
	 * al servidor varias peticiones a la misma persona
	 */
	@Expose
	private static List<String> requestSent;

	@Expose
	private static boolean isUserToUpdate;

	// @Expose
	private static Session session;
	private static ChatActivity chatActivity;
	private static SharedPreferences sharedPref;

 

	private Session() {
	}

	/**
	 * Devuelve la session de las sharedPreferences
	 * 
	 * @param context
	 * @return
	 */
	public static Session getSession(Activity context) {

		if (Session.session == null) {
			if (Session.sharedPref == null) {
				Log.i(Config.TAG_INFO, "Opening sharedPreferences ...");
				Session.sharedPref = openPreferences(context
						.getApplicationContext());
			}
			String sessonJson = Session.sharedPref.getString(
					Config.SHARED_PREF_SESSION, null);
			if (sessonJson != null && sessonJson.length() > 3) {
				Log.i(Config.TAG_INFO,
						"getSession session from sharedPreferences..."
								+ sessonJson);
				Session.session = (Session) Session.getInstance()
						.deSerializeFromJson(sessonJson);
			} else {
				Session.session = Session.getInstance();
			}
		}

		try {
			Log.i(Config.TAG_INFO, "Session - getSession() -> " + session);
			Log.i(Config.TAG_INFO, "Session - getSession().getUser() -> "
					+ Session.session.getUser());

		} catch (Exception e) {
			Log.w(Config.TAG_WARNING, "on Session : session -> " + session);
			e.printStackTrace();
		}
		return Session.session;
	}

	/**
	 * Guarda los datos de la session del usuario y el usuario en la base de
	 * datos
	 * 
	 * @param context
	 */
	public static void saveSessionOnPreferences(Activity context) {
		try {

			if (Session.sharedPref == null) {
				openPreferences(context.getApplicationContext());
			}

			Editor editor = Session.sharedPref.edit();
			final String sessionToSave = Session.session.serializeToJson();
			Log.i(Config.TAG_INFO, "SESSION TO SAVE:" + sessionToSave);
			Log.i(Config.TAG_INFO, "SESSION TO SAVE:" + Session.session);
			editor.putString(Config.SHARED_PREF_SESSION, sessionToSave);
			editor.commit();
			Log.i(Config.TAG_INFO, "The preferences was saved on "
					+ Config.SHARED_PREF_SESSION);
			try {
				ObjectContainer oc;
				Db4oHelper dbHelper;
				Log.i(Config.TAG_INFO, "- savePreferences() ...");
				dbHelper = new Db4oHelper(context);
				oc = dbHelper.oc;
				// actualizamos el usuario de la base de datos
				Db4oDAOUser.deleteUserOwnerFromDb(oc);
				User user = ModelConverter.convertToLocalUser(Session.session
						.getUser());
				user.setIsOwner(true);
				Db4oDAOUser.insertUserToDB(oc, user);

				Log.i(Config.TAG_INFO,
						"Se ha actualizado el usuario en la base de datos local "
								+ Session.session.user);
			} catch (Exception e) {
				e.printStackTrace();
				// Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	synchronized private static SharedPreferences openPreferences(
			Context context) {
		return context.getApplicationContext().getSharedPreferences(
				Config.SHARED_PREF_SESSION, Context.MODE_PRIVATE);

	}

	private static Session getInstance() {
		if (session == null) {
			session = new Session();
		}
		return session;
	}

	public static UsersServ getUser(Activity context) {
		return getSession(context).session.user;
	}

	@Override
	public String toString() {
		return "Session [] user" + user;
	}

	public static List<String> getFavoriteListToRemove() {
		return favoriteListToRemove;
	}

	public static void setFavoriteListToRemove(List<String> favoriteListToRemove) {
		Session.favoriteListToRemove = favoriteListToRemove;
	}

	public static List<String> getFavoriteListToUpdate() {
		return favoriteListToUpdate;
	}

	public static void setFavoriteListToUpdate(List<String> favoriteListToUpdate) {
		Session.favoriteListToUpdate = favoriteListToUpdate;
	}

	public static UsersServ getUser() {
		// if (user == null) {
		// Log.w(Config.TAG_WARNING, "Warinng! on Session the user  = " + user);
		// }
		return Session.session.user;
	}

	public static void setUser(UsersServ user) {
		// this.user = user;
		Session.session.user = user;
		// Session.user = user;
	}

	public static Session getSession() {
		return Session.session;
	}

	public static void setSession(Session session) {
		Session.session = session;
	}

	public static boolean isUserToUpdate() {
		return isUserToUpdate;
	}

	public static void setUserToUpdate(boolean isUserToUpdate) {
		Session.isUserToUpdate = isUserToUpdate;
	}

	 
}

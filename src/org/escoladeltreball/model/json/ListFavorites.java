package org.escoladeltreball.model.json;

import java.io.Serializable;
import java.util.List;

import org.escoladeltreball.model.server.persistence.FavoritesServ;
import org.escoladeltreball.support.SerializableJson;

import com.google.gson.annotations.Expose;

public class ListFavorites extends SerializableJson<ListFavorites> implements
		Serializable {
	
	private static final long serialVersionUID = 1L;
	@Expose
	public List<FavoritesServ> list;

	public ListFavorites() {
	}

}

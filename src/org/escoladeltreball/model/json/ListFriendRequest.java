package org.escoladeltreball.model.json;

import java.io.Serializable;
import java.util.List;

import org.escoladeltreball.support.SerializableJson;

import com.google.gson.annotations.Expose;

public class ListFriendRequest extends
		SerializableJson<ListFriendRequest.RequestModel> implements
		Serializable {

	private static final long serialVersionUID = 1L;
	@Expose
	public List<RequestModel> list;

	public ListFriendRequest() {
	}

	public static class RequestModel extends SerializableJson<RequestModel>
			implements Serializable {

		private static final long serialVersionUID = 1L;
		public static String REQUEST_ACEPTED = "Y";
		public static String REQUEST_DENIED = "N"; 
		public static String REQUEST = "-";

		@Expose
		private Integer imgSrcUser;
		@Expose
		private Integer idRequest;
		@Expose
		private Long idSrcUser;
		@Expose
		private String nameSrcUser;
		@Expose
		private String nameTrgUser;
		@Expose
		private Long idTrgUser;
		@Expose
		private String status;
		 

		public RequestModel() {
		}

		public RequestModel(Integer idRequest, Integer imgSrcUser,
				Long idSrcUser, String nameSrcUser, Long idTrgUser,
				String nameTrgUser, String status) {
			this.idRequest = idRequest;
			this.idSrcUser = idSrcUser;
			this.nameSrcUser = nameSrcUser;
			this.imgSrcUser = imgSrcUser;
			this.idTrgUser = idTrgUser;
			this.nameTrgUser = nameTrgUser;
			this.status = status;
		}

		public RequestModel(Long idSrcUser, String nameSrcUser, Long idTrgUser,
				String nameTrgUser, String status) {
			this.idSrcUser = idSrcUser;
			this.nameSrcUser = nameSrcUser;
			this.idTrgUser = idTrgUser;
			this.nameTrgUser = nameTrgUser;
			this.status = status;
		}

	 

		@Override
		public String toString() {
			return "RequestModel [imgSrcUser=" + imgSrcUser + ", idRequest="
					+ idRequest + ", idSrcUser=" + idSrcUser + ", nameSrcUser="
					+ nameSrcUser + ", nameTrgUser=" + nameTrgUser
					+ ", idTrgUser=" + idTrgUser + ", status=" + status + "]";
		}

		public Integer getIdRequest() {
			return idRequest;
		}

		public void setIdRequest(Integer idRequest) {
			this.idRequest = idRequest;
		}

		public Long getIdSrcUser() {
			return idSrcUser;
		}

		public void setIdSrcUser(Long idSrcUser) {
			this.idSrcUser = idSrcUser;
		}

		public Long getIdTrgUser() {
			return idTrgUser;
		}

		public void setIdTrgUser(Long idTrgUser) {
			this.idTrgUser = idTrgUser;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getNameSrcUser() {
			return nameSrcUser;
		}

		public void setNameSrcUser(String nameSrcUser) {
			this.nameSrcUser = nameSrcUser;
		}

		public String getNameTrgUser() {
			return nameTrgUser;
		}

		public void setNameTrgUser(String nameTrgUser) {
			this.nameTrgUser = nameTrgUser;
		}

		public Integer getImgSrcUser() {
			return imgSrcUser;
		}

		public void setImgSrcUser(Integer imgSrcUser) {
			this.imgSrcUser = imgSrcUser;
		}

	 

	}
}

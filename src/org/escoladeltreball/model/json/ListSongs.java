package org.escoladeltreball.model.json;
import java.io.Serializable;
import java.util.List;

import org.escoladeltreball.model.server.persistence.CancionesServ;
import org.escoladeltreball.support.SerializableJson;

import com.google.gson.annotations.Expose;


public class ListSongs extends SerializableJson<ListSongs> implements
		Serializable {

	private static final long serialVersionUID = 1L;
	@Expose
	public List<CancionesServ> list;

	public ListSongs() {
	}

}
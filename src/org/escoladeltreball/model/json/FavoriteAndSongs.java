package org.escoladeltreball.model.json;

import java.io.Serializable;
import java.util.List;

import org.escoladeltreball.model.server.persistence.CancionesServ;
import org.escoladeltreball.model.server.persistence.FavoritesServ;
import org.escoladeltreball.support.SerializableJson;

import com.google.gson.annotations.Expose;

public class FavoriteAndSongs extends SerializableJson<FavoriteAndSongs>
		implements Serializable {

	private static final long serialVersionUID = 1L;

	@Expose
	public Long idUser;
	@Expose
	public List<CancionesServ> songs;
	@Expose
	public FavoritesServ favorites;

	public FavoriteAndSongs() {
	}

}
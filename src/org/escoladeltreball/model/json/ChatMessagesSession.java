package org.escoladeltreball.model.json;

import java.util.List;

import org.escoladeltreball.activities.ChatActivity;
import org.escoladeltreball.model.server.persistence.UsersServ;
import org.escoladeltreball.support.Config;
import org.escoladeltreball.support.SerializableJson;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.google.gson.annotations.Expose;

/**
 * almacena los ultimos mensajes del chat
 * 
 */
public class ChatMessagesSession extends SerializableJson<ChatMessagesSession> {

	@Expose
	private static UsersServ user;
	private static ChatMessagesSession friendRequestSession;
	private static ChatActivity chatActivity;
	private static SharedPreferences sharedPref;
	List<String> chatMessages;
	List<String> chatMsgUserNames;

	private ChatMessagesSession() {

	}

	/**
	 * Devuelve la session de las sharedPreferences
	 * 
	 * @param context
	 * @return
	 */
	public static ChatMessagesSession getSession(Context context) {
		if (friendRequestSession == null) {
			if (sharedPref == null) {
				openPreferences(context);
			}
			String sessonJson = sharedPref.getString(
					Config.SHARED_PREF_SESSION_CHAT_MESSAGES, null);
			if (sessonJson != null && sessonJson.length() > 3) {
				friendRequestSession = (ChatMessagesSession) ChatMessagesSession
						.getInstance().deSerializeFromJson(sessonJson);
			} else {
				friendRequestSession = ChatMessagesSession.getInstance();
			}
		}

		return friendRequestSession;
	}

	public static void saveSessionOnPreferences(Activity context) {
		if (sharedPref == null) {
			openPreferences(context);
		}
		Editor editor = sharedPref.edit();
		editor.putString(Config.SHARED_PREF_SESSION_CHAT_MESSAGES,
				friendRequestSession.serializeToJson());
		editor.commit();

	}

	private static void openPreferences(Context context) {
		sharedPref = context.getSharedPreferences(
				Config.SHARED_PREF_SESSION_CHAT_MESSAGES, Context.MODE_PRIVATE);
	}

	private static ChatMessagesSession getInstance() {
		if (friendRequestSession == null) {
			friendRequestSession = new ChatMessagesSession();
		}
		return friendRequestSession;
	}

}

package org.escoladeltreball.model.json;

import org.escoladeltreball.support.Config;
import org.escoladeltreball.support.SerializableJson;

import com.google.gson.annotations.Expose;

public class StatusPackage extends SerializableJson<StatusPackage> implements
		Config {
	public static final String STATUS_SUCCESS = Config.JSON_OB_TYPE_SUCCESS,
			STATUS_ERROR = Config.JSON_OB_TYPE_ERROR;

	@Expose
	private String type;
	@Expose
	private String message;

	public StatusPackage() {

	}

	public StatusPackage(String type, String message) {
		this.type = type;
		this.message = message;

	}

	@Override
	public String toString() {
		return "StatusPackage [type=" + type + ", message=" + message + "]";
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
package org.escoladeltreball.model.server.persistence;

import java.util.Set;

import com.google.gson.annotations.Expose;

public class FavoritesServ implements java.io.Serializable, Comparable<FavoritesServ> {

	private static final long serialVersionUID = 1L;
	@Expose
	private Long idList;
	@Expose
	private String nameOfList;
	@Expose
	private String identification;

	private UsersServ users;

	private Set<CancionesServ> canciones;

	public FavoritesServ() {
	}

	public FavoritesServ(Long idList, String nameOfList, String identification,
			UsersServ users, Set<CancionesServ> canciones) {
		super();
		this.idList = idList;
		this.nameOfList = nameOfList;
		this.identification = identification;
		this.users = users;
		this.canciones = canciones;
	}

	public FavoritesServ(String identification, String nameOfList) {
		this.identification = identification;
		this.nameOfList = nameOfList;
	}

	public FavoritesServ(UsersServ users, String identification, String nameOfList) {
		this.users = users;
		this.identification = identification;
		this.nameOfList = nameOfList;
	}

	public FavoritesServ(UsersServ users) {
		this.users = users;
	}

	@Override
	public String toString() {
		return "Favorites [idList=" + idList + ", users=" + users
				+ ", nameOfList=" + nameOfList + "]";
	}

	public Long getIdList() {
		return this.idList;
	}

	/**
	 * elimina las canciones huerfanas al eliminar un "Favorito"
	 * 
	 * @return
	 */
	public Set<CancionesServ> getCanciones() {
		return canciones;
	}

	public void setCanciones(Set<CancionesServ> canciones) {
		this.canciones = canciones;
	}

	public void setIdList(Long idList) {
		this.idList = idList;
	}

	public UsersServ getUsers() {
		return this.users;
	}

	public void setUsers(UsersServ users) {
		this.users = users;
	}

	public String getNameOfList() {
		return this.nameOfList;
	}

	public void setNameOfList(String nameOfList) {
		this.nameOfList = nameOfList;
	}

	public String getIdentification() {
		return identification;
	}

	public void setIdentification(String identification) {
		this.identification = identification;
	}

	@Override
	public int compareTo(FavoritesServ o) {
		return (this.nameOfList).toLowerCase().compareTo(
				(o.nameOfList).toLowerCase());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idList == null) ? 0 : idList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FavoritesServ other = (FavoritesServ) obj;
		if (idList == null) {
			if (other.idList != null)
				return false;
		} else if (!idList.equals(other.idList))
			return false;
		return true;
	}

}

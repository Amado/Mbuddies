package org.escoladeltreball.model.server.persistence;

import com.google.gson.annotations.Expose;

/**
 * solo es necesario tener un seguimiento de las entidades no sincronizadas en
 * el cliente.
 *
 */
public interface Sincronizable {
	 
	public static final String SINC = "SINC";
	 
	public static final String DESYNC = "DESYNC";

	@Expose
	public String sincronizado = DESYNC;

}

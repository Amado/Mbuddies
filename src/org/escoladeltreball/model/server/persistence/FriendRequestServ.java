package org.escoladeltreball.model.server.persistence;

// Generated 27-abr-2015 16:57:28 by Hibernate Tools 4.3.1

import org.escoladeltreball.support.SerializableJson;

 
public class FriendRequestServ extends SerializableJson<FriendRequestServ> implements
		java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private UsersServ usersByIdUserTarget;
	private UsersServ usersByIdUserSrc;
	private String status;

	public FriendRequestServ() {
	}

	public FriendRequestServ(UsersServ usersByIdUserTarget, UsersServ usersByIdUserSrc,
			String status) {
		this.usersByIdUserTarget = usersByIdUserTarget;
		this.usersByIdUserSrc = usersByIdUserSrc;
		this.status = status;
	}

	@Override
	public String toString() {
		return "FriendRequest [id=" + id + ", usersByIdUserTarget="
				+ usersByIdUserTarget + ", usersByIdUserSrc="
				+ usersByIdUserSrc + ", status=" + status + "]";
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
 
	public UsersServ getUsersByIdUserTarget() {
		return this.usersByIdUserTarget;
	}

	public void setUsersByIdUserTarget(UsersServ usersByIdUserTarget) {
		this.usersByIdUserTarget = usersByIdUserTarget;
	}
 
	public UsersServ getUsersByIdUserSrc() {
		return this.usersByIdUserSrc;
	}

	public void setUsersByIdUserSrc(UsersServ usersByIdUserSrc) {
		this.usersByIdUserSrc = usersByIdUserSrc;
	}
 
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((usersByIdUserSrc == null) ? 0 : usersByIdUserSrc.hashCode());
		result = prime
				* result
				+ ((usersByIdUserTarget == null) ? 0 : usersByIdUserTarget
						.hashCode());
		return result;
	}

	/**
	 *  
	 *  
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FriendRequestServ other = (FriendRequestServ) obj;
		if (usersByIdUserSrc == null) {
			if (other.usersByIdUserSrc != null) {
				return false;
			}
		} else if (!usersByIdUserSrc.equals(other.usersByIdUserSrc)) {
			return false;
		}
		if (usersByIdUserTarget == null) {
			if (other.usersByIdUserTarget != null) {
				return false;
			}
		} else if (!usersByIdUserTarget.equals(other.usersByIdUserTarget)) {
			return false;
		}
		//
		if (usersByIdUserSrc == null) {
			if (other.usersByIdUserTarget != null) {
				return false;
			}
		} else if (!usersByIdUserTarget.equals(other.usersByIdUserSrc)) {
			return false;
		}
		if (usersByIdUserTarget == null) {
			if (other.usersByIdUserSrc != null) {
				return false;
			}
		} else if (!usersByIdUserTarget.equals(other.usersByIdUserSrc)) {
			return false;
		}
		return true;
	}

}

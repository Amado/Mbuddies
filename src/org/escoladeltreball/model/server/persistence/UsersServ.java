package org.escoladeltreball.model.server.persistence;

// Generated 27-abr-2015 16:57:28 by Hibernate Tools 4.3.1

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.escoladeltreball.support.SerializableJson;

import com.google.gson.annotations.Expose;

//import javax.persistence.CascadeType;

public class UsersServ extends SerializableJson<UsersServ> implements
		Serializable, Comparable<UsersServ> {

	private static final long serialVersionUID = 1L;
	@Expose
	private Long id;
	@Expose
	private String name;
	private String password;
	@Expose
	private String city;
	@Expose
	private String country;
	@Expose
	private Boolean isConnected;
	@Expose
	private String personalDescription;
	@Expose
	private String musicPreferences;
	@Expose
	private byte[] photo;
	@Expose
	private String photoResource;
	@Expose
	private Integer idImg;
	@Expose
	private CancionesServ canciones;

	private Set<FavoritesServ> favoriteses = new HashSet<FavoritesServ>(0);

	private Set<FriendRequestServ> friendRequestsForIdUserSrc = new HashSet<FriendRequestServ>(
			0);
	private Set<FriendRequestServ> friendRequestsForIdUserTarget = new HashSet<FriendRequestServ>(
			0);

	public UsersServ() {
	}

	public UsersServ(Long id) {
		this.id = id;
	}

	private SortedSet<UsersServ> owners = new TreeSet<UsersServ>();

	private SortedSet<UsersServ> friends = new TreeSet<UsersServ>();

	public SortedSet<UsersServ> getOwners() {
		return owners;
	}

	public SortedSet<UsersServ> getFriends() {
		return friends;
	}

	public void setOwners(SortedSet<UsersServ> owners) {
		this.owners = owners;
	}

	public void setFriends(SortedSet<UsersServ> friends) {
		this.friends = friends;
	}

	@Override
	public String toString() {
		return "Users [id=" + id + ", name=" + name + ", password=" + password
				+ ", city=" + city + ", country=" + country + ", isConnected="
				+ isConnected + ", personalDescription=" + personalDescription
				+ ", musicPreferences=" + musicPreferences + ", photo="
				+ Arrays.toString(photo) + ", photoResource=" + photoResource
				+ ", idImg=" + idImg + "]";
	}

	public UsersServ(String name, String password) {
		this.name = name;
		this.password = password;
	}

	public UsersServ(String name) {
		this.name = name;
	}

	public UsersServ(String name, String password, String city, String country,
			boolean isConnected) {
		this.name = name;
		this.password = password;
		this.city = city;
		this.country = country;
		this.isConnected = isConnected;
	}

	public UsersServ(CancionesServ canciones, String name, String city,
			String country, Boolean isConnected, String personalDescription,
			String musicPreferences, byte[] photo, String password,
			String photoResource,
			Set<FriendRequestServ> friendRequestsForIdUserTarget,
			Set<FavoritesServ> favoriteses,
			Set<FriendRequestServ> friendRequestsForIdUserSrc) {
		this.canciones = canciones;
		this.name = name;
		this.city = city;
		this.country = country;
		this.isConnected = isConnected;
		this.personalDescription = personalDescription;
		this.musicPreferences = musicPreferences;
		this.photo = photo;
		this.password = password;
		this.photoResource = photoResource;
		this.friendRequestsForIdUserTarget = friendRequestsForIdUserTarget;
		this.favoriteses = favoriteses;
		this.friendRequestsForIdUserSrc = friendRequestsForIdUserSrc;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CancionesServ getCanciones() {
		return this.canciones;
	}

	public void setCanciones(CancionesServ canciones) {
		this.canciones = canciones;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Boolean getIsConnected() {
		return this.isConnected;
	}

	public void setIsConnected(Boolean isConnected) {
		this.isConnected = isConnected;
	}

	public String getPersonalDescription() {
		return this.personalDescription;
	}

	public void setPersonalDescription(String personalDescription) {
		this.personalDescription = personalDescription;
	}

	public String getMusicPreferences() {
		return this.musicPreferences;
	}

	public void setMusicPreferences(String musicPreferences) {
		this.musicPreferences = musicPreferences;
	}

	public byte[] getPhoto() {
		return this.photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	public Integer getIdImg() {
		return idImg;
	}

	public void setIdImg(Integer idImg) {
		this.idImg = idImg;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhotoResource() {
		return this.photoResource;
	}

	public void setPhotoResource(String photoResource) {
		this.photoResource = photoResource;
	}

	public Set<FriendRequestServ> getFriendRequestsForIdUserTarget() {
		return this.friendRequestsForIdUserTarget;
	}

	public void setFriendRequestsForIdUserTarget(
			Set<FriendRequestServ> friendRequestsForIdUserTarget) {
		this.friendRequestsForIdUserTarget = friendRequestsForIdUserTarget;
	}

	public Set<FriendRequestServ> getFriendRequestsForIdUserSrc() {
		return this.friendRequestsForIdUserSrc;
	}

	public void setFriendRequestsForIdUserSrc(
			Set<FriendRequestServ> friendRequestsForIdUserSrc) {
		this.friendRequestsForIdUserSrc = friendRequestsForIdUserSrc;
	}

	public Set<FavoritesServ> getFavoriteses() {
		return this.favoriteses;
	}

	public void setFavoriteses(Set<FavoritesServ> favoriteses) {
		this.favoriteses = favoriteses;
	}

	@Override
	public int compareTo(UsersServ o) {
		return this.name.compareTo(o.getName());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsersServ other = (UsersServ) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}

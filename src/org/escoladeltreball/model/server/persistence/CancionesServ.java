package org.escoladeltreball.model.server.persistence;

import java.util.HashSet;
import java.util.Set;

import org.escoladeltreball.support.SerializableJson;

import com.google.gson.annotations.Expose;

public class CancionesServ extends SerializableJson<UsersServ> implements
		java.io.Serializable, Comparable<CancionesServ> {

	private static final long serialVersionUID = 1L;
	@Expose
	private Long idSong;
	@Expose
	private String songName;
	@Expose
	private String songAuthor;
	@Expose
	private String srcPath;
	@Expose
	private Integer idSrc;
	@Expose
	private byte[] photo;
	@Expose
	private String photoEncoded;

	private Set<UsersServ> userses = new HashSet<UsersServ>(0);

	private Set<FavoritesServ> favorites;

	// private Integer version;

	// public void setVersion(Integer version) {
	// this.version = version;
	// }

	public CancionesServ() {
	}

	public CancionesServ(Long idSong, String songName, String songAuthor,
			String srcPath) {
		this.idSong = idSong;
		this.songName = songName;
		this.songAuthor = songAuthor;
		this.srcPath = srcPath;
	}

	public CancionesServ(String songName, String songAuthor, String srcPath,
			String photoEncoded) {
		this.songName = songName;
		this.songAuthor = songAuthor;
		this.srcPath = srcPath;
	}

	public CancionesServ(String songName, String songAuthor, String srcPath) {
		this.songName = songName;
		this.songAuthor = songAuthor;
		this.srcPath = srcPath;
	}

	public CancionesServ(String songName) {
		this.songName = songName;
	}

	public CancionesServ(String songName, String songAuthor,
			Set<UsersServ> userses) {
		this.songName = songName;
		this.songAuthor = songAuthor;
		this.userses = userses;
	}

	@Override
	public String toString() {
		return "Songs [idSong=" + idSong + ", songName=" + songName
				+ ", songAuthor=" + songAuthor + "]";
	}

	public Long getIdSong() {
		return this.idSong;
	}

	public Set<FavoritesServ> getFavorites() {
		return this.favorites;
	}

	public void setFavorites(Set<FavoritesServ> favorites) {
		this.favorites = favorites;
	}

	public void setIdSong(Long idSong) {
		this.idSong = idSong;
	}

	public String getSongName() {
		return this.songName;
	}

	public void setSongName(String songName) {
		this.songName = songName;
	}

	public String getSongAuthor() {
		return this.songAuthor;
	}

	public void setSongAuthor(String songAuthor) {
		this.songAuthor = songAuthor;
	}

	public String getSrcPath() {
		return srcPath;
	}

	public void setSrcPath(String srcPath) {
		this.srcPath = srcPath;
	}

	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	public Integer getIdSrc() {
		return idSrc;
	}

	public void setIdSrc(Integer idSrc) {
		this.idSrc = idSrc;
	}

	public Set<UsersServ> getUserses() {
		return this.userses;
	}

	public void setUserses(Set<UsersServ> userses) {
		this.userses = userses;
	}

	public String getPhotoEncoded() {
		return photoEncoded;
	}

	public void setPhotoEncoded(String photoEncoded) {
		this.photoEncoded = photoEncoded;
	}

	@Override
	public int compareTo(CancionesServ o) {
		return (this.songName + this.songAuthor).toLowerCase().compareTo(
				(o.songName + o.songAuthor).toLowerCase());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idSong == null) ? 0 : idSong.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CancionesServ other = (CancionesServ) obj;
		if (idSong == null) {
			if (other.idSong != null)
				return false;
		} else if (!idSong.equals(other.idSong))
			return false;
		return true;
	}

}

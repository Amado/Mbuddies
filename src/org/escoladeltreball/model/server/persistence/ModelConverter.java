package org.escoladeltreball.model.server.persistence;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.escoladeltreball.db4o.Favorites;
import org.escoladeltreball.db4o.Song;
import org.escoladeltreball.db4o.User;
import org.escoladeltreball.model.Session;

import android.app.Activity;

/**
 * Factory class to shift between server and local data models
 */
public class ModelConverter {

	/**
	 * crea un usuario local not owner
	 * 
	 * @param serverUser
	 * @return
	 */
	public static User convertToLocalUser(final UsersServ serverUser) {

		if (serverUser == null) {
			return null;
		}

		User locUser = new User();
		locUser.setId(serverUser.getId());
		locUser.setName(serverUser.getName());
		locUser.setPass(serverUser.getPassword());
		locUser.setCity(serverUser.getCity());
		locUser.setCountry(serverUser.getCountry());
		locUser.setPersonalDescription(serverUser.getPersonalDescription());
		locUser.setMusicPreferences(serverUser.getMusicPreferences());

		locUser.setPhotoResource(serverUser.getIdImg() == null ? 0 : serverUser
				.getIdImg());
		locUser.setIsOwner(false);
		locUser.setFriendShipState(User.FRIENDSHIP_STATE_WAITING);
		return locUser;
	}

	public static List<User> converntTolocalUsers(
			final List<UsersServ> serverUsers) {
		if (serverUsers == null) {
			return null;
		}

		List<User> localUsers = new ArrayList<User>();

		if (serverUsers != null) {
			for (UsersServ servUserN : serverUsers) {
				localUsers.add(convertToLocalUser(servUserN));
			}
		}

		return localUsers;
	}

	public static UsersServ convertToServerUser(final User localUser) {

		if (localUser == null) {
			return null;
		}

		UsersServ servUser = new UsersServ();
		// servUser.setId(localUser.getId());
		servUser.setName(localUser.getName());
		servUser.setPassword(localUser.getPass());
		servUser.setCity(localUser.getCity());
		servUser.setCountry(localUser.getCountry());
		servUser.setPersonalDescription(localUser.getPersonalDescription());
		servUser.setMusicPreferences(localUser.getMusicPreferences());
		servUser.setIdImg(localUser.getPhotoResource());
		return servUser;
	}

	public static List<UsersServ> convertToServerUsers(
			final List<User> localUsers) {
		List<UsersServ> serverUsers = new ArrayList<UsersServ>();

		if (localUsers == null) {
			return null;
		}

		for (User localUserN : localUsers) {
			serverUsers.add(convertToServerUser(localUserN));
		}

		return serverUsers;
	}

	public static CancionesServ convertToServerCancion(Song localSong) {
		if (localSong == null) {
			return null;
		}
		CancionesServ servCancion = new CancionesServ();
		servCancion.setSrcPath(localSong.getSongPath());
		servCancion.setSongName(localSong.getSongName());
		servCancion.setPhoto(localSong.getSongImage());
		return servCancion;
	}

	public static List<CancionesServ> convertToServerCanciones(
			final List<Song> localSongs) {
		if (localSongs == null) {
			return null;
		}

		List<CancionesServ> serverCanciones = new ArrayList<CancionesServ>();

		for (Song localSongN : localSongs) {
			serverCanciones.add(convertToServerCancion(localSongN));
		}
		return serverCanciones;
	}

	public static ArrayList<Song> convertToLocalSongs(
			Set<CancionesServ> servCanciones) {

		if (servCanciones == null) {
			return null;
		}

		ArrayList<Song> localSongs = new ArrayList<Song>();

		for (CancionesServ servCancionN : servCanciones) {
			localSongs.add(convertToLocalSong(servCancionN));
		}
		return localSongs;
	}

	public static Song convertToLocalSong(CancionesServ servCancion) {
		if (servCancion == null) {
			return null;
		}
		Song locSong = new Song();
		locSong.setSongPath(servCancion.getSrcPath());
		locSong.setSongName(servCancion.getSongName());
		locSong.setSongImage(servCancion.getPhoto());
		return locSong;
	}

	public static FavoritesServ convertToServerFavorites(
			final Favorites localFavorite, final boolean withSongs) {

		if (localFavorite == null) {
			return null;
		}
		FavoritesServ servFavorite = new FavoritesServ();
		servFavorite.setNameOfList(localFavorite.getListName());
		servFavorite.setIdentification(localFavorite.getIdentification());
		if (withSongs == true) {
			servFavorite.setCanciones(new HashSet<CancionesServ>(
					convertToServerCanciones(localFavorite.getSongs())));
		}
		return servFavorite;
	}

	public static Favorites convertToLocalFavorites(
			final FavoritesServ servFavorites, boolean withSongs) {

		if (servFavorites == null) {
			return null;
		}
		Favorites locFavorite = new Favorites();
		locFavorite.setListName(servFavorites.getNameOfList());
		if (withSongs == true) {
			locFavorite.setSongs(convertToLocalSongs(servFavorites
					.getCanciones()));
		}
		locFavorite.setIdentification(servFavorites.getIdentification());
		return locFavorite;
	}

	public static List<FavoritesServ> convertToServerFavorites(
			final List<Favorites> localFavorites, final boolean withSongs) {

		if (localFavorites == null) {
			return null;
		}

		List<FavoritesServ> serverFavorites = new ArrayList<FavoritesServ>();
		for (Favorites localFavoriteN : localFavorites) {
			serverFavorites.add(convertToServerFavorites(localFavoriteN,
					withSongs));
		}
		return serverFavorites;
	}

	public static List<Favorites> convertToLocalFavorites(
			final List<FavoritesServ> servFavorites, final boolean withSongs) {

		if (servFavorites == null) {
			return null;
		}

		List<Favorites> serverFavorites = new ArrayList<Favorites>();
		for (FavoritesServ localFavoriteN : servFavorites) {
			serverFavorites.add(convertToLocalFavorites(localFavoriteN,
					withSongs));
		}
		return serverFavorites;
	}

	/**
	 * Crea un objeto favorito correpondiente al modelo del la base de datos
	 * local, que es compatible con la base de datos remota agregandole el
	 * atributo favoritesIdentification con el formato: idUsuario:x, donde x es
	 * la hora del sistema en milisegundos.
	 * 
	 * @param nameOfList
	 * @param context
	 * @return
	 */
	public static Favorites createLocalFavorite(String nameOfList,
			Activity actvity) {
		Session session = Session.getSession(actvity);
		final Long userId = session.getUser().getId();
		Favorites favoriteList = new Favorites(nameOfList, userId);
		//
		// final String favoritesIdentification = userId + ":"
		// + System.currentTimeMillis();
		// favoriteList.setIdentification(favoritesIdentification);

		return favoriteList;

	}

}

package org.escoladeltreball.model;

public class Contact {

	private Long id;
	private String sessionId, city, country, name, email;
	private Boolean isConnected;
	private Integer photoId;
	private int numNewMessages;
	private boolean isSelected;
 

	public Contact() {
	}

	public Contact(Long id, String sessionId, String city, String name,
			String email, Boolean isConnected, Integer photoId) {
		super();
		this.id = id;
		this.sessionId = sessionId;
		this.city = city;
		this.name = name;
		this.email = email;
		this.isConnected = isConnected;
		this.photoId = photoId;
		numNewMessages = 0;
		isSelected = false;

	}
	
	
	


	@Override
	public String toString() {
		return "Contact [id=" + id + ", sessionId=" + sessionId + ", city="
				+ city + ", country=" + country + ", name=" + name + ", email="
				+ email + ", isConnected=" + isConnected + ", photoId="
				+ photoId + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contact other = (Contact) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getIsConnected() {
		return isConnected;
	}

	public void setIsConnected(Boolean isConnected) {
		this.isConnected = isConnected;
	}

	public Integer getPhotoId() {
		return photoId;
	}

	public void setPhotoId(Integer photoId) {
		this.photoId = photoId;
	}

	public int getNumNewMessages() {
		return numNewMessages;
	}

	public void setNumNewMessages(int numNewMessages) {
		this.numNewMessages = numNewMessages;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

}

package org.escoladeltreball.model;

public class Message {
	private Long fromIdUser, targetId;
	private String message, fromUserName;
	private boolean isSelf;

	public Message() {
	}

	public Message(Long fromIdUser,String fromUserName, String message, boolean isSelf,
			Long targetId) {
		this.fromIdUser = fromIdUser;
		this.fromUserName = fromUserName;
		this.message = message;
		this.isSelf = isSelf;
		this.targetId = targetId;
	}

	public Long getFromIdUser() {
		return fromIdUser;
	}

	public void setFromIdUser(Long fromIdUser) {
		this.fromIdUser = fromIdUser;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isSelf() {
		return isSelf;
	}

	public void setSelf(boolean isSelf) {
		this.isSelf = isSelf;
	}
	
	public String getFromUserName() {
		return fromUserName;
	}

	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}
	
	public Long getTargetId() {
		return targetId;
	}

	public void setTargetId(Long targetId) {
		this.targetId = targetId;
	}

	@Override
	public String toString() {
		return "Message [fromName=" + fromIdUser + ", message=" + message
				+ ", targetName=" + targetId + ", isSelf=" + isSelf + "]";
	}

}

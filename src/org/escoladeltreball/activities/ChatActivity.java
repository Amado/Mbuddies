package org.escoladeltreball.activities;

import java.util.ArrayList;
import java.util.List;

import org.escoladeltreball.activities.adpaters.ContactsListAdapter;
import org.escoladeltreball.activities.adpaters.MessagesListAdapter;
import org.escoladeltreball.chat.R;
import org.escoladeltreball.chat.service.ChatIntentService;
import org.escoladeltreball.chat.service.ChatSocketListenerOnService;
import org.escoladeltreball.friendsList.FriendsListActivity;
import org.escoladeltreball.friendshipRequests.FriendshipRequestsActivity;
import org.escoladeltreball.manager.RequestManager;
import org.escoladeltreball.model.Contact;
import org.escoladeltreball.model.Message;
import org.escoladeltreball.model.Session;
import org.escoladeltreball.model.server.persistence.UsersServ;
import org.escoladeltreball.preferedList.friends.FavoriteFriendPlayList;
import org.escoladeltreball.support.Config;
import org.escoladeltreball.support.UtilsChat;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

/*
 * cuando creamos la acivity le tenemos que pasar el objeto session.
 */
public class ChatActivity extends Activity implements OnClickListener, Config {

	private static Button btnSend;
	private static EditText inputMsg;

	// Chat messages/contacts list adapters
	private static ListView listViewMessages;
	private static MessagesListAdapter messagesListAdapter;
	public static ListView listViewContactos;
	public static ContactsListAdapter contactsListAdapter;
	private static UtilsChat utilsChat;
	private static boolean nextAutoCloseValue = false, autoClose = false;
	private static boolean comesFromLogin = false;
	private static ChatSocketListenerOnService chatSocketListener;
	private static boolean isActive = false;

	// private static ChatIntentService chatIntentService;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.i(TAG_INFO,
				"Creating the Chat activity ...............................");

		// List<Contact> listContacts = ChatIntentService.getListContacts();
		// establish the screen orientation before the content view is loaded
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chat);

		utilsChat = new UtilsChat(getApplicationContext());

		btnSend = (Button) findViewById(R.id.btnSend);
		inputMsg = (EditText) findViewById(R.id.inputMsg);
		listViewMessages = (ListView) findViewById(R.id.list_view_messages);
		listViewContactos = (ListView) findViewById(R.id.list_view_contactos);

		btnSend.setOnClickListener(this);

		UtilActivity.enableGui(this.getViewRoot(), true);

		if (true) {
			comesFromLogin = false;
			new Thread() {
				@Override
				public void run() {

					try {
						chatSocketListener = new ChatSocketListenerOnService(
								ChatActivity.this);
						chatSocketListener.start();

					} catch (Error e) {
						/**
						 * cuando intentamos iniciar el chat socket listener,
						 * este ejecuta un metodo
						 * chatActivity.unregisterReceiver(receiver); que lanza
						 * una exception si no se ha llamado antes a
						 * chatActivity.unregisterReceiver(receiver);
						 */
						Log.i(Config.TAG_INFO,
								"------------------------------------------------------------------------------"
										+ " EXPECTED EXCEPTION (don't worry)"
										+ "-------------------------------------------------------------------");
						// e.printStackTrace();
					} catch (Throwable e) {
						Log.i(Config.TAG_INFO,
								"------------------------------------------------------------------------------"
										+ " EXPECTED EXCEPTION (don't worry)"
										+ "-------------------------------------------------------------------");
						// e.printStackTrace();
					}
				}

			}.start();
		}

		// ChatIntentService chatIntentService = new ChatIntentService(this);
		ChatIntentService.setActivity(this);
		startService(new Intent(this, ChatIntentService.class));

		this.runOnUiThread(new Runnable() {

			@Override
			public void run() {

				messagesListAdapter = new MessagesListAdapter(
						ChatActivity.this, new ArrayList<Message>());
				contactsListAdapter = new ContactsListAdapter(
						ChatActivity.this, ChatIntentService.getListContacts());

				listViewMessages.setAdapter(messagesListAdapter);
				listViewContactos.setAdapter(contactsListAdapter);

			}
		});
		/**
		 * cada vez que se recrea la activity recuperamos el estado del
		 * chatIntentService
		 */

		if (autoClose == true) {
			autoClose = nextAutoCloseValue;

			Intent nexintent = new Intent(ChatActivity.this,
					Config.ConfigActivity.ACTIVITY_POS_LOGIN);

			nexintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			nexintent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			startActivity(nexintent);
			this.finish();
		} else {

			isActive = true;
		}

		/**
		 * ------------------------------------------------------------
		 */

	}

	/**
	 * al volver a la activity recuperamos los datos del servicio del chat y
	 * actualizamos la gui
	 */
	@Override
	protected void onResume() {
		super.onResume();
		findFriends();

		this.runOnUiThread(new Runnable() {

			@Override
			public void run() {

				/**
				 * ------------------------------------------------------------
				 */

				if (autoClose == true) {
					autoClose = nextAutoCloseValue;
					Log.d(Config.TAG_INFO, "CHAT ON RESUME-CLOSE ON CREATE");
					finish();
				} else {
					isActive = true;
					ChatIntentService.setNumMessagesNotSeeing(0);

					/**
					 * refrescamos la lista de contactos al volver al chat
					 */
					Log.d(Config.TAG_INFO,
							"CHAT ON RESUME- NOT CLOSE ON CREATE");
					if (ChatIntentService.getSelectedContactId() != null) {
						ChatActivity.this.selectMessageList(
								ChatIntentService.getSelectedContactId(),
								ChatIntentService.getSelectedContactPos());
					}

					contactsListAdapter = new ContactsListAdapter(
							ChatActivity.this, ChatIntentService
									.getListContacts());

					listViewContactos.setAdapter(contactsListAdapter);
					contactsListAdapter.notifyDataSetChanged();

				}

			}
		});

	}

	/**
	 * Busca los amigos del usuario y actualiza el listview
	 * 
	 * @param idUser
	 */
	public void findFriends() {

		final ProgressDialog dialog = UtilMessage.showProgressDialog(this,
				"Connecting with the server.... please wait");

		new Thread(new Runnable() {
			public void run() {
				try {
					final Long idUser = Session.getSession(ChatActivity.this)
							.getUser().getId();

					Session.getSession(ChatActivity.this);
					Log.i("INFO", " viewProfile--" + idUser);

					List<UsersServ> responseUsers = RequestManager
							.fecthFriendsFromUser(String.valueOf(idUser));

					if (responseUsers == null) {
						ChatActivity.this.runOnUiThread(new Runnable() {

							@Override
							public void run() {
								Log.i(Config.TAG_INFO,
										"There isn't any friend from user "
												+ idUser);

								contactsListAdapter = new ContactsListAdapter(
										ChatActivity.this,
										new ArrayList<Contact>());
								listViewContactos
										.setAdapter(contactsListAdapter);
								contactsListAdapter.notifyDataSetChanged();

								ChatIntentService
										.setListContacts(new ArrayList<Contact>());

							}
						});
					} else {
						ChatIntentService.setSelectedContactId(null);

						final List<Contact> contactsConnected = new ArrayList<Contact>();
						for (UsersServ friendN : responseUsers) {
							if (friendN.getIsConnected()) {
								Contact contactTmp = new Contact();
								contactTmp.setId(friendN.getId());
								contactTmp.setName(friendN.getName());
								contactTmp.setPhotoId(friendN.getIdImg());
								for (Contact contact : ChatIntentService
										.getListContacts()) {
									if (contact.getId().equals(friendN.getId())) {
										contactTmp.setNumNewMessages(contact
												.getNumNewMessages());
										contactTmp.setSelected(contactTmp
												.isSelected());
										if (contactTmp.isSelected()) {
											ChatIntentService
													.setSelectedContactId(contact
															.getId());
										}
									}
								}
								contactsConnected.add(contactTmp);
							}
						}

						ChatActivity.this.runOnUiThread(new Runnable() {

							@Override
							public void run() {

								if (ChatIntentService.getSelectedContactId() == null) {
									messagesListAdapter = new MessagesListAdapter(
											ChatActivity.this,
											new ArrayList<Message>());
									messagesListAdapter.notifyDataSetChanged();

								} else {

									Log.i(TAG_INFO,
											" find friends ---- ChatIntentService.getSelectedContactId() "
													+ ChatIntentService
															.getSelectedContactId());
									ChatIntentService.getSelectedContactId();
								}

								contactsListAdapter = new ContactsListAdapter(
										ChatActivity.this, contactsConnected);
								listViewContactos
										.setAdapter(contactsListAdapter);
								contactsListAdapter.notifyDataSetChanged();
								ChatIntentService
										.setListContacts(contactsConnected);

							}
						});
					}

				} catch (Exception e) {
					e.printStackTrace();
					ChatActivity.this.runOnUiThread(new Runnable() {

						@Override
						public void run() {
							final String erroMsg = "Error retrieving the friends"
									+ " From Server. Try Later";
							UtilMessage.dismisProgressDialog(dialog);
							UtilMessage.showErrorMessage(ChatActivity.this,
									erroMsg);

						}
					});
				} finally {
					ChatActivity.this.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							UtilMessage.dismisProgressDialog(dialog);
						}
					});
				}
			}

		}).start();

	}

	synchronized public void selectMessageList(final Long keyListMsg,
			final Integer pos) {
		if (pos == null || keyListMsg == null) {
			return;
		}
		// this.selectedTargetUser = keyListMsg;

		ChatIntentService.setSelectedContactId(keyListMsg);
		ChatIntentService.setSelectedContactPos(pos);
		Log.i(Config.TAG_INFO, "changeMessageList()..... selectedTargetUser = "
				+ ChatIntentService.getSelectedContactId());

		try {

			/**
			 * cada vez que canviamos seleccionamos un contacto diferente,
			 * debemos instanciar un adaptador nuevo porque no podemos modificar
			 * la lista de mensajes actual
			 */

			this.runOnUiThread(new Runnable() {

				@Override
				public void run() {
					messagesListAdapter = new MessagesListAdapter(
							ChatActivity.this, ChatIntentService
									.getMapListMessages().get(keyListMsg));
					listViewMessages.setAdapter(messagesListAdapter);
					messagesListAdapter.notifyDataSetChanged();
				}
			});

			List<Contact> contactos = ChatIntentService.getListContacts();
			for (Contact contactN : contactos) {
				if (contactN.getId().equals(keyListMsg)) {
					contactN.setNumNewMessages(0);
					// contactN.setSelected(true);
					break;
				} else {
					contactN.setSelected(false);
				}
			}

			// // seleccionado

			this.runOnUiThread(new Runnable() {

				@Override
				public void run() {
					contactsListAdapter = new ContactsListAdapter(
							ChatActivity.this, ChatIntentService
									.getListContacts());
					listViewContactos.setAdapter(contactsListAdapter);

				}
			});

		} catch (Throwable e) {

			Log.e(Config.TAG_ERROR,
					"Error. no se encuentra la lista de mensajes para el usuario: "
							+ keyListMsg);
			e.printStackTrace();
		}
	}

	public View getViewRoot() {
		return (View) this.findViewById(R.id.chatActivityRootLayout);
	}

	@Override
	public void onClick(View view) {
		sendMessage();
	}

	/**
	 * Envia el mensaje al web socket del servidor
	 * */
	synchronized public void sendMessage() {
		// Enviamos el mensaje al servidor
		final String msg = inputMsg.getText().toString();

		if (msg.trim().isEmpty()) {
			return;
		}
		final Long sourceIdUser = ChatIntentService.getUserIdF();
		final String userName = ChatIntentService.getUserNameF();

		final String message = utilsChat.getSendMessageToTarget(msg,
				String.valueOf(sourceIdUser), userName,
				String.valueOf(ChatIntentService.getSelectedContactId()));

		if (ChatIntentService.getClient() != null) {
			ChatIntentService.getClient().send(message);
			inputMsg.setText("");
		}

	}

	synchronized public void updateFriendProfile(final Contact contact) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {

				List<Contact> contactos = ChatIntentService.getListContacts();
				List<Contact> contactosTmp = new ArrayList<Contact>();

				for (Contact contactN : contactos) {
					if (contactN.getId().equals(contact.getId())) {
						final int tmpNumMsgs = contactN.getNumNewMessages();

						Contact contactUp = new Contact();
						contactUp.setId(contact.getId());
						contactUp.setIsConnected(true);
						contactUp.setPhotoId(contact.getPhotoId());
						contactUp.setName(contact.getName());
						contactUp.setNumNewMessages(tmpNumMsgs);
						contactosTmp.add(contactUp);
					} else {
						contactosTmp.add(contactN);
					}

				}
				contactsListAdapter = new ContactsListAdapter(
						ChatActivity.this, contactosTmp);

				listViewContactos.setAdapter(contactsListAdapter);
				contactsListAdapter.notifyDataSetChanged();
			}
		});
	}

	synchronized public void removeContactFromList(final Long idContact) {
		Log.i("INFO", "CHAT ACTIVITY REMOVING CONTACT FROM LIST ID CONTACT = "
				+ idContact);
		runOnUiThread(new Runnable() {
			@Override
			public void run() {

				List<Contact> contactos = ChatIntentService.getListContacts();
				List<Contact> contactosTmp = new ArrayList<Contact>();

				for (Contact contactN : contactos) {
					if (contactN.getId().equals(idContact) == false) {
						contactosTmp.add(contactN);
					}
				}
				contactsListAdapter = new ContactsListAdapter(
						ChatActivity.this, contactosTmp);
				listViewContactos.setAdapter(contactsListAdapter);
				contactsListAdapter.notifyDataSetChanged();
				// Toast.makeText(
				// ChatActivity.this,
				// "REMOVED  CONTACT WITH ID " + idContact
				// + " FORM CHAT...", Toast.LENGTH_LONG).show();
				playBeep();
			}
		});
	}

	/**
	 * Añade el mensaje a la listView
	 * */
	synchronized public void appendMessageFromFriend(final Message newMsg) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				try {

					final Long listMsgsKey = newMsg.isSelf() ? newMsg
							.getTargetId() : newMsg.getFromIdUser();

					List<Message> messagesList = ChatIntentService
							.getMapListMessages().get(listMsgsKey);

					if (messagesList == null) {
						messagesList = new ArrayList<Message>();

						ChatIntentService.getMapListMessages().put(listMsgsKey,
								messagesList);

					}

					Log.i("ChatActivity", "adding new message message !! "
							+ newMsg + " from " + newMsg.getFromIdUser());

					messagesList.add(newMsg);
					messagesListAdapter.notifyDataSetChanged();
					//
					if (newMsg.isSelf() == false
							&& ChatIntentService.getSelectedContactId() != null
							&& ChatIntentService.getSelectedContactId().equals(
									newMsg.getFromIdUser()) == false) {

						int numMsgs = 0;
						playBeep();
						List<Contact> contactos = ChatIntentService
								.getListContacts();

						for (Contact contactN : contactos) {
							if (contactN.getId().equals(newMsg.getFromIdUser())) {
								numMsgs = contactN.getNumNewMessages() + 1;
								contactN.setNumNewMessages(numMsgs);
								break;
							}
						}

						contactsListAdapter = new ContactsListAdapter(
								ChatActivity.this,
								ChatIntentService.getListContacts());
						listViewContactos.setAdapter(contactsListAdapter);
						contactsListAdapter.notifyDataSetChanged();
					}

				} catch (Exception e) {
					e.printStackTrace();
					Log.e(Config.TAG_ERROR,
							"Error. no se encuentra la lista de mensajes para el usuario: "
									+ newMsg.getFromIdUser());
				}

			}
		});
	}

	/**
	 * Añade el mensaje a la listView
	 * */
	synchronized public void appendContactConnected(final Contact contact) {

		try {

			if (ChatIntentService.getSession().getUser().getId()
					.equals(contact.getId())) {
				Log.e("ChatActivity",
						"appendContactConnected(): ERROR ! el contacto a agregar es uno mismo !!!!");
			}
			if (ChatIntentService.getSession().getUser().getId()
					.equals(contact.getId()) == false
					&& ChatIntentService.getListContacts().contains(contact) == false) {

				this.runOnUiThread(new Runnable() {

					@Override
					public void run() {
						ChatIntentService.getListContacts().add(contact);
						contactsListAdapter = new ContactsListAdapter(
								ChatActivity.this, ChatIntentService
										.getListContacts());
						listViewContactos.setAdapter(contactsListAdapter);
						contactsListAdapter.notifyDataSetChanged();

					}
				});

				// playBeep();
			} else {
				Log.i(Config.TAG_INFO,
						"from:  the contact " + contact.getName()
								+ " with id: " + contact.getId()
								+ " is already in list");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void notifyProfileUpdate(String srcName, String srcUserId,
			String message, String idImage, final Activity activity) {

		Log.i(Config.TAG_INFO, "notifying user change profile....");
		/**
		 * String srcName, String srcUserId, String message, String idImage
		 */
		final String updateMsg = utilsChat.getUpdateProfile(srcName, srcUserId,
				message, idImage);

		new Thread() {
			@Override
			public void run() {

				/**
				 * Al realizarse el login la aplicacion realiza una conexion al
				 * websocket del servidor pero puede tardar un rato en ralizarse
				 * la operacion
				 */
				boolean done = false;
				int i = 0;
				final int MaxIntents = 20;
				do {
					i++;
					try {
						Log.i(Config.TAG_INFO,
								"Trying to send notify the profile update at your friends connected on chat (intent "
										+ i
										+ ": - ChatIntentService.getClient() = "
										+ ChatIntentService.getClient());

						ChatIntentService.getClient().send(updateMsg);

						done = true;
						Thread.sleep(5000);
					} catch (Exception e) {
						e.printStackTrace();
					}
				} while (done == false && i < MaxIntents);
				if (done == true) {

					Log.i(Config.TAG_INFO,
							"The notification of user profile change  was sent");
				} else {
					Log.i(Config.TAG_INFO,
							"The notification of user profile change couldn't been sent");
				}
			}
		}.start();

	}

	/**
	 * menu de contactos del chat
	 * 
	 * @param menu
	 * @return
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.chat_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	/*
	 * opciones del menu chat
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.favoritePlaylist:
			// lanzamos la activity en la que se muestran las listas de
			// canciones favoritas
			Intent intent = new Intent(this, FavoriteFriendPlayList.class);
			this.startActivity(intent);
			return true;

		case R.id.openConcactsManager:
			Intent friendsListAct = new Intent(this, FriendsListActivity.class);
			startActivity(friendsListAct);
			return true;

		case R.id.openFriendRequestManager:
			Intent friendReqAct = new Intent(this,
					FriendshipRequestsActivity.class);
			startActivity(friendReqAct);
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Reproduce la notificacio por defecto del sistema
	 * */
	public void playBeep() {

		try {
			Uri notification = RingtoneManager
					.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			Ringtone ringToneMg = RingtoneManager.getRingtone(
					getApplicationContext(), notification);
			ringToneMg.play();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void onPause() {
		/**
		 * ------------------------------------------------------------
		 */
		isActive = false;

		Log.i(Config.TAG_INFO, "ChatActivity - onPause()....");
		super.onPause();
	}

	@Override
	protected void onStop() {
		try {
			// chatSocketListener.unregisterReceiver(this);

		} catch (Throwable th) {
			th.printStackTrace();
		}
		super.onStop();
	}

	public static ChatSocketListenerOnService getChatSocketListener() {
		return chatSocketListener;
	}

	public static void setChatSocketListener(
			ChatSocketListenerOnService chatSocketListener) {
		ChatActivity.chatSocketListener = chatSocketListener;
	}

	public static boolean isAutoClose() {
		return autoClose;
	}

	public static void setAutoClose(boolean autoClose) {
		ChatActivity.autoClose = autoClose;
	}

	public static boolean isNextAutoCloseValue() {
		return nextAutoCloseValue;
	}

	public static void setNextAutoCloseValue(boolean nextAutoCloseValue) {
		ChatActivity.nextAutoCloseValue = nextAutoCloseValue;
	}

	public static boolean isComesFromLogin() {
		return comesFromLogin;
	}

	/**
	 * Indica que se ha de iniciar el servicio de chat (se usa en el login para
	 * iniciar el servicio solo una vez)
	 * 
	 * @param comesFromLogin
	 */
	public static void setComesFromLogin(boolean comesFromLogin) {
		ChatActivity.comesFromLogin = comesFromLogin;
	}

	public static boolean isActive() {
		return isActive;
	}

	public static void setActive(boolean isActive) {
		ChatActivity.isActive = isActive;
	}

}

package org.escoladeltreball.activities;

import org.escoladeltreball.support.Config;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

public class UtilMessage implements Config {
	private static final int TOAST_INFO_COLOR, TOAST_ERROR_COLOR,
			TOAST_WARNING_COLOR;
	public static final String PARAM_DESCRIPTION = "description";
	private Context context;

	static {
		TOAST_INFO_COLOR = Style.TOAST_INFO_COLOR;
		TOAST_ERROR_COLOR = Style.TOAST_ERROR_COLOR;
		TOAST_WARNING_COLOR = Style.TOAST_WARNING_COLOR;
	}

	public UtilMessage() {
	}

	public UtilMessage(Context context) {
		this.context = context;
	}

	private static void showMessage(Context context, int Color, String message,
			int length) {
		
		Toast toast = Toast.makeText(context, message, length);
		View v = toast.getView();
		v.setBackgroundColor(Color);
		toast.setGravity(Gravity.CENTER | Gravity.TOP, 0, 20);
		toast.show();
	}

	public static void showInfoMessage(Context context, String message) {
		showMessage(context, TOAST_INFO_COLOR, message, Toast.LENGTH_SHORT);
	}

	public static void showErrorMessage(Context context, String message) {
		showMessage(context, TOAST_ERROR_COLOR, message, Toast.LENGTH_LONG);
	}

	public static void showWarningMessage(Context context, String message) {
		showMessage(context, TOAST_WARNING_COLOR, message, Toast.LENGTH_SHORT);
	}

	public static void showInfoMessageOnMainThread(final String message,
			final Activity activity) {
		activity.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				showMessage(activity.getBaseContext(), TOAST_INFO_COLOR,
						message, Toast.LENGTH_SHORT);

			}
		});
	}

	public static void showErrorMessageOnMainThread(final String message,
			final Activity activity) {
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				showMessage(activity.getBaseContext(), TOAST_ERROR_COLOR,
						message, Toast.LENGTH_LONG);
			}
		});
	}

	public static void showWarningMessageOnMainThread(final String message,
			final Activity activity) {
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				showMessage(activity.getBaseContext(), TOAST_WARNING_COLOR,
						message, Toast.LENGTH_SHORT);
			}
		});
	}

	public static ProgressDialog showProgressDialog(Context context,
			final String message) {
		ProgressDialog dialog = new ProgressDialog(context);
		dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		dialog.setMessage(message);
		dialog.setIndeterminate(true);
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();
		return dialog;
	}

	public static void dismisProgressDialog(ProgressDialog dialog) {
		if (dialog != null) {
			dialog.dismiss();
		}
	}
}
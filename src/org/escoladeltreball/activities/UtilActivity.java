package org.escoladeltreball.activities;

import org.escoladeltreball.chat.R;

import android.view.View;
import android.view.ViewGroup;

public class UtilActivity {

	public static void enableGui(View view, boolean enable) {
		view.setEnabled(enable);
		if (view instanceof ViewGroup) {
			ViewGroup viewGroup = (ViewGroup) view;
			for (int i = 0; i < viewGroup.getChildCount(); i++) {
				View child = viewGroup.getChildAt(i);
				enableGui(child, enable);
			}
		}
	}

	/**
	 * carga la imagen del avatar en funcion del identificador recurso drawable
	 * si el valor es 0 y la foto es visible a otros usuarios, mostramos un
	 * avatar distinto a si no lo es (editor perfil)
	 * 
	 * @param view
	 * @param idImg
	 * @param isDefaultPublic
	 *            true si el avatar es visible a otros usuarios
	 */
	public static void setProfileBackgroundFromIdImg(View view, Integer idImg,
			boolean isDefaultPublic) {

		if (idImg == null) {
			idImg = 0;
		}

		if (idImg == 1) {
			view.setBackgroundResource(R.drawable.av1);
		} else if (idImg == 2) {
			view.setBackgroundResource(R.drawable.av2);
		} else if (idImg == 3) {
			view.setBackgroundResource(R.drawable.av3);
		} else if (idImg == 4) {
			view.setBackgroundResource(R.drawable.av4);
		} else if (idImg == 5) {
			view.setBackgroundResource(R.drawable.av5);
		} else if (idImg == 6) {
			view.setBackgroundResource(R.drawable.av6);
		} else if (idImg == 7) {
			view.setBackgroundResource(R.drawable.av7);
		} else if (idImg == 8) {
			view.setBackgroundResource(R.drawable.av8);
		} else if (idImg == 9) {
			view.setBackgroundResource(R.drawable.av9);
		} else if (idImg == 10) {
			view.setBackgroundResource(R.drawable.av10);
		} else if (idImg == 11) {
			view.setBackgroundResource(R.drawable.av11);
		} else if (idImg == 12) {
			view.setBackgroundResource(R.drawable.av12);
		} else if (idImg == 13) {
			view.setBackgroundResource(R.drawable.av13);
		} else if (idImg == 14) {
			view.setBackgroundResource(R.drawable.av14);
		} else if (idImg == 15) {
			// view.setBackgroundResource(R.drawable.av15);
			// } else if (idImg == 16) {
			// view.setBackgroundResource(R.drawable.av16);
			// } else if (idImg == 17) {
			// view.setBackgroundResource(R.drawable.av17);
			// } else if (idImg == 18) {
			// view.setBackgroundResource(R.drawable.av18);
			// } else if (idImg == 19) {
			// view.setBackgroundResource(R.drawable.av19);
			// } else if (idImg == 20) {
			// view.setBackgroundResource(R.drawable.av20);
			// } else if (idImg == 21) {
			// view.setBackgroundResource(R.drawable.av21);
			// } else if (idImg == 22) {
			// view.setBackgroundResource(R.drawable.av22);
			// } else if (idImg == 23) {
			// view.setBackgroundResource(R.drawable.av23);
			// } else if (idImg == 24) {
			// view.setBackgroundResource(R.drawable.av24);
			// } else if (idImg == 25) {
			// view.setBackgroundResource(R.drawable.av25);
			// } else if (idImg == 26) {
			// view.setBackgroundResource(R.drawable.av26);
			// } else if (idImg == 27) {
			// view.setBackgroundResource(R.drawable.av27);
			// } else if (idImg == 28) {
			// view.setBackgroundResource(R.drawable.av28);
			// } else if (idImg == 29) {
			// view.setBackgroundResource(R.drawable.av29);
			// } else if (idImg == 30) {
			// view.setBackgroundResource(R.drawable.av30);
			// } else if (idImg == 31) {
			// view.setBackgroundResource(R.drawable.av31);
			// } else if (idImg == 32) {
			// view.setBackgroundResource(R.drawable.av32);
			// } else if (idImg == 33) {
			// view.setBackgroundResource(R.drawable.av33);
			// } else if (idImg == 34) {
			// view.setBackgroundResource(R.drawable.av34);
			// } else if (idImg == 35) {
			// view.setBackgroundResource(R.drawable.av35);
			// } else if (idImg == 36) {
			// view.setBackgroundResource(R.drawable.av36);
			// } else if (idImg == 37) {
			// view.setBackgroundResource(R.drawable.av37);
			// } else if (idImg == 38) {
			// view.setBackgroundResource(R.drawable.av38);
			// } else if (idImg == 39) {
			// view.setBackgroundResource(R.drawable.av39);
			// } else if (idImg == 40) {
			// view.setBackgroundResource(R.drawable.av40);
			// } else if (idImg == 41) {
			// view.setBackgroundResource(R.drawable.av41);
			// } else if (idImg == 42) {
			// view.setBackgroundResource(R.drawable.av42);
			// } else if (idImg == 43) {
			// view.setBackgroundResource(R.drawable.av43);
			// } else if (idImg == 44) {
			// view.setBackgroundResource(R.drawable.av44);
			// } else if (idImg == 45) {
			// view.setBackgroundResource(R.drawable.av45);
			// } else if (idImg == 46) {
			// view.setBackgroundResource(R.drawable.av46);
			// } else if (idImg == 47) {
			// view.setBackgroundResource(R.drawable.av47);
			// } else if (idImg == 48) {
			// view.setBackgroundResource(R.drawable.av48);
			// } else if (idImg == 49) {
			// view.setBackgroundResource(R.drawable.av49);
			// } else if (idImg == 50) {
			// view.setBackgroundResource(R.drawable.av50);
		} else {
			if (isDefaultPublic) {
				view.setBackgroundResource(R.drawable.av0_1);
			} else {
				view.setBackgroundResource(R.drawable.av0);
			}
		}
	}

	public static int getResourceIdFromIdAvatar(Integer idImg,
			boolean isDefaultPublic) {

		if (idImg == null) {
			idImg = 0;
		}

		if (idImg == 1) {
			return R.drawable.av1;
		} else if (idImg == 2) {
			return R.drawable.av2;
		} else if (idImg == 3) {
			return R.drawable.av3;
		} else if (idImg == 4) {
			return R.drawable.av4;
		} else if (idImg == 5) {
			return R.drawable.av5;
		} else if (idImg == 6) {
			return R.drawable.av6;
		} else if (idImg == 7) {
			return R.drawable.av7;
		} else if (idImg == 8) {
			return R.drawable.av8;
		} else if (idImg == 9) {
			return R.drawable.av9;
		} else if (idImg == 10) {
			return R.drawable.av10;
		} else if (idImg == 11) {
			return R.drawable.av11;
		} else if (idImg == 12) {
			return R.drawable.av12;
		} else if (idImg == 13) {
			return R.drawable.av13;
		} else if (idImg == 14) {
			return R.drawable.av14;
			// } else if (idImg == 15) {
			// return R.drawable.av15;
			// } else if (idImg == 16) {
			// return R.drawable.av16;
			// } else if (idImg == 17) {
			// return R.drawable.av17;
			// } else if (idImg == 18) {
			// return R.drawable.av18;
			// } else if (idImg == 19) {
			// return R.drawable.av19;
			// } else if (idImg == 20) {
			// return R.drawable.av20;
			// } else if (idImg == 21) {
			// return R.drawable.av21;
			// } else if (idImg == 22) {
			// return R.drawable.av22;
			// } else if (idImg == 23) {
			// return R.drawable.av23;
			// } else if (idImg == 24) {
			// return R.drawable.av24;
			// } else if (idImg == 25) {
			// return R.drawable.av25;
			// } else if (idImg == 26) {
			// return R.drawable.av26;
			// } else if (idImg == 27) {
			// return R.drawable.av27;
			// } else if (idImg == 28) {
			// return R.drawable.av28;
			// } else if (idImg == 29) {
			// return R.drawable.av29;
			// } else if (idImg == 30) {
			// return R.drawable.av30;
			// } else if (idImg == 31) {
			// return R.drawable.av31;
			// } else if (idImg == 32) {
			// return R.drawable.av32;
			// } else if (idImg == 33) {
			// return R.drawable.av33;
			// } else if (idImg == 34) {
			// return R.drawable.av34;
			// } else if (idImg == 35) {
			// return R.drawable.av35;
			// } else if (idImg == 36) {
			// return R.drawable.av36;
			// } else if (idImg == 37) {
			// return R.drawable.av37;
			// } else if (idImg == 38) {
			// return R.drawable.av38;
			// } else if (idImg == 39) {
			// return R.drawable.av39;
			// } else if (idImg == 40) {
			// return R.drawable.av40;
			// } else if (idImg == 41) {
			// return R.drawable.av41;
			// } else if (idImg == 42) {
			// return R.drawable.av42;
			// } else if (idImg == 43) {
			// return R.drawable.av43;
			// } else if (idImg == 44) {
			// return R.drawable.av44;
			// } else if (idImg == 45) {
			// return R.drawable.av45;
			// } else if (idImg == 46) {
			// return R.drawable.av46;
			// } else if (idImg == 47) {
			// return R.drawable.av47;
			// } else if (idImg == 48) {
			// return R.drawable.av48;
			// } else if (idImg == 49) {
			// return R.drawable.av49;
			// } else if (idImg == 50) {
			// return R.drawable.av50;
		} else {
			if (isDefaultPublic) {
				return R.drawable.av0_1;
			} else {
				return R.drawable.av0;
			}
		}
	}
}

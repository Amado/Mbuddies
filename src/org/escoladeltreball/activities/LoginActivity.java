package org.escoladeltreball.activities;

import java.util.List;

import org.escoladeltreball.chat.R;
import org.escoladeltreball.chat.service.ChatIntentService;
import org.escoladeltreball.db4o.Db4oDAOUser;
import org.escoladeltreball.db4o.Db4oHelper;
import org.escoladeltreball.db4o.Favorites;
import org.escoladeltreball.db4o.User;
import org.escoladeltreball.manager.Login;
import org.escoladeltreball.manager.RequestManager;
import org.escoladeltreball.manager.UpdateLocationListener;
import org.escoladeltreball.model.Session;
import org.escoladeltreball.model.json.StatusPackage;
import org.escoladeltreball.model.server.persistence.FavoritesServ;
import org.escoladeltreball.model.server.persistence.ModelConverter;
import org.escoladeltreball.model.server.persistence.UsersServ;
import org.escoladeltreball.musicPlayer.MusicPlayerActivity;
import org.escoladeltreball.support.Config;
import org.escoladeltreball.support.FindMyAddress;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.db4o.ObjectContainer;

public class LoginActivity extends Activity implements OnClickListener, Config,
		UpdateLocationListener {

	public static String userName = "";
	private static Session session;
	private Button btLogin, btSignUp;
	private EditText etName, etPassword;
	private String username, password, city, country;
	private String SHARED_PREF_MAIL = "mail",
			SHARED_PREF_USERNAME = "username";

	private static SharedPreferences sharedPref;
	private static boolean autoClose = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		if (autoClose == true) {
			autoClose = false;
			// this.finishAffinity();
			this.finish();
		}

		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_login);
		// this.utilMessage = new UtilMessage(this.getBaseContext());
		btLogin = (Button) findViewById(R.id.btLogin);
		btSignUp = (Button) findViewById(R.id.btSignUp);
		etName = (EditText) findViewById(R.id.etName);
		etPassword = (EditText) findViewById(R.id.etPassword);

		getActionBar().hide();
		btLogin.setOnClickListener(this);
		btSignUp.setOnClickListener(this);

		/**
		 * buscamos en las shared preferences los datos del login
		 */
		try {

			UsersServ user = Session.getSession(this).getUser();
			Log.i("info", "LOGIN: " + user);
			if (user != null) {
				Log.i(TAG_INFO, "retrieved user from previous session");
				this.username = user.getName();
				this.etName.setText(user.getName());
				this.password = user.getPassword();
				this.etPassword.setText(this.password);

				if (OBLIGATE_LOGIN == false) {
					// si el usuario ya se ha logeado previamente pasamos a la
					// siguiente actividad
					navigateToNextActivity(null);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.i(TAG_INFO,
					"Couldn't get the user from session on shared preferences");
		}

	}

	@Override
	protected void onResume() {
		Log.i("login", "disconneting from chat......");
		// Toast.makeText(this, "Disconecting from chat...", Toast.LENGTH_LONG)
		// .show();
		try {
			if (ChatIntentService.getClient() != null) {
				 ChatIntentService.getClient().disconnect();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onResume();
	}

	/**
	 * log pruebas
	 */
	public void LoginTest() {
		User emilio = null;
		try {
			Db4oHelper dbHelper = new Db4oHelper(this);
			ObjectContainer oc = dbHelper.oc;

			emilio = Db4oDAOUser.getUserOwnerFromDB(oc);

			if (emilio == null) {
				emilio = new User("emilio", "emilioxxx", "Italia", "Roma",
						"Soy guay", "Me gusta el hard rock", 0, true);

				/*
				 * from chat emilio = new User(LoginActivity.userName ,
				 * "emilioxxx", "Barcelona", "España", "Soy guay",
				 * "Me gusta el hard rock", 0, true);
				 */

				Db4oDAOUser.insertUserToDB(oc, emilio);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		Session.getSession(this).setUser(
				ModelConverter.convertToServerUser(emilio));
		LoginActivity.this.savePreferences();
		LoginActivity.this.navigateToNextActivity(null);

		findLocation();
	}

	public void findLocation() {
		Toast.makeText(getApplicationContext(), "obtaining location ...",
				Toast.LENGTH_SHORT).show();
		FindMyAddress fma = new FindMyAddress(this, this);
		fma.start();

	}

	/**
	 * pasa de la pantalla del login a la siguiente actividad
	 */
	public void navigateToNextActivity(Class<?> nexActivity) {

		Thread savePrefThread = new Thread() {
			@Override
			public void run() {
				savePreferences();
			}
		};

		final ProgressDialog dialog2 = UtilMessage.showProgressDialog(
				LoginActivity.this, "please wait ...");

		Thread commitFavsThread = new Thread() {
			public void run() {
				Log.i(TAG_INFO, "Actualizando los datos del servidor");

				try {
					final Long idUser = Session.getSession(LoginActivity.this)
							.getUser().getId();

					Log.i(TAG_INFO, "idUser = " + idUser);

					List<Favorites> favsLocal = RequestManager
							.loadPreferedLists(LoginActivity.this, idUser);

					List<FavoritesServ> favsServ = ModelConverter
							.convertToServerFavorites(favsLocal, true);

					if (favsLocal == null || favsLocal.isEmpty()) {
						RequestManager.commitFavorites(favsServ,
								String.valueOf(idUser));
					}

				} catch (Throwable e) {
					e.printStackTrace();
				} finally {
					UtilMessage.dismisProgressDialog(dialog2);
				}

				Log.i(TAG_INFO,
						"se han actualizado los favoritos en el servidor ......................");

			}
		};
		findLocation();
		savePrefThread.start();
		commitFavsThread.start();

		if (nexActivity == null) {
			nexActivity = ConfigActivity.ACTIVITY_POS_LOGIN;
		}

		MusicPlayerActivity.setComesFromLogin(true);

		Log.i(TAG_INFO,
				"Starting the chat activity to init the webSocket connection ......");
		Intent intentChat = new Intent(LoginActivity.this, ChatActivity.class);
		intentChat.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intentChat.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		ChatActivity.setComesFromLogin(true);
		ChatActivity.setAutoClose(true);
		ChatActivity.setNextAutoCloseValue(false);
		startActivity(intentChat);

		if (ConfigActivity.ACTIVITY_POS_LOGIN.equals(ChatActivity.class)) {
			ChatIntentService.setSession(session);
		}
		ChatIntentService.setSession(session);
	}

	public void savePreferences() {
		Session.saveSessionOnPreferences(this);
	}

	@Override
	public void onClick(View view) {
		Log.i(TAG_INFO, "LOGING_ACTIVITY onClick() ...");

		if (view == btLogin) {
			login();
			// LoginTest();
		} else {
			signUp();
		}

	}

	public void enableForm(boolean enable) {
		this.btLogin.setEnabled(enable);
		this.btSignUp.setEnabled(enable);
		this.etName.setEnabled(enable);
		this.etPassword.setEnabled(enable);
	}

	public void login() {

		Log.i(TAG_INFO, "loginDetach...");
		if (validateLogin() == false) {
			Log.i(TAG_INFO, "login() ... not valid");
			return;
		}

		Log.i(TAG_INFO, "login() ... begin");
		// TODO: Buscar ciudad y pais
		if (city == null) {
			city = "";
		}
		if (country == null) {
			country = "";
		}

		this.enableForm(false);

		final ProgressDialog dialog = UtilMessage.showProgressDialog(this,
				"Connecting with the server.... please wait");

		new Thread(new Runnable() {
			public void run() {
				try {
					UsersServ tmpUser = Login.performLogin(username, password,
							country, city, LoginActivity.this);

					LoginActivity.session = Session
							.getSession(LoginActivity.this);

					Session.setUser(tmpUser);

					Log.i(Config.TAG_INFO,
							"login() - user fetched from server = "
									+ Session.getUser());

					if (session.getUser() != null
							&& session.getUser().getName() != null) {
						Log.i(TAG_INFO, "LoginActivity - userConnected = "
								+ session.getUser());
						// si la operacion de signup en el servidor es correcta
						runOnUiThread(new Runnable() {
							public void run() {

								Log.i(TAG_INFO,
										"LoginActivity - userConnected = "
												+ Session.getSession()
														.getUser());
								Session.getSession(LoginActivity.this)
										.getUser().setPassword(password);

								Session.getSession(LoginActivity.this)
										.getUser().setName(username);
								LoginActivity.this.savePreferences();
								LoginActivity.this.navigateToNextActivity(null);
								// utilMessage.showInfoMessage("Welcome "
								// + LoginActivity.session.getUser());
							}
						});
					} else {
						runOnUiThread(new Runnable() {
							public void run() {
								UtilMessage.showErrorMessage(
										LoginActivity.this,
										"wrong username or password");
							}
						});
					}

				} catch (Throwable e) {
					Log.w(Config.TAG_ERROR, e.toString());
					runOnUiThread(new Runnable() {
						public void run() {
							UtilMessage.showErrorMessage(LoginActivity.this,
									"Couldn't Connect to Server\nTry later");
						}
					});
					e.printStackTrace();
				} finally {
					runOnUiThread(new Runnable() {
						public void run() {
							UtilMessage.dismisProgressDialog(dialog);
							LoginActivity.this.enableForm(true);
						}
					});
				}
			}
		}).start();

	}

	public void signUp() {
		Log.i(TAG_INFO, "signUp...");
		if (validateSignUp() == false) {
			Log.i(TAG_INFO, "signUp() ... not valid");
			return;
		}

		Log.i(TAG_INFO, "signUp() ... begin");

		// TODO: Buscar ciudad y pais
		city = "Barcelona";
		country = "España";

		final ProgressDialog dialog = UtilMessage.showProgressDialog(this,
				"Connecting with the server.... please wait");

		this.enableForm(false);

		new Thread(new Runnable() {
			public void run() {
				try {
					UsersServ tmpUser = null;
					StatusPackage statusPackage = null;
					final Object returnObject = Login.performSignUp(username,
							password, country, city);

					if (returnObject instanceof UsersServ) {
						tmpUser = (UsersServ) returnObject;

						session = Session.getSession(LoginActivity.this);
						session.setUser(tmpUser);

						Log.i(Config.TAG_INFO, "tmpUser =  " + tmpUser);

						if (session.getUser() != null
								&& session.getUser().getName() != null) {
							runOnUiThread(new Runnable() {
								public void run() {
									LoginActivity.session.getUser()
											.setPassword(password);
									LoginActivity.this
											.navigateToNextActivity(null);
								}
							});
						}

					} else {
						statusPackage = (StatusPackage) returnObject;
						final String errorMsg = statusPackage.getMessage();
						runOnUiThread(new Runnable() {
							public void run() {
								UtilMessage.showErrorMessage(
										LoginActivity.this, errorMsg);
							}
						});

					}

				} catch (Exception e) {
					e.printStackTrace();
					Log.w(Config.TAG_ERROR, e.toString());
					runOnUiThread(new Runnable() {
						public void run() {
							UtilMessage.showErrorMessage(LoginActivity.this,
									"Couldn't Connect to Server\nTry later");

						}
					});
					e.printStackTrace();
				} finally {
					runOnUiThread(new Runnable() {
						public void run() {
							UtilMessage.dismisProgressDialog(dialog);
							LoginActivity.this.enableForm(true);
						}
					});
				}
			}
		}).start();

	}

	/**
	 * valida el formulario al realizar el registro
	 * 
	 * @return
	 */
	public boolean validateSignUp() {
		this.getFields();
		boolean isValid = true;
		StringBuilder errorMsg = new StringBuilder();
		if (this.username == null || this.username.isEmpty()) {
			errorMsg.append("The name is empty\n");
		} else if (this.username.length() < Validation.USER_NAME_MIN_LENGTH
				|| this.username.length() > Validation.USER_NAME_MAX_LENGTH) {
			errorMsg.append(String.format(
					"The name has to be beetwen %d and %d%n" + " characters",
					Validation.USER_NAME_MIN_LENGTH,
					Validation.USER_NAME_MAX_LENGTH));
		}

		if (this.password == null || this.password.isEmpty()) {
			errorMsg.append("The password is empty\n");
		} else if (this.password.length() < Validation.PASS_MIN_LENGTH
				|| this.password.length() > Validation.PASS_MAX_LENGTH) {
			errorMsg.append(String.format(
					"The password has to be beetwen %d and %d%n"
							+ " characters", Validation.PASS_MIN_LENGTH,
					Validation.PASS_MAX_LENGTH));
		}

		isValid = (errorMsg.toString().isEmpty() == true);
		Log.i("info", "validate signUp -> " + isValid);
		if (isValid == false) {
			UtilMessage.showWarningMessage(LoginActivity.this,
					errorMsg.toString());
		}
		return isValid;
	}

	/**
	 * valida el formulario al realizar el login (se puede logear con el email o
	 * el nombre de usuario) va
	 * 
	 * @return
	 */
	public boolean validateLogin() {
		this.getFields();
		boolean isValid = false;

		StringBuilder errorMsg = new StringBuilder();
		if (this.username == null || this.username.isEmpty()) {
			errorMsg.append("The name is empty\n");
		} else if (this.username.length() < Validation.USER_NAME_MIN_LENGTH
				|| this.username.length() > Validation.USER_NAME_MAX_LENGTH) {
			errorMsg.append(String.format(
					"The name has to be beetwen %d and %d%n" + " characters",
					Validation.USER_NAME_MIN_LENGTH,
					Validation.USER_NAME_MAX_LENGTH));
		}

		if (this.password == null || this.password.isEmpty()) {
			errorMsg.append("The password is empty\n");
		} else if (this.password.length() < Validation.PASS_MIN_LENGTH
				|| this.password.length() > Validation.PASS_MAX_LENGTH) {
			errorMsg.append(String.format(
					"The password has to be beetwen %d and %d%n"
							+ " characters", Validation.PASS_MIN_LENGTH,
					Validation.PASS_MAX_LENGTH));
		}
		isValid = (errorMsg.toString().isEmpty() == true);
		Log.i("info", "validate login -> " + isValid);
		if (isValid == false) {
			UtilMessage.showWarningMessage(LoginActivity.this,
					errorMsg.toString());
		}
		return isValid;
	}

	public void getFields() {
		this.username = this.etName.getText().toString().trim();
		this.password = this.etPassword.getText().toString().trim();

	}

	@Override
	public void updateLocalization(String city, String country) {
		Log.i(Config.TAG_INFO,
				"LoginActivity - updateLocalization() ............. ");
		this.country = country;
		this.city = city;
		Log.i(Config.TAG_INFO, "updateLocalization()  country = " + country
				+ ", city =  " + city);
		displayUpdateLocalizationConfirmationDialog();
	}

	public void displayUpdateLocalizationConfirmationDialog() {

		this.runOnUiThread(new Runnable() {
			public void run() {

				final UsersServ user = Session.getUser(LoginActivity.this);

				// user.setCity(city);
				// user.setCountry(country);

				if (user.getCity().equals(city) == false
						|| user.getCountry().equals(country) == false) {

					final AlertDialog.Builder alert = new AlertDialog.Builder(
							LoginActivity.this);

					alert.setTitle("A new Localization was found");
					alert.setMessage("Do you wish to update your localization\n"
							+ "Country: " + country + "\nCity: " + city);

					alert.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int whichButton) {
									user.setCity(city);
									user.setCountry(country);
									Session.saveSessionOnPreferences(LoginActivity.this);
								}
							});
					alert.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int whichButton) {
									dialog.cancel();
								}
							});
					alert.show();
				}
			}
		});
	}

	@Override
	protected void onDestroy() {

		try {
			if (ChatIntentService.getClient() != null) {
				ChatIntentService.getClient().disconnect();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		ActionActivity.updateServerCurrentPlaySong(null, null, this);
		Log.i(Config.TAG_INFO, "THE ACTIVITY LOGIN ---- CALLED ON DESTROY !!!");
		Toast.makeText(this, "THE ACTIVITY LOGIN ---- CALLED ON DESTROY !!!",
				Toast.LENGTH_LONG);
		super.onDestroy();
	}

	public static boolean isAutoClose() {
		return autoClose;
	}

	public static void setAutoClose(boolean autoClose) {
		LoginActivity.autoClose = autoClose;
	}

}

package org.escoladeltreball.activities.adpaters;

import java.util.List;

import org.escoladeltreball.chat.R;
import org.escoladeltreball.model.Message;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MessagesListAdapter extends BaseAdapter {

	@SuppressWarnings("unused")
	private Context context;
	private List<Message> messagesItems;
	private LayoutInflater layoutInflater;

	//
	public MessagesListAdapter(Context context, List<Message> navDrawerItems) {
		this.context = context;
		this.messagesItems = navDrawerItems;
		this.layoutInflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return messagesItems != null ? messagesItems.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		return messagesItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressWarnings("unused")
	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Message message = messagesItems.get(position);

		ViewHolder viewHolder = null;
		boolean recycleCondition = convertView != null
				&& ((ViewHolder) convertView.getTag()).self == message.isSelf();

		// if (recycleCondition == false) {
		viewHolder = new ViewHolder();
		// identifica el mensaje como propio
		if (message.isSelf()) {
			// si los mensajes son propios se alienan a la derecha
			convertView = this.layoutInflater.inflate(
					R.layout.list_item_chat_message_right, null);
		} else {
			// si el mensaje no es propio se alinea a la derecha
			convertView = this.layoutInflater.inflate(
					R.layout.list_item_chat_message_left, null);
		}

		viewHolder.msg = (TextView) convertView.findViewById(R.id.tvMsg);
		viewHolder.from = (TextView) convertView.findViewById(R.id.tvFrom);
		viewHolder.self = message.isSelf();

		convertView.setTag(viewHolder);
		// } else {
		// viewHolder = (ViewHolder) convertView.getTag();
		// }

		viewHolder.msg.setText(message.getMessage());
		viewHolder.from.setText(message.getFromUserName());

		return convertView;
	}

	class ViewHolder {
		TextView msg, from;
		boolean self;
	}

}

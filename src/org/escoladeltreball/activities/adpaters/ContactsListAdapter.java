package org.escoladeltreball.activities.adpaters;

import java.util.List;

import org.escoladeltreball.activities.ChatActivity;
import org.escoladeltreball.activities.UtilActivity;
import org.escoladeltreball.chat.R;
import org.escoladeltreball.chat.service.ChatIntentService;
import org.escoladeltreball.model.Contact;
import org.escoladeltreball.support.Config;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ContactsListAdapter extends BaseAdapter {

	@SuppressWarnings("unused")
	private List<Contact> messagesItems;
	private LayoutInflater layoutInflater;
	private ChatActivity chatActivity;
	public static final String SEL_CONTACT_COLOR = "#B7854F",
			CONTACT_NEW_MSGS = "#A2B75C", CONTACT_COLOR = "#DEDFB6";
	private static int selectedPos;

	public ContactsListAdapter(ChatActivity chatActivity,
			List<Contact> contactos) {
		this.chatActivity = chatActivity;
		// this.context = chatActivity.getApplicationContext();
		this.messagesItems = contactos;
		this.layoutInflater = LayoutInflater.from(chatActivity
				.getApplicationContext());
	}

	@Override
	public int getCount() {
		return messagesItems.size();
	}

	@Override
	public Object getItem(int position) {
		return messagesItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressWarnings("unused")
	@SuppressLint("InflateParams")
	@Override
	public View getView(final int position, View convertView,
			final ViewGroup parent) {
		final Contact contact = messagesItems.get(position);

		Log.i(Config.TAG_INFO, "get View " + position);

		ViewHolder viewHolder = null;

		convertView = this.layoutInflater.inflate(
				R.layout.list_item_contactos_chat, null);

		viewHolder = new ViewHolder();
		viewHolder.name = (TextView) convertView
				.findViewById(R.id.tvContactName);
		viewHolder.imgButton = (ImageButton) convertView
				.findViewById(R.id.ibContact);
		viewHolder.border = (RelativeLayout) convertView
				.findViewById(R.id.ibContactBorder);

		viewHolder.numNewMsgs = (TextView) convertView
				.findViewById(R.id.tvNumNewMsg);

		convertView.setTag(contact.getId());

		viewHolder.name.setText(contact.getName());

		Log.i(Config.TAG_INFO, "contact.getName =  " + contact.getName());
		Log.i(Config.TAG_INFO, "contact.getPhotoId =  " + contact.getPhotoId());
		Log.i(Config.TAG_INFO, "contact.getId =  " + contact.getId());

		Log.i(Config.TAG_INFO, "ContactPhoto = " + contact.getPhotoId());
		UtilActivity.setProfileBackgroundFromIdImg(viewHolder.imgButton,
				contact.getPhotoId(), true);

		if (contact.getNumNewMessages() > 0) {
			viewHolder.numNewMsgs.setText("(" + contact.getNumNewMessages()
					+ ")");
		} else {
			viewHolder.numNewMsgs.setText("");
		}

		// al hacer click sobre la imagen del contacto cambiamos la lista de
		// mensajes
		final ImageButton imgButton = viewHolder.imgButton;
		final RelativeLayout imgButtonBorder = viewHolder.border;

		try {

			if (ChatIntentService.getListContacts().get(position).isSelected()) {
				viewHolder.border.setBackgroundColor(Color
						.parseColor(SEL_CONTACT_COLOR));
			} else {

				boolean noHasMsgs = viewHolder.numNewMsgs.getText().toString()
						.isEmpty();
				if (noHasMsgs) {
					viewHolder.border.setBackgroundColor(Color
							.parseColor(CONTACT_COLOR));
				} else {
					viewHolder.border.setBackgroundColor(Color
							.parseColor(CONTACT_NEW_MSGS));
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		final ImageButton imgBImageButton = viewHolder.imgButton;

		try {

			for (int i = 0; i < parent.getChildCount(); i++) {
				try {

					Contact contactI = ChatIntentService.getListContacts().get(
							i);
					View v = parent.getChildAt(i);
					ViewGroup vg = (ViewGroup) v
							.findViewById(R.id.ibContactBorder);
					boolean noHasMsgs = ((TextView) v
							.findViewById(R.id.tvNumNewMsg)).getText()
							.toString().isEmpty();

					if (i != selectedPos) {
						vg.setBackgroundColor(Color
								.parseColor(SEL_CONTACT_COLOR));
					} else {
						if (noHasMsgs) {
							vg.setBackgroundColor(Color
									.parseColor(CONTACT_COLOR));
						} else {
							vg.setBackgroundColor(Color
									.parseColor(CONTACT_NEW_MSGS));

						}

					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		viewHolder.imgButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				imgButtonBorder.setBackgroundColor(Color
						.parseColor(SEL_CONTACT_COLOR));
				chatActivity.selectMessageList(contact.getId(), position);
				ChatIntentService.setSelectedContactPos(position);
				contact.setSelected(true);
				selectedPos = position;
			}
		});

		return convertView;
	}

	class ViewHolder {
		TextView name;
		RelativeLayout border;
		TextView numNewMsgs;
		ImageButton imgButton;
	}

	public static int getSelectedPos() {
		return selectedPos;
	}

	public static void setSelectedPos(int selectedPos) {
		ContactsListAdapter.selectedPos = selectedPos;
	}

}

package org.escoladeltreball.activities.adpaters;

import java.util.List;

import org.escoladeltreball.activities.ActionActivity;
import org.escoladeltreball.activities.UtilActivity;
import org.escoladeltreball.chat.R;
import org.escoladeltreball.model.UserListViewModel;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

public class UsersListAdapter extends BaseAdapter {

	@SuppressWarnings("unused")
	// private Context context;
	private List<UserListViewModel> items;
	private LayoutInflater layoutInflater;
	private Activity activity;
	private final Drawable anonymousUser;

	public UsersListAdapter(Activity activity, List<UserListViewModel> contactos) {
		this.activity = activity;
		// this.context = activity.getBaseContext();
		this.items = contactos;
		this.layoutInflater = LayoutInflater.from(activity.getBaseContext());
		anonymousUser = activity.getResources().getDrawable(R.drawable.av0_1);
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Object getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	@SuppressLint({ "InflateParams", "NewApi" })
	public View getView(int position, View convertView, ViewGroup parent) {
		final UserListViewModel user = items.get(position);

		ViewHolder viewHolder = null;

		if (convertView == null) {

			convertView = this.layoutInflater.inflate(
					R.layout.list_item_search_user, null);

			viewHolder = new ViewHolder();
			viewHolder.name = (TextView) convertView
					.findViewById(R.id.tvliSearchUserName);

			viewHolder.city = (TextView) convertView
					.findViewById(R.id.tvliSearchCity);

			viewHolder.country = (TextView) convertView
					.findViewById(R.id.tvliSearchCountry);

			viewHolder.imgUser = (ImageButton) convertView
					.findViewById(R.id.ibliSearchImgUser);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		viewHolder.name.setText(user.getName());
		viewHolder.city.setText(user.getCity());
		viewHolder.country.setText(user.getCountry());

		UtilActivity.setProfileBackgroundFromIdImg(viewHolder.imgUser,
				user.getImg(), true);

		viewHolder.imgUser.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// ActionActivity.viewProfile(user.getId(), activity,
				// SearchUsersActivity.class);
				ActionActivity.viewProfile(user.getId(), activity, null);
			}
		});

		if (user.getImg() == 0 || user.getImg() == null) {
			viewHolder.imgUser.setBackground(anonymousUser);
		} else {
			UtilActivity.setProfileBackgroundFromIdImg(viewHolder.imgUser,
					user.getImg(), true);
		}
		((ViewGroup) viewHolder.name.getParent())
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// ActionActivity.displayUsersOptionsDialog(
						// user.getName(), user.getId(), activity,
						// SearchUsersActivity.class);

						ActionActivity.displayUsersOptionsDialog(
								user.getName(), user.getId(), user.getImg(),
								activity, null);
					}
				});

		return convertView;
	}

	/**
	 * send using websocket connection
	 * 
	 * @param username
	 */
	public void sendFriendRequest(String username) {

	}

	class ViewHolder {
		TextView name, city, country;
		// ImageButton sendFriendRequest, viewProfile,
		ImageButton imgUser;
	}

}

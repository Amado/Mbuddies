package org.escoladeltreball.activities;

import android.app.Activity;
import android.widget.Toast;

public class GuiMessages {
	Activity activity;

	public GuiMessages(Activity activity) {
		this.activity = activity;
	}

	public void showToast(final String message) {

		activity.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				Toast.makeText(activity.getApplicationContext(), message,
						Toast.LENGTH_LONG).show();
			}
		});

	}
}

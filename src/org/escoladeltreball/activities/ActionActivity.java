package org.escoladeltreball.activities;

import java.util.ArrayList;

import org.escoladeltreball.chat.R;
import org.escoladeltreball.chat.service.ChatIntentService;
import org.escoladeltreball.db4o.Song;
import org.escoladeltreball.db4o.User;
import org.escoladeltreball.exception.HttpRequestException;
import org.escoladeltreball.manager.RequestManager;
import org.escoladeltreball.model.Session;
import org.escoladeltreball.model.json.ListFriendRequest.RequestModel;
import org.escoladeltreball.model.server.persistence.FavoritesServ;
import org.escoladeltreball.model.server.persistence.ModelConverter;
import org.escoladeltreball.model.server.persistence.UsersServ;
import org.escoladeltreball.preferedList.friends.FavoriteFriendPlayList;
import org.escoladeltreball.profileClasses.PerfilFriendActivity;
import org.escoladeltreball.support.Config;
import org.escoladeltreball.support.ImageConverter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.Gravity;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ActionActivity {
	public static void viewProfile(final Long idUser, final Activity activity,
			final Class<?> comesFrom) {

		new Thread(new Runnable() {
			public void run() {
				Log.i("INFO", " viewProfile--" + idUser);
				UsersServ servUser = RequestManager.fechUserById(
						String.valueOf(idUser), false);
				Log.i(Config.TAG_INFO, "viewProfile - user: " + servUser);

				// userForPerfilFriendActivity = friend1; // perfil que queremos
				// visualizar
				PerfilFriendActivity.setProfileTitle(String.format(
						Config.ConfigActivity.PROFILE_TILE, servUser.getName()));
				Intent perfilFriendActivityIntent = new Intent(activity,
						PerfilFriendActivity.class);
				User locUser = ModelConverter.convertToLocalUser(servUser);
				PerfilFriendActivity.setUser(locUser);
				PerfilFriendActivity.setComesFrom(comesFrom);
				activity.startActivity(perfilFriendActivityIntent);

			}
		}).start();

	}

	public static void showNowListening(final Long idUser,
			final String userName, final Activity activity) {

		new Thread() {
			@Override
			public void run() {
				try {

					Log.i("INFO", " viewProfile--" + idUser);
					final UsersServ servUser = RequestManager.fechUserById(
							String.valueOf(idUser), true);
					// Log.i(Config.TAG_INFO,
					// "viewProfile - user: " + servUser);

					// vemos la canción que está escuchando

					// si hubiese algún error con el server
					if (servUser == null || servUser.getId() == null) {
						throw new HttpRequestException(
								"servUser - Error al conectar con el servidor");
					}

					activity.runOnUiThread(new Runnable() {
						@SuppressLint("NewApi")
						@SuppressWarnings("deprecation")
						public void run() {

							final AlertDialog.Builder alert = new AlertDialog.Builder(
									activity);

							alert.setTitle(userName + " now is listening");
							LinearLayout layout = new LinearLayout(activity);
							layout.setOrientation(LinearLayout.VERTICAL);
							String songName = null;

							LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
									LinearLayout.LayoutParams.WRAP_CONTENT,
									LinearLayout.LayoutParams.WRAP_CONTENT);
							layoutParams.weight = 1.0f;
							layoutParams.gravity = Gravity.CENTER_HORIZONTAL;
							layoutParams.bottomMargin = 22;
							layoutParams.topMargin = 60;

							Button bt = new Button(activity);

							if (servUser.getCanciones() != null) {
								songName = servUser.getCanciones()
										.getSongName();

								final String photoEncoded = servUser
										.getCanciones().getPhotoEncoded();
								if (photoEncoded != null
										&& photoEncoded.isEmpty() == false) {
									Bitmap bitmap = Song
											.getBitmapFromByteArray(ImageConverter
													.decodeImage(photoEncoded));

									bitmap = Song.scaleImage(bitmap, 300);

									bt.setBackgroundDrawable(new BitmapDrawable(
											activity.getResources(), bitmap));
								} else {
									bt.setBackground(activity
											.getResources()
											.getDrawable(
													R.drawable.playerbackgroundicon));
								}

								bt.setLayoutParams(layoutParams);
								layout.addView(bt);

							}
							if (songName == null) {
								songName = userName
										+ " isn't listening any song at this moment";
							}

							TextView tv = new TextView(activity);
							tv.setText(songName);

							layoutParams.topMargin = 0;
							layoutParams.bottomMargin = 7;
							tv.setLayoutParams(layoutParams);

							layout.addView(tv);

							alert.setView(layout);
							alert.setPositiveButton("Close",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog,
												int whichButton) {
											dialog.cancel();
										}
									});

							alert.show();

						}
					});

				} catch (Exception e) {
					UtilMessage.showErrorMessage(activity,
							"Error al conectar con el servidor");
				}
			}
		}.start();

	}

	/**
	 * Despliega en pantalla un AlertDialog que seleccionar enrtre las opciones
	 * de ver perfil o ver listas de favoritos, enviar al usuario una petición
	 * de amistad, o ver qué está escuchando en este momento.
	 * 
	 * @param userName
	 *            el nombre del usuario
	 */
	public static void displayUsersOptionsDialog(final String userName,
			final Long idUser, final Integer userIdPhoto,
			final Activity activity, final Class<?> comesFrom) {
		activity.runOnUiThread(new Runnable() {
			public void run() {
				String[] options = { "User profile", "User favorites list",
						"Send friendship request", "Now listening" };

				final AlertDialog.Builder optionsDialog = new AlertDialog.Builder(
						activity);
				optionsDialog.setTitle(userName);
				optionsDialog.setItems(options,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								if (which == 0) {
									// desplegamos el perfil del usuario
									ActionActivity.viewProfile(idUser,
											activity, comesFrom);

									if (comesFrom != null) {
										activity.finish();
									}

								} else if (which == 1) {

									FavoriteFriendPlayList
											.setFriendName(userName);
									FavoriteFriendPlayList.setIdFriend(idUser);
									FavoriteFriendPlayList
											.setUserIdPhoto(userIdPhoto);
									Intent favFriendPlayList = new Intent(
											activity,
											FavoriteFriendPlayList.class);
									FavoriteFriendPlayList
											.setComesFrom(comesFrom);
									activity.startActivity(favFriendPlayList);

									if (comesFrom != null) {
										activity.finish();
									}
								} else if (which == 2) {

									// enviamos una petición de amistad al
									// usuario seleccionado
									sendFriendRequest(idUser, userName,
											activity);

								} else if (which == 3) {
									ActionActivity.showNowListening(idUser,
											userName, activity);

								}
							}
						});
				optionsDialog.show();
			}
		});
	}

	public static void sendFriendRequest(final Long idTargetUser,
			final String nameTargetUser, final Activity activity) {

		UsersServ srcUser = Session.getSession(activity).getUser();
		Long idSrcUser = srcUser.getId();
		String nameSrcUser = srcUser.getName();

		if (idTargetUser.equals(idSrcUser)) {
			UtilMessage.showWarningMessage(activity,
					"You couldn't send a request to yourself");
			return;
		}

		RequestModel friendRequest = new RequestModel(idSrcUser, nameSrcUser,
				idTargetUser, nameTargetUser, RequestModel.REQUEST);
		final String requestJsonMsg = friendRequest.serializeToJson();

		Log.i(Config.TAG_INFO, "requestJsonMsg = " + requestJsonMsg);
		Log.i(Config.TAG_INFO, "ChatIntentService.getClient() = "
				+ ChatIntentService.getClient());

		final ProgressDialog dialog = UtilMessage.showProgressDialog(activity,
				"Please wait until we send the request to " + nameTargetUser
						+ " ....");
		new Thread() {
			@Override
			public void run() {

				try {

					/**
					 * Al realizarse el login la aplicacion realiza una conexion
					 * al websocket del servidor pero puede tardar un rato en
					 * ralizarse la operacion
					 */
					boolean done = false;
					int i = 0;
					final int MaxIntents = 20;
					do {
						i++;
						try {
							Log.i(Config.TAG_INFO,
									"Trying to send friends request by sockets (intent "
											+ i
											+ ": - ChatIntentService.getClient() = "
											+ ChatIntentService.getClient());
							ChatIntentService.getClient().send(requestJsonMsg);
							done = true;
							Thread.sleep(5000);
						} catch (Exception e) {
							e.printStackTrace();
						}
					} while (done == false && i < MaxIntents);
					if (done == true) {

						Log.i(Config.TAG_INFO, "The friend request was sent");
					} else {
						Log.i(Config.TAG_INFO, "The friend request wasn't sent");
					}
					/**
					 * En caso de que no se haya podido enviar la peticion (por
					 * ejemplo si se ha desconectado), notificamos al usuario
					 */

					if (done == false) {
						throw new Exception(
								"Error sending the request to the server");
					}

					activity.runOnUiThread(new Runnable() {

						@Override
						public void run() {
							UtilMessage.showInfoMessage(activity,
									"The request to " + nameTargetUser
											+ " has been sent");
						}
					});

				} catch (Exception e) {
					activity.runOnUiThread(new Runnable() {

						@Override
						public void run() {
							UtilMessage
									.showErrorMessage(
											activity,
											"The friend request couldn't been sent at this moment"
													+ "\nCheck your internet connection and try later");
						}
					});
				} finally {

					UtilMessage.dismisProgressDialog(dialog);
				}

			}
		}.start();
	}

	public static void updateServerCurrentPlaySong(final String srcPath,
			final String songName, final Activity activity) {
		new Thread() {
			@Override
			public void run() {

				try {
					// Session.getSession(MusicPlayerActivity.this);
					final Long idUser = Session.getUser(activity).getId();
					// Log.i(Config.TAG_INFO,
					// "addFavoritesToServer() ... locFavorites = "
					// + locFavorites);
					boolean processOk = false;

					if (srcPath != null && idUser != null) {
						processOk = RequestManager
								.updateCurrentPlaySong(srcPath, songName, null,
										String.valueOf(idUser));
					}

					if (processOk == false) {
						throw new HttpRequestException(
								"the current play song ins null");
					}

				} catch (Exception e) {
					e.printStackTrace();

				}
			}
		}.start();

	}

	public static void notifyUpdateProfileOnChat(String srcName,
			String srcUserId, String message, String idImage,
			final Activity activity) {
		ChatActivity.notifyProfileUpdate(srcName, srcUserId, message, idImage,
				activity);
	}

	/**
	 * 
	 * @param idFriend
	 * @param userName
	 * @param activity
	 * @param comesFrom
	 *            la activity desde la que se llama al metodo y donde ha de
	 *            volver al retroceder
	 */
	public static void diplayFriendOptionsDialog(final Long idFriend,
			final String userName, final Integer userIdPhoto,
			final Activity activity, final Class<?> comesFrom) {
		Log.i(Config.TAG_INFO, "diplayFriendOptionsDialog() - idFriend  = "
				+ idFriend);
		String[] opciones = { "Friend Profile", "Friend favorites list",
				"Now Listening" };
		AlertDialog.Builder optionsDialog = new AlertDialog.Builder(activity);
		optionsDialog.setTitle(userName).setItems(opciones,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {

						if (which == 0) {
							viewProfile(idFriend, activity, comesFrom);

							if (comesFrom != null) {
								activity.finish();
							}
						} else if (which == 1) {
							findFavoritesFromUser(idFriend, userName,
									userIdPhoto, activity, comesFrom);

						} else if (which == 2) {
							ActionActivity.showNowListening(idFriend, userName,
									activity);
						}
					}
				});
		optionsDialog.show();
	}

	/**
	 * recupera los favoritos del usuario en el servidor y los muestra en la
	 * lista
	 * 
	 * @param idFriend
	 * @param userName
	 * @param activity
	 * @param comesFrom
	 */
	public static void findFavoritesFromUser(final Long idFriend,
			final String userName, final Integer userIdImg,
			final Activity activity, final Class<?> comesFrom) {
		final ProgressDialog dialog = UtilMessage.showProgressDialog(activity,
				"Connecting with the server.... please wait");

		new Thread(new Runnable() {
			public void run() {
				try {
					// Log.i(Config.TAG_INFO,
					// "findFavoritesFromUser()... idUser = " + idFriend);

					ArrayList<FavoritesServ> responseSerFavs = (ArrayList<FavoritesServ>) RequestManager
							.fetchFavoritesFromUser(String.valueOf(idFriend));

					// Log.i(Config.TAG_INFO, "responseSerFavs =   "
					// + responseSerFavs);

					if (responseSerFavs != null
							&& responseSerFavs.isEmpty() == false) {
						Log.i(Config.TAG_INFO, "favorites =  "
								+ responseSerFavs);
						//
						// FavoriteFriendPlayList
						// .setFavoritesList(responseSerFavs);

						FavoriteFriendPlayList.setIdFriend(idFriend);
						FavoriteFriendPlayList.setFavoriteListTitle(userName
								+ " - Favorites List ");
						FavoriteFriendPlayList.setComesFrom(comesFrom);
						FavoriteFriendPlayList.setUserIdPhoto(userIdImg);
						Intent favFriendPlayList = new Intent(activity,
								FavoriteFriendPlayList.class);
						activity.startActivity(favFriendPlayList);

						if (comesFrom != null) {
							activity.finish();
						}

					} else {
						activity.runOnUiThread(new Runnable() {

							@Override
							public void run() {
								final String msg = "the user " + userName
										+ " doesn't have any favorite list yet"
										+ " from Server. Try Later";
								UtilMessage.showWarningMessage(activity, msg);
							}
						});

					}

				} catch (Exception e) {
					e.printStackTrace();
					activity.runOnUiThread(new Runnable() {

						@Override
						public void run() {
							final String erroMsg = "Error retrieving the favorites list "
									+ " from Server. Try Later";
							UtilMessage.showErrorMessage(activity, erroMsg);

						}
					});
				} finally {
					activity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							UtilMessage.dismisProgressDialog(dialog);
						}
					});
				}
			}

		}).start();
	}

}

package org.escoladeltreball.activities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.escoladeltreball.activities.adpaters.UsersListAdapter;
import org.escoladeltreball.chat.R;
import org.escoladeltreball.manager.UtilsHttp;
import org.escoladeltreball.model.UserListViewModel;
import org.escoladeltreball.model.json.ListCountriesCities;
import org.escoladeltreball.model.json.ListUsers;
import org.escoladeltreball.model.json.StatusPackage;
import org.escoladeltreball.model.server.persistence.UsersServ;
import org.escoladeltreball.support.Config;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

public class SearchUsersActivity extends Activity implements Config,
		OnItemSelectedListener, OnClickListener {

	String country, city;

	List<UserListViewModel> userList = new ArrayList<UserListViewModel>();
	UsersListAdapter adapter;
	ListView lvUsers;
	Button btSearchUsers;
	Spinner spinnerCountries, spinnerCities;
	UsersListAdapter userListAdpater;
	private TextView tvEmpty;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_users);

		lvUsers = (ListView) SearchUsersActivity.this
				.findViewById(R.id.listViewUsersSearch);

		spinnerCountries = new Spinner(SearchUsersActivity.this);
		spinnerCities = new Spinner(SearchUsersActivity.this);

		spinnerCities = (Spinner) findViewById(R.id.spSearchCity);
		spinnerCountries = (Spinner) findViewById(R.id.spSearchCountry);

		btSearchUsers = (Button) findViewById(R.id.btSearchUsers);
		findCountries();

		this.spinnerCities.setOnItemSelectedListener(this);
		this.spinnerCountries.setOnItemSelectedListener(this);
		btSearchUsers.setOnClickListener(this);

		tvEmpty = (TextView) this.findViewById(R.id.emptySearchUsers);
		lvUsers.setEmptyView(tvEmpty);

	}

	@Override
	protected void onResume() {
		super.onResume();
		// findCountries();
	}

	public void findUsers() {
		this.userList.clear();
		Log.i(TAG_INFO, "Find Users....");
		final String city = spinnerCities.getSelectedItem().toString();
		final String country = spinnerCountries.getSelectedItem().toString();

		final ProgressDialog dialog = UtilMessage.showProgressDialog(this,
				"Connecting with the server.... please wait");

		new Thread(new Runnable() {
			public void run() {

				try {
					Map<String, String> paramsMap = new HashMap<String, String>();

					if (country != null) {
						paramsMap.put(HttpArgs.country, country);
					}

					if (city != null) {
						paramsMap.put(HttpArgs.city, city);
					}

					String responseString = UtilsHttp
							.performPostCall(
									Config.SERVER_URL
											+ Config.HttpActions.FIND_USERS_BY_COUNTRY_AND_CITY,
									paramsMap);

					System.out
							.println("testFindUsersByCountryAndCity() ... retrieving the data");

					System.out
							.println("testFindUsersByCountryAndCity() response String from server : "
									+ responseString);

					final Object responseObject = (Object) UtilsHttp
							.parseResponseFromServer(responseString,
									new ListUsers());

					/**
					 * parseamos la respuesta del servidor y en funcion de si el
					 * resultado es una lista de usuarios/objeto serializable o
					 * un [mensaje de error/exito] -> StatusPackage (en funcion
					 * de la peticion realizada), luego realizamos las
					 * operaciones correspondientes
					 */

					runOnUiThread(new Thread() {
						@Override
						public void run() {

							if (responseObject instanceof ListUsers) {

								ListUsers listUsers = (ListUsers) responseObject;

								if (listUsers != null) {

									// si no se encuentra ningun usario
									// mostramos noficamos al usuario y salimos
									if (listUsers.list != null
											&& listUsers.list.isEmpty()) {

										userList.clear();
										userListAdpater = new UsersListAdapter(
												SearchUsersActivity.this,
												SearchUsersActivity.this.userList);
										lvUsers.setAdapter(userListAdpater);
										userListAdpater.notifyDataSetChanged();

										UtilMessage
												.showInfoMessage(
														SearchUsersActivity.this,
														"No User has been found with this criteria");

										return;
									} else {
										UtilMessage
												.showInfoMessage(
														SearchUsersActivity.this,
														listUsers.list.size()
																+ " users has been found");

										userList = new ArrayList<UserListViewModel>();
										for (UsersServ u : listUsers.list) {
											Log.i(TAG_INFO,
													" creating user.............."
															+ u);
											final int idImg = u.getIdImg() == null ? 0
													: u.getIdImg();
											userList.add(new UserListViewModel(
													u.getId(), idImg, u
															.getName(), u
															.getCountry(), u
															.getCity(), u
															.getIsConnected()));
										}
										userListAdpater = new UsersListAdapter(
												SearchUsersActivity.this,
												SearchUsersActivity.this.userList);
										lvUsers.setAdapter(userListAdpater);
										userListAdpater.notifyDataSetChanged();
									}

								}

								if (listUsers.list.isEmpty() == false) {
									Log.i(TAG_INFO, listUsers.list.get(0)
											.toString());
								} else {
									Log.i(TAG_INFO,
											"no se ha encontrano ningún usuario con los parametros");
								}

							} else {
								StatusPackage statusPackage = (StatusPackage) responseObject;
								UtilMessage.showErrorMessage(
										SearchUsersActivity.this,
										statusPackage.getMessage());
							}
						}
					});
				} catch (final Throwable e) {
					runOnUiThread(new Thread() {

						@Override
						public void run() {
							// UtilMessage.dismisProgressDialog(dialog);
							Log.e(TAG_ERROR, "error" + e.toString());
							e.printStackTrace();
							UtilMessage.showErrorMessage(
									SearchUsersActivity.this,
									"Error Searching The Users from server");

							UtilMessage.dismisProgressDialog(dialog);
						}
					});
				} finally {
					UtilMessage.dismisProgressDialog(dialog);
				}

			}
		}).start();

	}

	public void findCountries() {
		final ProgressDialog dialog = UtilMessage.showProgressDialog(this,
				"Connecting with the server.... please wait");

		new Thread(new Runnable() {
			public void run() {

				try {
					Map<String, String> paramsMap = new HashMap<String, String>();

					String responseString = UtilsHttp
							.performPostCall(
									Config.SERVER_URL
											+ Config.HttpActions.FIND_DISTINCT_COUNTRIES_AND_CITIES,
									paramsMap);

					Log.i(TAG_INFO,
							"testFindUsersByCountryAndCity() response String from server : "
									+ responseString);

					final ListCountriesCities listCountriesCities = new ListCountriesCities();
					final Object responseObject = (Object) UtilsHttp
							.parseResponseFromServer(responseString,
									listCountriesCities);

					/**
					 * parseamos la respuesta del servidor y en funcion de si el
					 * resultado es una lista de usuarios/objeto serializable o
					 * un [mensaje de error/exito] -> StatusPackage (en funcion
					 * de la peticion realizada), luego realizamos las
					 * operaciones correspondientes
					 */
					runOnUiThread(new Thread() {
						@Override
						public void run() {
							try {

								if (responseObject instanceof ListCountriesCities) {
									ListCountriesCities listCitiesAndCountries = (ListCountriesCities) responseObject;
									if (listCitiesAndCountries != null
											&& listCitiesAndCountries.listCountries != null
											& listCountriesCities != null) {

										List<String> countries = new ArrayList<String>();
										countries.add(HttpArgs.VALUE_ALL);
										countries
												.addAll(listCitiesAndCountries.listCountries);

										ArrayAdapter<String> spinnerCoArrayAdapter = new ArrayAdapter<String>(
												SearchUsersActivity.this,
												android.R.layout.simple_spinner_item,
												countries);
										spinnerCoArrayAdapter
												.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

										spinnerCountries
												.setAdapter(spinnerCoArrayAdapter);

									} else {
										Log.i(TAG_ERROR,
												"SEARCH USER ACTIVITY - -Error retrieving the countries "
														+ "and cities cities from server");
										UtilMessage
												.showInfoMessage(
														SearchUsersActivity.this,
														"No countries and cities cities was found on server");
									}

								} else {
									Log.i(TAG_ERROR,
											"SEARCH USER ACTIVITY - -Error retrieving the countries "
													+ "and cities cities from server");

									UtilMessage
											.showErrorMessage(
													SearchUsersActivity.this,
													"Error retrieving the countries and cities cities from server");
								}
							} catch (Exception e) {
							} finally {
								UtilMessage.dismisProgressDialog(dialog);
							}

						}
					});

				} catch (final Throwable e) {
					runOnUiThread(new Thread() {
						@Override
						public void run() {
							Log.e(TAG_ERROR, "error" + e.toString());
							e.printStackTrace();

							UtilMessage
									.showErrorMessage(SearchUsersActivity.this,
											"No countries and cities cities was found on server");

						}
					});
				} finally {
					UtilMessage.dismisProgressDialog(dialog);
				}

			}
		}).start();
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		if (view != null) {
			Log.i(Config.TAG_INFO,
					"onItemSelected View v.getClass() = " + view.getClass());
			Log.i(Config.TAG_INFO, "onItemSelected View v = " + view);

		}

		Log.i(Config.TAG_INFO, "onItemSelected View parent.getClass() = "
				+ parent.getClass());
		Log.i(Config.TAG_INFO, "onItemSelected View parent = " + parent);

		if (parent == this.spinnerCountries) {
			updateCities();
		}

	}

	public void updateCities() {
		final ProgressDialog dialog = UtilMessage.showProgressDialog(this,
				"Connecting with the server.... please wait");

		final String selCountry = this.spinnerCountries.getSelectedItem()
				.toString();

		new Thread(new Runnable() {
			public void run() {

				try {
					Map<String, String> paramsMap = new HashMap<String, String>();
					paramsMap.put(HttpArgs.country, selCountry);
					String responseString = UtilsHttp
							.performPostCall(
									Config.SERVER_URL
											+ Config.HttpActions.FIND_DISTINCT_CITIES_FROM_COUNTRY,
									paramsMap);

					Log.i(TAG_INFO,
							"updateCities() response String from server : "
									+ responseString);

					final ListCountriesCities listCountriesCities = new ListCountriesCities();
					final Object responseObject = (Object) UtilsHttp
							.parseResponseFromServer(responseString,
									listCountriesCities);

					/**
					 * parseamos la respuesta del servidor y en funcion de si el
					 * resultado es una lista de usuarios/objeto serializable o
					 * un [mensaje de error/exito] -> StatusPackage (en funcion
					 * de la peticion realizada), luego realizamos las
					 * operaciones correspondientes
					 */
					runOnUiThread(new Thread() {
						@Override
						public void run() {
							try {

								if (responseObject instanceof ListCountriesCities) {
									ListCountriesCities listCitiesAndCountries = (ListCountriesCities) responseObject;
									if (listCitiesAndCountries != null
											&& listCitiesAndCountries.listCities != null
											& listCountriesCities != null) {

										List<String> cities = new ArrayList<String>();
										cities.add(HttpArgs.VALUE_ALL);
										cities.addAll(listCitiesAndCountries.listCities);

										ArrayAdapter<String> spinnerCiArrayAdapter = new ArrayAdapter<String>(
												SearchUsersActivity.this,
												android.R.layout.simple_spinner_item,
												cities);
										spinnerCiArrayAdapter
												.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

										spinnerCities
												.setAdapter(spinnerCiArrayAdapter);

										Log.i(Config.TAG_INFO,
												"updateCities - updated spinnerCities");
										spinnerCiArrayAdapter
												.notifyDataSetChanged();

									} else {
										Log.i(TAG_INFO,
												"SEARCH USER ACTIVITY - -Error updating the cities "
														+ "and cities cities from server");

										UtilMessage.showErrorMessage(
												SearchUsersActivity.this,
												"No cities was found on server from country "
														+ country);
									}

								} else {
									Log.i(TAG_INFO,
											"SEARCH USER ACTIVITY - -Error retrieving the cities "
													+ "and cities cities from server");
									UtilMessage
											.showErrorMessage(
													SearchUsersActivity.this,
													"Error retrieving the countries and cities cities from server");

								}
							} catch (Exception e) {
								e.printStackTrace();
							} finally {
								UtilMessage.dismisProgressDialog(dialog);
							}

						}
					});

				} catch (final Throwable e) {
					runOnUiThread(new Thread() {
						@Override
						public void run() {
							Log.e(TAG_ERROR, "error" + e.toString());
							e.printStackTrace();

							UtilMessage
									.showErrorMessage(SearchUsersActivity.this,
											"No countries and cities cities was found on server");

						}
					});
				} finally {
					UtilMessage.dismisProgressDialog(dialog);
				}

			}
		}).start();
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// do nothing

	}

	@Override
	public void onClick(View v) {
		// v == this.btSearchUsers
		// if (v == this.btSearchUsers) {
		findUsers();
		// }
	}

}

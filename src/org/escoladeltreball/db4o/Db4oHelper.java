package org.escoladeltreball.db4o;

import java.io.File;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;

/**
 * Administra sobre la db las operaciones de :
 * 
 * 1º crear/abrir una db local. 2º cerrar la db local. 3º eliminar la db local.
 * 4º mostrar por la UI mensajes de error de la db local.
 * 
 * @author Emilio Amado 2015
 * @see Db4oDAOSong
 */
public class Db4oHelper {

	public static final String ERROR_DB = "Error accessing local db";

	public static final String ERROR_LIST_DONT_EXISTS = "Error, list does not exist";
	public static final String ERROR_LIST_EXISTS = "Error, list already exists";

	public static final String ERROR_SONG_DONT_EXISTS = "Error, song does not exist";
	public static final String ERROR_SONG_EXISTS = "song already exists";

	public static final String ERROR_OWNER_EXISTS = "Error, owner already exists";
	public static final String ERROR_OWNER_DONT_EXISTS = "Error, owner does not exist";

	public static final String ERROR_USER_EXISTS = "Error, user already exists";
	public static final String ERROR_USER_DONT_EXISTS = "Error, user does not exist";

	public static final String ERROR_FRIEND_EXISTS = "Error, friend already exists";
	public static final String ERROR_FRIEND_DONT_EXISTS = "Error, friend does not exist";

	public static final String ERROR_LOCAL_APP_WILL_CLOSE = "Error at local db, the app will be closed in 5 seconds";
	public static final String ERROR_SERVER_APP_WILL_CLOSE = "Error at server, the app will be closed in 5 seconds";

	public static final String ERROR_NOT_ANY_USERS = "Error, no friendship requests";
	public static final String ERROR_NOT_ANY_LISTS = "Error, there are not any lists";
	public static final String ERROR_NOT_ANY_SONGS = "Error, there are not any songs";
	public static final String ERROR_NOT_ANY_FRIENDS = "Error, there are not any friends";

	public static final String WARNING_SAME_PHOTO = "Selected avatar is the same\n please use another one";
	public static final String WARNING_SAME_NAME = "Name is the same\n please use another one";
	public static final String WARNING_SAME_PASS = "Password is the same\n please use another one";
	public static final String WARNING_SAME_CITY = "City is the same\n please use another one";
	public static final String WARNING_SAME_COUNTRY = "Country is the same\n please use another one";
	public static final String WARNING_SAME_DESCRIPTION = "Personal description is the same\n please type another one";
	public static final String WARNING_SAME_PREFERENCES = "Music preferences are the same\n please enter new preferences";

	public static final String SUCCESS_OWNER_ADDED = "Owner added";
	public static final String SUCCESS_FRIEND_ADDED = "Friend added";
	public static final String SUCCESS_USER_REFUSED = "User refused";
	public static final String SUCCESS_ALL_USER_ADDED = "All user friendship requests accepted";
	public static final String SUCCESS_ALL_USER_REFUSED = "All user friendship requests refused";
	public static final String SUCCESS_SONG_ADDED = "Song added";
	public static final String SUCCESS_LIST_ADDED = "List added";
	public static final String SUCCESS_OWNER_REMOVED = "Owner removed";
	public static final String SUCCESS_FRIEND_REMOVED = "Friend removed";
	public static final String SUCCESS_ALL_FRIEND_REMOVED = "All friends removed";
	public static final String SUCCESS_SONG_REMOVED = "Song removed";
	public static final String SUCCESS_LIST_REMOVED = "List removed";
	public static final String SUCCESS_ALL_USERS_REMOVED = "Non-owner users removed";
	public static final String SUCCESS_USER_REMOVED = "User removed";

	public static final String SUCCESS_PHOTO_CHANGED = "Avatar changed";
	public static final String SUCCESS_NAME_CHANGED = "Name changed";
	public static final String SUCCESS_PASS_CHANGED = "Pass changed";
	public static final String SUCCESS_CITY_CHANGED = "City changed";
	public static final String SUCCESS_COUNTRY_CHANGED = "Country changed";
	public static final String SUCCESS_DESCRIPTION_CHANGED = "Description changed";
	public static final String SUCCESS_PREFERENCES_CHANGED = "Preferences changed";

	public static final int WARNING_DELETE_USER = 0;
	public static final String WARNING_DELETE_USER_MESSAGE = "         The following action will\n"
			+ "           completely erase user\n"
			+ "         registration and friends,\n"
			+ "                all favorites lists\n"
			+ "               will be erased too,\n"
			+ "           you want to continue?\n";

	// ATENCION LEER CUIDADOSAMENTE!!!
	// ===============================
	//
	// Cualquier clase que deba tener acceso a la db local db4o, deberá
	// seguir los siguientes pasos :
	//
	// 1º Declarar un att ObjectContainer y otro Db4oHelper. instanciar un
	// nuevo Db4oHelper.
	//
	// 2º Instanciar un nuevo Db4oHelper.
	// Instanciar un nuevo ObjectConatainer con el get de Db4oHelper.
	//
	// 3º El método que instancie a Db4oHelper y a ObjectContainer debe ser
	// tratado
	// con try - catch de la siguiente forma :
	//
	// try{
	// instancia a Db4oHelper
	// instancia a ObjectContainer
	// resto de código
	//
	// } catch (Exception e) {
	// Db4oHelper.displayDbError(this);
	// }
	//
	//
	//
	// 4º Para realizar acciones sobre la db local, hacer llamadas estáticas
	// a
	// los métodos de Db4oDAO, y siempre como primer arg se usará el
	// ObjectContainer anterior.
	//
	// Ejemplo de llamada :
	//
	// Song s1 = new Song("s1", "", null); -> instanciamos una nueva canción
	// a insertar.
	// Db4oDAO.insertSongToDB(oc, s1); -> llamamos estáticamente al método
	// correspondiente.

	public static ObjectContainer oc = null;
	private static Activity activity;

	/**
	 * Constructor
	 * 
	 * @param activity
	 *            la activity que inicializa la db
	 */
	public Db4oHelper(Activity activity) {
		this.activity = activity;
		createDb();
	}

	/**
	 * Crea o abre la db.
	 */
	public void createDb() {

		String db4oPath = "/data/data/" + activity.getPackageName()
				+ "/database.db4o";

		try {
			if (oc == null || oc.ext().isClosed()) {
				oc = Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(),
						db4oPath);
			}
		} catch (Exception e) {
			e.printStackTrace();
			displayDbError(Db4oHelper.ERROR_DB);
		}
	}

	/**
	 * Cierra la db.
	 */
	// public void closeDb(ObjectContainer oc) {
	// try {
	// if (oc != null) {
	// oc.close();
	// }
	// } catch (Exception e) {
	// e.printStackTrace();
	// displayDbError(Db4oHelper.ERROR_DB);
	// }
	// }

	/**
	 * elimina por completo la Db local del sistema. Una vez eliminada deberá
	 * volverse a crear un nueva db local.
	 */
	public boolean eliminateDb(ObjectContainer oc) {
		try {
			oc.close();
			String db4oPath = "/data/data/" + activity.getPackageName()
					+ "/database.db4o";
			File fileToDelete = new File(db4oPath);
			fileToDelete.delete();
		} catch (Exception e) {
			e.printStackTrace();
			// displayDbError(Db4oHelper.ERROR_DB);
			return false;
		}
		return true;
	}

	/**
	 * Muestra un Toast de error.
	 * 
	 * @param activity
	 *            Activity donde se muestra el Toast
	 */
	public static void displayDbError(final String message) {
		Log.e("ERROR", message);
		// activity.runOnUiThread(new Runnable() {
		// public void run() {
		// Toast toast = Toast.makeText(activity, message,
		// Toast.LENGTH_LONG);
		// View v = toast.getView();
		// v.setBackgroundColor(-65536);
		// toast.setGravity(Gravity.CENTER | Gravity.TOP, 0, 20);
		// toast.show();
		// }
		// });
	}

	/**
	 * Muestra un Toast con mensaje de éxito en la operación.
	 * 
	 * @param activity
	 *            Activity donde se muestra el Toast
	 */
	public static void displaySuccess(final String message) {
		activity.runOnUiThread(new Runnable() {
			public void run() {
				Toast toast = Toast.makeText(activity, message,
						Toast.LENGTH_LONG);
				View v = toast.getView();
				v.setBackgroundColor(-16776961);
				toast.setGravity(Gravity.CENTER | Gravity.TOP, 0, 20);
				toast.show();
			}
		});
	}

	/**
	 * Muestra un Toast con mensaje de advertencia.
	 * 
	 * @param activity
	 *            Activity donde se muestra el Toast
	 */
	public static void displayWarning(final String message) {
		activity.runOnUiThread(new Runnable() {
			public void run() {
				Toast toast = Toast.makeText(activity, message,
						Toast.LENGTH_LONG);
				View v = toast.getView();
				v.setBackgroundColor(Color.rgb(255, 204, 0));
				toast.setGravity(Gravity.CENTER | Gravity.TOP, 0, 20);
				toast.show();
			}
		});
	}

	/**
	 * Retorna el Object Container de la db para poder operar sobre la db desde
	 * una clase DAO.
	 * 
	 * @return el object container de la db
	 */
	public ObjectContainer getObjectContainer() {
		try {
			return oc;
		} catch (Exception e) {
			e.printStackTrace();
			// displayDbError(activity, Db4oHelper.ERROR_DB);
		}
		return oc;
	}

	/**
	 * Muestra por pantalla un dialog de advertncia para seguir o cancelar la
	 * operación.
	 */
	public static void displayWarningDialog(final int warningType) {

		String message = "";
		if (warningType == WARNING_DELETE_USER) {
			message = WARNING_DELETE_USER_MESSAGE;
		}
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
		// alertDialog.setTitle("               WARNING");
		alertDialog.setTitle(Html
				.fromHtml("<font color='#FF0000'>WARNING</font>"));
		alertDialog.setMessage(message);
		alertDialog.setPositiveButton("Accept",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						if (warningType == WARNING_DELETE_USER) {
							// borramos al owner
							Db4oDAOUser.deleteUserOwnerFromDb(oc);
							// borramos a los amigos
							Db4oDAOUser.refuseAllFriendshipUserAccepted(oc);
							// borramos al resto de usuarios
							Db4oDAOUser.deleteAllUserNotOwnerFromDb(oc);
							// borramos las listas de favoritos
							Db40DAOFavorites.deleteAllListFromDb(oc);
							// borramos las canciones
							Db4oDAOSong.deleteAllSongFromDb(oc);
						}
					}
				});
		alertDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		alertDialog.show();
	}
}
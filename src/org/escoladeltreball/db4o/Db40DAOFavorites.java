package org.escoladeltreball.db4o;

import java.util.ArrayList;

import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.ext.DatabaseClosedException;
import com.db4o.query.Predicate;

/**
 * Clase estática para llevar a cabo los ataques a la db local, referentes a los
 * objetos Favorites, inserciones, actualizaciones y demas operaciones sobre la
 * db local.
 * 
 * @author Emilio Amado 2015
 * @see Db4oHelper
 * 
 */
public class Db40DAOFavorites {

	/**
	 * Inserta una nueva lista de canciones favoritas en la DB local.
	 * 
	 * @param oc
	 *            el ObjectContainer de la db
	 * @param favorites
	 *            la lista a insertar
	 */
	public static void insertFavoritesToDB(ObjectContainer oc,
			final Favorites favorites) {

		User owner = Db4oDAOUser.getUserOwnerFromDB(oc);
		if (owner == null) {
			return;
		}

		try {
			ObjectSet<Favorites> result = oc.query(new Predicate<Favorites>() {
				@Override
				public boolean match(Favorites s) {
					if (s.getListName().equals(favorites.getListName())) {
						return true;
					} else {
						return false;
					}
				}
			});
			// si la lista existe en la db
			if (!result.isEmpty()) {
				Db4oHelper.displayDbError(Db4oHelper.ERROR_LIST_EXISTS);
				return;
			}
			oc.store(favorites);
			oc.commit();
			// Db4oHelper.displaySuccess(Db4oHelper.SUCCESS_LIST_ADDED);
			return;
		} catch (DatabaseClosedException e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
	}

	/**
	 * Dada una lista de favoritos, la busca en la Db local.
	 * 
	 * @param oc
	 *            el ObjectContainer de la db
	 * @param favorites
	 *            la lista a buscar
	 * @return la lista encontrada
	 */
	public static Favorites getFavoritesFromDB(ObjectContainer oc,
			final Favorites favorites) {

		// Favorites s = new Favorites("");
		Favorites s = new Favorites();
		// s.setListName("");
		try {
			ObjectSet<Favorites> result = oc.query(new Predicate<Favorites>() {
				@Override
				public boolean match(Favorites f) {
					if (f.getListName().equals(favorites.getListName())) {
						return true;
					} else {
						return false;
					}
				}
			});
			// si la lista existe
			if (!result.isEmpty()) {
				while (result.hasNext()) {
					s = result.next();
					return s;
				}
				// si la lista no existe
			} else {
				Db4oHelper.displayDbError(Db4oHelper.ERROR_LIST_DONT_EXISTS);
				return null;
			}
		} catch (DatabaseClosedException e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
		return null;
	}

	/**
	 * Retorna todas las listas insertadas en la Db local.
	 * 
	 * @param oc
	 *            el ObjectContainer de la db
	 * @return ArrayList<Favorites>
	 */
	public static ArrayList<Favorites> getAllFavoritesFromDB(ObjectContainer oc) {

		ArrayList<Favorites> allFavorites = new ArrayList<Favorites>();
		try {
			ObjectSet<Favorites> result = oc.query(new Predicate<Favorites>() {

				@Override
				public boolean match(Favorites s) {
					return true;
				}
			});
			// si hay listas
			if (!result.isEmpty()) {
				while (result.hasNext()) {
					Favorites favorites = result.next();
					allFavorites.add(favorites);
				}
				return allFavorites;
			}
			// si no hay listas
			else {
				Db4oHelper.displayDbError(Db4oHelper.ERROR_NOT_ANY_LISTS);
				return null;
			}
		} catch (DatabaseClosedException e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
		return null;
	}

	/**
	 * Dada una lista, elimina su registro de la DB local.
	 * 
	 * @param oc
	 *            el ObjectContainer de la db
	 * @param favorites
	 *            la lista a eliminar
	 */
	public static boolean deleteFavoritesFromDB(ObjectContainer oc,
			final Favorites favorites) {

		try {
			ObjectSet<Favorites> result = oc.query(new Predicate<Favorites>() {
				@Override
				public boolean match(Favorites s) {
					if (s.getListName().equals(favorites.getListName())) {
						return true;
					} else {
						return false;
					}
				}
			});
			// si la cancion existe en la db la borramos
			if (!result.isEmpty()) {
				while (result.hasNext()) {
					Favorites s = result.next();
					oc.delete(s);
					oc.commit();
					// Db4oHelper.displaySuccess(Db4oHelper.SUCCESS_LIST_REMOVED);
					return true;
				}
			}
			// si la lista no existe
			else {
				Db4oHelper.displayDbError(Db4oHelper.ERROR_LIST_DONT_EXISTS);
				return false;
			}
		} catch (DatabaseClosedException e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
		return false;
	}

	/**
	 * Dada una lista de favoritos y una canción, actualiza el registro de la
	 * lista de favorito, eliminando la canción de la lista.
	 * 
	 * @param oc
	 *            el ObjectContainer de la db
	 * @param favoritesList
	 *            la lista de canciones favoritas
	 * @param song
	 *            la canción
	 * @return true si se ha podido eliminar la canción de la lista
	 */
	public static boolean deleteSongFromFavorites(ObjectContainer oc,
			final Favorites favoritesList, Song song) {

		try {
			ObjectSet<Favorites> result = oc.query(new Predicate<Favorites>() {
				@Override
				public boolean match(Favorites favorites) {
					if (favorites.getListName().equals(
							favoritesList.getListName())) {
						return true;
					} else {
						return false;
					}
				}
			});

			Favorites favoritesSwap = null;
			ArrayList<Song> favoritesSawpSongs = null;
			ArrayList<Song> favoritesNewSongs = new ArrayList<Song>();
			if (!result.isEmpty()) {
				favoritesSwap = result.next();
				favoritesSawpSongs = favoritesSwap.getSongs();
				boolean songFound = false;
				for (Song s : favoritesSawpSongs) {
					if (!s.getSongName().equals(song.getSongName())) {
						favoritesNewSongs.add(s);
					} else {
						// Db4oHelper
						// .displaySuccess(Db4oHelper.SUCCESS_SONG_REMOVED);
						songFound = true;
					}
				}
				// si no se ecuentra la cancion
				if (!songFound) {
					Db4oHelper
							.displayDbError(Db4oHelper.ERROR_SONG_DONT_EXISTS);
					return false;
				}
				oc.delete(favoritesSwap);
				oc.commit();
				favoritesSwap.setSongs(favoritesNewSongs);
				oc.store(favoritesSwap);
				oc.commit();
				// por último eliminamos la canción de la db
				Db4oDAOSong.deleteSongFromDB(oc, song);
				return true;
				// si la lista no existe en la db local
			} else {
				Db4oHelper.displayDbError(Db4oHelper.ERROR_LIST_DONT_EXISTS);
				return false;
			}

		} catch (DatabaseClosedException e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
		return false;
	}

	/**
	 * Elimina el registro de todas las listas de canciones de la DB local.
	 * 
	 * @param oc
	 *            el ObjectContainer de la db
	 */
	public static boolean deleteAllListFromDb(ObjectContainer oc) {
		ArrayList<Favorites> allFavorites = Db40DAOFavorites
				.getAllFavoritesFromDB(oc);
		if (allFavorites != null) {
			for (Favorites f : allFavorites) {
				Db40DAOFavorites.deleteFavoritesFromDB(oc, f);
			}
			return true;
		}
		return false;
	}

	/**
	 * Dada una lista de favoritos y una canción, actualiza el registro de la
	 * lista de favoritos, añadiendo la canción de la lista.
	 * 
	 * @param oc
	 *            el ObjectContainer de la db
	 * @param favoritesList
	 *            la lista de canciones favoritas
	 * @param song
	 *            la canción
	 * @return true si se ha podido añadir la canción de la lista
	 */
	public static boolean addSongToFavorites(ObjectContainer oc,
			final Favorites favoritesList, Song song) {
		// primero debemos añadir la cancion a la db local
		System.out.println("daoFavorites ");
		Db4oDAOSong.insertSongToDB(oc, song);
		try {
			ObjectSet<Favorites> result = oc.query(new Predicate<Favorites>() {
				@Override
				public boolean match(Favorites favorites) {
					if (favorites.getListName().equals(
							favoritesList.getListName())) {
						return true;
					} else {
						return false;
					}
				}
			});

			Favorites favoritesSwap = null;
			ArrayList<Song> favoritesSawpSongs = null;
			ArrayList<Song> favoritesNewSongs = new ArrayList<Song>();
			if (!result.isEmpty()) {
				favoritesSwap = result.next();
				favoritesSawpSongs = favoritesSwap.getSongs();
				for (Song s : favoritesSawpSongs) {
					// si la canción ya existe en la lista no la añadimos
					if (s.getSongName().equals(song.getSongName())) {
						Db4oHelper.displayDbError(Db4oHelper.ERROR_SONG_EXISTS);
						return false;
					}
				}
				// si la cancion no existe
				favoritesNewSongs.add(song);
				for (Song s : favoritesSawpSongs) {
					favoritesNewSongs.add(s);
				}

				oc.delete(favoritesSwap);
				oc.commit();
				favoritesSwap.setSongs(favoritesNewSongs);
				oc.store(favoritesSwap);
				oc.commit();
				// Db4oHelper.displaySuccess(Db4oHelper.SUCCESS_SONG_ADDED);
				return true;
				// si la lista no existe en la db local
			} else {
				Db4oHelper.displayDbError(Db4oHelper.ERROR_LIST_DONT_EXISTS);
				return false;
			}

		} catch (DatabaseClosedException e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
		return false;
	}

	/**
	 * Cambia el estado del objecto Favorites a sincronizado con el servidor
	 * (true) o no soncronizado (false).
	 * 
	 * @param oc
	 *            el ObjectContainer de la db
	 * @param favoritesList
	 *            la lista de canciones favoritas
	 * @param newSynchronizedStatus
	 *            el nuevo estado
	 * @return true si se ha podido sincronizar la lista con el servidor
	 */
	public static boolean setSynchonizedWithServerStatusToSingleList(
			ObjectContainer oc, final Favorites favoritesList,
			boolean newSynchronizedStatus) {

		try {
			ObjectSet<Favorites> result = oc.query(new Predicate<Favorites>() {
				@Override
				public boolean match(Favorites favorites) {
					if (favorites.getListName().equals(
							favoritesList.getListName())) {
						return true;
					} else {
						return false;
					}
				}
			});

			Favorites favoritesSwap = null;
			if (!result.isEmpty()) {
				favoritesSwap = result.next();
				oc.delete(favoritesSwap);
				oc.commit();
				favoritesSwap
						.setIsListSynchronizedWithServer(newSynchronizedStatus);
				oc.store(favoritesSwap);
				oc.commit();
				return true;
				// si la lista no existe
			} else {
				Db4oHelper.displayDbError(Db4oHelper.ERROR_LIST_DONT_EXISTS);
				return false;
			}

		} catch (DatabaseClosedException e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
		return false;
	}

	/**
	 * Cambia el estado de todos lo objectos Favorites a sincronizado con el
	 * servidor (true) o no soncronizado (false).
	 * 
	 * @param oc
	 *            el ObjectContainer de la db
	 * @param newSynchronizedStatus
	 *            el nuevo estado
	 * @return true si se han podido sincronizar las listas con el servidor
	 */
	public static boolean setSynchonizedWithServerStatusToAllList(
			ObjectContainer oc, boolean newSynchronizedStatus) {

		try {
			ObjectSet<Favorites> result = oc.query(new Predicate<Favorites>() {
				@Override
				public boolean match(Favorites favorites) {
					return true;
				}
			});

			Favorites favoritesSwap = null;
			if (!result.isEmpty()) {
				while (result.hasNext()) {
					favoritesSwap = result.next();
					oc.delete(favoritesSwap);
					oc.commit();
					favoritesSwap
							.setIsListSynchronizedWithServer(newSynchronizedStatus);
					oc.store(favoritesSwap);
					oc.commit();
				}
				return true;
				// si no existe ninguna lista
			} else {
				Db4oHelper.displayDbError(Db4oHelper.ERROR_NOT_ANY_LISTS);
				return false;
			}

		} catch (DatabaseClosedException e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
		return false;
	}
}
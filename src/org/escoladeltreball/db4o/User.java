package org.escoladeltreball.db4o;

import org.escoladeltreball.model.Session;

/**
 * Modela una objeto que representa usuario.
 * 
 * @author Emilio Amado 2015
 * 
 */
public class User {

	public static final int FRIENDSHIP_STATE_ACCEPTED = 1;
	public static final int FRIENDSHIP_STATE_REFUSED = -1;
	public static final int FRIENDSHIP_STATE_WAITING = 0;

	private String name;
	private String pass;
	private String city;
	private String country;
	private String personalDescription;
	private String musicPreferences;
	private int photoResource;
	private boolean isOwner;
	private int friendShipState;
	private Long id;
	private Session session;

	public User(String name, String pass, String city, String country,
			String personalDescription, String musicPreferences,
			int photoResource, boolean isOwner) {
		super();
		this.name = name;
		this.pass = pass;
		this.city = city;
		this.country = country;
		this.personalDescription = personalDescription;
		this.musicPreferences = musicPreferences;
		this.photoResource = photoResource;
		this.isOwner = isOwner;
		this.friendShipState = FRIENDSHIP_STATE_WAITING;
	}

	public User() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean getIsOwner() {
		return isOwner;
	}

	public void setName(boolean isOwner) {
		this.isOwner = isOwner;
	}

	public int getFriendShipState() {
		return this.friendShipState;
	}

	public void setFriendShipState(int friendShipState) {
		this.friendShipState = friendShipState;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPersonalDescription() {
		return personalDescription;
	}

	public void setPersonalDescription(String personalDescription) {
		this.personalDescription = personalDescription;
	}

	public String getMusicPreferences() {
		return musicPreferences;
	}

	public void setMusicPreferences(String musicPreferences) {
		this.musicPreferences = musicPreferences;
	}

	public int getPhotoResource() {
		return photoResource;
	}

	public void setPhotoResource(int photoResource) {
		this.photoResource = photoResource;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + friendShipState;
		result = prime * result + (isOwner ? 1231 : 1237);
		result = prime
				* result
				+ ((musicPreferences == null) ? 0 : musicPreferences.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((pass == null) ? 0 : pass.hashCode());
		result = prime
				* result
				+ ((personalDescription == null) ? 0 : personalDescription
						.hashCode());
		result = prime * result + photoResource;
		return result;
	}

	@Override
	public String toString() {
		return "User [name=" + name + ", pass=" + pass + ", city=" + city
				+ ", country=" + country + ", personalDescription="
				+ personalDescription + ", musicPreferences="
				+ musicPreferences + ", photoResource=" + photoResource
				+ ", isOwner=" + isOwner + ", friendShipState="
				+ friendShipState + ", id=" + id + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (friendShipState != other.friendShipState)
			return false;
		if (isOwner != other.isOwner)
			return false;
		if (musicPreferences == null) {
			if (other.musicPreferences != null)
				return false;
		} else if (!musicPreferences.equals(other.musicPreferences))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (pass == null) {
			if (other.pass != null)
				return false;
		} else if (!pass.equals(other.pass))
			return false;
		if (personalDescription == null) {
			if (other.personalDescription != null)
				return false;
		} else if (!personalDescription.equals(other.personalDescription))
			return false;
		if (photoResource != other.photoResource)
			return false;
		return true;
	}

	public void setIsOwner(boolean b) {
		this.isOwner = b;

	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}
}
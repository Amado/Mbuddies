package org.escoladeltreball.db4o;

import java.util.ArrayList;

import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.ext.DatabaseClosedException;
import com.db4o.query.Predicate;

/**
 * Clase estática para llevar a cabo los ataques a la db local, referentes a los
 * objetos User, inserciones, actualizaciones y demas operaciones sobre la db
 * local.
 * 
 * @author Emilio Amado 2015
 * @see Db4oHelper
 * 
 */
public class Db4oDAOUser {

	public static final String NAME_DATA = "newName";
	public static final String PASS_DATA = "newPass";
	public static final String CITY_DATA = "newCity";
	public static final String COUNTRY_DATA = "newCountry";
	public static final String PERSONAL_DESCRIPTION_DATA = "newPersonalDescription";
	public static final String MUSIC_PREFERENCES_DATA = "newMusicPreferences";

	/**
	 * Inserta un nuevo usuario en la DB local, propietario o amigo.
	 * 
	 * @param oc
	 *            el ObjectContainer de la db
	 * @param user
	 *            el usuario a insertar
	 */
	public static boolean insertUserToDB(ObjectContainer oc, final User user) {
		try {
			ObjectSet<User> result = oc.query(new Predicate<User>() {
				@Override
				public boolean match(User s) {
					return true;
				}
			});
			boolean isSameUser = false;
			// si hay usuarios en la db local
			if (!result.isEmpty()) {
				// si el usuario a insertar es owner, salimos con false
				if (user.getIsOwner()) {
					// Db4oHelper.displayDbError(Db4oHelper.ERROR_OWNER_EXISTS);
					return false;
				}
				while (result.hasNext()) {
					User nextUser = result.next();
					// si el usuario ya existe salimos con false
					if (nextUser.getName().equals(user.getName())) {
						return false;
					}
				}
				// si no se ha encontrado una coincidencia hacemos la inserciom
				// y salimos con true
				oc.store(user);
				oc.commit();
				return true;
			}
			// si no hay usuarios y el usuario a insertar es owner, lo
			// insertamos
			else {
				if (user.getIsOwner()) {
					oc.store(user);
					oc.commit();
					// Db4oHelper.displaySuccess(Db4oHelper.SUCCESS_OWNER_ADDED);
					return true;
				} else {
					// Db4oHelper
					// .displayDbError(Db4oHelper.ERROR_OWNER_DONT_EXISTS);
					return false;
				}
			}

			 
		} catch (DatabaseClosedException e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
		return false;
	}

	/**
	 * Dado un usuario propietario,lo busca en la Db local.
	 * 
	 * @param oc
	 *            el ObjectContainer de la db
	 * @return el owner encontrado
	 */
	public static User getUserOwnerFromDB(ObjectContainer oc) {

		User s = null;

		try {
			ObjectSet<User> result = oc.query(new Predicate<User>() {
				@Override
				public boolean match(User f) {
					if (f.getIsOwner()) {
						return true;
					} else {
						return false;
					}
				}
			});
			// si la lista existe
			if (!result.isEmpty()) {
				while (result.hasNext()) {
					s = result.next();
					return s;
				}
				// si la lista no existe
			} else {
				Db4oHelper.displayDbError(Db4oHelper.ERROR_OWNER_DONT_EXISTS);
				return null;
			}
		} catch (DatabaseClosedException e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
		return null;
	}

	/**
	 * Busca todos los usuarios en la db local, propietario y amigos.
	 * 
	 * @param oc
	 *            el ObjectContainer de la db
	 * @return los User encontrados
	 */
	public static ArrayList<User> getAllUsersFromDb(ObjectContainer oc) {
		try {
			ObjectSet<User> result = oc.query(new Predicate<User>() {
				@Override
				public boolean match(User s) {
					return true;
				}
			});
			// si la cancion existe en la db la borramos

			ArrayList<User> allUsers = new ArrayList<User>();
			if (!result.isEmpty()) {
				while (result.hasNext()) {
					allUsers.add(result.next());
				}
				return allUsers;
			}
			// si la lista no existe
			else {
				Db4oHelper.displayDbError(Db4oHelper.ERROR_NOT_ANY_USERS);
				return null;
			}
		} catch (DatabaseClosedException e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
		return null;
	}

	/**
	 * Dado el nomrbre de un amigo, la busca en la Db local.
	 * 
	 * @param oc
	 *            el ObjectContainer de la db
	 * @param notOwner
	 *            el User a buscar
	 * @return la canción encontrada
	 */
	public static User getUserNotOwnerFromDb(ObjectContainer oc, User notOwner) {

		try {
			ObjectSet<User> result = oc.query(new Predicate<User>() {
				@Override
				public boolean match(User s) {
					if (!s.getIsOwner()) {
						return true;
					} else {
						return false;
					}
				}
			});
			// si la cancion existe en la db la borramos
			if (!result.isEmpty()) {
				while (result.hasNext()) {
					User u = result.next();
					if (u.getName().equals(notOwner.getName())) {
						// si no hemos encontrado a ningún propietario
						return u;
					}
				}
				// si no se ha encontrado al amigo
				Db4oHelper.displayDbError(Db4oHelper.ERROR_USER_DONT_EXISTS);
				return null;
			}
			// si la lista no existe
			else {
				Db4oHelper.displayDbError(Db4oHelper.ERROR_NOT_ANY_USERS);
				return null;
			}
		} catch (DatabaseClosedException e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
		return null;
	}

	/**
	 * Retorna todos los amigos del usuario.
	 * 
	 * @param oc
	 *            el ObjectContainer de la db
	 * @return ArrayList<User> los amigos
	 */
	public static ArrayList<User> getAllUserNotOwnerFromDb(ObjectContainer oc) {

		try {
			ObjectSet<User> result = oc.query(new Predicate<User>() {
				@Override
				public boolean match(User s) {
					if (!s.getIsOwner()) {
						return true;
					} else {
						return false;
					}
				}
			});
			// si la cancion existe en la db la borramos

			ArrayList<User> allUsers = new ArrayList<User>();
			if (!result.isEmpty()) {
				while (result.hasNext()) {
					allUsers.add(result.next());
				}
				return allUsers;
			}
			// si la lista no existe
			else {
				Db4oHelper.displayDbError(Db4oHelper.ERROR_NOT_ANY_USERS);
				return null;
			}
		} catch (DatabaseClosedException e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
		return null;
	}

	/**
	 * Da la orden de desplegar un Dialog de confirmación de borrado de Owner.
	 * 
	 * @param activity
	 *            Activity desde donde se lanza el método
	 */
	public static void deleteUserOwnerFromDbRequest() {
		int warningType = Db4oHelper.WARNING_DELETE_USER;
		Db4oHelper.displayWarningDialog(warningType);
	}

	/**
	 * Dado un usuario propietario, elimina su registro de la DB local.
	 * 
	 * @param oc
	 *            el ObjectContainer de la db
	 */
	public static boolean deleteUserOwnerFromDb(ObjectContainer oc) {

		try {
			ObjectSet<User> result = oc.query(new Predicate<User>() {
				@Override
				public boolean match(User s) {
					if (s.getIsOwner()) {
						return true;
					} else {
						return false;
					}
				}
			});
			// si la cancion existe en la db la borramos
			if (!result.isEmpty()) {
				oc.delete(result.next());
				oc.commit();
				// si no hemos encontrado a ningún propietario
				// Db4oHelper.displaySuccess(Db4oHelper.SUCCESS_OWNER_REMOVED);
				return true;
			}
			// si la lista no existe
			else {
				Db4oHelper.displayDbError(Db4oHelper.ERROR_OWNER_DONT_EXISTS);
				return false;
			}
		} catch (DatabaseClosedException e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
		return false;
	}

	/**
	 * Dado un usuario no owner, lo elimina en la db local.
	 * 
	 * @param oc
	 *            el ObjectContainer de la db
	 * @param notOwner
	 *            el USer a eliminar
	 * @return true si el friend ha sido encontrado
	 */
	public static boolean deleteUserNotOwnerFromDb(ObjectContainer oc,
			User notOwner) {

		try {
			ObjectSet<User> result = oc.query(new Predicate<User>() {
				@Override
				public boolean match(User s) {
					if (!s.getIsOwner()) {
						return true;
					} else {
						return false;
					}
				}
			});
			// si la cancion existe en la db la borramos
			if (!result.isEmpty()) {
				while (result.hasNext()) {
					User userSwap = result.next();
					if (userSwap.getName().equals(notOwner.getName())) {
						oc.delete(userSwap);
						oc.commit();
						// Db4oHelper
						// .displaySuccess(Db4oHelper.SUCCESS_USER_REMOVED);
						return true;
					}
				}
				// si no se ha encontrado al amigo
				Db4oHelper.displayDbError(Db4oHelper.ERROR_USER_DONT_EXISTS);
				return false;
			}
			// si la lista no existe
			else {
				Db4oHelper.displayDbError(Db4oHelper.ERROR_NOT_ANY_USERS);
				return false;
			}
		} catch (DatabaseClosedException e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
		return false;
	}

	/**
	 * Elimina de la db local los registros de todos los usuarios que no sean
	 * owner, entendiéndose estos usuarios, como perfiles consultados, o
	 * usuarios esperando respuesta de amistad.
	 * 
	 * @param oc
	 *            el ObjectContainer de la db
	 * @return true si se tiene éxito
	 */
	public static boolean deleteAllUserNotOwnerFromDb(ObjectContainer oc) {
		try {
			ObjectSet<User> result = oc.query(new Predicate<User>() {
				@Override
				public boolean match(User s) {
					if (!s.getIsOwner()) {
						return true;
					} else {
						return false;
					}
				}
			});
			// si la cancion existe en la db la borramos
			if (!result.isEmpty()) {
				while (result.hasNext()) {
					oc.delete(result.next());
					oc.commit();
				}
				// Db4oHelper.displaySuccess(Db4oHelper.SUCCESS_ALL_USERS_REMOVED);
				return true;
			}
			// si la lista no existe
			else {
				Db4oHelper.displayDbError(Db4oHelper.ERROR_NOT_ANY_USERS);
				return false;
			}
		} catch (DatabaseClosedException e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
		return false;
	}

	/**
	 * Devuelve todos los usuarios confirmados como amigos contenidos en la db.
	 * 
	 * @param oc
	 *            el ObjectContainer de la db
	 * @return todos los usuarios amigos
	 */
	public static ArrayList<User> getAllFriends(ObjectContainer oc) {

		ArrayList<User> friends = new ArrayList<User>();

		try {
			ObjectSet<User> result = oc.query(new Predicate<User>() {
				@Override
				public boolean match(User s) {
					if (!s.getIsOwner()) {
						return true;
					} else {
						return false;
					}
				}
			});
			// si la cancion existe en la db la borramos
			if (!result.isEmpty()) {
				while (result.hasNext()) {
					User u = result.next();
					if (u.getFriendShipState() == User.FRIENDSHIP_STATE_ACCEPTED) {
						friends.add(u);
					}
				}
				// si no hay amigos
				if (friends.size() == 0) {
					Db4oHelper.displayDbError(Db4oHelper.ERROR_NOT_ANY_FRIENDS);
					return null;
				}
				// devolvemos la lista de amigos
				return friends;
			}
			// si la lista no existe
			else {
				Db4oHelper.displayDbError(Db4oHelper.ERROR_NOT_ANY_USERS);
				return null;
			}
		} catch (DatabaseClosedException e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}

		return null;
	}

	/**
	 * Devuelve todos los usuarios que estén esperando respuesta de amistad
	 * contenidos en la db.
	 * 
	 * @param oc
	 *            el ObjectContainer de la db
	 * @return todos los usuarios en espera
	 */
	public static ArrayList<User> getAllUserWaiting(ObjectContainer oc) {

		ArrayList<User> friends = new ArrayList<User>();
		try {
			ObjectSet<User> result = oc.query(new Predicate<User>() {
				@Override
				public boolean match(User s) {
					if (!s.getIsOwner()) {
						if (s.getFriendShipState() == User.FRIENDSHIP_STATE_WAITING) {
							return true;
						} else {
							return false;
						}
					} else {
						return false;
					}
				}
			});
			// si la cancion existe en la db la borramos
			if (!result.isEmpty()) {
				while (result.hasNext()) {
					User u = result.next();
					friends.add(u);
				}
				// devolvemos la lista de amigos
				return friends;
			}
			// si la lista no existe
			else {
				Db4oHelper.displayDbError(Db4oHelper.ERROR_NOT_ANY_USERS);
				return null;
			}
		} catch (DatabaseClosedException e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
		return null;
	}

	/**
	 * Dado un usuario se le acepta como amigo.
	 * 
	 * @param newFriend
	 *            el usuario a ser aceptado como amigo
	 * @param oc
	 *            el ObjectContainer de la db
	 * @return true si se tiene éxito
	 */
	public static boolean acceptFriendshipUserWaiting(User newFriend,
			ObjectContainer oc) {

		try {
			ObjectSet<User> result = oc.query(new Predicate<User>() {
				@Override
				public boolean match(User s) {
					if (!s.getIsOwner()) {
						if (s.getFriendShipState() == User.FRIENDSHIP_STATE_WAITING) {
							return true;
						} else {
							return false;
						}
					} else {
						return false;
					}
				}
			});
			// si la cancion existe en la db la borramos
			if (!result.isEmpty()) {
				while (result.hasNext()) {
					User u = result.next();
					if (u.getName().equals(newFriend.getName())) {
						oc.delete(u);
						oc.commit();
						u.setFriendShipState(User.FRIENDSHIP_STATE_ACCEPTED);
						oc.store(u);
						oc.commit();
						// Db4oHelper
						// .displaySuccess(Db4oHelper.SUCCESS_FRIEND_ADDED);
						return true;
					}
				}
				User us = getUserNotOwnerFromDb(oc, newFriend);

				if (us.getName().equals(newFriend.getName())) {
					Db4oHelper.displayDbError(Db4oHelper.ERROR_FRIEND_EXISTS);
					return false;
				}
				// si no existe el amigo
				Db4oHelper.displayDbError(Db4oHelper.ERROR_USER_DONT_EXISTS);
				return false;
			}
			// si la lista no existe
			else {
				Db4oHelper.displayDbError(Db4oHelper.ERROR_NOT_ANY_USERS);
				return false;
			}
		} catch (DatabaseClosedException e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
		return false;
	}

	/**
	 * Acepta a todos los amigos como usuarios.
	 * 
	 * @param oc
	 *            el ObjectContainer de la db
	 * @return true si se tiene éxito
	 */
	public static boolean acceptAllFriendshipUserWaiting(ObjectContainer oc) {

		try {
			ObjectSet<User> result = oc.query(new Predicate<User>() {
				@Override
				public boolean match(User s) {
					if (!s.getIsOwner()) {
						if (s.getFriendShipState() == User.FRIENDSHIP_STATE_WAITING) {
							return true;
						} else {
							return false;
						}
					} else {
						return false;
					}
				}
			});
			// si la cancion existe en la db la borramos
			if (!result.isEmpty()) {
				while (result.hasNext()) {
					User u = result.next();
					oc.delete(u);
					oc.commit();
					u.setFriendShipState(User.FRIENDSHIP_STATE_ACCEPTED);
					oc.store(u);
					oc.commit();
				}
				// si no existe el amigo
				Db4oHelper.displaySuccess(Db4oHelper.SUCCESS_ALL_USER_ADDED);
				return true;
			}
			// si la lista no existe
			else {
				Db4oHelper.displayDbError(Db4oHelper.ERROR_NOT_ANY_USERS);
				return false;
			}
		} catch (DatabaseClosedException e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
		return false;
	}

	/**
	 * Dado un usuario esperando respuesta se le rechaza la amistad.
	 * 
	 * @param exFriend
	 *            el usuario a ser aceptado como amigo
	 * @param oc
	 *            el ObjectContainer de la db
	 * @return true si se tiene éxito
	 */
	public static boolean refuseFriendshipUserWaiting(User exFriend,
			ObjectContainer oc) {

		try {
			ObjectSet<User> result = oc.query(new Predicate<User>() {
				@Override
				public boolean match(User s) {
					if (!s.getIsOwner()) {
						if (s.getFriendShipState() == User.FRIENDSHIP_STATE_WAITING) {
							return true;
						} else {
							return false;
						}
					} else {
						return false;
					}
				}
			});
			// si la cancion existe en la db la borramos
			if (!result.isEmpty()) {
				while (result.hasNext()) {
					User u = result.next();
					if (u.getName().equals(exFriend.getName())) {
						oc.delete(u);
						oc.commit();
						u.setFriendShipState(User.FRIENDSHIP_STATE_REFUSED);
						oc.store(u);
						oc.commit();
						// Db4oHelper
						// .displaySuccess(Db4oHelper.SUCCESS_USER_REFUSED);
						return true;
					}
				}
				// si no existe el amigo
				Db4oHelper.displayDbError(Db4oHelper.ERROR_USER_DONT_EXISTS);
				return false;
			}
			// si la lista no existe
			else {
				Db4oHelper.displayDbError(Db4oHelper.ERROR_NOT_ANY_USERS);
				return false;
			}
		} catch (DatabaseClosedException e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
		return false;
	}

	/**
	 * Dado un usuario ya amigo se le rechaza la amistad.
	 * 
	 * @param exFriend
	 *            el usuario a ser rechazado como amigo
	 * @param oc
	 *            el ObjectContainer de la db
	 * @return true si se tiene éxito
	 */
	public static boolean refuseFriendshipUserAlredyFriend(User exFriend,
			ObjectContainer oc) {

		try {
			ObjectSet<User> result = oc.query(new Predicate<User>() {
				@Override
				public boolean match(User s) {
					if (!s.getIsOwner()) {
						if (s.getFriendShipState() == User.FRIENDSHIP_STATE_ACCEPTED) {
							return true;
						} else {
							return false;
						}
					} else {
						return false;
					}
				}
			});
			// si la cancion existe en la db la borramos
			if (!result.isEmpty()) {
				while (result.hasNext()) {
					User u = result.next();
					if (u.getName().equals(exFriend.getName())) {
						oc.delete(u);
						oc.commit();
						u.setFriendShipState(User.FRIENDSHIP_STATE_REFUSED);
						oc.store(u);
						oc.commit();
						// Db4oHelper
						// .displaySuccess(Db4oHelper.SUCCESS_FRIEND_REMOVED);
						return true;
					}
				}
				// si no existe el amigo
				Db4oHelper.displayDbError(Db4oHelper.ERROR_FRIEND_DONT_EXISTS);
				return false;
			}
			// si la lista no existe
			else {
				Db4oHelper.displayDbError(Db4oHelper.ERROR_NOT_ANY_FRIENDS);
				return false;
			}
		} catch (DatabaseClosedException e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
		return false;
	}

	/**
	 * rechaza todas las peticiones de amistad de usuarios esperando respuesta.
	 * 
	 * @param oc
	 *            el ObjectContainer de la db
	 * @return true si se tiene éxito
	 */
	public static boolean refuseAllFriendshipUserWaiting(ObjectContainer oc) {

		try {
			ObjectSet<User> result = oc.query(new Predicate<User>() {
				@Override
				public boolean match(User s) {
					if (!s.getIsOwner()) {
						if (s.getFriendShipState() == User.FRIENDSHIP_STATE_WAITING) {
							return true;
						} else {
							return false;
						}
					} else {
						return false;
					}
				}
			});
			// si la cancion existe en la db la borramos
			if (!result.isEmpty()) {
				while (result.hasNext()) {
					User u = result.next();
					oc.delete(u);
					oc.commit();
					u.setFriendShipState(User.FRIENDSHIP_STATE_REFUSED);
					oc.store(u);
					oc.commit();
				}
				// si no existe el amigo
				Db4oHelper.displaySuccess(Db4oHelper.SUCCESS_ALL_USER_REFUSED);
				return true;
			}
			// si la lista no existe
			else {
				Db4oHelper.displayDbError(Db4oHelper.ERROR_NOT_ANY_FRIENDS);
				return false;
			}
		} catch (DatabaseClosedException e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
		return false;
	}

	/**
	 * rechaza la amistad de todos los usuarios ya amigos.
	 * 
	 * @param oc
	 *            el ObjectContainer de la db
	 * @return true si se tiene éxito
	 */
	public static boolean refuseAllFriendshipUserAccepted(ObjectContainer oc) {

		try {
			ObjectSet<User> result = oc.query(new Predicate<User>() {
				@Override
				public boolean match(User s) {
					if (!s.getIsOwner()) {
						if (s.getFriendShipState() == User.FRIENDSHIP_STATE_ACCEPTED) {
							return true;
						} else {
							return false;
						}
					} else {
						return false;
					}
				}
			});
			// si la cancion existe en la db la borramos
			if (!result.isEmpty()) {
				while (result.hasNext()) {
					User u = result.next();
					oc.delete(u);
					oc.commit();
					u.setFriendShipState(User.FRIENDSHIP_STATE_REFUSED);
					oc.store(u);
					oc.commit();
				}
				// si no existe el amigo
				// Db4oHelper
				// .displaySuccess(Db4oHelper.SUCCESS_ALL_FRIEND_REMOVED);
				return true;
			}
			// si la lista no existe
			else {
				Db4oHelper.displayDbError(Db4oHelper.ERROR_NOT_ANY_FRIENDS);
				return false;
			}
		} catch (DatabaseClosedException e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
		return false;
	}

	/**
	 * Dado un resource de avatar, cambia el avatar del usuario owner.
	 * 
	 * @param oc
	 *            el ObjectContainer de la db
	 * @param newPhoto
	 *            el resource del nuevo avatar
	 * @return
	 */
	public static boolean changeOwnerPhoto(ObjectContainer oc, int newPhoto) {
		try {
			ObjectSet<User> result = oc.query(new Predicate<User>() {
				@Override
				public boolean match(User s) {
					if (s.getIsOwner()) {
						return true;
					} else {
						return false;
					}
				}
			});
			// si hay un usuario en la db
			if (!result.isEmpty()) {
				while (result.hasNext()) {
					User userSwap = result.next();
					int userSwapPhoto = userSwap.getPhotoResource();
					// si la nueva foto es la misma que ya tiene el usuario
					if (userSwapPhoto == newPhoto) {
						Db4oHelper
								.displayWarning(Db4oHelper.WARNING_SAME_PHOTO);
						return false;
					}
					oc.delete(userSwap);
					oc.commit();
					userSwap.setPhotoResource(newPhoto);
					oc.store(userSwap);
					oc.commit();
					// Db4oHelper.displaySuccess(Db4oHelper.SUCCESS_PHOTO_CHANGED);
					return true;
				}
			}
			// si no hay un usuario en la db
			else {
				Db4oHelper.displayDbError(Db4oHelper.ERROR_USER_DONT_EXISTS);
				return false;
			}

		} catch (DatabaseClosedException e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
		return false;
	}

	/**
	 * Dado un nuevo nombre, o email, o pass, o ciudad, o país, o preferencias
	 * musicales, o descripción personal, pero no la foto de usuario, cambia el
	 * dato del owner actual.
	 * 
	 * @param oc
	 *            el ObjectContainer de la db
	 * @param dataType
	 *            el tipo de dato a cambiar
	 * @param newData
	 *            el nuevo dato para el usuario
	 * @return
	 */
	public static boolean changeOwnerData(ObjectContainer oc, String dataType,
			String newData) {

		try {
			ObjectSet<User> result = oc.query(new Predicate<User>() {
				@Override
				public boolean match(User s) {
					if (s.getIsOwner()) {
						return true;
					} else {
						return false;
					}
				}
			});
			// si hay un usuario en la db
			if (!result.isEmpty()) {
				while (result.hasNext()) {
					User userSwap = result.next();

					if (dataType.equals(NAME_DATA)) {
						if (userSwap.getName().equals(newData)) {
							Db4oHelper
									.displayWarning(Db4oHelper.WARNING_SAME_NAME);
							return false;
						}
						oc.delete(userSwap);
						oc.commit();
						// Db4oHelper
						// .displaySuccess(Db4oHelper.SUCCESS_NAME_CHANGED);
						userSwap.setName(newData);
					} else if (dataType.equals(PASS_DATA)) {
						if (userSwap.getPass().equals(newData)) {
							Db4oHelper
									.displayWarning(Db4oHelper.WARNING_SAME_PASS);
							return false;
						}
						oc.delete(userSwap);
						oc.commit();
						// Db4oHelper
						// .displaySuccess(Db4oHelper.SUCCESS_PASS_CHANGED);
						userSwap.setPass(newData);
					} else if (dataType.equals(CITY_DATA)) {
						if (userSwap.getCity().equals(newData)) {
							Db4oHelper
									.displayWarning(Db4oHelper.WARNING_SAME_CITY);
							return false;
						}
						oc.delete(userSwap);
						oc.commit();
						// Db4oHelper
						// .displaySuccess(Db4oHelper.SUCCESS_CITY_CHANGED);
						userSwap.setCity(newData);
					} else if (dataType.equals(COUNTRY_DATA)) {
						if (userSwap.getCountry().equals(newData)) {
							Db4oHelper
									.displayWarning(Db4oHelper.WARNING_SAME_COUNTRY);
							return false;
						}
						oc.delete(userSwap);
						oc.commit();
						// Db4oHelper
						// .displaySuccess(Db4oHelper.SUCCESS_COUNTRY_CHANGED);
						userSwap.setCountry(newData);
					} else if (dataType.equals(PERSONAL_DESCRIPTION_DATA)) {
						if (userSwap.getPersonalDescription().equals(newData)) {
							Db4oHelper
									.displayWarning(Db4oHelper.WARNING_SAME_DESCRIPTION);
							return false;
						}
						oc.delete(userSwap);
						oc.commit();
						// Db4oHelper
						// .displaySuccess(Db4oHelper.SUCCESS_DESCRIPTION_CHANGED);
						userSwap.setPersonalDescription(newData);
					} else if (dataType.equals(MUSIC_PREFERENCES_DATA)) {
						if (userSwap.getMusicPreferences().equals(newData)) {
							Db4oHelper
									.displayWarning(Db4oHelper.WARNING_SAME_PREFERENCES);
							return false;
						}
						oc.delete(userSwap);
						oc.commit();
						// Db4oHelper
						// .displaySuccess(Db4oHelper.SUCCESS_PREFERENCES_CHANGED);
						userSwap.setMusicPreferences(newData);
					}
					oc.store(userSwap);
					oc.commit();
					return true;
				}
			}
			// si no hay un usuario en la db
			else {
				Db4oHelper.displayDbError(Db4oHelper.ERROR_USER_DONT_EXISTS);
				return false;
			}
		} catch (DatabaseClosedException e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
		return false;
	}

}
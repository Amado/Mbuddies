package org.escoladeltreball.db4o;

import java.io.ByteArrayOutputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * Modela una objeto que representa una canción.
 * 
 * @author Emilio Amado 2015
 *
 */
public class Song {

	private String songName;
	private String songPath;
	private byte[] songImage;

	public Song(String songName, String songPath, byte[] songImage) {
		super();
		this.songName = songName;
		this.songPath = songPath;
		this.songImage = songImage;
	}

	public Song() {

	}

	public String getSongName() {
		return songName;
	}

	public void setSongName(String songName) {
		this.songName = songName;
	}

	public String getSongPath() {
		return songPath;
	}

	public void setSongPath(String songPath) {
		this.songPath = songPath;
	}

	public byte[] getSongImage() {
		return songImage;
	}

	public void setSongImage(byte[] songImage) {
		this.songImage = songImage;
	}

	/**
	 * Convierte un array de bytes en un objeto Bitmap.
	 * 
	 * @param songImage
	 *            array de bytes con los bytes de la imagen
	 * @return el objeto Bitmap
	 */
	public static Bitmap getBitmapFromByteArray(byte[] songImageArray) {

		Bitmap bm = BitmapFactory.decodeByteArray(songImageArray, 0,
				songImageArray.length);

		return bm;
	}

	/**
	 * Convierte un objeto Bitmap en un array de bytes.
	 * 
	 * @param songImageBitmap
	 *            el objeto Bitmap a convertir
	 * @return el array de bytes
	 */
	public static byte[] getByteArrayFromBitmap(Bitmap songImageBitmap) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		// songImageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
		// songImageBitmap = scaleImage(songImageBitmap, 400);
		songImageBitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream);
		byte[] byteArray = stream.toByteArray();

		return byteArray;
	}

	public static Bitmap getBitmpaFromByteArray(byte[] songImageBytes) {
		return BitmapFactory.decodeByteArray(songImageBytes, 0,
				songImageBytes.length);

	}

	@Override
	public String toString() {
		Bitmap songImageBitmap = getBitmapFromByteArray(songImage);

		return "Song [songName=" + songName + ", songPath=" + songPath
				+ ", songImage=" + songImageBitmap.getWidth() + "x"
				+ songImageBitmap.getHeight() + " pixels]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((songName == null) ? 0 : songName.hashCode());
		result = prime * result
				+ ((songPath == null) ? 0 : songPath.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Song other = (Song) obj;
		if (songName == null) {
			if (other.songName != null)
				return false;
		} else if (!songName.equals(other.songName))
			return false;
		if (songPath == null) {
			if (other.songPath != null)
				return false;
		} else if (!songPath.equals(other.songPath))
			return false;
		return true;
	}

	/**
	 * 
	 * @param bitmap
	 * @param size
	 * @return
	 */
	public static Bitmap scaleImage(Bitmap bitmap, int size) {
		Bitmap resizedBitmap = null;
		int originalWidth = bitmap.getWidth();
		int originalHeight = bitmap.getHeight();
		int newWidth = -1;
		int newHeight = -1;
		float multFactor = -1.0F;
		if (originalHeight > originalWidth) {
			newHeight = size;
			multFactor = (float) originalWidth / (float) originalHeight;
			newWidth = (int) (newHeight * multFactor);
		} else if (originalWidth > originalHeight) {
			newWidth = size;
			multFactor = (float) originalHeight / (float) originalWidth;
			newHeight = (int) (newWidth * multFactor);
		} else if (originalHeight == originalWidth) {
			newHeight = size;
			newWidth = size;
		}
		resizedBitmap = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight,
				false);
		return resizedBitmap;
	}

	/**
	 * 
	 * @param songImageBitmap
	 * @return
	 */
	public static byte[] getLowByteArrayFromBitmap(Bitmap songImageBitmap) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		// songImageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
		songImageBitmap = scaleImage(songImageBitmap, 150);
		songImageBitmap.compress(Bitmap.CompressFormat.JPEG, 60, stream);
		byte[] byteArray = stream.toByteArray();

		return byteArray;
	}
}
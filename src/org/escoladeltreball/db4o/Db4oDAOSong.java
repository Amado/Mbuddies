package org.escoladeltreball.db4o;

import java.util.ArrayList;

import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.ext.DatabaseClosedException;
import com.db4o.query.Predicate;

/**
 * Clase estática para llevar a cabo los ataques a la db local,
 * referentes a los objetos Song, inserciones, actualizaciones 
 * y demas operaciones sobre la db local.
 * 
 * @author Emilio Amado 2015
 * @see Db4oHelper
 *
 */
public class Db4oDAOSong {

	/**
	 * Inserta una nueva cancion en la DB local.
	 * 
	 * @param oc el ObjectContainer de la db
	 * @param song la canción a insertar
	 */
	public static void insertSongToDB(ObjectContainer oc, final Song song) {
		try{
			
			User owner = Db4oDAOUser.getUserOwnerFromDB(oc);
			if(owner == null){
				return;
			}
			
			ObjectSet<Song> result = oc.query(new Predicate<Song>() {
				@Override
				public boolean match(Song s) {
					if (s.getSongName().equals(song.getSongName())) {
						return true;
					} else {
						return false;
					}
				}
			});
			//si no se encuentra la cancion, se almacena
			if (result.isEmpty()) {
				oc.store(song);
				oc.commit();
			}
		} catch (DatabaseClosedException e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
	}

	/**
	 * Dada una canción, la busca en la Db local.
	 * 
	 * @param oc el ObjectContainer de la db
	 * @param song la canción a buscar
	 * @return la canción encontrada
	 */
	public static Song getSongFromDB(ObjectContainer oc, final Song song) {

		try{
			ObjectSet<Song> result = oc.query(new Predicate<Song>() {
				@Override
				public boolean match(Song s) {
					if (s.getSongName().equals(song.getSongName())) {
						return true;
					} else {
						return false;
					}
				}
			});
			//si se encuentra la cancion
			if (!result.isEmpty()) {
				while (result.hasNext()) {
					Song s = result.next();
					return s;
				}
				//si no se encuantra la cancion
			} else {
				Db4oHelper.displayDbError(Db4oHelper.ERROR_SONG_DONT_EXISTS);
				return null;
			}
		} catch (DatabaseClosedException e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
		return null;
	}

	/**
	 * Retorna todas las canciones insertadas en la Db local.
	 * 
	 * @param oc el ObjectContainer de la db
	 * @return ArrayList<Song>
	 */
	public static ArrayList<Song> getAllSongsFromDB(ObjectContainer oc) {

		ArrayList<Song> songs = new ArrayList<Song>();

		try {

			ObjectSet<Song> result = oc.query(new Predicate<Song>() {

				@Override
				public boolean match(Song s) {
					return true;
				}
			});
			//si se encuentran canciones
			if(!result.isEmpty()){
				while (result.hasNext()) {
					Song song = result.next();
					songs.add(song);
				}
				return songs;
			}
			//si no hay canciones
			else{
				Db4oHelper.displayDbError(Db4oHelper.ERROR_NOT_ANY_SONGS);
				return null;
			}
		} catch (DatabaseClosedException e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
		return null;
	}

	/**
	 * Dada una canción, elimina su registro de la DB local.
	 * 
	 * @param oc el ObjectContainer de la db
	 * @param song la canción a eliminar
	 */
	public static boolean deleteSongFromDB(ObjectContainer oc, final Song song) {

		try{
			ObjectSet<Song> result = oc.query(new Predicate<Song>() {
				@Override
				public boolean match(Song s) {
					if (s.getSongName().equals(song.getSongName())) {
						return true;
					} else {
						return false;
					}
				}
			});
			// si la cancion existe en la db la borramos
			if (!result.isEmpty()) {
				while (result.hasNext()) {
					Song s = result.next();
					oc.delete(s);
					oc.commit();
					return true;
				}
			}
			// si la cancion no existe
			else {
				Db4oHelper.displayDbError(Db4oHelper.ERROR_SONG_DONT_EXISTS);
				return false;
			}
		} catch (DatabaseClosedException e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
		return false;
	}
	
	/**
	 * Elimina el registro de todas las canciones de la DB local.
	 * 
	 * @param oc el ObjectContainer de la db
	 */
	public static boolean deleteAllSongFromDb(ObjectContainer oc){
		ArrayList<Song> allSongs = Db4oDAOSong.getAllSongsFromDB(oc);
		if(allSongs != null){
			for(Song s : allSongs){
				deleteSongFromDB(oc, s);
			}
			return true;
		}
		return false;
	}
}
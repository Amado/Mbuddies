package org.escoladeltreball.db4o;

import java.util.ArrayList;

/**
 * Modela una objeto que representa una lista de canciones favoritas.
 * 
 * @author Emilio Amado 2015
 * 
 */
public class Favorites {

	private String listName;
	private ArrayList<Song> songs;
	private boolean isListSynchronizedWithServer;
	private String identification;
	private Long idUser;

	public Favorites(String listName) {
		super();
		this.listName = listName;
		this.songs = new ArrayList<Song>();
		this.isListSynchronizedWithServer = false;
	}

	public Favorites(String listName, Long idUser) {
		super();
		this.listName = listName;
		this.songs = new ArrayList<Song>();
		this.idUser = idUser;
		this.identification = idUser + ":" + listName + ":"
				+ System.currentTimeMillis();
		this.isListSynchronizedWithServer = false;
	}

	public Favorites() {

	}
	
	public Long getIdUser() {
		return idUser;
	}

	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}

	public String getIdentification() {
		return identification;
	}

	public void setIdentification(String identification) {
		this.identification = identification;
	}

	public String getListName() {
		return listName;
	}

	public void setListName(String listName) {
		this.listName = listName;
	}

	public ArrayList<Song> getSongs() {
		return songs;
	}

	public void setSongs(ArrayList<Song> songs) {
		this.songs = songs;
	}

	public boolean getIsListSynchronizedWithServer() {
		return this.isListSynchronizedWithServer;
	}

	public void setIsListSynchronizedWithServer(
			boolean isListSynchronizedWithServer) {
		this.isListSynchronizedWithServer = isListSynchronizedWithServer;
	}

	/**
	 * Dada una canción, la añade a la lista de canciones.
	 * 
	 * @param song
	 *            la canción a añadir.
	 */
	public void addSong(Song song) {
		this.songs.add(song);
	}

	/**
	 * Dada una canción, la eliminia de la lista de canciones.
	 * 
	 * @param song
	 *            la acanción a eliminar.
	 */
	public void removeSong(Song song) {
		this.songs.remove(song);
	}

	@Override
	public String toString() {
		return "Favorites [listName=" + listName + ", songs=" + songs
				+ ", isListSynchronizedWithServer="
				+ isListSynchronizedWithServer + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (isListSynchronizedWithServer ? 1231 : 1237);
		result = prime * result
				+ ((listName == null) ? 0 : listName.hashCode());
		result = prime * result + ((songs == null) ? 0 : songs.hashCode());
		return result;
	}

	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Favorites other = (Favorites) obj;
		if (isListSynchronizedWithServer != other.isListSynchronizedWithServer)
			return false;
		if (listName == null) {
			if (other.listName != null)
				return false;
		} else if (!listName.equals(other.listName))
			return false;
		if (songs == null) {
			if (other.songs != null)
				return false;
		} else if (!songs.equals(other.songs))
			return false;
		return true;
	}
}
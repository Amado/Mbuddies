package org.escoladeltreball.manager;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;

import org.apache.http.HttpException;
import org.escoladeltreball.exception.ValidationException;
import org.escoladeltreball.model.json.StatusPackage;
import org.escoladeltreball.support.Config;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public final class UtilsHttp {

	/**
	 * 
	 * @param responseString
	 * @param o
	 *            objeto del tipo a deserializar (en caso de exito)
	 * 
	 * @return un objeto del tipo de la clase pasada o StatusPackage si la
	 *         peticion al servidor no ha de devolver ningún objeto/colección en
	 *         particular; o null si no se puede parsear
	 * @throws Exception
	 */
	public static <T> Object parseResponseFromServer(String responseString,
			Object o) throws Exception {
		System.out.println("parseResponseFromServer()... ");
		if (responseString == null) {
			return null;
		}

		StatusPackage statusPackate = null;
		Gson gson = new GsonBuilder().create();
		try {
			statusPackate = gson.fromJson(responseString.toString(),
					new StatusPackage().getClass());

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (o != null) {
			if (statusPackate == null || statusPackate.getType() == null) {
				System.out.println("parsing custom class...");
				@SuppressWarnings("unchecked")
				T c = (T) gson.fromJson(responseString, o.getClass());
				return c;
			} else {
				return statusPackate;
			}
		}
		return null;
	}

	public static <T> T convertInstanceOfObject(Object o, Class<T> clazz) {
		try {
			return clazz.cast(o);
		} catch (ClassCastException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static byte[] getBytesArrayFromBitMap(Bitmap bmp) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
		byte[] byteArray = stream.toByteArray();
		return byteArray;

	}

	public static Bitmap getBitmapFromBytes(byte[] byteArray) {
		Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0,
				byteArray.length);
		return bmp;
	}

	public static Class<?> deSerializeFromJson(String jsonObject, Class<?> clase) {
		Class<?> paquete = null;
		try {

			Gson gson = new GsonBuilder().disableHtmlEscaping()
					.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
					.setPrettyPrinting().serializeNulls().create();

			paquete = (Class<?>) gson.fromJson(jsonObject, clase);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Log.i("info", "paquete.class = " + paquete.getClass());
		Log.i("info", "paquete.toString = " + paquete.toString());
		return paquete;
	}

	public static String createUrl2(final String action, final String[] params,
			final String[] values) throws UnsupportedEncodingException,
			ValidationException {

		String result = null;
		try {
			if (params == null || values == null) {
				result = Config.SERVER_URL + action;
			} else {
				if (params.length != values.length) {
					throw new ValidationException(
							"Exception creating url the number of params and values must be the same");
				}
				StringBuilder paramsValues = new StringBuilder("?");
				for (int i = 0; i < values.length; i++) {
					paramsValues
							.append(params[i]
									+ "="
									+ URLEncoder
											.encode(values[i],
													Config.Connection.CHARACTER_ENCODING)
									+ "&");
				}
 
			}
			int a = 0;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			Log.i("INFO", "result = " + result);
		}
 

		return result;
	}

	/**
	 * @deprecated
	 * @param action
	 * @param params
	 * @param values
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws ValidationException
	 */
	public static String createUrl(final String action, final String[] params,
			final String[] values) throws UnsupportedEncodingException,
			ValidationException {

		String result = null;
		try {
			if (params == null || values == null) {
				result = Config.SERVER_URL + action;
			} else {
				if (params.length != values.length) {
					throw new ValidationException(
							"Exception creating url the number of params and values must be the same");
				}
				StringBuilder paramsValues = new StringBuilder("?");
				for (int i = 0; i < values.length; i++) {
					paramsValues
							.append(URLEncoder.encode(params[i],
									Config.Connection.CHARACTER_ENCODING)
									+ "="
									+ URLEncoder
											.encode(values[i],
													Config.Connection.CHARACTER_ENCODING)
									+ "&");

					// paramsValues.append(params[i] + "="
					// + URLEncoder.encode(values[i], CHARACTER_ENCODING)
					// + "&");
				}

				paramsValues = paramsValues.delete(paramsValues.length() - 1,
						paramsValues.length());

				result = Config.SERVER_URL + action + paramsValues.toString();
				System.out.println("before encode");
				System.out.println("result = " + result);

			}
			int a = 0;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.out.println("result = " + result);
		}

		// String requestUrlStr = String.format(SERVER_URL + HttpActions.LOGIN
		// + "?%s=%s&%s=%s&%s=%s&%s=%s&%s=%s", HttpArgs.usernameArg,
		// username, HttpArgs.mailArg, mail, HttpArgs.passwordArg,
		// password, HttpArgs.city, city, HttpArgs.country, country);

		return result;
	}

	public static String performPostCall(String requestURL,
			Map<String, String> postDataParams) throws Exception {
		// final int readTimeout = 15000;
		// final int connectTimeout = 15000;
		Log.i(Config.TAG_INFO, "performPostCall...");
		URL url;
		String response = "";
		try {
			url = new URL(requestURL);

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setReadTimeout(Config.Connection.readTimeout);
			conn.setConnectTimeout(Config.Connection.connectTimeout);
			conn.setRequestMethod("POST");
			conn.setDoInput(true);
			conn.setDoOutput(true);

			OutputStream os = conn.getOutputStream();
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
					os, Config.Connection.CHARACTER_ENCODING));
			final String postdata = getPostData(postDataParams);
			Log.i(Config.TAG_INFO, "performPostCall: " + requestURL + "?"
					+ postdata);
			writer.write(getPostData(postDataParams));

			writer.flush();
			writer.close();
			os.close();
			int responseCode = conn.getResponseCode();

			if (responseCode == HttpsURLConnection.HTTP_OK) {
				String line;
				BufferedReader br = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
				while ((line = br.readLine()) != null) {
					response += line;
				}
			} else {
				response = "";

				throw new HttpException(responseCode + "");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

		return response;
	}

	private static String getPostData(Map<String, String> params)
			throws UnsupportedEncodingException {
		StringBuilder paramsStr = new StringBuilder();
		Set<Entry<String, String>> paramsSet = params.entrySet();
		for (Map.Entry<String, String> entry : paramsSet) {
			paramsStr.append("&");
			paramsStr.append(URLEncoder.encode(entry.getKey(),
					Config.Connection.CHARACTER_ENCODING));
			paramsStr.append("=");
			paramsStr.append(URLEncoder.encode(entry.getValue(),
					Config.Connection.CHARACTER_ENCODING));

		}
		// eliminamos el utilmo caracter &
		paramsStr.delete(paramsStr.length(), paramsStr.length());

		return paramsStr.toString();
	}
}

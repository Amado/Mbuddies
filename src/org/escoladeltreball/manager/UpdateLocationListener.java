package org.escoladeltreball.manager;


public interface UpdateLocationListener {

	/**
	 * Se obtinene los datos de la localización por arg y se actualizan los att
	 * ciudad y pais.
	 * 
	 * @param data
	 *            el array con los datos de la localización
	 */
	public abstract void updateLocalization(String city, String country);

}

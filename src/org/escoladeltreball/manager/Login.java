package org.escoladeltreball.manager;

import java.util.HashMap;
import java.util.Map;

import org.escoladeltreball.model.json.StatusPackage;
import org.escoladeltreball.model.server.persistence.UsersServ;
import org.escoladeltreball.support.Config;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Login implements Config {

	public static UsersServ performLogin(String username, String password,
			String country, String city, Activity activity) throws Exception {
		UsersServ user = null;
		Map<String, String> paramsMap = new HashMap<String, String>();
		paramsMap.put(HttpArgs.username, username);
		paramsMap.put(HttpArgs.password, password);
		paramsMap.put(HttpArgs.country, country);
		paramsMap.put(HttpArgs.city, city);

		String responseString = UtilsHttp.performPostCall(SERVER_URL
				+ HttpActions.LOGIN, paramsMap);

		Log.i(TAG_INFO, "performLogin() ... retrieving the data from user : "
				+ username);

		Log.i(TAG_INFO, "performLogin() response String from server : "
				+ responseString);

		Gson gson = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting()
				.serializeNulls().create();

		user = gson.fromJson(responseString.toString(),
				new UsersServ().getClass());
		// session = Session.getInstance(user);
		Log.i(TAG_INFO,
				"performLogin() -- responseString = "
						+ responseString.toString());

		System.out.println("Login  -> userId: " + user.getId());

		return user;
	}

	public static Object performSignUp(String username, String password,
			String country, String city) throws Exception {

		// construimos la peticion al servidor
		// pasamos email,nombre,password, pais y ciudad
		Map<String, String> paramsMap = new HashMap<String, String>();
		paramsMap.put(HttpArgs.username, username);
		paramsMap.put(HttpArgs.password, password);
		paramsMap.put(HttpArgs.country, country);
		paramsMap.put(HttpArgs.city, city);

		String responseString = UtilsHttp.performPostCall(SERVER_URL
				+ HttpActions.SIGNUP, paramsMap);

		Log.i(TAG_INFO, "perfomrSignUp() ... retrieving the data from user : "
				+ username);

		Log.i(TAG_INFO, "perfomrSignUp() response String from server : "
				+ responseString);

		Gson gson = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting()
				.serializeNulls().create();
		final UsersServ user = gson.fromJson(responseString,
				new UsersServ().getClass());

		Log.i(TAG_INFO, "performsignUp() -- responseString = " + responseString);

		System.out.println("SignUp  -> userId: " + user);

		if (user != null && user.getName() != null) {
			System.out.println("SignUp  ->returning user: " + user);
			return user;
		} else {
			StatusPackage statusPackage = gson.fromJson(responseString,
					new StatusPackage().getClass());
			System.out.println("SignUp  -> statusPackatge: " + statusPackage);
			return statusPackage;
		}

	}
}

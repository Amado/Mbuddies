package org.escoladeltreball.manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.escoladeltreball.db4o.Db40DAOFavorites;
import org.escoladeltreball.db4o.Db4oHelper;
import org.escoladeltreball.db4o.Favorites;
import org.escoladeltreball.model.json.FavoriteAndSongs;
import org.escoladeltreball.model.json.ListFavorites;
import org.escoladeltreball.model.json.ListFriendRequest;
import org.escoladeltreball.model.json.ListFriendRequest.RequestModel;
import org.escoladeltreball.model.json.ListSongs;
import org.escoladeltreball.model.json.ListUsers;
import org.escoladeltreball.model.json.StatusPackage;
import org.escoladeltreball.model.server.persistence.CancionesServ;
import org.escoladeltreball.model.server.persistence.FavoritesServ;
import org.escoladeltreball.model.server.persistence.UsersServ;
import org.escoladeltreball.support.Config;
import org.escoladeltreball.support.ImageConverter;

import android.app.Activity;
import android.util.Log;

import com.db4o.ObjectContainer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class RequestManager implements Config {

	/**
	 * <pre>
	 * Recupera los datos de un usuario del servidor usando su id como identificador
	 * @param id id del usuario
	 * @return un usuario modelado desde el servidor
	 * 
	 * </pre>
	 */
	public static UsersServ fechUserById(final String id, boolean withSongObject) {

		UsersServ user = null;
		try {
			Map<String, String> paramsMap = new HashMap<String, String>();
			paramsMap.put(HttpArgs.idUser, id);
			if (withSongObject == true) {
				paramsMap.put(HttpArgs.withSongObject, HttpArgs.VALUE_TRUE);
			}

			String responseString = UtilsHttp.performPostCall(SERVER_URL
					+ HttpActions.FIND_USER, paramsMap);

			System.out
					.println("FindUser() ... retrieving the data from user : "
							+ id);

			Log.i(TAG_INFO, "findUser() response String from server : "
					+ responseString);

			Gson gson = new GsonBuilder().create();

			user = gson.fromJson(responseString.toString(),
					new UsersServ().getClass());

			Log.i(TAG_INFO, "findUser() - user fetched from server = " + user);

		} catch (Throwable e) {
			Log.e(TAG_ERROR, "error" + e.toString());
			e.printStackTrace();
		}
		return user;

	}

	public void getAllfavoritesFromUser() {
		ObjectContainer oc = null;
		// final Long userId = 21L;
		ArrayList<Favorites> favsLocal = null;

		try {
			favsLocal = Db40DAOFavorites.getAllFavoritesFromDB(oc);

		} catch (Exception e) {

		}
	}

	public static ArrayList<Favorites> loadPreferedLists(Activity activity,
			Long idUser) {
		Db4oHelper helper = null;
		ObjectContainer oc = null;
		ArrayList<Favorites> favsFromDB = null;
		// cargamos las listas de canciones
		try {
			helper = new Db4oHelper(activity);
			oc = helper.oc;
		} catch (Exception e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}

		try {
			favsFromDB = Db40DAOFavorites.getAllFavoritesFromDB(oc);
			if (favsFromDB == null) {
				favsFromDB = new ArrayList<Favorites>();
				Log.d("RequestManager", "there isn't any fav from user");
			} else {
				Log.d("RequestManager", "there isn't any fav from user");
			}
		} catch (Exception e) {

		}
		// // obtenemos nuestro usuario
		// User owner = null;
		// try {
		// helper = new Db4oHelper(activity);
		// oc = helper.oc;
		// owner = Db4oDAOUser.getUserOwnerFromDB(oc);
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
		// ////////////////
		ArrayList<Favorites> favsFromUser = null;
		for (Favorites favorites : favsFromDB) {
			Log.i("RequestManaguer", "loadPreferedLists favs.idUser ->");
			if (favorites.getIdUser().equals(idUser)) {
				favsFromUser.add(favorites);
			}
		}

		return favsFromUser;

	}

	public static void commitFavorites(final List<FavoritesServ> favsServ,
			String idUser) throws Throwable {
		Log.i("RequestManager", "commitFavorites() ..................... ");
		/**
		 * creamos el objeto FavoriteAndSongs, usando los objetos que hemos
		 * creado
		 */
		ListFavorites favoritesList = new ListFavorites();
		favoritesList.list = favsServ;
		// Debemos especificarle el id del usuario

		final String favoritesAndItsSongs = favoritesList.serializeToJson();

		try {
			Map<String, String> paramsMap = new HashMap<String, String>();

			paramsMap.put(HttpArgs.favoritesList, favoritesAndItsSongs);
			paramsMap.put(HttpArgs.idUser, idUser);

			String responseString = UtilsHttp.performPostCall(SERVER_URL
					+ Config.HttpActions.COMMIT_FAVORITES, paramsMap);

			Log.i("RequestManager",
					"commitFavorites() response String from server : "
							+ responseString);

		} catch (Throwable e) {
			System.out.println("error" + e.toString());
			e.printStackTrace();
			throw e;
		} finally {
		}

	}

	/**
	 * <pre>
	 * Recupera los datos de un usuario del servidor usando su nombre como identificador
	 * 
	 * @param name nombre del usuario
	 * @return un usuario modelado desde el servidor
	 * </pre>
	 */
	public static UsersServ fechUserByName(final String name) {

		UsersServ user = null;
		try {
			Map<String, String> paramsMap = new HashMap<String, String>();
			paramsMap.put(HttpArgs.username, name);

			String responseString = UtilsHttp.performPostCall(SERVER_URL
					+ HttpActions.FIND_USER, paramsMap);

			System.out
					.println("FindUser() ... retrieving the data from user : "
							+ name);

			Log.i(TAG_INFO, "findUser() response String from server : "
					+ responseString);

			Gson gson = new GsonBuilder().create();

			user = gson.fromJson(responseString.toString(),
					new UsersServ().getClass());

			Log.i(TAG_INFO, "findUser() - user fetched from server = " + user);

		} catch (Throwable e) {
			Log.e(Config.TAG_ERROR, "error" + e.toString());
			e.printStackTrace();
		}
		return user;

	}

	/**
	 * 
	 * Actualiza los datos del usuario en el servidor
	 * 
	 * @param user
	 * @return true si se ha actualizado y false o lanzar una Exception si no ha
	 *         podido actualizarse debido a algun error (de conexion con el
	 *         servidor, al realizar la peticion o al realizar la operacion)
	 * @throws Exception
	 */
	public static boolean updateUser(final UsersServ user) throws Exception {
		boolean updatedOk = false;
		Log.i(TAG_INFO, "updating user on server .... " + user);
		try {
			String fotoStr = "";
			if (user.getPhoto() != null) {
				fotoStr = ImageConverter.encodeImage(user.getPhoto());
			}

			Map<String, String> paramsMap = new HashMap<String, String>();
			paramsMap.put(HttpArgs.idUser, String.valueOf(user.getId()));
			paramsMap.put(HttpArgs.username, user.getName());
			paramsMap.put(HttpArgs.password, user.getPassword());

			/**
			 * como no se pueden enviar nulls debemos enviar solo aquellos datos
			 * que no lo sean
			 */
			if (user.getCountry() != null) {
				paramsMap.put(HttpArgs.city, user.getCity());
			}
			if (user.getCountry() != null) {
				paramsMap.put(HttpArgs.country, user.getCountry());
			}
			if (user.getPersonalDescription() != null) {
				paramsMap.put(HttpArgs.personalDescription,
						user.getPersonalDescription());

			}
			if (user.getMusicPreferences() != null) {
				paramsMap.put(HttpArgs.musicPreferences,
						user.getMusicPreferences());
			}

			if (user.getPhoto() != null) {
				paramsMap.put(HttpArgs.photo, fotoStr);
			}

			if (user.getIdImg() != null) {
				paramsMap
						.put(HttpArgs.idPhoto, String.valueOf(user.getIdImg()));
			}
			// paramsMap.put(HttpArgs.photoResource, user.getPhotoResource());

			String responseString = UtilsHttp.performPostCall(SERVER_URL
					+ HttpActions.UPDATE_USER_PROFILE, paramsMap);

			Log.i(TAG_INFO, "updateUser() response String from server : "
					+ responseString);

			Gson gson = new GsonBuilder().create();

			final UsersServ userUpdated = gson.fromJson(
					responseString.toString(), new UsersServ().getClass());

			updatedOk = userUpdated != null && userUpdated.getId() != null;
			if (updatedOk == true) {
				Log.i(TAG_INFO, "updated user on server OK .... " + userUpdated);
			} else {
				throw new Exception();
			}

		} catch (Exception e) {
			Log.e(Config.TAG_ERROR,
					"error updating user on server " + e.toString());
			e.printStackTrace();
			throw e;
		}
		return updatedOk;
	}

	/**
	 * recupera del servidor los amigos de un usuario concreto
	 * 
	 * @return una lista con los amigos del usuario, null si ha habido algun
	 *         error, o una lista vacía si no el usuario no declarado ningun
	 *         amigo
	 */
	public static List<UsersServ> fecthFriendsFromUser(final String idUser)
			throws Exception {
		List<UsersServ> responseUsers = new ArrayList<UsersServ>();
		Log.i(TAG_INFO, "feching friends from on server .... " + idUser);
		try {

			Map<String, String> paramsMap = new HashMap<String, String>();
			paramsMap.put(HttpArgs.idUser, idUser);

			String responseString = UtilsHttp.performPostCall(SERVER_URL
					+ HttpActions.FIND_FRIENDS_FROM_USER, paramsMap);

			Log.i(TAG_INFO, "updateUser() response String from server : "
					+ responseString);

			final Object responseObject = (Object) UtilsHttp
					.parseResponseFromServer(responseString, new ListUsers());

			if (responseObject instanceof ListUsers) {
				ListUsers listUsers = (ListUsers) responseObject;
				responseUsers = listUsers.list;
			} else {
				throw new Exception("Error retrieving the friends From Server");
			}

		} catch (Throwable e) {
			Log.e(Config.TAG_ERROR,
					"error updating user on server " + e.toString());
			e.printStackTrace();
			return null;
		}
		return responseUsers;
	}

	/**
	 * Elimina un amigo de un usuario concreto en el servidor
	 * 
	 * @param idUserSelf
	 * @param idUserFriend
	 * @return una lista de usuarios amigos resultantes despues de la operacion;
	 *         null si ha habido un error y una lista vacia, si no queda ningun
	 *         amigo
	 */
	public static List<UsersServ> removeFriend(String idUserSelf,
			String idUserFriend) throws Exception {
		List<UsersServ> responseUsers = new ArrayList<UsersServ>();
		try {
			Map<String, String> paramsMap = new HashMap<String, String>();
			paramsMap.put(HttpArgs.idUserOwner, idUserSelf);
			paramsMap.put(HttpArgs.idUserFriend, idUserFriend);

			String responseString = UtilsHttp.performPostCall(SERVER_URL
					+ HttpActions.REMOVE_FRIEND, paramsMap);

			Log.i(TAG_INFO, "removeFriend() response String from server : "
					+ responseString);

			final Object responseObject = (Object) UtilsHttp
					.parseResponseFromServer(responseString, new ListUsers());

			if (responseObject instanceof ListUsers) {

				ListUsers listUsers = (ListUsers) responseObject;
				responseUsers = listUsers.list;
			} else {
				throw new Exception("Error removing the user From Server");
			}

		} catch (Throwable e) {
			Log.e(Config.TAG_ERROR, "error" + e.toString());
			e.printStackTrace();
			return null;
		}
		return responseUsers;
	}

	/**
	 * <pre>
	 * recupera del servidor las listas de favoritos que pertenecen a un usuario concreto
	 * @param idUser
	 * @return una lista de favoritos null si ha habido un error y una lista
	 *         vacia, si no existe ningun favorito
	 * </pre>
	 */
	public static List<FavoritesServ> fetchFavoritesFromUser(String idUser)
			throws Exception {
		List<FavoritesServ> responseFavorites = new ArrayList<FavoritesServ>();
		try {
			Map<String, String> paramsMap = new HashMap<String, String>();
			paramsMap.put(HttpArgs.idUser, idUser);

			String responseString = UtilsHttp.performPostCall(SERVER_URL
					+ HttpActions.FIND_FAVORITES_FROM_USER, paramsMap);

			// System.out
			// .println("fetchFavoritesFromUser() response String from server : "
			// + responseString);

			final Object responseObject = (Object) UtilsHttp
					.parseResponseFromServer(responseString,
							new ListFavorites());

			if (responseObject instanceof ListFavorites) {
				ListFavorites listUsers = (ListFavorites) responseObject;
				responseFavorites = listUsers.list;
			} else {
				throw new Exception(
						"Error retrieving the favorites from server");
			}

		} catch (Throwable e) {
			Log.e(Config.TAG_ERROR, "error" + e.toString());
			e.printStackTrace();
			return null;
		}

		return responseFavorites;

	}

	/**
	 * <pre>
	 * recupera del servidor las canciones de una lista de favoritos perteneciente a un usuario concreto
	 * @param idUser id del usuario al que pertenece la lista
	 * @return una lista de favoritos, null si ha habido un error y una lista
	 *         vacia si no existe ningun favorito
	 * </pre>
	 */
	public static List<CancionesServ> fetchSongsFromUserAndFavList(
			final String idUser, final String favoritesIdentification)
			throws Exception {
		List<CancionesServ> responseFavorites = new ArrayList<CancionesServ>();
		try {
			Map<String, String> paramsMap = new HashMap<String, String>();
			if (favoritesIdentification != null) {
				paramsMap.put(HttpArgs.favoritesIdentification,
						favoritesIdentification);
			}
			if (idUser != null) {
				paramsMap.put(HttpArgs.idUser, idUser);
			}
			String responseString = UtilsHttp.performPostCall(SERVER_URL
					+ HttpActions.FIND_SONGS, paramsMap);

			final Object responseObject = (Object) UtilsHttp
					.parseResponseFromServer(responseString, new ListSongs());

			if (responseObject instanceof ListSongs) {
				ListSongs listUsers = (ListSongs) responseObject;
				responseFavorites = listUsers.list;
			} else {
				throw new Exception("Error retrieving the songs from server");
			}

		} catch (Throwable e) {
			Log.e(Config.TAG_ERROR, "error" + e.toString());
			e.printStackTrace();
			return null;
		}

		return responseFavorites;

	}

	/**
	 * <pre>
	 * Agrega una cancion a una lista de favoritos del servidor, que pertenece a
	 * un usuario concreto
	 * 
	 * @param favoritesIdentification identificador de la lista de favoritos (formato   'idUser':'nombreLista''x')
	 *                  donde x puede ser un timestamp un long que representa la fecha del sistema en milisegundos
	 *                  ejemplo:  51:lista favoritos 1:1431812305259
	 *            
	 * @param favoritesName nombre de la lista de favoritos
	 * @param idUser id del usuario al que pertenece la lista
	 * @param songImgEnconded la imagen comprimida y codificada como una cadena de texto
	 * @param songSrcPath ruta de la cancion en el sistema android
	 * @param songName el nombre de la cancion
	 * @param songAuthor el autor de la cancion (admite null)
	 * @return true si la operacion se realiza correctamente en el servidor,
	 *         false en caso contrario
	 * 
	 * </pre>
	 */
	public static boolean addSongToFavorites(
			final String favoritesIdentification, final String favoritesName,
			final String idUser, final String songImgEnconded,
			final String songSrcPath, final String songName,
			final String songAuthor) throws Exception {

		boolean processOk = false;
		try {
			Log.i(TAG_INFO,
					" ******* addSongToFavorites() favorites  = favoritesName: "
							+ favoritesName + ", favoritesIdentification = "
							+ favoritesIdentification + " *******");

			Map<String, String> paramsMap = new HashMap<String, String>();

			paramsMap.put(HttpArgs.favoritesIdentification,
					favoritesIdentification);
			paramsMap.put(HttpArgs.idUser, idUser);
			paramsMap.put(HttpArgs.favoritesName, favoritesName);
			paramsMap.put(HttpArgs.songSrcPath, songSrcPath);
			paramsMap.put(HttpArgs.songPhotoEncoded,
					songImgEnconded == null ? "" : songImgEnconded);
			paramsMap.put(HttpArgs.songName, songName);
			// nulls aren't allowed
			paramsMap.put(HttpArgs.songAuthor, songAuthor == null ? ""
					: songAuthor);

			String responseString = UtilsHttp.performPostCall(SERVER_URL
					+ HttpActions.ADD_SONG_TO_FAVORITE, paramsMap);

			System.out
					.println("testUpdateSong() response String from server : "
							+ responseString);

			Gson gson = new GsonBuilder().create();

			final StatusPackage statusPackage = gson.fromJson(
					responseString.toString(), new StatusPackage().getClass());

			// comprobamos si la operacion se ha realizado correctamente
			// consultando la respuesta del servidor
			processOk = statusPackage != null
					&& statusPackage.getType() != null;
			if (statusPackage != null
					&& processOk == StatusPackage.STATUS_SUCCESS
							.equals(statusPackage.getType())) {
				Log.i(TAG_INFO, "add song to favorite "
						+ favoritesIdentification + " OK .... " + processOk);
			}

			System.out
					.println("testAddSongToFavorites() - song fetched from server = "
							+ statusPackage);

		} catch (Throwable e) {
			Log.e(Config.TAG_ERROR, "error" + e.toString());
			e.printStackTrace();
		}
		return processOk;
	}

	/**
	 * <pre>
	 * elimina una cancion de la lista de favoritos en el servidor, que
	 * pertenece a un usuario concreto
	 * 
	 * @param favoritesIdentification identificador de la lista de favoritos
	 * @param songSrcPath ruta de la cancion en el sistema android
	 * @param idUser id del usuario al que pertenece la lista
	 * @param favoritesName nombre de la lista de favoritos
	 * @return true si la operacion se realiza correctamente en el servidor,
	 *         false en caso contrario
	 * </pre>
	 */
	public static boolean removeSongFromFavorites(
			final String favoritesIdentification, final String songSrcPath,
			final String idUser, final String favoritesName) {
		boolean processOk = false;
		try {
			Map<String, String> paramsMap = new HashMap<String, String>();

			paramsMap.put(HttpArgs.favoritesIdentification,
					favoritesIdentification);
			// paramsMap.put(HttpArgs.favoritesName, favoritesName);
			paramsMap.put(HttpArgs.songSrcPath, songSrcPath);
			paramsMap.put(HttpArgs.idUser, idUser);
			paramsMap.put(HttpArgs.favoritesName, favoritesName);
			String responseString = UtilsHttp.performPostCall(Config.SERVER_URL
					+ HttpActions.REMOVE_SONG_FROM_FAVORITE, paramsMap);

			Gson gson = new GsonBuilder().create();

			final StatusPackage statusPackage = gson.fromJson(
					responseString.toString(), new StatusPackage().getClass());

			// comprobamos si la operacion se ha realizado correctamente
			// consultando la respuesta del servidor
			processOk = statusPackage != null
					&& statusPackage.getType() != null;
			if (processOk == StatusPackage.STATUS_SUCCESS.equals(statusPackage
					.getType())) {
				Log.i(TAG_INFO, "remove song from favorites "
						+ favoritesIdentification + " OK .... " + processOk);
			}

		} catch (Throwable e) {
			Log.e(Config.TAG_ERROR, "error" + e.toString());
			e.printStackTrace();
		}
		return processOk;

	}

	/**
	 * <pre>
	 * Agrega una lista de favoritos en el servidor
	 * 
	 * @param favoritesIdentification identificador de la lista de favoritos
	 * @param favoritesName nombre de la lista de favoritos
	 * @param idUser id del usuario al que pertenece la lista
	 * @return true si la operacion se realiza correctamente en el servidor,
	 *         false en caso contrario
	 * </pre>
	 */
	public static boolean addOrUpdateFavorite(
			final String favoritesIdentification, final String favoritesName,
			final String idUser) {

		boolean processOk = false;
		/*
		 * IMPORTANTE: favoritesIdentification sirve como identificador por lo
		 * que no se puede repetir nunca formato: idusuario:xxxxxxxxxxxxxxxxxxx
		 * 
		 * eg: "21:" + System.currentTimeMillis()
		 */
		try {
			Map<String, String> paramsMap = new HashMap<String, String>();
			paramsMap.put(HttpArgs.idUser, idUser);
			paramsMap.put(HttpArgs.favoritesNewName, favoritesName);
			paramsMap.put(HttpArgs.favoritesIdentification,
					favoritesIdentification);
			String responseString = UtilsHttp.performPostCall(SERVER_URL
					+ HttpActions.ADD_OR_UPDATE_FAVORITE, paramsMap);

			System.out
					.println("addOrUpdateFavorite() response String from server : "
							+ responseString);

			Gson gson = new GsonBuilder().create();

			final StatusPackage statusPackage = gson.fromJson(
					responseString.toString(), new StatusPackage().getClass());

			// comprobamos si la operacion se ha realizado correctamente
			// consultando la respuesta del servidor
			processOk = statusPackage != null
					&& statusPackage.getType() != null;
			if (processOk == StatusPackage.STATUS_SUCCESS.equals(statusPackage
					.getType())) {
				Log.i(TAG_INFO, "addOrUpdateFavorite  "
						+ favoritesIdentification + " OK .... " + processOk);
			}

		} catch (Throwable e) {
			Log.e(Config.TAG_ERROR,
					"addOrUpdateFavorite - ERROR" + e.toString());
			e.printStackTrace();
		}

		return processOk;
	}

	/**
	 * <pre>
	 * actualiza, en el servidor, la cancion que esta escuchando el usuario
	 * 
	 * @param srcSongPath
	 * @param songName nombre de la lista de favoritos
	 * @param songAuthor el autor de la cancion (admite null)
	 * @param idUser id del usuario al que pertenece la lista
	 * @return true si la operacion se realiza correctamente en el servidor,
	 *         false en caso contrario
	 * </pre>
	 */
	public static boolean updateCurrentPlaySong(final String srcSongPath,
			final String songName, final String songAuthor, final String idUser) {

		boolean processOk = false;

		try {
			Map<String, String> paramsMap = new HashMap<String, String>();
			// la src de la nueva cancion
			paramsMap.put(HttpArgs.idUser, idUser);
			if (srcSongPath != null) {
				paramsMap.put(HttpArgs.songSrcPath, srcSongPath);
			}
			if (songName != null) {
				paramsMap.put(HttpArgs.songName, songName);
			}
			if (songAuthor != null) {
				paramsMap.put(HttpArgs.songAuthor, songAuthor);
			}

			String responseString = UtilsHttp.performPostCall(SERVER_URL
					+ HttpActions.UPDATE_CURRENT_PLAY_SONG, paramsMap);
			System.out
					.println("testUpdateUpdateCurrentPlaySong() response String from server : "
							+ responseString);

			Gson gson = new GsonBuilder().create();

			final CancionesServ songFrServer = gson.fromJson(
					responseString.toString(), new CancionesServ().getClass());

			// comprobamos si la operacion se ha realizado correctamente
			// consultando la respuesta del servidor
			processOk = songFrServer != null
					&& songFrServer.getSongName() != null;
			if (processOk) {
				Log.i(TAG_INFO, "updateUpdateCurrentPlaySong  OK .... "
						+ processOk);
			}

		} catch (Throwable e) {
			System.out.println("error" + e.toString());
			e.printStackTrace();
		}
		return processOk;

	}

	/**
	 * <pre>
	 * elimina una lista de favoritos en el servidor, incluyendo sus canciones (si quedan huerfanas)
	 * @param favoritesIdentification
	 * @return true si la operacion se realiza correctamente en el servidor,
	 *         false en caso contrario
	 * </pre>
	 */
	public static boolean removeFavorite(final String favoritesIdentification) {
		boolean processOk = false;
		try {
			Map<String, String> paramsMap = new HashMap<String, String>();

			paramsMap.put(HttpArgs.favoritesIdentification,
					favoritesIdentification);

			String responseString = UtilsHttp.performPostCall(SERVER_URL
					+ HttpActions.REMOVE_FAVORITE, paramsMap);

			System.out
					.println("removeFavorite() response String from server : "
							+ responseString);

			Gson gson = new GsonBuilder().create();

			final StatusPackage statusPackage = gson.fromJson(
					responseString.toString(), new StatusPackage().getClass());

			// comprobamos si la operacion se ha realizado correctamente
			// consultando la respuesta del servidor
			processOk = statusPackage != null
					&& statusPackage.getType() != null;
			if (processOk == StatusPackage.STATUS_SUCCESS.equals(statusPackage
					.getType())) {
				Log.i(TAG_INFO, "remove favorite list "
						+ favoritesIdentification + " OK .... " + processOk);
			}

		} catch (Throwable e) {
			Log.e(Config.TAG_ERROR, "error" + e.toString());
			e.printStackTrace();
		}
		return processOk;
	}

	/**
	 * <pre>
	 * Actualiza una lista de favoritos en el servidor, incluyendo todas sus canciones usado,
	 * por ejemplo, cuando una lista por algun motivo deja de estar sincronizada
	 * con el servidor
	 * 
	 * @param favoriteServ la lista de favoritos
	 * @param userId id del usuario al que pertenece la lista
	 * 
	 * </pre>
	 */
	public static boolean updateFavoritesAndItsSongs(
			final FavoritesServ favoriteServ, final Long userId) {
		boolean processOk = false;

		// creamos el objeto FavoriteAndSongs, usando los objetos
		// pasados por parametro
		FavoriteAndSongs favAndSongs = new FavoriteAndSongs();
		favAndSongs.favorites = favoriteServ;
		// Debemos especificarle el id del usuario
		favAndSongs.idUser = userId;
		favAndSongs.songs = new ArrayList<CancionesServ>(
				favoriteServ.getCanciones());

		final String favoritesAndItsSongs = favAndSongs.serializeToJson();

		try {
			Map<String, String> paramsMap = new HashMap<String, String>();

			paramsMap.put(HttpArgs.favoriteAndItsSongs, favoritesAndItsSongs);

			String responseString = UtilsHttp.performPostCall(SERVER_URL
					+ HttpActions.UPDATE_FAVORITE_AND_ITS_SONGS, paramsMap);

			System.out
					.println("testupdateFavoritesAndItsSongs() response String from server : "
							+ responseString);

			Gson gson = new GsonBuilder().create();

			final StatusPackage statusPackage = gson.fromJson(
					responseString.toString(), new StatusPackage().getClass());

			// comprobamos si la operacion se ha realizado correctamente
			// consultando la respuesta del servidor
			processOk = statusPackage != null
					&& statusPackage.getType() != null;
			if (processOk == StatusPackage.STATUS_SUCCESS.equals(statusPackage
					.getType())) {
				Log.i(TAG_INFO,
						"remove favorite list "
								+ favoriteServ.getIdentification()
								+ " OK .... " + processOk);
			}

		} catch (Throwable e) {
			System.out.println("error" + e.toString());
			e.printStackTrace();
		}

		return processOk;
	}

	/**
	 * recupera del servidor las peticiones de amistad para un usuario concreto
	 * 
	 * @return una lista de peticiones de amistad modeladas con RequestModel,
	 *         null si hay algun error y una lista vacia si no existe ninguna
	 *         peticion
	 */
	public static List<RequestModel> fecthFriendsRequest(final String idUser)
			throws Exception {
		List<RequestModel> listRequestModel = new ArrayList<RequestModel>();
		Log.i(TAG_INFO, "feching friends request from on server .... " + idUser);
		try {

			Map<String, String> paramsMap = new HashMap<String, String>();
			paramsMap.put(HttpArgs.idUser, idUser);

			String responseString = UtilsHttp.performPostCall(SERVER_URL
					+ HttpActions.FIND_FRIENDS_REQUEST, paramsMap);

			Log.i(TAG_INFO, "updateUser() response String from server : "
					+ responseString);

			final Object responseObject = (Object) UtilsHttp
					.parseResponseFromServer(responseString,
							new ListFriendRequest());

			if (responseObject instanceof ListFriendRequest) {
				ListFriendRequest listFriendRequest = (ListFriendRequest) responseObject;
				listRequestModel = listFriendRequest.list;
			} else {
				throw new Exception();
			}

		} catch (Throwable e) {
			Log.e(Config.TAG_ERROR,
					"error feching friends request from on server "
							+ e.toString());
			e.printStackTrace();
			return null;
		}
		return listRequestModel;
	}

	public static boolean addFriend(final String idUserSelf,
			final String idUserFriend) throws Exception {

		boolean processOk = false;

		try {
			Map<String, String> paramsMap = new HashMap<String, String>();
			paramsMap.put(HttpArgs.idUserOwner, idUserSelf);
			paramsMap.put(HttpArgs.idUserFriend, idUserFriend);

			String responseString = UtilsHttp.performPostCall(SERVER_URL
					+ Config.HttpActions.ADD_FRIEND, paramsMap);

			System.out
					.println("testAddFriend() ... retrieving the data from user : "
							+ idUserSelf);

			System.out.println("testAddFriend() response String from server : "
					+ responseString);

			Gson gson = new GsonBuilder().create();

			final StatusPackage statusPackage = gson.fromJson(
					responseString.toString(), new StatusPackage().getClass());

			// comprobamos si la operacion se ha realizado correctamente
			// consultando la respuesta del servidor
			processOk = statusPackage != null
					&& statusPackage.getType() != null;
			if (processOk == StatusPackage.STATUS_SUCCESS.equals(statusPackage
					.getType())) {
				Log.i(TAG_INFO, "ADD FRIEND OK ");
			} else {
				throw new Exception();
			}

		} catch (Throwable e) {
			System.out.println("error" + e.toString());
			e.printStackTrace();
		}

		return processOk;

	}

	public static boolean dismisFriendRequest(final String idUserSelf,
			final String idUserFriend) throws Exception {

		boolean processOk = false;

		try {
			Map<String, String> paramsMap = new HashMap<String, String>();
			paramsMap.put(HttpArgs.idUserOwner, idUserSelf);
			paramsMap.put(HttpArgs.idUserFriend, idUserFriend);

			String responseString = UtilsHttp.performPostCall(SERVER_URL
					+ Config.HttpActions.DISMIS_FRIEND_REQUEST, paramsMap);

			System.out
					.println("testAddFriend() ... retrieving the data from user : "
							+ idUserSelf);

			System.out.println("testAddFriend() response String from server : "
					+ responseString);

			Gson gson = new GsonBuilder().create();

			final StatusPackage statusPackage = gson.fromJson(
					responseString.toString(), new StatusPackage().getClass());

			// comprobamos si la operacion se ha realizado correctamente
			// consultando la respuesta del servidor
			processOk = statusPackage != null
					&& statusPackage.getType() != null;
			if (processOk == StatusPackage.STATUS_SUCCESS.equals(statusPackage
					.getType())) {
				Log.i(TAG_INFO, "DISMIS FRIEND REQUEST OK ");
			} else {
				throw new Exception();
			}

		} catch (Throwable e) {
			System.out.println("error" + e.toString());
			e.printStackTrace();
		}

		return processOk;

	}

}

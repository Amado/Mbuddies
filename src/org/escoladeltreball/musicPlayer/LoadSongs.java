package org.escoladeltreball.musicPlayer;

import java.util.ArrayList;

import org.escoladeltreball.chat.R;
import org.escoladeltreball.preferedList.ChoseSongToList;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore.Audio;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public class LoadSongs extends AsyncTask<Object, Void, Bitmap>{

    private ImageView imv;
    private String path;
    
    Activity activity;
    Cursor musiccursor;
    MediaMetadataRetriever mmr = new MediaMetadataRetriever();
    
    ArrayList<String> titlesList = new ArrayList<String>(); // array con los titulos de las canciones
	ArrayList<Bitmap> musicImageList = new ArrayList<Bitmap>(); // array con las imagenes de las canciones
	ArrayList<String> dataMusic = new ArrayList<String>(); // array con los path de las canciones
    
	
	int classToLoad = 0;
	
	Bitmap icon; 	
	
    public LoadSongs(Activity activity, int classToLoad) {
         this.activity = activity;
         this.classToLoad = classToLoad;
    }

@Override
protected Bitmap doInBackground(Object... params) {
    Bitmap bitmap = null;

 icon = BitmapFactory.decodeResource(activity.getResources(),
            R.drawable.playerbackgroundicon);
 	Bitmap bm = null;   
 
    try {
		ContentResolver contentResolver = activity.getContentResolver();
		Uri uri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
		// cursor del cual podremos obtener el path de la cancion, el titulo
		// , y las imagenes
		musiccursor = contentResolver.query(uri, null, null, null, null);
		// definimos el tamaño de los array sabiendo cuantas canciones
		// tenemos
		if (musiccursor == null) {
			// query failed, handle error.
		} else if (!musiccursor.moveToFirst()) {
			// no media on the device
//			Toast.makeText(this, "No sdcard presents", Toast.LENGTH_SHORT)
//					.show();
			Log.d("songs", "no hay sdcard");
		} else {
			// obtenemos el indice de la columna en la que se guarda el
			// titulo de la cancion dentro del cursor
			int titleColumn = musiccursor
					.getColumnIndex(android.provider.MediaStore.Audio.Media.DISPLAY_NAME);
			// obtenemos el indice de la columna en la que se guarda el path
			// de la cancion dentro del cursor
			int data = musiccursor.getColumnIndex(Audio.Media.DATA);

			// recoremos de todas las filas del cursor
			for (int i = 0; i < musiccursor.getCount(); i++) {
				// obtenoms el titulo de la cancion de la columna de titulos
				// del cursor y lo añadimos al array correspondiente
				titlesList.add(musiccursor.getString(titleColumn));
				// obtenoms el path de la cancion de la columna de paths del
				// cursor y lo añadimos al array correspondiente
				dataMusic.add(musiccursor.getString(data));

				// le pasamos al objeto que nos permitira obtener las
				// imagenes el path de la cancions
				mmr.setDataSource(musiccursor.getString(data));
				// obtenemos la imagen
				byte[] artBytes = mmr.getEmbeddedPicture();
				// si tiene imagen la añadimos al array corespondiente
				if (artBytes != null) {
					 //pasamos la imagen de bytes a bitmap					
								
					//bm = BitmapFactory.decodeByteArray(artBytes, 0,
						//	artBytes.length);
					
					
					
					BitmapFactory.Options options = new BitmapFactory.Options();
					options.inSampleSize = 8;
					bm = BitmapFactory.decodeByteArray(artBytes, 0,
								artBytes.length,options);
					
					
					
					musicImageList.add(bm);					
				}else{
					//por el contrario añadimos una imagen predeterminada										
					musicImageList.add(icon);						
				}
				// nos movemos a la siguiente fila del cursor
				musiccursor.moveToNext();
			}
			
			Log.d("myApp", "grid");
		}
	} catch (Exception e) {
		System.out.println("No sdcard presents");
		Log.d("myApp", "grid error");
	}

    if(classToLoad == 0){ //the first time tha app start
    	MusicPlayerActivity.dataMusic = dataMusic;
    	MusicPlayerActivity.titlesList = titlesList;
    	MusicPlayerActivity.musicImageList = musicImageList;
    }else if (classToLoad == 1){//when th user chose the button the see the songs from the player
    	AudioList.dataMusic = dataMusic;
        AudioList.titlesList = titlesList;
        AudioList.musicImageList = musicImageList;	
    }else{
    	ChoseSongToList.dataMusic = dataMusic;
    	ChoseSongToList.titlesList = titlesList;
    	ChoseSongToList.musicImageList = musicImageList;
    }
    

    return bitmap;
}
@Override
protected void onPostExecute(Bitmap result) {
    if (!imv.getTag().toString().equals(path)) {
           /* The path is not same. This means that this
              image view is handled by some other async task. 
              We don't do anything and return. */
           return;
    }

    if(result != null && imv != null){
        imv.setVisibility(View.VISIBLE);
        imv.setImageBitmap(result);
    }else{
        imv.setVisibility(View.GONE);
    }
}

}
package org.escoladeltreball.musicPlayer;

import java.util.ArrayList;

import org.escoladeltreball.chat.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 
 *Classe que permite customisar un list view mostrando el nombre de las canciones y sus imagenes 
 * 
 * @author Miquel Graells Monreal
 *
 */
public class MusicAdapter  extends BaseAdapter{

	ArrayList<String> songsTitles;
	ArrayList<Bitmap> songsImages;
	LayoutInflater layoutInflater;
	
	/**
	 * 
	 * @param context contexto para inflar el layout
	 * @param song arrayList con los titulos de las canciones
	 * @param songsImages arrayList con las imagenes de las canciones
	 */
	public MusicAdapter(Context context, ArrayList<String> song, ArrayList<Bitmap> songsImages) {
		this.songsTitles = song;
		this.songsImages = songsImages;
		layoutInflater = LayoutInflater.from(context);
	}
	
	@Override	
	public int getCount() {
		return songsTitles.size();
	}

	@Override
	public Object getItem(int position) {
		return songsTitles.get(position);
	}

	@Override
	public long getItemId(int position) { 
		return 0;
	}

	// Si utilitzem el patró ViewHolder, el primer que farem és crear el model,
	// classe, per als objectes
	// que representen
	static class ViewHolder {
		TextView nameSong;
		ImageView imageSong;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if (convertView == null) {
			// Inflem el Layout de cada item
			convertView = layoutInflater.inflate(R.layout.item, null);
			holder = new ViewHolder();
			// Capturem els TextView
			holder.nameSong = (TextView) convertView
					.findViewById(R.id.strangeWord);
			holder.imageSong = (ImageView) convertView
					.findViewById(R.id.imageArtist);			
			// Associem el viewholder,
			// la informació de l'estructura que hi ha d'haver dins el layout,
			// amb la vista que haurà de retornar aquest mètode.
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.nameSong.setText("" + songsTitles.get(position));
		
		holder.imageSong.setImageBitmap(songsImages.get(position));
		return convertView;
	}

}
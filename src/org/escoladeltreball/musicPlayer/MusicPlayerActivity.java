package org.escoladeltreball.musicPlayer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.escoladeltreball.activities.ActionActivity;
import org.escoladeltreball.activities.ChatActivity;
import org.escoladeltreball.activities.LoginActivity;
import org.escoladeltreball.activities.SearchUsersActivity;
import org.escoladeltreball.chat.R;
import org.escoladeltreball.db4o.Db4oDAOUser;
import org.escoladeltreball.db4o.Db4oHelper;
import org.escoladeltreball.db4o.User;
import org.escoladeltreball.preferedList.FavoritePlayList;
import org.escoladeltreball.profileClasses.DisplayAutentificationDialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.db4o.ObjectContainer;

/**
 * 
 * Acivity que muestra el reproductor y gestiona los controles de los botones, y
 * permita la reproduccion de canciones
 * 
 * @author Miquel Graells Monreal
 * 
 */
@SuppressLint("NewApi")
public class MusicPlayerActivity extends Activity implements OnClickListener,
		OnCompletionListener {

	// private boolean goOnUpdateSongTime = true;

	public TextView songName, startTimeField, endTimeField; // etiquetas
	private static MediaPlayer mediaPlayer;
	private double startTime = 0; // variable en la que guardaremos el segundo
									// actual de reproduccion
	private double finalTime = 0; // variable en la que guardaremos la durada de
									// la cancion
	private Handler myHandler = new Handler();
	private SeekBar seekbar; // barra de progreso
	private ImageButton playButton, pauseButton, browseButton, nextButton,
			beforeButton; // botones de
	// reproduccio,
	// pause, next,
	// befor
	private ImageView image; // imageview en la que se muestra la imagen de la
								// cancion si tienen o una por defecto si no
								// tiene
	public static int oneTimeOnly = 0;
	public static ArrayList<String> titlesList = new ArrayList<String>(); // lista
	// con
	// todos los
	// titulos
	// de las
	// canciones
	public static ArrayList<Bitmap> musicImageList = new ArrayList<Bitmap>(); //
	// lista
	// con
	// todas
	// las
	// imagenes
	// de
	// las
	// canciones
	public static ArrayList<String> dataMusic = new ArrayList<String>(); // lista
	// con
	// // todos los
	// // paths de
	// // las
	// canciones
	static int posicionLlistaReproductor = 0; // posicion en la que empieza la
												// lista de reproduccion
	int posicionLlistaReproductorAux = -1;
	private static boolean comesFromBacspace = false;
	// moves the music cursos and mr to local method
	private UpdateSongTime updateSongTime;

	ObjectContainer oc;
	Db4oHelper dbHelper;
	User emilio;
	User owner;
	private static boolean isOnLaunch = true;
	private static boolean comesFromLogin;
	private static boolean comesFromNewSong = false;

	// public static Object musicImgListLock = new Object();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (isOnLaunch == true) {
			isOnLaunch = false;
			Intent intent = new Intent(this, LoginActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			this.finish();
		}

		// establish the screen orientation before the content view is loaded
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		setContentView(R.layout.activity_main);

		if (mediaPlayer != null) {
			Log.d("myapp", "mediaplayer creado");
		} else {
			Log.d("myapp", "mediaplayer nullo");
		}

		// creamos el usuario propietario
		try {
			dbHelper = new Db4oHelper(this);
			oc = dbHelper.oc;

			User owner = Db4oDAOUser.getUserOwnerFromDB(oc);
			if (owner == null) {
				emilio = new User("emilio", "emilioxxx", "Barcelona", "España",
						"Soy guay", "Me gusta el hard rock", 0, true);

				/*
				 * from chat emilio = new User(LoginActivity.userName ,
				 * "emilioxxx", "Barcelona", "España", "Soy guay",
				 * "Me gusta el hard rock", 0, true);
				 */

				Db4oDAOUser.insertUserToDB(oc, emilio);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}

		/*
		 * from chat icon = BitmapFactory.decodeResource(this.getResources(),
		 * R.drawable.playerbackgroundicon);
		 */

		/**
		 * Initzialices the songs on other thread to avoid overloading the main
		 * thread
		 */

		LoadSongs los = new LoadSongs(this, 0);
		los.doInBackground(null);

		// new Thread() {
		// @Override
		// public void run() {
		// init_phone_music_grid();
		//
		// }
		// }.start();

		// cargamos todas las views
		image = (ImageView) findViewById(R.id.imageView1);
		songName = (TextView) findViewById(R.id.textView4);
		startTimeField = (TextView) findViewById(R.id.textView1);
		endTimeField = (TextView) findViewById(R.id.textView2);
		seekbar = (SeekBar) findViewById(R.id.seekBar1);
		playButton = (ImageButton) findViewById(R.id.btnPlay);
		pauseButton = (ImageButton) findViewById(R.id.btnPause);
		browseButton = (ImageButton) findViewById(R.id.btnBrowse);
		nextButton = (ImageButton) findViewById(R.id.btnBefore);
		beforeButton = (ImageButton) findViewById(R.id.btnNext);
		browseButton.setOnClickListener(this);

		// la primera vez que se inicia el reproductor tenemos los botones
		// desabilitados ya que aun no se a seleccionado ninguna cancion
		// playButton.setEnabled(false);
		pauseButton.setEnabled(false);
		beforeButton.setEnabled(false);
		nextButton.setEnabled(false);

		if (!(getDataMusic().size() > 0)) {
			playButton.setEnabled(true);
		}

		// definimos el listener de la seekbar
		seekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			int progress = 0;

			/**
			 * si la barra de progreso avansa porque la mueve el usuario tambien
			 * avansamos la cancion
			 */
			@Override
			public void onProgressChanged(SeekBar seekBar, int progresValue,
					boolean fromUser) {
				if (mediaPlayer != null && fromUser) {
					mediaPlayer.seekTo(progresValue);
				}
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}
		});

		try {
			mediaPlayer = new MediaPlayer();
			if (dataMusic.size() > 0) { // puede que al cargar la app no haya
										// canciones en el dispositivo
				mediaPlayer.setDataSource(dataMusic
						.get(posicionLlistaReproductor));

				mediaPlayer.prepare();
			} else {
				playButton.setEnabled(false);
			}
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Log.d("myApp", String.valueOf(getTitlesList().size()));

		/**
		 *  
		 * 
		 */
		try {

			if (mediaPlayer.isPlaying()) {
				this.pause(null);

			}
			// if (comesFromLogin == true) {
			// }
		} catch (Exception e) {
			// do nothing
			e.printStackTrace();
		}
	}

	/**
	 * inflamos el menu
	 * 
	 * @param menu
	 * @return
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.mainmenu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.favoritePlaylist:
			// lansamos la activiti en la que se muestran las listas de
			// canciones favoritas
			Intent intent = new Intent(this, FavoritePlayList.class);
			this.startActivity(intent);

			return true;

		case R.id.chat:
			Intent chatAct = new Intent(this, ChatActivity.class);
			ChatActivity.setAutoClose(false);
			ChatActivity.setNextAutoCloseValue(false);
			this.startActivity(chatAct);

			return true;

		case R.id.searchUsers:
			Intent searchUsersAc = new Intent(this, SearchUsersActivity.class);
			this.startActivity(searchUsersAc);
			return true;

		case R.id.settings:
			DisplayAutentificationDialog check = new DisplayAutentificationDialog(
					this);

			return true;

		case R.id.about:
			displayCCLicense();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Muestra la licencia Creative Commons durante 5 segundos al iniciarse la
	 * app, transcurridos los 5 segundos, la app, prosigue con el login. La
	 * licencia también puede ocultarse simplemente tocando la pantalla si no se
	 * quisiera esperar los cinco segundos.
	 */
	public void displayCCLicense() {

		AlertDialog.Builder licenseDialog = new AlertDialog.Builder(this);

		Button btLicense = new Button(this);
		btLicense.setBackgroundColor(Color.rgb(250, 105, 0));
		btLicense.setTextColor(Color.WHITE);
		btLicense.setTextSize(20);
		btLicense
				.setText("Mbuddies v0.1\n\n"
						+ "All icons and avatars are Creative Commons licensed, feel free to "
						+ "use them for personal or commercial purposes. In exchange, it's "
						+ "necessary to credit the author for the original creation:\n\"made by Freepik.com\".\n\n"
						+ "More details about Creative Commons License:\nhttp://creativecommons.org/licenses/by/3.0/");

		licenseDialog.setView(btLicense);

		licenseDialog.show();

	}

	/**
	 * 
	 */
	@Override
	protected void onRestart() {
		super.onRestart();

		if (comesFromNewSong == false) {
			return;
		} else {
			comesFromNewSong = false;
		}

		if (dataMusic.size() > 0) {
			// al volver de la listview tenemos que abilitar los botones
			// si hemos seleccionado alguna cancion
			playButton.setEnabled(true);
			beforeButton.setEnabled(true);
			nextButton.setEnabled(true);
		}

		// si la cancion ya estuviese sonando (por lo tanto quiere decir que
		// vendria del background)
		// no haria falta
		// if (!comesFromBacspace) {
		Log.d("musicFrinds", "canvi de canco");
		try {
			Log.d("debugandoporlaapp", dataMusic.get(posicionLlistaReproductor));
			if (mediaPlayer != null) {
				try {
					/**
					 * fix ----------------------------------------
					 */
					mediaPlayer.reset();
					mediaPlayer.stop();
				} catch (Exception e) {
					// nothing to do
				}
				mediaPlayer.release();
				mediaPlayer = null;
			}

			// ///////////////////////7
			if (mediaPlayer == null) {
				mediaPlayer = new MediaPlayer();

			}
			mediaPlayer.reset(); // ///////////
			// mediaPlayer.stop(); // //////////////////////
			mediaPlayer.setOnCompletionListener(this);
			mediaPlayer.setDataSource(getDataMusic().get(
					posicionLlistaReproductor));
			mediaPlayer.prepare();
			play(null);

		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		// } else {
		// Log.d("musicFrinds", "la mateixa canco");
		// }

	}

	@Override
	protected void onResume() {
		playButton.setEnabled(!comesFromLogin);
		comesFromLogin = false;
		super.onResume();
	}

	/**
	 * obtenemos cuanto tiempo dura la cancion y la reproducimos
	 * 
	 * @param view
	 *            view que hace la accion
	 */
	// synchronized public void play(View view) {
	synchronized public void play(View view) {
		// try {
		// mediaPlayer.reset();
		//
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
		pauseButton.setEnabled(true);
		beforeButton.setEnabled(true);
		nextButton.setEnabled(true);

		Bitmap icon = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.playerbackgroundicon);

		// synchronized (musicImgListLock) {

		if (getMusicImageList().size() > posicionLlistaReproductor
				&& getMusicImageList().get(posicionLlistaReproductor) == null
				|| getMusicImageList().get(posicionLlistaReproductor).sameAs(
						icon)) {
			image.setImageDrawable((getResources()
					.getDrawable(R.drawable.playerbackground)));
			Log.d("myappImage", "app image");
		} else {
			image.setImageBitmap(getMusicImageList().get(
					posicionLlistaReproductor));
			Log.d("myappImage", "disc image");
		}
		// }

//		Toast.makeText(getApplicationContext(), "Playing sound",
//				Toast.LENGTH_SHORT).show();
		mediaPlayer.start();
		finalTime = mediaPlayer.getDuration();
		startTime = mediaPlayer.getCurrentPosition();
		// if (oneTimeOnly == 0) {
		seekbar.setMax((int) finalTime);
		// oneTimeOnly = 1;
		// }

		endTimeField.setText(String.format(
				"%d : %d ",
				TimeUnit.MILLISECONDS.toMinutes((long) finalTime),
				TimeUnit.MILLISECONDS.toSeconds((long) finalTime)
						- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
								.toMinutes((long) finalTime))));
		startTimeField.setText(String.format(
				"%d : %d ",
				TimeUnit.MILLISECONDS.toMinutes((long) startTime),
				TimeUnit.MILLISECONDS.toSeconds((long) startTime)
						- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
								.toMinutes((long) startTime))));
		seekbar.setProgress((int) startTime);
		updateSongTime = new UpdateSongTime();
		myHandler.postDelayed(updateSongTime, 100);
		pauseButton.setEnabled(true);
		playButton.setEnabled(false);

		String songNameStr = getTitlesList().get(posicionLlistaReproductor);
		songNameStr = songNameStr.substring(0, songNameStr.lastIndexOf('.'));
		songName.setText(songNameStr);

		try {
			ActionActivity.updateServerCurrentPlaySong(
					getDataMusic().get(posicionLlistaReproductor),
					getTitlesList().get(posicionLlistaReproductor), this);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class UpdateSongTime extends Thread {
		@Override
		public void run() {
			startTime = mediaPlayer.getCurrentPosition();
			startTimeField.setText(String.format(
					"%d : %d ",
					TimeUnit.MILLISECONDS.toMinutes((long) startTime),
					TimeUnit.MILLISECONDS.toSeconds((long) startTime)
							- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
									.toMinutes((long) startTime))));
			seekbar.setProgress((int) startTime);
			myHandler.postDelayed(this, 100);

		}
	}

	// private Runnable UpdateSongTime = new Runnable() {
	//
	// public void run() {
	// startTime = mediaPlayer.getCurrentPosition();
	// startTimeField.setText(String.format(
	// "%d : %d ",
	// TimeUnit.MILLISECONDS.toMinutes((long) startTime),
	// TimeUnit.MILLISECONDS.toSeconds((long) startTime)
	// - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
	// .toMinutes((long) startTime))));
	// seekbar.setProgress((int) startTime);
	// myHandler.postDelayed(this, 100);
	//
	// }
	// };

	/**
	 * pausar cancion
	 * 
	 * @param view
	 *            view que hace la accion
	 */
	public void pause(View view) {
//		Toast.makeText(getApplicationContext(), "Pausing sound",
//				Toast.LENGTH_SHORT).show();

		// mediaPlayer.reset(); // ////////////////////////

		if (mediaPlayer.isPlaying()) {
			mediaPlayer.pause(); // /////////////////////////
		}

		// mediaPlayer.pause();
		pauseButton.setEnabled(false);
		playButton.setEnabled(true);

		try {
			ActionActivity.updateServerCurrentPlaySong(null, null, this);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// mediaPlayer = new MediaPlayer();
		// mediaPlayer.setOnCompletionListener(this);
		try {
			mediaPlayer.setDataSource(getDataMusic().get(
					posicionLlistaReproductor));
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * reproducir siguiente cancion
	 * 
	 * @param view
	 *            view que hace la accion
	 */
	public void forward(View view) {

		posicionLlistaReproductor++;
		posicionLlistaReproductorAux++;

		if (posicionLlistaReproductor > dataMusic.size() - 1) {
			posicionLlistaReproductor = 0;
			posicionLlistaReproductorAux = 0;
		}
		mediaPlayer.reset(); // ///////////////////////////////7
		mediaPlayer.release();
		mediaPlayer = null;
		mediaPlayer = new MediaPlayer();
		mediaPlayer.setOnCompletionListener(this);

		try {
			mediaPlayer.setDataSource(getDataMusic().get(
					posicionLlistaReproductor));
			mediaPlayer.prepare();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		play(null);

	}

	/**
	 * reproducir cancion anterior
	 * 
	 * @param view
	 *            view que hace la accion
	 */
	public void rewind(View view) {

		posicionLlistaReproductor--;
		posicionLlistaReproductorAux--;

		if (posicionLlistaReproductor < 0) {
			posicionLlistaReproductor = getDataMusic().size() - 1;
			posicionLlistaReproductorAux = getDataMusic().size() - 1;
		}

		mediaPlayer.reset(); // //////////////////////
		// mediaPlayer.stop();
		; // //////////////////////
		mediaPlayer.release();
		mediaPlayer = null;
		mediaPlayer = new MediaPlayer();
		mediaPlayer.setOnCompletionListener(this);
		try {
			mediaPlayer.setDataSource(getDataMusic().get(
					posicionLlistaReproductor));
			mediaPlayer.prepare();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		play(null);

	}

	/**
	 * si salimos de esta activity cerramos para que no siga sonando la musica
	 */
	@Override
	public void onBackPressed() {
		// try {
		// this.updateSongTime.interrupt();
		// this.pause(null);

		try {
			if (mediaPlayer.isPlaying()) {
				// mediaPlayer.stop()
				try {

					this.pause(null);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// try {
		//
		// mediaPlayer.reset();
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
		//
		// try {
		//
		// mediaPlayer.release();
		// } catch (Exception e) {
		// e.printStackTrace();
		// }

		// mediaPlayer.stop();
		//
		// } catch (Throwable e) {
		// }
		// try {
		// mediaPlayer.release();
		// } catch (Throwable e) {
		// }
		// pauseButton.setEnabled(false);
		// playButton.setEnabled(true);

//		Toast.makeText(this, "ON BACK PRESSED", Toast.LENGTH_LONG).show();
		Intent intent = new Intent(this, LoginActivity.class);
		ChatActivity.setAutoClose(true);
		ChatActivity.setNextAutoCloseValue(false);
		LoginActivity.setAutoClose(true);
		this.startActivity(intent);
		this.finish();

		// super.onBackPressed();
		//
		// android.os.Process.killProcess(android.os.Process.myPid());

	}

	/**
	 * cambiamos de activity para que el usuario pueda escoger la cancion
	 */
	@Override
	public void onClick(View v) {
		Intent intent = new Intent(this, AudioList.class);
		this.startActivity(intent);

	}

	/**
	 * en este metodo definimos que es lo que passa cuando termina la
	 * reproduccion de una cancion
	 * 
	 * @param mp
	 *            mediaplayer
	 */
	@Override
	public void onCompletion(MediaPlayer mp) {
		// TODO Auto-generated method stub
		try {

			mediaPlayer.release();
			mediaPlayer = null;
			if (posicionLlistaReproductor < getDataMusic().size() - 1) {
				posicionLlistaReproductor++;
				posicionLlistaReproductorAux++;
			} else {
				posicionLlistaReproductor = 0;
				posicionLlistaReproductorAux = 0;
			}

			if (mediaPlayer == null) {
				mediaPlayer = new MediaPlayer();

			}
			mediaPlayer.reset(); // ////////////////////////////
			// mediaPlayer.stop();// ////////////////////////
			mediaPlayer.setOnCompletionListener(this);
			mediaPlayer.setDataSource(getDataMusic().get(
					posicionLlistaReproductor));
			mediaPlayer.prepare();
			play(null);
			Log.d("debugandoPorLaApp", "new song");
			// mediaPlayer.prepare();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		play(null);
		Log.d("debugandoPorLaApp", "playing song");
		seekbar.refreshDrawableState();

	}

	/**
	 * 
	 * @return lista de los titulos de canciones que se reproduciran
	 */
	public static ArrayList<String> getTitlesList() {
		return titlesList;
	}

	/**
	 * 
	 * @param titlesList
	 *            lista de los titulos de canciones que se reproduciran
	 */
	public static void setTitlesList(ArrayList<String> titlesList) {
		MusicPlayerActivity.titlesList = titlesList;
 
	}

	/**
	 * 
	 * @return lista con las imagenes de canciones que se reproduciran
	 */
	public static ArrayList<Bitmap> getMusicImageList() {
		return musicImageList;
	}

	/**
	 * 
	 * @param musicImageList
	 *            lista con las imagenes de canciones que se reproduciran
	 */
	public static void setMusicImageList(ArrayList<Bitmap> musicImageList) {
		MusicPlayerActivity.musicImageList = musicImageList;
	}

	/**
	 * 
	 * @return lista con los dirctorios de las canciones que se reproduciran
	 */
	public static ArrayList<String> getDataMusic() {
		return dataMusic;
	}

	/**
	 * 
	 * @param dataMusic
	 *            lista con los dirctorios de las canciones que se reproduciran
	 */
	public static void setDataMusic(ArrayList<String> dataMusic) {
		synchronized (musicImageList) {
			MusicPlayerActivity.dataMusic = dataMusic;
		}
	}

	/**
	 * 
	 * @return la posicion de la cancion que se esta reproduciendo en la lista
	 *         de canciones
	 */
	public static int getPosicionLlistaReproductor() {
		return posicionLlistaReproductor;
	}

	/**
	 * 
	 * @param posicionLlistaReproductor
	 *            posicion de la cancion en la que empezara a reproducirse la
	 *            lista
	 */
	public static void setPosicionLlistaReproductor(
			int posicionLlistaReproductor) {
		MusicPlayerActivity.posicionLlistaReproductor = posicionLlistaReproductor;
	}

	/**
	 * 
	 * @return si se ha vuelto a esta activity a traves de un backspace
	 */
	public static boolean isComesFromBacspace() {
		return comesFromBacspace;
	}

	/**
	 * 
	 * @param comesFromBacspace
	 *            si se vuelve a esta activity a traves de un backspace
	 */
	public static void setComesFromBacspace(boolean comesFromBacspace) {
		MusicPlayerActivity.comesFromBacspace = comesFromBacspace;
	}
	/*
	 * Evita que se gire panatalla al girar el dispositivo
	 */
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}

	public static boolean isComesFromLogin() {
		return comesFromLogin;
	}

	public static void setComesFromLogin(boolean comesFromLogin) {
		MusicPlayerActivity.comesFromLogin = comesFromLogin;
	}

	public static boolean isComesFromNewSong() {
		return comesFromNewSong;
	}

	public static void setComesFromNewSong(boolean comesFromNewSong) {
		MusicPlayerActivity.comesFromNewSong = comesFromNewSong;
	}

}

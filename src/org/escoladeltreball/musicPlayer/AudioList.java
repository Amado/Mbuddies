package org.escoladeltreball.musicPlayer;

import java.util.ArrayList;

import org.escoladeltreball.chat.R;

import android.app.Activity;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

//import com.example.projectm13.R;

/**
 * Classe que muestra un list view con el nombre de las canciones y sus imagenes
 * del dispositivo del usuario permitiendo al seleccionar una cancion su
 * reproduccion en la clase MusicPlayerActivity
 * 
 * @author Miquel Graells Monreal
 * 
 */

public class AudioList extends Activity {
	ListView musiclist;
	Cursor musiccursor;
	static ArrayList<String> titlesList = new ArrayList<String>(); // array con
																	// los
																	// titulos
																	// de las
																	// canciones
	static ArrayList<Bitmap> musicImageList = new ArrayList<Bitmap>(); // array
																		// con
																		// las
																		// imagenes
																		// de
																		// las
																		// canciones
	static ArrayList<String> dataMusic = new ArrayList<String>(); // array con
																	// los path
																	// de las
																	// canciones
	MediaMetadataRetriever mmr = new MediaMetadataRetriever(); // objeto que nos
																// permitira
																// obtener la
																// imagen de la
																// cancion

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		musiclist = (ListView) findViewById(R.id.PhoneMusicList);
		// creamos el adaptador pasandole el array de titulos y de
		// imagenes

		LoadSongs ls = new LoadSongs(this, 1);
		ls.doInBackground(null);

		MusicAdapter musicAdapter = new MusicAdapter(this, titlesList,
				musicImageList);
		// añadimos el adaptador a la listview
		musiclist.setAdapter(musicAdapter);
		// indicamos qual sera el onclick de cada item de la listview
		musiclist.setOnItemClickListener(musicgridlistener);

		// init_phone_music_grid();
		// BitmapWorkerTask task = new BitmapWorkerTask(null);
		// task.execute();

	}


	/**
	 * Cuando el usuario aprieta uno de los items se carrga de manera estatica
	 * en la calse MusicPlayerActivity los tres arrysList y la posicion de la
	 * cancion seleccionada para su reproduccion
	 */
	private OnItemClickListener musicgridlistener = new OnItemClickListener() {
		public void onItemClick(AdapterView parent, View v, int position,
				long id) {
			MusicPlayerActivity.setPosicionLlistaReproductor(position);
			MusicPlayerActivity.setDataMusic(dataMusic);
			MusicPlayerActivity.setTitlesList(titlesList);
			MusicPlayerActivity.setMusicImageList(musicImageList);
			MusicPlayerActivity.setComesFromNewSong(true);
			Log.d("myapp", "changedList");
			finish();
			// Toast.makeText(AudioList.this, "!!!!!!!!!!",
			// Toast.LENGTH_LONG).show();
		}
	};

	/**
	 * En el caso de que el usuario no escoga ninguna cancion nueva le indicamos
	 * a la Clase MusicPlayerActivty que volvemos con el backspace
	 */
	@Override
	public void onBackPressed() {
		MusicPlayerActivity.setComesFromBacspace(true);
		finish();
	}

}
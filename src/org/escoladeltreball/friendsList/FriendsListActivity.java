package org.escoladeltreball.friendsList;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.escoladeltreball.activities.UtilMessage;
import org.escoladeltreball.chat.R;
import org.escoladeltreball.db4o.Db4oHelper;
import org.escoladeltreball.db4o.User;
import org.escoladeltreball.manager.RequestManager;
import org.escoladeltreball.model.Session;
import org.escoladeltreball.model.server.persistence.ModelConverter;
import org.escoladeltreball.model.server.persistence.UsersServ;
import org.escoladeltreball.support.Config;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewStub;
import android.widget.ListView;
import android.widget.TextView;

public class FriendsListActivity extends Activity {

	public static final String ERROR_LOCAL_DB_TYPE = "local error";
	public static final String ERROR_SERVER_TYPE = "server error";

	private ArrayList<User> friendsList;
	private ListView listView;
	private FriendsListAdapter friendsListAdapter;
	private Session session;

	private ViewStub vstEmtpy;
	private TextView texviewEmpty;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_friends_list);

		this.friendsList = new ArrayList<User>();

		vstEmtpy = (ViewStub) findViewById(R.id.vstFriendsListEmpty);
		texviewEmpty = (TextView) findViewById(R.id.emptyFriendList);

		listView = (ListView) findViewById(R.id.lvFriends);
		listView.setEmptyView(vstEmtpy);
		friendsListAdapter = new FriendsListAdapter(this, friendsList);
		listView.setAdapter(friendsListAdapter);

		session = Session.getSession(this);
		UsersServ user = session.getUser(this);
		if (session != null && user != null) {
			findFriends(user.getId());
		}
	}

	/**
	 * Busca los amigos del usuario y actualiza el listview
	 * 
	 * @param idUser
	 */
	public void findFriends(final Long idUser) {

		final ProgressDialog dialog = UtilMessage.showProgressDialog(this,
				"Connecting with the server.... please wait");

		new Thread(new Runnable() {
			public void run() {
				try {

					Session.getSession(FriendsListActivity.this);
					Log.i("INFO", " viewProfile--" + idUser);
					// friendsList = new ArrayList<User>();

					friendsList.clear();

					List<User> responseUsers = ModelConverter
							.converntTolocalUsers(RequestManager
									.fecthFriendsFromUser(String
											.valueOf(idUser)));
					if (responseUsers == null) {
						Log.i(Config.TAG_INFO,
								"There isn't any friend from user " + idUser);
						return;
					}
					friendsList.addAll(responseUsers);

					FriendsListActivity.this.runOnUiThread(new Runnable() {

						@Override
						public void run() {
							Log.i("INFO", " friends of user: " + idUser);
							friendsListAdapter.notifyDataSetChanged();

						}
					});

				} catch (Exception e) {
					e.printStackTrace();
					FriendsListActivity.this.runOnUiThread(new Runnable() {

						@Override
						public void run() {
							final String erroMsg = "Error retrieving the friends"
									+ " From Server. Try Later";
							UtilMessage.dismisProgressDialog(dialog);
							UtilMessage.showErrorMessage(
									FriendsListActivity.this, erroMsg);
							texviewEmpty.setText(erroMsg);

						}
					});
				} finally {
					FriendsListActivity.this.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							UtilMessage.dismisProgressDialog(dialog);
						}
					});
				}
			}

		}).start();

	}

	public void updateList(ArrayList<User> users) {
		this.friendsList.clear();
		this.friendsList.addAll(users);
		this.friendsListAdapter.notifyDataSetChanged();
	}

	/**
	 * Inicia un temporizador para cerrar la app si hubiese algún error en la db
	 * local o en el servidor , para evitar que la app se quede colgada.
	 * 
	 * @param errorType
	 *            si el error es de db local o del server
	 */
	public void executeCountDownToShutDown(final String errorType) {
		Thread db4oServerThread = new Thread(new Runnable() {
			@Override
			public void run() {
				if (errorType == FriendsListActivity.ERROR_LOCAL_DB_TYPE) {
					Db4oHelper
							.displayDbError(Db4oHelper.ERROR_LOCAL_APP_WILL_CLOSE);
				} else if (errorType == FriendsListActivity.ERROR_SERVER_TYPE) {
					Db4oHelper
							.displayDbError(Db4oHelper.ERROR_SERVER_APP_WILL_CLOSE);
				}
			}
		});
		Thread gpxClientThread = new Thread(new Runnable() {
			@Override
			public void run() {
				android.os.Process.killProcess(android.os.Process.myPid());
			}
		});
		// gestionamos la ejecucion de los hilos
		ScheduledExecutorService ses = Executors.newScheduledThreadPool(3);
		ses.schedule(db4oServerThread, 0, TimeUnit.SECONDS);// ejecutamos el
															// servidor
															// inmediatamente
		ses.schedule(gpxClientThread, 10, TimeUnit.SECONDS);// ejecutamos el
															// cliente gpx
	}

}

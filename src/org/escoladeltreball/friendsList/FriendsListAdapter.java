package org.escoladeltreball.friendsList;

import java.util.ArrayList;
import java.util.List;

import org.escoladeltreball.activities.ActionActivity;
import org.escoladeltreball.activities.UtilActivity;
import org.escoladeltreball.activities.UtilMessage;
import org.escoladeltreball.chat.R;
import org.escoladeltreball.chat.service.ChatIntentService;
import org.escoladeltreball.db4o.User;
import org.escoladeltreball.manager.RequestManager;
import org.escoladeltreball.model.Session;
import org.escoladeltreball.model.server.persistence.ModelConverter;
import org.escoladeltreball.model.server.persistence.UsersServ;
import org.escoladeltreball.support.Config;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

public class FriendsListAdapter extends BaseAdapter implements OnClickListener {

	public static User userForPerfilFriendActivity;

	// el array list con todas las peliculas creado en main activity
	private ArrayList<User> friends;
	// necesitamos un layout inflater asociado a un context para poder llenar un
	// det. layout
	// con un contenido, inflaremos la layout item con los datos de las pelis
	private LayoutInflater layoutInflater;
	private FriendsListActivity activity;
	private int maxWidthForTextView;
	private ViewHolder holder;

	private User user = null;

	// necesitamos un constructor que será llamado desde mainActivity
	// para inicializar el array y el layoutInflater
	public FriendsListAdapter(FriendsListActivity context,
			ArrayList<User> friends) {
		// instanciamos el array con las peliculas
		this.friends = friends;
		// instanciamos el inflater con el context de mainActivity
		this.layoutInflater = LayoutInflater.from(context);
		this.activity = context;

		Display display = context.getWindowManager().getDefaultDisplay();
		int displayWidth = display.getWidth();
		int maxWidthForTextView = (int) displayWidth / 2;
	}

	@Override
	public int getCount() {
		return friends == null ? 0 : friends.size();
	}

	@Override
	public Object getItem(int position) {
		return friends.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	static class ViewHolder {
		Button profile;
		Button name;
		Button garbage;
	}

	/**
	 * ESte metodo nos devolvera una View de nuestro item inflada xon el
	 * contenido correspondiente
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			// Inflem el Layout de cada item
			convertView = layoutInflater.inflate(R.layout.item_friend_layout,
					null);
			holder = new ViewHolder();
			// Capturem els TextView
			holder.profile = (Button) convertView
					.findViewById(R.id.btFriendListFriendProfile);
			holder.name = (Button) convertView
					.findViewById(R.id.btFriendListName);
			holder.name.setMaxWidth(maxWidthForTextView);
			holder.name.setMaxLines(1);

			holder.garbage = (Button) convertView
					.findViewById(R.id.btFriendListGarbage);
			// Associem el viewholder,
			// la informació de l'estructura que hi ha d'haver dins el layout,
			// amb la vista que haurà de retornar aquest mètode.
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.garbage.setBackgroundResource(R.drawable.bt_garbage_images);

		holder.profile.setTag(position);
		holder.garbage.setTag(position);
		holder.name.setTag(position);

		holder.profile.setOnClickListener(this);
		holder.garbage.setOnClickListener(this);
		holder.name.setOnClickListener(this);
		holder.name.setBackgroundResource(R.drawable.bt_friend_name_images);

		user = friends.get(position);

		int friendAvatar = user.getPhotoResource();

		UtilActivity.setProfileBackgroundFromIdImg(holder.profile,
				friendAvatar, true);

		holder.name.setText("" + friends.get(position).getName());

		return convertView;
	}

	@Override
	public void onClick(View v) {

		int position = (Integer) v.getTag();

		User user = friends.get(position);

		int vId = v.getId();

		if (vId == R.id.btFriendListFriendProfile) {
			ActionActivity.viewProfile(user.getId(), activity,
					FriendsListActivity.class);

		} else if (vId == R.id.btFriendListGarbage) {
			displayConfirmationDialog(user.getId());

		} else {
			// el amigo cuyo perfil queremos visualizar
			userForPerfilFriendActivity = user;
			ActionActivity.diplayFriendOptionsDialog(user.getId(),
					user.getName(), user.getPhotoResource(), activity,
					FriendsListActivity.class);
		}
	}

	/**
	 * // * Despliega en pantalla un AlertDialog que seleccionar enrtre las
	 * opciones // * de ver perfil o ver listas de favoritos. //
	 */

	/**
	 * Despliega en pantalla un AlertDialog que pide confirmación para el
	 * borrado del amigo.
	 */
	public void displayConfirmationDialog(final Long selFriendId) {

		activity.runOnUiThread(new Runnable() {
			public void run() {

				final AlertDialog.Builder alert = new AlertDialog.Builder(
						activity);

				alert.setTitle("Are you sure ?");
				alert.setPositiveButton("Remove",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								// eliminamos el amigo del serivor
								RemoveFriend(selFriendId);
							}
						});
				alert.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								dialog.cancel();
							}
						});
				alert.show();
			}
		});
	}

	public void RemoveFriend(final Long idFriend) {
		new Thread() {
			@Override
			public void run() {
				try {
					// Session session = Session.getSession(activity);
					UsersServ userServ = Session.getUser(activity);
					if (userServ == null) {
						Log.w(Config.TAG_WARNING,
								"Warinng on FriendListAdapter - removeFriend - the user  = "
										+ user + " on session");
					}
					final Long selfId = userServ.getId();

					List<UsersServ> responseFriends = RequestManager
							.removeFriend(String.valueOf(selfId),
									String.valueOf(idFriend));

					ArrayList<User> friends = null;
					if (responseFriends == null) {
						friends = new ArrayList<User>();
					} else {
						friends = new ArrayList<User>(
								ModelConverter
										.converntTolocalUsers(responseFriends));
					}

					final ArrayList<User> tmpFriends = friends;

					/**
					 * actualizamos la lista de amigos del chat
					 */
					activity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							activity.updateList(tmpFriends);
							 
							ChatIntentService.getActivity()
									.removeContactFromList(idFriend);
						}
					});

				} catch (Exception e) {
					e.printStackTrace();
					activity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							UtilMessage.showErrorMessage(activity,
									"Error removing the friend on server");

						}
					});
				}
			}
		}.start();

	}
}

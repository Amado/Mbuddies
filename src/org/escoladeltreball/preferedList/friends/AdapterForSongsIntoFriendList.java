package org.escoladeltreball.preferedList.friends;

import java.util.ArrayList;

import org.escoladeltreball.chat.R;
import org.escoladeltreball.db4o.Db4oHelper;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.db4o.ObjectContainer;

/**
 * 
 * Adaptador que muesta las canciones de una lista de favoritos y gestiona que
 * passa cuando se borra una cancion o se selecciona para reproducir
 * 
 * @author Miquel Graells Monreal
 * 
 */

public class AdapterForSongsIntoFriendList extends BaseAdapter {

	ArrayList<String> songsTitles;
	ArrayList<Bitmap> songsImages;
	LayoutInflater layoutInflater;
	Activity activity;
	Db4oHelper helper = null;
	ObjectContainer oc = null;

	/**
	 * 
	 * @param activity
	 *            activity que usaremos para inflar el layout
	 * @param song
	 *            arrayList con los titulos de las canciones
	 * @param songsImages
	 *            arrayList con las imagenes de las canciones
	 */
	public AdapterForSongsIntoFriendList(Activity activity,
			ArrayList<String> song, ArrayList<Bitmap> songsImages) {
		this.activity = activity;
		this.songsTitles = song;
		this.songsImages = songsImages;
		layoutInflater = LayoutInflater.from(activity);
	}

	@Override
	public int getCount() {
		return songsTitles.size();
	}

	@Override
	public Object getItem(int position) {
		return songsTitles.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	// Si utilitzem el patró ViewHolder, el primer que farem és crear el model,
	// classe, per als objectes
	// que representen
	static class ViewHolder {
		TextView nameSong;
		ImageView imageSong;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			convertView = layoutInflater.inflate(
					R.layout.itemsongintfriendlist, null);
			holder = new ViewHolder();
			holder.nameSong = (TextView) convertView
					.findViewById(R.id.songNameliFriend);
			holder.imageSong = (ImageView) convertView
					.findViewById(R.id.imageliFriendArtist);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		 
		holder.nameSong.setText(songsTitles.get(position));
		holder.imageSong.setImageBitmap(songsImages.get(position));
		return convertView;
	}

}
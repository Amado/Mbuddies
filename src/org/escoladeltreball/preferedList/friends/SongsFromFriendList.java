package org.escoladeltreball.preferedList.friends;

import java.util.ArrayList;
import java.util.List;

import org.escoladeltreball.activities.UtilMessage;
import org.escoladeltreball.chat.R;
import org.escoladeltreball.manager.RequestManager;
import org.escoladeltreball.model.server.persistence.CancionesServ;
import org.escoladeltreball.support.Config;
import org.escoladeltreball.support.ImageConverter;
import org.escoladeltreball.support.Utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 
 * Activity que muestra un List view con una lista de canciones sacadas de una
 * lista de Favorites apartir del adaptador AdapterForSongsIntoList
 * 
 * @author Miquel Graells Monreal
 * 
 */
public class SongsFromFriendList extends Activity {

	private static Long idUser;
	private static String userName;
	private static int userIdPhoto = 0;
	ListView musiclist;
	// array con los titulos de las canciones
	private ArrayList<String> titlesList = new ArrayList<String>();
	// array con las imagenes de las canciones
	private ArrayList<Bitmap> musicImageList = new ArrayList<Bitmap>();
	// array con los path de las canciones
	// ArrayList<String> dataMusic = new ArrayList<String>();
	AdapterForSongsIntoFriendList musicAdapter;

	private TextView listName;
	private static String favoriteIdentification,
			favoriteName = "Favorite Playlist";
	private TextView tvEmpty;

	// private ImageView ivFriend;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_songs_from_friend_list);

		listName = (TextView) findViewById(R.id.nameListFriend);

		musiclist = (ListView) findViewById(R.id.PhoneMusicFriendList);
		// ivFriend = (ImageView)
		// findViewById(R.id.ivSrcPhotoSongsFromFriendList);
		// creamos el adaptador pasandole el array de titulos y de
		// imagenes

		musicAdapter = new AdapterForSongsIntoFriendList(this, titlesList,
				musicImageList);
		// añadimos el adaptador a la listview
		musiclist.setAdapter(musicAdapter);
		// indicamos qual sera el onclick de cada item de la listview
		musicAdapter.notifyDataSetChanged();

		tvEmpty = (TextView) this.findViewById(R.id.emptySongListFriend);
		musiclist.setEmptyView(tvEmpty);

	}

	public ArrayList<String> getTitlesList() {
		return this.titlesList;
	}

	public void setTitlesList(ArrayList<String> titlesList) {
		this.titlesList = titlesList;
	}

	public ArrayList<Bitmap> getMusicImageList() {
		return musicImageList;
	}

	public void setMusicImageList(ArrayList<Bitmap> musicImageList) {
		this.musicImageList = musicImageList;
	}

	@Override
	protected void onResume() {
		super.onResume();
		musicAdapter.notifyDataSetChanged();

//		Toast.makeText(this, "SongsTItle = " + favoriteName, Toast.LENGTH_LONG)
//				.show();
		listName.setText(userName + " - " + favoriteName);
		// UtilActivity.setProfileBackgroundFromIdImg(ivFriend, userIdPhoto,
		// true);
		findSongsFromFriend(favoriteIdentification, favoriteName, idUser);
		// onCreate(null);
	}

	public void findSongsFromFriend(final String favoriteIdentification,
			final String aFavoritesName, final Long idUser) {
		final ProgressDialog dialog = UtilMessage.showProgressDialog(this,
				"Connecting with the server.... please wait");

		Log.i(Config.TAG_INFO,
				"findSongsFromFriend() favoriteIdentification = "
						+ favoriteIdentification);

		this.titlesList.clear();
		this.musicImageList.clear();
		// SongsFromFriendList.favoriteName = "Favorite Playlist";

		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					List<CancionesServ> responseSerFavs = RequestManager
							.fetchSongsFromUserAndFavList(
									String.valueOf(idUser),
									favoriteIdentification);

					Log.i(Config.TAG_INFO, "responseSerFavs =  = "
							+ responseSerFavs);
					if (responseSerFavs != null) {

						final ArrayList<String> songsPaths = new ArrayList<String>();
						final ArrayList<String> songsTitles = new ArrayList<String>();
						final ArrayList<Bitmap> songsImages = new ArrayList<Bitmap>();
						for (int i = 0; i < responseSerFavs.size(); i++) {

							final CancionesServ songN = responseSerFavs.get(i);
							Log.i(Config.TAG_INFO,
									"songName = " + songN.getSongName());
							songsTitles.add(songN.getSongName());
							songsPaths.add(songN.getSrcPath());

							if (songN.getPhotoEncoded() != null
									&& songN.equals("") == false) {

							}

							Bitmap bm = null;
							// si no existe una imagen de la cancion le ponemos
							// una por defecto
							if (songN.getPhotoEncoded() == null
									|| songN.getPhotoEncoded().equals("")) {
								bm = BitmapFactory.decodeResource(
										SongsFromFriendList.this.getResources(),
										R.drawable.playerbackgroundicon);
							} else {
								bm = Utils.getBitmapFromBytes(ImageConverter
										.decodeImage(songN.getPhotoEncoded()));
								// bm = Utils.getBitmapFromBytes(bitmapArr);
							}
							songsImages.add(bm);
						}

						SongsFromFriendList.this.runOnUiThread(new Runnable() {

							@Override
							public void run() {

								if (songsTitles == null
										|| songsTitles.isEmpty()) {
									Log.i(Config.TAG_INFO,
											"******* SongsFromFriendList listview Emtpy ******");
									SongsFromFriendList.this.musiclist
											.setEmptyView(tvEmpty);

									SongsFromFriendList.this.musicAdapter
											.notifyDataSetInvalidated();
								} else {

									SongsFromFriendList.this.titlesList
											.addAll(songsTitles);
									SongsFromFriendList.this.musicImageList
											.addAll(songsImages);

									SongsFromFriendList.favoriteName = aFavoritesName;

									SongsFromFriendList.this.musicAdapter
											.notifyDataSetChanged();
								}
							}
						});

					} else {

					}

				} catch (Exception e) {
					e.printStackTrace();
					SongsFromFriendList.this.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							final String erroMsg = "Error retrieving the songs"
									+ " from Server. Try Later";
							UtilMessage.showErrorMessage(
									SongsFromFriendList.this, erroMsg);
						}
					});
				} finally {
					SongsFromFriendList.this.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							UtilMessage.dismisProgressDialog(dialog);
						}
					});
				}
			}

		}).start();

	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		Intent intent = new Intent(this, FavoriteFriendPlayList.class);
		this.startActivity(intent);
		// findSongsFromFriend(favoriteIdentification, favoriteName, idUser);
	}

	public static String getFavoriteIdentification() {
		return favoriteIdentification;
	}

	public static void setFavoriteIdentification(String favoriteIdentification) {
		SongsFromFriendList.favoriteIdentification = favoriteIdentification;
	}

	public static String getFavoriteName() {
		return favoriteName;
	}

	public static void setFavoriteName(String favoriteName) {
		SongsFromFriendList.favoriteName = favoriteName;
	}

	public static Long getIdUser() {
		return idUser;
	}

	public static void setIdUser(Long idUser) {
		SongsFromFriendList.idUser = idUser;
	}

	public TextView getListName() {
		return listName;
	}

	public void setListName(TextView listName) {
		this.listName = listName;
	}

	public static String getUserName() {
		return userName;
	}

	public static void setUserName(String userName) {
		SongsFromFriendList.userName = userName;
	}

	public static int getUserIdPhoto() {
		return userIdPhoto;
	}

	public static void setUserIdPhoto(int userIdPhoto) {
		SongsFromFriendList.userIdPhoto = userIdPhoto;
	}

}

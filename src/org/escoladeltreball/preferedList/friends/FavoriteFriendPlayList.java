package org.escoladeltreball.preferedList.friends;

import java.util.ArrayList;

import org.escoladeltreball.activities.UtilMessage;
import org.escoladeltreball.chat.R;
import org.escoladeltreball.manager.RequestManager;
import org.escoladeltreball.model.server.persistence.FavoritesServ;
import org.escoladeltreball.support.Config;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

/**
 * 
 * Activity que muestra un listView con los nombres de las listas de canciones
 * favoritas, tambien permite anadir mas lista, para borrar listas se gestiona
 * en el adaptador(FavoritePlayListAdapter)
 * 
 * @author Miquel Graells Monreal
 * 
 */

public class FavoriteFriendPlayList extends Activity implements
		OnItemClickListener {

	private ListView listPreferedListSongs;

	private FavoritesFriendAdapter adapter;
	private TextView tvFavoritesFriendTitle;
	private ArrayList<FavoritesServ> favoritesList;
	private static Long idFriend;
	private static String friendName;
	private static int userIdPhoto = 0;
	private static String favoriteListTitle = "Favorites List";
	private TextView tvEmpty;
	private static Class<?> comesFrom;

	// private ImageView ivFriend;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_favorite_play_list_friend);

		favoritesList = new ArrayList<FavoritesServ>();

		adapter = new FavoritesFriendAdapter(this, favoritesList);
		listPreferedListSongs = (ListView) findViewById(R.id.listFavorites);
		tvFavoritesFriendTitle = (TextView) findViewById(R.id.tvFavoritesFriendTitle);

		listPreferedListSongs.setAdapter(adapter);
		listPreferedListSongs.setOnItemClickListener(this);

		tvEmpty = (TextView) this.findViewById(R.id.emptyFavoritesFriend);
		listPreferedListSongs.setEmptyView(tvEmpty);
		// ivFriend = (ImageView)
		// findViewById(R.id.ivSrcPhotoFavoriteListFriend);
	}

	@Override
	protected void onResume() {
		super.onResume();
		tvFavoritesFriendTitle.setText(friendName + " - " + favoriteListTitle);
		// UtilActivity.setProfileBackgroundFromIdImg(ivFriend, userIdPhoto,
		// true);
		this.findFavoritesFromUser(idFriend, friendName);
	}

	@Override
	public void onBackPressed() {
		if (comesFrom != null) {
			Intent intent = new Intent(this, comesFrom);
			this.startActivity(intent);
			this.finish();
		} else {
			super.onBackPressed();
		}
	}

	public ArrayList<FavoritesServ> getFavoritesList() {
		return favoritesList;
	}

	public void setFavoritesList(ArrayList<FavoritesServ> favoritesList) {
		this.favoritesList = favoritesList;
	}

	public static String getFavoriteListTitle() {
		return favoriteListTitle;
	}

	public static void setFavoriteListTitle(String favoriteListTitle) {
		FavoriteFriendPlayList.favoriteListTitle = favoriteListTitle;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		FavoritesServ fav = favoritesList.get(position);
		SongsFromFriendList.setFavoriteName(fav.getNameOfList());
		SongsFromFriendList.setUserName(friendName);
		SongsFromFriendList.setFavoriteIdentification(fav.getIdentification());
		SongsFromFriendList.setUserIdPhoto(userIdPhoto);
		Intent intent = new Intent(this, SongsFromFriendList.class);
		this.startActivity(intent);
		// findSongsFromFriend(fav.getIdentification(), fav.getNameOfList(),

	}

	public static Long getIdFriend() {
		return idFriend;
	}

	public static void setIdFriend(Long idFriend) {
		FavoriteFriendPlayList.idFriend = idFriend;
	}

	public static String getFriendName() {
		return friendName;
	}

	public static void setFriendName(String friendName) {
		FavoriteFriendPlayList.friendName = friendName;
	}

	// /**
	// * recupera los favoritos del usuario en el servidor y los muestra en la
	// * lista
	// */
	public void findFavoritesFromUser(final Long idFriend, final String userName) {
		final ProgressDialog dialog = UtilMessage.showProgressDialog(this,
				"Connecting with the server.... please wait");
		favoritesList.clear();
		new Thread(new Runnable() {
			public void run() {
				try {
					// Log.i(Config.TAG_INFO,
					// "findFavoritesFromUser()... idUser = " + idFriend);

					final ArrayList<FavoritesServ> responseSerFavs = (ArrayList<FavoritesServ>) RequestManager
							.fetchFavoritesFromUser(String.valueOf(idFriend));

					// Log.i(Config.TAG_INFO, "responseSerFavs =   "
					// + responseSerFavs);

					if (responseSerFavs != null
							&& responseSerFavs.isEmpty() == false) {
						Log.i(Config.TAG_INFO, "favorites =  "
								+ responseSerFavs);

						FavoriteFriendPlayList.this
								.runOnUiThread(new Runnable() {

									@Override
									public void run() {

										if (responseSerFavs == null
												|| responseSerFavs.isEmpty()) {

											FavoriteFriendPlayList.this.adapter
													.notifyDataSetChanged();

										} else {
											favoritesList
													.addAll(responseSerFavs);

											FavoriteFriendPlayList.this.adapter
													.notifyDataSetChanged();
										}

									}
								});

					}

				} catch (Exception e) {
					e.printStackTrace();
					FavoriteFriendPlayList.this.runOnUiThread(new Runnable() {

						@Override
						public void run() {
							final String erroMsg = "Error retrieving the favorites list "
									+ " from Server. Try Later";
							UtilMessage.showErrorMessage(
									FavoriteFriendPlayList.this, erroMsg);

						}
					});
				} finally {
					FavoriteFriendPlayList.this.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							UtilMessage.dismisProgressDialog(dialog);
						}
					});
				}
			}

		}).start();
	}

	public static Class<?> getComesFrom() {
		return comesFrom;
	}

	public static void setComesFrom(Class<?> comesFrom) {
		FavoriteFriendPlayList.comesFrom = comesFrom;
	}

	public static int getUserIdPhoto() {
		return userIdPhoto;
	}

	public static void setUserIdPhoto(int userIdPhoto) {
		FavoriteFriendPlayList.userIdPhoto = userIdPhoto;
	}

}

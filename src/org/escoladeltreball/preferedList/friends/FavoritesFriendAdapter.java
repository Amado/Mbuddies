package org.escoladeltreball.preferedList.friends;

import java.util.ArrayList;

import org.escoladeltreball.chat.R;
import org.escoladeltreball.db4o.Db4oHelper;
import org.escoladeltreball.model.server.persistence.FavoritesServ;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.db4o.ObjectContainer;

/**
 * classe que customisa un listview para mostrar la lista de canciones
 * favoritas. tambien se encarga de gestionar los esdevenimientos cuando se
 * clica en los botones de eliminar y reproducir lista
 * 
 * @author Miquel Graells Monreal
 * 
 */
public class FavoritesFriendAdapter extends BaseAdapter {

	ArrayList<FavoritesServ> fls = new ArrayList<FavoritesServ>();
	LayoutInflater layoutInflater;
	FavoriteFriendPlayList activity;
	Db4oHelper helper = null;
	ObjectContainer oc = null;
	static View v;
	static FavoritesFriendAdapter thisAdapter;

	private FavoriteFriendPlayList acitivity;

	public ArrayList<FavoritesServ> getFls() {
		return fls;
	}

	public void setFls(ArrayList<FavoritesServ> fls) {
		this.fls = fls;
	}

	/**
	 * 
	 * @param activity
	 *            con la que inflaremos el layout
	 */
	public FavoritesFriendAdapter(FavoriteFriendPlayList activity,
			ArrayList<FavoritesServ> fls) {
		this.fls = fls;
		thisAdapter = this;
		this.acitivity = activity;
		layoutInflater = LayoutInflater.from(activity);
		this.activity = activity;
		Log.d("ListAdapter", "constructor done");
	}

	@Override
	public int getCount() {
		return fls == null ? 0 : fls.size();
	}

	@Override
	public Object getItem(int position) {
		return fls.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	// Si utilitzem el patró ViewHolder, el primer que farem és crear el model,
	// classe, per als objectes
	// que representen
	static class ViewHolder {
		TextView nameList;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			// Inflem el Layout de cada item
			convertView = layoutInflater.inflate(
					R.layout.item_favorites_friend_playlist, null);
			holder = new ViewHolder();
			// Capturem els TextView
			holder.nameList = (TextView) convertView
					.findViewById(R.id.songFriendName);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.nameList.setText("" + fls.get(position).getNameOfList());
		return convertView;
	}

}
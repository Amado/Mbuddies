package org.escoladeltreball.preferedList;

import org.escoladeltreball.chat.R;
import org.escoladeltreball.db4o.Favorites;
import org.escoladeltreball.model.Session;
import org.escoladeltreball.support.Config;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

/**
 * 
 * Activity que muestra un listView con los nombres de las listas de canciones
 * favoritas, tambien permite anadir mas lista, para borrar listas se gestiona
 * en el adaptador(FavoritePlayListAdapter)
 * 
 * @author Miquel Graells Monreal
 *
 */

public class FavoritePlayList extends Activity implements OnItemClickListener,
		OnClickListener {

	ListView listPreferedListSongs;
	static boolean comesFromSelectSong = false;
	ImageButton btnAdd;
	String nameList = "";
	static boolean result = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_favorite_play_list);
		btnAdd = (ImageButton) findViewById(R.id.btnAdd);
		btnAdd.setOnClickListener(this);

		Log.d("ListAdapter", "constructor done");

		FavoritePlayListAdapter adapter = new FavoritePlayListAdapter(this);
		listPreferedListSongs = (ListView) findViewById(R.id.listFavorites);
		listPreferedListSongs.setAdapter(adapter);
		listPreferedListSongs.setOnItemClickListener(this);
	}

	/**
	 * si venimos de seleccionar una cancion para que se reprodusca cerramos
	 * esta activity para que vaya al reproductor
	 */
	@Override
	protected void onRestart() {
		super.onRestart();
		if (comesFromSelectSong) {
			finish();
		}
	}

	/**
	 * si volvemos a la activity anterior (MainPlayerActivity) se lo tenemos que
	 * indicar
	 */
	// @Override
	// public void onBackPressed() {
	// MusicPlayerActivity.setComesFromBacspace(true);
	// finish();
	// }

	/**
	 * metodo que lanza una activity en la que se muestra un listview con las
	 * canciones de la lista de favoritos seleccionada
	 */
	public void startIntent() {
		Intent intent = new Intent(this, SongsFromList.class);
		this.startActivity(intent);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

	}

	/**
	 * 
	 * @return nos dice si emos vuelto a esta activity poruqe emos selecionado
	 *         una cancion
	 */
	public static boolean isComesFromSelectSong() {
		return comesFromSelectSong;
	}

	/**
	 * 
	 * @param comesFromSelectSong
	 *            si emos vuelto a esta activity poruqe emos selecionado una
	 *            cancion
	 */
	public static void setComesFromSelectSong(boolean comesFromSelectSong) {
		FavoritePlayList.comesFromSelectSong = comesFromSelectSong;
	}

	/**
	 * cuando se aprieta el boton de añadir lista, se muestra un dialogo
	 * pidiendote el nombre de la lista
	 */
	@Override
	public void onClick(View v) {

		// get prompts.xml view
		LayoutInflater layoutInflater = LayoutInflater
				.from(FavoritePlayList.this);
		View promptView = layoutInflater.inflate(R.layout.input_dialog, null);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				FavoritePlayList.this);
		alertDialogBuilder.setView(promptView);

		final EditText editText = (EditText) promptView
				.findViewById(R.id.edittext);
		// setup a dialog window
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// obtenemos el nombre de la lista que vamos a crear
						nameList = editText.getText().toString();
						addToList();
						onCreate(null);
					}
				})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		// create an alert dialog
		AlertDialog alert = alertDialogBuilder.create();
		alert.show();

	}

	/**
	 * metodo que añade la nueva lista de favoritos a la base de datos db4o
	 */
	public void addToList() {
		try {

			// declaramos la nueva lista con el nombre dado desde el dialogo
			final Long userId = Session.getSession(this).getUser().getId();
			Favorites favoriteList = new Favorites(nameList, userId);
			// declaromos el objeto que nos permite añadir la lista a db4o
			AuxiliarAddList aux = new AuxiliarAddList(this);
			// añadimos la lista
			aux.addList(favoriteList);

		} catch (Exception e) {
			Log.e(Config.TAG_ERROR,
					"Error persisting the favorites list: "
							+ nameList
							+ " probably because an error retrieving userId from the session");
			e.printStackTrace();
		}
	}

	/**
	 * metodo que añade la nueva lista de favoritos a la base de datos db4o
	 */
	/**
	 * public void addToList() { // declaramos la nueva lista con el nombre dado
	 * desde el dialogo Favorites favoriteList = new Favorites(nameList); //
	 * declaromos el objeto que nos permite añadir la lista a db4o
	 * AuxiliarAddList aux = new AuxiliarAddList(this); // añadimos la lista
	 * aux.addList(favoriteList);
	 * 
	 * }
	 */
}

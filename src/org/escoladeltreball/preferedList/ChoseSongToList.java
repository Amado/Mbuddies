package org.escoladeltreball.preferedList;

import java.util.ArrayList;

import org.escoladeltreball.chat.R;
import org.escoladeltreball.db4o.Favorites;
import org.escoladeltreball.db4o.Song;
import org.escoladeltreball.musicPlayer.MusicAdapter;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore.Audio;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

/**
 * 
 * Activity que muestra todas las canciones disponibles en el dispositivo, para
 * poder añadirlas en la lista de favoritos
 * 
 * @author Miquel Graells Monreal
 * 
 */

public class ChoseSongToList extends Activity {

	Activity activity;
	ListView musiclist;
	Cursor musiccursor;
	public static ArrayList<String> titlesList = new ArrayList<String>(); // array con los
															// titulos de las
															// canciones
	public static ArrayList<Bitmap> musicImageList = new ArrayList<Bitmap>(); // array con las
																// imagenes de
																// las canciones
	public static ArrayList<String> dataMusic = new ArrayList<String>(); // array con los path
															// de las canciones
	MediaMetadataRetriever mmr = new MediaMetadataRetriever(); // objeto que nos
																// permitira
																// obtener la
																// imagen de la
																// cancion

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		activity = this;
		//init_phone_music_grid();
		
		LoadSongs los = new LoadSongs(this , 0);
		los.doInBackground(null);
		
		// obtenemos la list view
		musiclist = (ListView) findViewById(R.id.PhoneMusicList);
		// creamos el adaptador pasandole el array de titulos y de
		// imagenes
		MusicAdapter musicAdapter = new MusicAdapter(this, titlesList,
				musicImageList);
		// añadimos el adaptador a la listview
		musiclist.setAdapter(musicAdapter);
		// indicamos qual sera el onclick de cada item de la listview
		musiclist.setOnItemClickListener(musicgridlistener);
		
	}

	private void init_phone_music_grid() {
		System.gc();

		try {
			ContentResolver contentResolver = getContentResolver();
			Uri uri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
			// cursor del cual podremos obtener el path de la cancion, el titulo
			// , y las imagenes
			musiccursor = contentResolver.query(uri, null, null, null, null);
			// definimos el tamaño de los array sabiendo cuantas canciones
			// tenemos
			// titlesList = new String[musiccursor.getCount()];
			// musicImageList = new Bitmap[musiccursor.getCount()];
			// dataMusic = new String[musiccursor.getCount()];
			if (musiccursor == null) {
				// query failed, handle error.
			} else if (!musiccursor.moveToFirst()) {
				// no media on the device
				Toast.makeText(this, "No sdcard presents", Toast.LENGTH_SHORT)
						.show();
				Log.d("songs", "no hay sdcard");
			} else {
				// obtenemos el indice de la columna en la que se guarda el
				// titulo de la cancion dentro del cursor
				int titleColumn = musiccursor
						.getColumnIndex(android.provider.MediaStore.Audio.Media.DISPLAY_NAME);
				// obtenemos el indice de la columna en la que se guarda el path
				// de la cancion dentro del cursor
				int data = musiccursor.getColumnIndex(Audio.Media.DATA);

				// recoremos de todas las filas del cursor
				for (int i = 0; i < musiccursor.getCount(); i++) {
					// obtenoms el titulo de la cancion de la columna de titulos
					// del cursor y lo añadimos al array correspondiente
					// titlesList[i] = musiccursor.getString(titleColumn);
					titlesList.add(musiccursor.getString(titleColumn));
					// obtenoms el path de la cancion de la columna de paths del
					// cursor y lo añadimos al array correspondiente
					// dataMusic[i] = musiccursor.getString(data);
					dataMusic.add(musiccursor.getString(data));

					// le pasamos al objeto que nos permitira obtener las
					// imagenes el path de la cancions
					mmr.setDataSource(musiccursor.getString(data));
					// obtenemos la imagen
					byte[] artBytes = mmr.getEmbeddedPicture();
					// si tiene imagen la añadimos al array corespondiente
					if (artBytes != null) {
						// pasamos la imagen de bytes a bitmap
						Bitmap bm = BitmapFactory.decodeByteArray(artBytes, 0,
								artBytes.length);
						// musicImageList[i] = bm;
						musicImageList.add(bm);
					} else {
						// por el contrario añadimos una imagen predeterminada
						Bitmap icon = BitmapFactory.decodeResource(
								this.getResources(),
								R.drawable.playerbackgroundicon);
						// musicImageList[i] = icon;
						musicImageList.add(icon);
					}
					// nos movemos a la siguiente fila del cursor
					musiccursor.moveToNext();
				}
				// obtenemos la list view
				musiclist = (ListView) findViewById(R.id.PhoneMusicList);
				// creamos el adaptador pasandole el array de titulos y de
				// imagenes
				MusicAdapter musicAdapter = new MusicAdapter(this, titlesList,
						musicImageList);
				// añadimos el adaptador a la listview
				musiclist.setAdapter(musicAdapter);
				// indicamos qual sera el onclick de cada item de la listview
				musiclist.setOnItemClickListener(musicgridlistener);
			}
		} catch (Exception e) {
        e.printStackTrace();
			Toast.makeText(this, "No sdcard presents", Toast.LENGTH_SHORT)
					.show();
		}

	}

	private OnItemClickListener musicgridlistener = new OnItemClickListener() {
		public void onItemClick(AdapterView parent, View v, int position,
				long id) {
			// añadimos la cancion seleccionada a la lista en la que estamos
			final Bitmap songBitmap = musicImageList.get(position);
			byte[] bitmapArr = Song.getByteArrayFromBitmap(musicImageList
					.get(position));
			Favorites f = SongInfo.getFavorite();
			Song s = new Song(titlesList.get(position),
					dataMusic.get(position), bitmapArr);

			AuxiliarAddSongToList aux = new AuxiliarAddSongToList(activity);
			aux.addSongToList(f, s, songBitmap);

			// --------------------- necesario para que se actualize
			// directamente la list view de las canciones de la lista
			ArrayList<String> titles = new ArrayList<String>();
			ArrayList<String> paths = new ArrayList<String>();
			ArrayList<Bitmap> image = new ArrayList<Bitmap>();

			SongInfo.setFavorite(f);
			for (int i = 0; i < f.getSongs().size(); i++) {
				titles.add(f.getSongs().get(i).getSongName());
				paths.add(f.getSongs().get(i).getSongPath());
				image.add(Song.getBitmapFromByteArray(f.getSongs().get(i)
						.getSongImage()));
			}

			SongInfo.setTitles(titles);
			SongInfo.setPaths(paths);
			SongInfo.setImages(image);
			// ----------------------------------------------------------------------------------

		

			finish();
		}
	};

	// @Override
	// public void onBackPressed() {
	// MusicPlayerActivity.setComesFromBacspace(true);
	// finish();
	// }

	

}
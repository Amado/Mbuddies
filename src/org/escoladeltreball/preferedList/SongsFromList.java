package org.escoladeltreball.preferedList;

import java.util.ArrayList;

import org.escoladeltreball.chat.R;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

/**
 * 
 * Activity que muestra un List view con una lista de canciones sacadas de una
 * lista de Favorites apartir del adaptador AdapterForSongsIntoList
 * 
 * @author Miquel Graells Monreal
 * 
 */

public class SongsFromList extends Activity implements OnClickListener {

	ListView musiclist;
	ArrayList<String> titlesList = new ArrayList<String>(); // array con los
															// titulos de las
															// canciones
	ArrayList<Bitmap> musicImageList = new ArrayList<Bitmap>(); // array con las
																// imagenes de
																// las canciones
	ArrayList<String> dataMusic = new ArrayList<String>(); // array con los path
															// de las canciones
	AdapterForSongsIntoList musicAdapter;

	ImageButton btnAddSong;
	TextView listName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_songs_from_list);

		listName = (TextView) findViewById(R.id.nameList);
		listName.setText(SongInfo.getFavorite().getListName());
		btnAddSong = (ImageButton) findViewById(R.id.btnAdd);
		btnAddSong.setOnClickListener(this);
		musiclist = (ListView) findViewById(R.id.PhoneMusicList);
		// creamos el adaptador pasandole el array de titulos y de
		// imagenes

		titlesList = SongInfo.getTitles();
		musicImageList = SongInfo.getImages();
		dataMusic = SongInfo.getPaths();

		Log.d("debuga esto si puedes", String.valueOf(titlesList.size()));

		musicAdapter = new AdapterForSongsIntoList(this, titlesList,
				musicImageList);
		// añadimos el adaptador a la listview
		musiclist.setAdapter(musicAdapter);
		// indicamos qual sera el onclick de cada item de la listview
		musiclist.setOnItemClickListener(musicgridlistener);
		musicAdapter.notifyDataSetChanged();
	}

	@Override
	protected void onResume() {
		super.onResume();
		musicAdapter.notifyDataSetChanged();
		Log.d("SongsFromList", "refresList2");
		onCreate(null);
	}

	private OnItemClickListener musicgridlistener = new OnItemClickListener() {
		public void onItemClick(AdapterView parent, View v, int position,
				long id) {
		}
	};

	/**
	 * Al apretar el boton de añadir cancion se lanza una nueva activity en la
	 * que se podra seleccionar la cancion a añadir
	 */
	@Override
	public void onClick(View v) {
		SongInfo.setActivity(this);
		Intent intent = new Intent(this, ChoseSongToList.class);
		this.startActivity(intent);
	}
}

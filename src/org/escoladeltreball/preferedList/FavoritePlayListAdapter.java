package org.escoladeltreball.preferedList;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.http.HttpException;
import org.escoladeltreball.chat.R;
import org.escoladeltreball.db4o.Db40DAOFavorites;
import org.escoladeltreball.db4o.Db4oDAOUser;
import org.escoladeltreball.db4o.Db4oHelper;
import org.escoladeltreball.db4o.Favorites;
import org.escoladeltreball.db4o.Song;
import org.escoladeltreball.db4o.User;
import org.escoladeltreball.manager.RequestManager;
import org.escoladeltreball.model.Session;
import org.escoladeltreball.musicPlayer.MusicPlayerActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.db4o.ObjectContainer;

/**
 * classe que customisa un listview para mostrar la lista de canciones favoritas.
 * tambien se encarga de gestionar los esdevenimientos cuando se clica en los botones de eliminar y reproducir lista
 * 
 * @author Miquel Graells Monreal
 *
 */

public class FavoritePlayListAdapter extends BaseAdapter implements OnClickListener {

	ArrayList<Favorites> fls = new ArrayList<Favorites>();
	LayoutInflater layoutInflater;	
	Activity activity;
	Db4oHelper helper = null;
	ObjectContainer oc = null;
	static View v;
	static FavoritePlayListAdapter thisAdapter;

	/**
	 * 
	 * @param activity
	 *            con la que inflaremos el layout
	 */
	public FavoritePlayListAdapter(Activity activity) {
		fls = new ArrayList<Favorites>();
		thisAdapter = this;
		layoutInflater = LayoutInflater.from(activity);
		this.activity = activity;
		// cargamos las listas de canciones
		try {
			helper = new Db4oHelper(activity);
			oc = helper.oc;
		} catch (Exception e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
		loadPreferedLists();
		Log.d("ListAdapter", "constructor done");
	}

	@Override
	public int getCount() {
		return fls.size();
	}

	@Override
	public Object getItem(int position) {
		return fls.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	// Si utilitzem el patró ViewHolder, el primer que farem és crear el model,
	// classe, per als objectes
	// que representen
	static class ViewHolder {
		TextView nameList;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			// Inflem el Layout de cada item
			convertView = layoutInflater.inflate(
					R.layout.itemfavoritesplaylist, null);
			holder = new ViewHolder();
			// Capturem els TextView
			holder.nameList = (TextView) convertView
					.findViewById(R.id.nameList);
			// Associem el viewholder,
			// la informació de l'estructura que hi ha d'haver dins el layout,
			// amb la vista que haurà de retornar aquest mètode.
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		TextView nameList = (TextView) convertView.findViewById(R.id.nameList);
		nameList.setTag(position);
		nameList.setOnClickListener(this);

		ImageButton deleteList = (ImageButton) convertView
				.findViewById(R.id.deleteList);
		deleteList.setTag(position);
		deleteList.setOnClickListener(this);

		ImageButton playList = (ImageButton) convertView
				.findViewById(R.id.playList);
		playList.setTag(position);

		playList.setOnClickListener(this);

		holder.nameList.setText("" + fls.get(position).getListName());
		return convertView;
	}

	/**
	 * metodo que recupera de la base de datos db40, todas las listas de Favorites y las guarda en una varriable
	 */
	public void loadPreferedLists() {
		try {
			fls = Db40DAOFavorites.getAllFavoritesFromDB(oc);
			if (fls == null) {
				fls = new ArrayList<Favorites>();
				Log.d("ListAdapter", "fls is fucking null");
			} else {
				Log.d("ListAdapter", "fls is NOT  null");
			}
		} catch (Exception e) {
			// Db4oHelper.displayDbError(activity, "error joder");
		}

		// obtenemos nuestro usuario
		User owner = null;
		try {
			helper = new Db4oHelper(activity);
			oc = helper.oc;
			owner = Db4oDAOUser.getUserOwnerFromDB(oc);
		} catch (Exception e) {
			e.printStackTrace();
//			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
		// ////////////////

		Iterator<Favorites> itPlayList = fls.iterator();
		while (itPlayList.hasNext()) {
			// Log.d("myappid",
			// "id 1 : "+String.valueOf(itPlayList.next().getIdUser()) +
			// " id 2 : " + String.valueOf(owner.getId()));
			// if(!itPlayList.next().getIdUser().equals(owner.getId())){
			if (!owner.getId().equals(itPlayList.next().getIdUser())) {

				itPlayList.remove();
				Log.d("myappid", "removed");
			}
		}

	}

	/**
	 * metodo en el que gestionamos que passa aundo se clica en el nombre, el
	 * boton delete o el boton de play de cada lista
	 */
	@Override
	public void onClick(View v) {

		int position = (Integer) v.getTag();

		// en el caso de que cliquemos en el nombre de la lista cargamos todas
		// las canciones de esa lista y las mostramos en una activity nueva
		if (v.getId() == R.id.nameList) {
			// titulo de la lista
			SongInfo songInfo = new SongInfo();

			String titleList = fls.get(position).getListName();
			// arrayList con las canciones de la lista
			ArrayList<Song> songs = fls.get(position).getSongs();

			ArrayList<String> songsTitles = new ArrayList<String>();
			ArrayList<String> songsPaths = new ArrayList<String>();
			ArrayList<Bitmap> songsImages = new ArrayList<Bitmap>();
			for (int i = 0; i < songs.size(); i++) {
				songsTitles.add(songs.get(i).getSongName());
				songsPaths.add(songs.get(i).getSongPath());
				byte[] bitmapArr = songs.get(i).getSongImage();
				Bitmap bm = Song.getBitmapFromByteArray(bitmapArr);
				songsImages.add(bm);
			}

			SongInfo.setTitles(songsTitles);
			SongInfo.setPaths(songsPaths);
			SongInfo.setImages(songsImages);

			SongInfo.setFavorite(fls.get(position));

			Intent intent = new Intent(activity, SongsFromList.class);
			activity.startActivity(intent);
		}
		// si apretamos el boton de borrar lista mostramos un dialogo pidiendole
		// la confirmacion al usuario y luego
		// borramos esa lista de db4o
		else if (v.getId() == R.id.deleteList) {
			FavoritePlayListAdapter.v = v;
			try {
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						activity);

				alertDialogBuilder.setTitle("Are you sure ?");
				alertDialogBuilder
						.setCancelable(false)
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										int position = (Integer) FavoritePlayListAdapter.v
												.getTag();

										/*
										 * Eliminamos los favoritos de la base
										 * de datos local y del servidor
										 */
										Db40DAOFavorites.deleteFavoritesFromDB(
												oc, fls.get(position));

										deleteFavoriteListOnServer(fls.get(
												position).getIdentification());

										// teneoms que actualiazar el listview
										// una vez borrada una
										// lista
										fls.remove(position);
										FavoritePlayListAdapter.thisAdapter
												.notifyDataSetChanged();
										// /////////////////////////////////
										Log.d("deleting list",
												String.valueOf(position));

									}
								})
						.setNegativeButton("Cancel",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								});

				// create an alert dialog
				AlertDialog alert = alertDialogBuilder.create();
				alert.show();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		// si apretamos el boton de reproducir lista cargamos todas las caniones
		// de la lista y se las passamos a la clase MusicPlayerActivity que es
		// la que
		// se encargara de reproducirlas
		else {

			// titulo de la lista
			String titleList = fls.get(position).getListName();
			// arrayList con las canciones de la lista
			ArrayList<Song> songs = fls.get(position).getSongs();

			// si la lista de canciones no esta vacia entonses podemos
			// reproducirla
			if (songs.size() > 0) {

				ArrayList<String> songsTitles = new ArrayList<String>();
				ArrayList<String> songsPaths = new ArrayList<String>();
				ArrayList<Bitmap> songsImages = new ArrayList<Bitmap>();
				for (int i = 0; i < songs.size(); i++) {
					songsTitles.add(songs.get(i).getSongName());
					songsPaths.add(songs.get(i).getSongPath());
					byte[] bitmapArr = songs.get(i).getSongImage();
					Bitmap bm = Song.getBitmapFromByteArray(bitmapArr);
					songsImages.add(bm);
				}

				SongInfo.setTitles(songsTitles);
				SongInfo.setPaths(songsPaths);
				SongInfo.setImages(songsImages);
				// un ves selecionado un item lo que hacemos es pasarle a la
				// activity del reproductor
				// una lista con los titulos de las canciones
				MusicPlayerActivity.setTitlesList(songsTitles);
				// una lista con las imagenes
				MusicPlayerActivity.setMusicImageList(songsImages);
				// otra lista con los path de las canciones
				MusicPlayerActivity.setDataMusic(songsPaths);
				// y por ultimo le indicamos cual sera la posicion por la que
				// tendra
				// que empear a reproducir
				// la lista, la posicion sera la misma que la posicion del item
				// que
				// se clica
				MusicPlayerActivity.setPosicionLlistaReproductor(0);
				// cerramos esta activity para volver a la activity del
				// reproductor
				MusicPlayerActivity.setComesFromBacspace(false);
				FavoritePlayList.comesFromSelectSong = true;
				activity.finish();
			}
		}

	}

	public void deleteFavoriteListOnServer(final String favoritesIdentification) {

		new Thread() {
			@Override
			public void run() {

				try {

					// Log.i(Config.TAG_INFO,
					// "addFavoritesToServer() ... locFavorites = "
					// + locFavorites);
					boolean processOk = false;

					if (favoritesIdentification != null) {
						processOk = RequestManager
								.removeFavorite(favoritesIdentification);
					}

					/**
					 * si no se ha podido eliminar la lista del servidor por
					 * algun motivo, almacenamos la operacion pendiente y la
					 * persistimos
					 */
					if (processOk == false) {
						throw new HttpException("Couldn't delete favorite on server at this moment");
					}

				} catch (Exception e) {
					e.printStackTrace();
					Session session = Session
							.getSession(FavoritePlayListAdapter.this.activity);
					session.getFavoriteListToRemove().add(
							favoritesIdentification);
					Session.saveSessionOnPreferences(FavoritePlayListAdapter.this.activity);
				}
			}
		}.start();

	}
}
package org.escoladeltreball.preferedList;

import org.escoladeltreball.db4o.Db40DAOFavorites;
import org.escoladeltreball.db4o.Db4oHelper;
import org.escoladeltreball.db4o.Favorites;
import org.escoladeltreball.db4o.Song;
import org.escoladeltreball.exception.HttpRequestException;
import org.escoladeltreball.manager.RequestManager;
import org.escoladeltreball.model.Session;
import org.escoladeltreball.support.Config;
import org.escoladeltreball.support.ImageConverter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.util.Log;

import com.db4o.ObjectContainer;

public class AuxiliarAddSongToList {

	Activity activity;
	ObjectContainer oc;
	Db4oHelper dbHelper;

	public AuxiliarAddSongToList(Activity activity) {
		this.activity = activity;
		try {
			dbHelper = new Db4oHelper(activity);
			oc = dbHelper.oc;
		} catch (Exception e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}

	}

	public void addSongToList(Favorites f, Song s, Bitmap songBitmap) {
		try {
			Db40DAOFavorites.addSongToFavorites(oc, f, s);
			Log.d("favoritePlayList", "list inserted");

		} catch (Exception e) {
			e.printStackTrace();
			Db4oHelper.displayDbError("otro  error");
		}

		try {
			// realizamos la misma operacion en el servidor
			addSongToServerFavorites(f, s, songBitmap);
		} catch (Exception e) {

		}
	}

	/**
	 * 
	 * @param locFavorites
	 * @param locSong
	 */
	public void addSongToServerFavorites(final Favorites locFavorites,
			final Song locSong, final Bitmap songImgBitmap) {

		new Thread() {
			@Override
			public void run() {
				boolean processOk = false;
				try {
					Session.getSession(activity);
					final Long idUser = Session.getUser(activity).getId();

					final byte[] lowBitmapArr = Song
							.getByteArrayFromBitmap(songImgBitmap);

					String imgEncoded;
					if (lowBitmapArr == null) {
						imgEncoded = "";
					} else {
						Log.i(Config.TAG_INFO,
								"AuxiliarSongTolist- songImage = "
										+ locSong.getSongImage());
						imgEncoded = ImageConverter.encodeImage(lowBitmapArr);

					}

					if (locFavorites != null && locSong != null
							&& idUser != null) {
						processOk = RequestManager.addSongToFavorites(
								locFavorites.getIdentification(),
								locFavorites.getListName(),
								String.valueOf(idUser), imgEncoded,
								locSong.getSongPath(), locSong.getSongName(),
								"");

					}

					if (processOk == false) {
						throw new HttpRequestException(
								"Error al agregar la cancion en el servidor en este momento");

					}

				} catch (Exception e) {
					e.printStackTrace();
					// locFavorites.setIsListSynchronizedWithServer(false);

					/**
					 * si no se puede agregar la cancion en este momento se
					 * agrega a la lista
					 */
					Session session = Session.getSession(activity);
					session.getFavoriteListToUpdate().add(
							locFavorites.getIdentification());
					Session.saveSessionOnPreferences(activity);

				}
			}
		}.start();

	}
}
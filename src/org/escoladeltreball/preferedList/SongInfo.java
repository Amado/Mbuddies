package org.escoladeltreball.preferedList;

import java.util.ArrayList;

import org.escoladeltreball.db4o.Favorites;
import org.escoladeltreball.db4o.Song;

import android.app.Activity;
import android.graphics.Bitmap;

/**
 * 
 * Al no poder passar muchos objetos atravez de intent lo que hacemos es usar
 * esta clase a modo de puente entre activities Por ejemplo para que una
 * activity que muestra las canciones de una lista de favoritos pueda recuperar
 * la lista de canciones previemnte insertadas en esta clase * 
 * 
 * @author Miquel Graells Monreal
 * 
 */

public class SongInfo {

	private static Activity activity;
	private static Favorites favorite;
	private static ArrayList<String> titles;
	private static ArrayList<String> paths;
	private static ArrayList<Bitmap> images;
	private static ArrayList<Song> songList;

	public SongInfo(){
		
	}
	
	public SongInfo(ArrayList<Song> songList2) {
		SongInfo.songList = songList2;
	}

	public SongInfo(ArrayList<String> titles, ArrayList<String> paths,
			ArrayList<Bitmap> images) {
		super();
		this.titles = titles;
		this.paths = paths;
		this.images = images;
		SongInfo.songList = null;
	}

	/**
	 * 
	 * @return arrayList con los titulos de las canciones
	 */
	public static ArrayList<String> getTitles() {
		return titles;
	}

	/**
	 * 
	 * @param titles2 arrayList con los titulos de las canciones
	 */
	public static void setTitles(ArrayList<String> titles2) {
		titles = titles2;
	}

	/**
	 * 
	 * @return arrayList con los directorios de las canciones
	 */
	public static ArrayList<String> getPaths() {
		return paths;
	}

	/**
	 * 
	 * @param paths2 arrayList con los directrios de las canciones
	 */
	public static void setPaths(ArrayList<String> paths2) {
		paths = paths2;
	}

	/**
	 * 
	 * @return arrayList con las imagenes de las canciones
	 */
	public static ArrayList<Bitmap> getImages() {
		return images;
	}

	/**
	 * 
	 * @param images2 arrayList con las imagenes de las canciones
	 */
	public static void setImages(ArrayList<Bitmap> images2) {
		images = images2;
	}

	/**
	 * 
	 * @return arrayList con los objetos Song
	 */
	public static ArrayList<Song> getSongList() {
		return songList;
	}

	/**
	 * 
	 * @param songList arrayList con los objetos Song
	 */
	public static void setSongList(ArrayList<Song> songList) {
		SongInfo.songList = songList;
	}

	/**
	 * 
	 * @return favorite
	 */
	public static Favorites getFavorite() {
		return favorite;
	}

	/**
	 * 
	 * @param favorite
	 */
	public static void setFavorite(Favorites favorite) {
		SongInfo.favorite = favorite;
	}

	/**
	 * 
	 * @return activity
	 */
	public static Activity getActivity() {
		return activity;
	}

	/**
	 * 
	 * @param activity
	 */
	public static void setActivity(Activity activity) {
		SongInfo.activity = activity;
	}

}

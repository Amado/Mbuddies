package org.escoladeltreball.preferedList;

import org.escoladeltreball.db4o.Db40DAOFavorites;
import org.escoladeltreball.db4o.Db4oDAOUser;
import org.escoladeltreball.db4o.Db4oHelper;
import org.escoladeltreball.db4o.Favorites;
import org.escoladeltreball.db4o.User;
import org.escoladeltreball.exception.HttpRequestException;
import org.escoladeltreball.manager.RequestManager;
import org.escoladeltreball.model.Session;
import org.escoladeltreball.support.Config;

import android.app.Activity;
import android.util.Log;

import com.db4o.ObjectContainer;

public class AuxiliarAddList {

	Activity activity;
	ObjectContainer oc;
	Db4oHelper dbHelper;
	User emilio;

	public AuxiliarAddList(Activity activity) {
		this.activity = activity;

		try {
			dbHelper = new Db4oHelper(activity);
			oc = dbHelper.oc;
		} catch (Exception e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}

	}

	public void addList(Favorites f) {
		try {
			Db40DAOFavorites.insertFavoritesToDB(oc, f);
			Log.d("favoritePlayList", "list inserted");

			addFavoriteToServer(f, activity);
		} catch (Exception e) {
			Db4oHelper.displayDbError("otro puto error");
		}
	}

	// para cambiar la foto del User creado
	public void changeUserPhoto() {

		// insertamos en la db el usuario propietario
		Db4oDAOUser.insertUserToDB(oc, emilio);
		// le cambiamos la foto para hacer una prueba
		Db4oDAOUser.changeOwnerPhoto(oc, 1);
		// obtenemos el usuario y vemos sus datos por LogCat
		User u = Db4oDAOUser.getUserOwnerFromDB(oc);
		Log.i("Log.i", u.toString());
	}

	/*
	 * addOrUpdateFavorite( final String favoritesIdentification, final String
	 * favoritesNewName, final String idUser)
	 */

	/**
	 * Agrega la lista de favoritos en el servidor (si ya existe no hace nada)
	 * 
	 * @param locFavorites
	 * @param locSong
	 */
	public void addFavoriteToServer(final Favorites locFavorites,
			final Activity aActivity) {

		new Thread() {
			@Override
			public void run() {

				try {
					Session.getSession(activity);
					final Long idUser = Session.getUser(activity).getId();
					Log.i(Config.TAG_INFO,
							"addFavoritesToServer() ... locFavorites = "
									+ locFavorites);
					boolean processOk = false;
					if (locFavorites != null && idUser != null) {
						processOk = RequestManager.addOrUpdateFavorite(
								locFavorites.getIdentification(),
								locFavorites.getListName(),
								String.valueOf(idUser));
					}

					if (processOk == false) {
						throw new HttpRequestException(
								"Error al a la base de datos del servidor");
					}

				} catch (Exception e) {
					e.printStackTrace();
					// locFavorites.setIsListSynchronizedWithServer(false);
					Session session = Session.getSession(aActivity);
					session.getFavoriteListToUpdate().add(
							locFavorites.getIdentification());
					Session.saveSessionOnPreferences(aActivity);

				}
			}
		}.start();

	}
}
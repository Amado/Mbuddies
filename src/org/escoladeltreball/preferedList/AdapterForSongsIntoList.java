package org.escoladeltreball.preferedList;

import java.util.ArrayList;

import org.escoladeltreball.chat.R;
import org.escoladeltreball.db4o.Db40DAOFavorites;
import org.escoladeltreball.db4o.Db4oHelper;
import org.escoladeltreball.db4o.Favorites;
import org.escoladeltreball.db4o.Song;
import org.escoladeltreball.exception.HttpRequestException;
import org.escoladeltreball.manager.RequestManager;
import org.escoladeltreball.model.Session;
import org.escoladeltreball.musicPlayer.MusicPlayerActivity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.db4o.ObjectContainer;

/**
 * 
 * Adaptador que muesta las canciones de una lista de favoritos y gestiona que
 * passa cuando se borra una cancion o se selecciona para reproducir
 * 
 * @author Miquel Graells Monreal
 * 
 */

public class AdapterForSongsIntoList extends BaseAdapter implements
		OnClickListener {

	ArrayList<String> songsTitles;
	ArrayList<Bitmap> songsImages;
	LayoutInflater layoutInflater;
	Activity activity;
	Db4oHelper helper = null;
	ObjectContainer oc = null;

	/**
	 * 
	 * @param activity
	 *            activity que usaremos para inflar el layout
	 * @param song
	 *            arrayList con los titulos de las canciones
	 * @param songsImages
	 *            arrayList con las imagenes de las canciones
	 */
	public AdapterForSongsIntoList(Activity activity, ArrayList<String> song,
			ArrayList<Bitmap> songsImages) {
		this.activity = activity;
		this.songsTitles = song;
		this.songsImages = songsImages;
		layoutInflater = LayoutInflater.from(activity);
		try {
			helper = new Db4oHelper(activity);
			oc = helper.oc;
		} catch (Exception e) {
			e.printStackTrace();
			Db4oHelper.displayDbError(Db4oHelper.ERROR_DB);
		}
	}

	@Override
	public int getCount() {
		return songsTitles.size();
	}

	@Override
	public Object getItem(int position) {
		return songsTitles.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	// Si utilitzem el patró ViewHolder, el primer que farem és crear el model,
	// classe, per als objectes
	// que representen
	static class ViewHolder {
		TextView nameSong;
		ImageView imageSong;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			// Inflem el Layout de cada item
			convertView = layoutInflater
					.inflate(R.layout.itemsongintlist, null);
			holder = new ViewHolder();
			// Capturem els TextView
			holder.nameSong = (TextView) convertView
					.findViewById(R.id.strangeWord);
			holder.imageSong = (ImageView) convertView
					.findViewById(R.id.imageArtist);
			ImageButton deleteList = (ImageButton) convertView
					.findViewById(R.id.deleteSong);
			deleteList.setTag(position);
			deleteList.setOnClickListener(this);
			// Associem el viewholder,
			// la informació de l'estructura que hi ha d'haver dins el layout,
			// amb la vista que haurà de retornar aquest mètode.
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		TextView nameList = (TextView) convertView
				.findViewById(R.id.strangeWord);
		nameList.setTag(position);
		nameList.setOnClickListener(this);

		holder.nameSong.setText("" + songsTitles.get(position));
		holder.imageSong.setImageBitmap(songsImages.get(position));
		return convertView;
	}

	/**
	 * gestionamos que ocurre al clicar en el nombre de la cancion o de borrar
	 * cancion
	 */
	@Override
	public void onClick(View v) {

		// Cuando clicamos en el nombre la cancion cargamos las listas de
		// nombres , directorios e imagenes, la posicion en la que empezara a
		// reproducirse la lista y se las pasamos a la activiy encargada de la
		// reproduccion
		// (MusicPlayerActivity)

		if (v.getId() == R.id.strangeWord) { // titulo cancion
			int position = (Integer) v.getTag();

			ArrayList<String> titlesList = SongInfo.getTitles();
			ArrayList<Bitmap> musicImageList = SongInfo.getImages();
			ArrayList<String> dataMusic = SongInfo.getPaths();

			MusicPlayerActivity.setTitlesList(titlesList);
			// una lista con las imagenes
			MusicPlayerActivity.setMusicImageList(musicImageList);
			// otra lista con los path de las canciones
			MusicPlayerActivity.setDataMusic(dataMusic);
			// y por ultimo le indicamos cual sera la posicion por la que tendra
			// que empear a reproducir
			// la lista, la posicion sera la misma que la posicion del item que
			// se clica
			MusicPlayerActivity.setPosicionLlistaReproductor(position);
			// cerramos esta activity para volver a la activity del reproductor
			MusicPlayerActivity.setComesFromBacspace(false);

			FavoritePlayList.comesFromSelectSong = true;
			MusicPlayerActivity.setComesFromNewSong(true);

			activity.finish();
		} else {
			// en el caso de que el usuario aprite el boton de eliminar cancion
			// borramos esa cancion de su lista en db4o

			int position = (Integer) v.getTag();
			Favorites fls = SongInfo.getFavorite();
			ArrayList<Song> songs = fls.getSongs(); // SongInfo.getSongList();
			Song s = songs.get(position);

			try {
				Db40DAOFavorites.deleteSongFromFavorites(oc, fls, s);
				Log.d("deleting list", String.valueOf(position));

				// tenemos que actualiazar el listview una vez borrada una lista
				songsTitles.remove(position);
				songsImages.remove(position);
				this.notifyDataSetChanged();
				// /////////////////////////////////
				removeSongFromFavorites(oc, fls, s, position);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

	public void removeSongFromFavorites(final ObjectContainer oc,
			final Favorites fls, final Song s, final Integer position) {
		new Thread() {
			@Override
			public void run() {

				try {
					Db40DAOFavorites.deleteSongFromFavorites(oc, fls, s);
					// Log.d("deleting list", String.valueOf(position));

					// tenemos que actualiazar el listview una vez borrada una
					// lista
					songsTitles.remove(position);
					songsImages.remove(position);

					activity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							AdapterForSongsIntoList.this.notifyDataSetChanged();
						}
					});

				} catch (Exception e) {
					e.printStackTrace();
					fls.setIsListSynchronizedWithServer(false);
					Db40DAOFavorites
							.setSynchonizedWithServerStatusToSingleList(oc,
									fls, false);

				}
				try {
					Long idUser = Session.getSession(activity).getUser()
							.getId();

					boolean processOk = RequestManager.removeSongFromFavorites(
							fls.getIdentification(), s.getSongPath(),
							String.valueOf(idUser), fls.getListName());

					if (processOk == false) {
						throw new HttpRequestException(
								"No se ha podido eliminar la cancion de la lista en este momento ");
					}

				} catch (Exception e) {
					// UtilMessage.showErrorMessageOnMainThread(
					// "Error Updating server Favority list", activity);

					Session session = Session.getSession(activity);
					session.getFavoriteListToUpdate().add(
							fls.getIdentification());
					Session.saveSessionOnPreferences(activity);
				}

			}
		}.start();

	}
}
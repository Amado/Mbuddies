package org.escoladeltreball.friendshipRequests;

import java.util.ArrayList;

import org.escoladeltreball.activities.ActionActivity;
import org.escoladeltreball.activities.UtilActivity;
import org.escoladeltreball.activities.UtilMessage;
import org.escoladeltreball.chat.R;
import org.escoladeltreball.db4o.Db4oHelper;
import org.escoladeltreball.db4o.User;
import org.escoladeltreball.exception.HttpRequestException;
import org.escoladeltreball.manager.RequestManager;
import org.escoladeltreball.model.Session;
import org.escoladeltreball.model.json.ListFriendRequest.RequestModel;
import org.escoladeltreball.support.Config;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Toast;

import com.db4o.ObjectContainer;

/**
 * Clase que Adapta el contenido de un ArrayList<User> a un ListView.
 * 
 * @author Emilio Amado 2015
 * 
 */
public class FriendshipRequestsListAdapter extends BaseAdapter {

	private ObjectContainer oc;
	private Db4oHelper dbHelper;
	private User owner;

	private static final String MODE_ACCEPT = "Accept";
	private static final String MODE_DECLINE = "Decline";

	private FriendshipRequestsActivity activity;
	private ArrayList<RequestModel> usersFriendshipRequestsList;
	private LayoutInflater layoutInflater;
	private int maxWidthForTextView;
	private ViewHolder holder;

	private RequestModel user = null;

	public FriendshipRequestsListAdapter(FriendshipRequestsActivity activity,
			ArrayList<RequestModel> usersFriendshipRequestsList) {
		this.activity = activity;
		this.usersFriendshipRequestsList = usersFriendshipRequestsList;
		this.layoutInflater = LayoutInflater.from(activity);

		Display display = activity.getWindowManager().getDefaultDisplay();
		int displayWidth = display.getWidth();
		int maxWidthForTextView = (int) displayWidth / 2;

		try {
			dbHelper = new Db4oHelper(activity);
			oc = dbHelper.oc;
		} catch (Exception e) {
			e.printStackTrace();
			// Db4oHelper.displayDbError(this, Db4oHelper.ERROR_DB);
		}
	}

	@Override
	public int getCount() {
		return usersFriendshipRequestsList == null ? 0
				: usersFriendshipRequestsList.size();
	}

	@Override
	public Object getItem(int position) {
		return usersFriendshipRequestsList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	static class ViewHolder {
		Button profile;
		Button name;
		Button accept;
		Button refuse;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		final RequestModel requestModel = usersFriendshipRequestsList
				.get(position);

		Log.i(Config.TAG_INFO, "requestModel = " + requestModel.toString());

		if (convertView == null) {
			// Inflem el Layout de cada item
			convertView = layoutInflater.inflate(
					R.layout.item_user_friendship_request, null);
			holder = new ViewHolder();
			// Capturem els TextView
			holder.profile = (Button) convertView
					.findViewById(R.id.btUserFriendshipRequestProfile);
			holder.name = (Button) convertView
					.findViewById(R.id.btUserFriendshipRequestName);
			holder.name.setMaxWidth(maxWidthForTextView);
			holder.name.setMaxLines(1);

			holder.accept = (Button) convertView
					.findViewById(R.id.btUserFriendshipRequestAccept);
			holder.refuse = (Button) convertView
					.findViewById(R.id.btUserFriendshipRequestRefuse);

			// Associem el viewholder,
			// la informació de l'estructura que hi ha d'haver dins el layout,
			// amb la vista que haurà de retornar aquest mètode.
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.accept
				.setBackgroundResource(R.drawable.bt_accept_friendship_images);
		holder.refuse
				.setBackgroundResource(R.drawable.bt_decline_friendship_images);
		holder.name.setBackgroundResource(R.drawable.bt_friend_name_images);

		holder.name.setText(requestModel.getNameSrcUser());

		final Long friendId = requestModel.getIdSrcUser();

		holder.profile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				viewProfile(requestModel.getIdSrcUser());
			}
		});

		holder.name.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ActionActivity.diplayFriendOptionsDialog(
						requestModel.getIdSrcUser(),
						requestModel.getNameSrcUser(),
						requestModel.getImgSrcUser(), activity,
						FriendshipRequestsActivity.class);

			}
		});

		holder.accept.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				diplayFriendRequestOptionsDialog(MODE_ACCEPT, requestModel,
						friendId);
			}
		});

		holder.refuse.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				diplayFriendRequestOptionsDialog(MODE_DECLINE, requestModel,
						friendId);
			}
		});

		user = usersFriendshipRequestsList.get(position);
		Integer friendAvatar = user.getImgSrcUser();

		UtilActivity.setProfileBackgroundFromIdImg(holder.profile,
				friendAvatar, true);
		return convertView;
	}

	public void viewProfile(Long id) {
		ActionActivity.viewProfile(id, activity,
				FriendshipRequestsActivity.class);
	}

	/**
	 * Despliega en pantalla un AlertDialog que pide la confirmación en las
	 * operaciones de acceptar o rechazar una petición de amistad.
	 * 
	 * @param mode
	 *            entrar como modo Accept o Decline
	 */
	public void diplayFriendRequestOptionsDialog(final String mode,
			final RequestModel requestModel, final Long idFriend) {

		AlertDialog.Builder friendshipRequestDialog = new AlertDialog.Builder(
				activity);
		friendshipRequestDialog.setTitle(mode + " friendship request?");
		friendshipRequestDialog.setMessage(requestModel.getNameSrcUser());

		friendshipRequestDialog.setPositiveButton("Confirm",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {

						if (mode.equals(MODE_ACCEPT)) {
							addFriend(String.valueOf(idFriend));

						} else if (mode.equals(MODE_DECLINE)) {
							dismisFriendRequest(String.valueOf(idFriend));

							// displaySuccess("Success on decline "
							// + requestModel.getNameSrcUser()
							// + " as a friend");
							//
							// Intent friendshipRequestActivityIntent = new
							// Intent(
							// activity, FriendshipRequestsActivity.class);
							// activity.startActivity(friendshipRequestActivityIntent);
						}
					}
				});

		friendshipRequestDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						dialog.cancel();
					}
				});

		friendshipRequestDialog.show();
	}

	/**
	 * Muestra un Toast con mensaje de éxito en la operación. // * // * @param
	 * activity // * Activity donde se muestra el Toast //
	 */
	// public void displaySuccess(final String message) {
	// activity.runOnUiThread(new Runnable() {
	// public void run() {
	// Toast toast = Toast.makeText(activity, message,
	// Toast.LENGTH_LONG);
	// View v = toast.getView();
	// v.setBackgroundColor(-16776961);
	// toast.setGravity(Gravity.CENTER | Gravity.TOP, 0, 20);
	// toast.show();
	// }
	// });
	// }

	public void addFriend(final String idFriend) {

		final ProgressDialog dialog = UtilMessage.showProgressDialog(activity,
				"Please wait....");
		new Thread() {
			@Override
			public void run() {

				try {
					boolean process = false;

					Session.getSession(activity);
					final Long idUser = Session.getUser(activity).getId();

					final String myId = String.valueOf(Session
							.getSession(activity).getUser().getId());

					process = RequestManager.addFriend(myId, idFriend);

					if (process == false) {
						throw new HttpRequestException(
								"Error al procesar la peticion addFriend");
					}
					final Long idFriendL = Long.parseLong(idFriend);
					activity.runOnUiThread(new Runnable() {

						@Override
						public void run() {
							ArrayList<RequestModel> request = activity
									.getFriendshiprequestsList();

							ArrayList<RequestModel> tmpRequest = new ArrayList<RequestModel>();
							for (RequestModel requestModel : request) {
								if (requestModel.getIdSrcUser().equals(
										idFriendL) == false) {
									tmpRequest.add(requestModel);
								}
							}

							// FriendshipRequestActivity para que se refresquen
							// los datos, volviendo a hacer un ataque al server
							Intent friendshipRequestActivityIntent = new Intent(
									activity, FriendshipRequestsActivity.class);
							friendshipRequestActivityIntent
									.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
							activity.startActivity(friendshipRequestActivityIntent);

							// activity.setFriendshiprequestsList(tmpRequest);
							// activity.setFriendshipRequestsListAdapter(new
							// FriendshipRequestsListAdapter(
							// activity, tmpRequest));
							// activity.getFriendshipRequestsListAdapter()
							// .notifyDataSetChanged();

						}
					});
				} catch (Exception e) {
					e.printStackTrace();
					activity.runOnUiThread(new Runnable() {

						@Override
						public void run() {
							UtilMessage
									.showWarningMessage(activity,
											"coulnd't connect to server at this moment\nTry later");

						}

					});
				} finally {
					UtilMessage.dismisProgressDialog(dialog);
				}
			}
		}.start();

	}

	public void dismisFriendRequest(final String idFriend) {

		final ProgressDialog dialog = UtilMessage.showProgressDialog(activity,
				"Please wait....");
		new Thread() {
			@Override
			public void run() {

				try {
					boolean process = false;

					Session.getSession(activity);
					final Long idUser = Session.getUser(activity).getId();

					final String myId = String.valueOf(Session
							.getSession(activity).getUser().getId());

					process = RequestManager
							.dismisFriendRequest(myId, idFriend);

					if (process == false) {
						throw new HttpRequestException(
								"Error al procesar la peticion dismisFriendRequest");
					}
					final Long idFriendL = Long.parseLong(idFriend);

					activity.runOnUiThread(new Runnable() {

						@Override
						public void run() {
							ArrayList<RequestModel> request = activity
									.getFriendshiprequestsList();

							ArrayList<RequestModel> tmpRequest = new ArrayList<RequestModel>();
							for (RequestModel requestModel : request) {
								if (requestModel.getIdSrcUser().equals(
										idFriendL) == false) {
									tmpRequest.add(requestModel);
								}
							}

							Intent friendshipRequestActivityIntent = new Intent(
									activity, FriendshipRequestsActivity.class);
							friendshipRequestActivityIntent
									.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
							activity.startActivity(friendshipRequestActivityIntent);

							// activity.setFriendshiprequestsList(tmpRequest);
							// activity.setFriendshipRequestsListAdapter(new
							// FriendshipRequestsListAdapter(
							// activity, tmpRequest));
							// activity.getFriendshipRequestsListAdapter()
							// .notifyDataSetChanged();

						}
					});
				} catch (Exception e) {
					e.printStackTrace();
					activity.runOnUiThread(new Runnable() {

						@Override
						public void run() {
							UtilMessage
									.showWarningMessage(activity,
											"coulnd't connect to server at this moment\nTry later");

						}

					});

				} finally {
					UtilMessage.dismisProgressDialog(dialog);
				}
			}
		}.start();

	}

}
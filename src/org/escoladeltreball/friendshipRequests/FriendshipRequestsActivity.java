package org.escoladeltreball.friendshipRequests;

import java.util.ArrayList;

import org.escoladeltreball.activities.UtilMessage;
import org.escoladeltreball.chat.R;
import org.escoladeltreball.manager.RequestManager;
import org.escoladeltreball.model.Session;
import org.escoladeltreball.model.json.ListFriendRequest.RequestModel;
import org.escoladeltreball.model.server.persistence.UsersServ;
import org.escoladeltreball.support.Config;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewStub;
import android.widget.ListView;

/**
 * Activity que despliega un ListView con los User que han enviado peticiones de
 * amistad al User owner de ESTE dispositivo. Dado el usuario propietario se
 * ataca al server con los datos del owner y se obtiene un ArrayList<User> de
 * los usuarios que han hecho peticiones de amistad.
 * 
 * @author Emilio Amdo 2015
 *
 */
public class FriendshipRequestsActivity extends Activity {

	private ListView listView;
	private FriendshipRequestsListAdapter friendshipRequestsListAdapter;
	private ViewStub tvEmpty;
	private ArrayList<RequestModel> friendshiprequestsList = new ArrayList<RequestModel>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_friendship_requests);

		listView = (ListView) findViewById(R.id.lvUsersFriendshipRequests);
		tvEmpty = (ViewStub) findViewById(R.id.emptyFriendRequest);
		friendshipRequestsListAdapter = new FriendshipRequestsListAdapter(this,
				friendshiprequestsList);
		listView.setAdapter(friendshipRequestsListAdapter);
		listView.setEmptyView(tvEmpty);
	}

	@Override
	public void onBackPressed() {
		this.finish();
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.i("INFO",
				"..................    findFriendsRequest    ............................");
		findFriendsRequest();
	}

	public void findFriendsRequest() {

		final UsersServ user = Session.getSession(
				FriendshipRequestsActivity.this).getUser();
		final Long idUser = user.getId();

		final ProgressDialog dialog = UtilMessage.showProgressDialog(
				FriendshipRequestsActivity.this,
				"Connecting with the server.... please wait");

		FriendshipRequestsActivity.this.friendshiprequestsList.clear();

		new Thread(new Runnable() {
			public void run() {
				try {

					final ArrayList<RequestModel> responseServReqsModel = (ArrayList<RequestModel>) RequestManager
							.fecthFriendsRequest(String.valueOf(idUser));

					if (responseServReqsModel != null
							&& responseServReqsModel.isEmpty() == false) {
						Log.i(Config.TAG_INFO, "favorites =  "
								+ responseServReqsModel);

						FriendshipRequestsActivity.this
								.runOnUiThread(new Runnable() {

									@Override
									public void run() {
										FriendshipRequestsActivity.this.friendshiprequestsList
												.addAll(responseServReqsModel);
										FriendshipRequestsActivity.this.friendshipRequestsListAdapter
												.notifyDataSetChanged();

									}
								});

					}

				} catch (Exception e) {
					e.printStackTrace();
					FriendshipRequestsActivity.this
							.runOnUiThread(new Runnable() {

								@Override
								public void run() {
									final String erroMsg = "Error retrieving the friend request list "
											+ " from Server. Try Later";
									UtilMessage.showErrorMessage(
											FriendshipRequestsActivity.this,
											erroMsg);

								}
							});
				} finally {
					FriendshipRequestsActivity.this
							.runOnUiThread(new Runnable() {
								@Override
								public void run() {
									UtilMessage.dismisProgressDialog(dialog);
								}
							});
				}
			}

		}).start();
	}

	public ArrayList<RequestModel> getFriendshiprequestsList() {
		return friendshiprequestsList;
	}

	public void setFriendshiprequestsList(
			ArrayList<RequestModel> friendshiprequestsList) {
		this.friendshiprequestsList = friendshiprequestsList;

	}

	public FriendshipRequestsListAdapter getFriendshipRequestsListAdapter() {
		return friendshipRequestsListAdapter;
	}

	public void setFriendshipRequestsListAdapter(
			FriendshipRequestsListAdapter friendshipRequestsListAdapter) {
		this.friendshipRequestsListAdapter = friendshipRequestsListAdapter;
	}

}
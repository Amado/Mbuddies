package org.escoladeltreball.chat.service;

import java.net.URI;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.escoladeltreball.activities.ChatActivity;
import org.escoladeltreball.activities.UtilActivity;
import org.escoladeltreball.activities.UtilMessage;
//import org.escoladeltreball.chat.service.ChatIntentService; 
import org.escoladeltreball.model.Contact;
import org.escoladeltreball.model.Message;
import org.escoladeltreball.model.Session;
import org.escoladeltreball.model.server.persistence.UsersServ;
import org.escoladeltreball.support.Config;
import org.escoladeltreball.support.UtilsChat;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.codebutler.android_websockets.WebSocketClient;

public class ChatIntentService extends Service {

	private String URL = null;
	private int numStarts = 0;
	private UsersServ user;
	//
	private static Long userIdF;
	private static String userNameF;

	// almacena el contacto que se ha se seleccionado la ultima vez
	private static Long selectedContactId;
	private static Integer selectedContactPosition;
	private static WebSocketClient client;
	// private static ChatBroadcastReceiver receiver;
	private static UtilsChat utilsChat;
	private static ChatActivity chatActivity;
	private static boolean isConnected = false;
	private static Integer selectedContactPos = null;
	private static String cityGeoLoc, contryGeoLoc;
	private static int numMessagesNotSeeing;

	// private static ChatActivity chatActivity;

	/*
	 * map que contiene los mensajes del usuario guardan en el intent service,
	 * para que no se pierdan los mensajes al destruirse la activity del chat
	 */
	private static HashMap<Long, List<Message>> mapListMessages = new HashMap<Long, List<Message>>();
	// hacemos lo mismo con la lista de contactos conectados
	private static List<Contact> listContacts = new ArrayList<Contact>();

	//

	//
	private static Session session;
	public static final Long LIST_MESSAGES_SHARED = -1L;

	// private static UtilsChat utilsChat;

	@Override
	public IBinder onBind(Intent intent) {
		Log.d("SERVICIO DEMO",
				"****************** ServiceDemo ON CREATE **************************************");
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();

	}

	@Override
	public void onStart(Intent intent, int startId) {
		Log.d("Info", "CHAT INTENT SERVICE ON START!!!!!!!!!!!!!!!!!!");

		ChatIntentService.this.createChatClientConnection();
		// new Thread() {
		// @Override
		// public void run() {
		//
		// try {
		//
		// ChatIntentService.this.createChatClientConnection();
		// } catch (Throwable e) {
		// e.printStackTrace();
		// }
		//
		// }
		// }.start();

	}

	@Override
	public void onDestroy() {
		// unregisterReceiver(receiver);
		try {
			client.disconnect();

		} catch (Throwable e) {
			Log.w("WARINING", "ERROR DISCONNECTING THE CLIENT !!!!");
			e.printStackTrace();
		}
		System.out.println("El servicio a Terminado");
		super.onDestroy();
	}

	public void createChatClientConnection() {
		Log.i(Config.TAG_INFO,
				"**************************************************** "
						+ "INICIANDO EL SERVICIO DE CHAT (ChatIntentService - createClientConnection)"
						+ "****************************************************");

		this.utilsChat = new UtilsChat(this.getApplicationContext());
		List<BasicNameValuePair> extraHeaders = Arrays
				.asList(new BasicNameValuePair("Cookie", "session=abcd"));

		// String username = this.user.getName();
		user = Session.getSession(chatActivity).getUser();
		userIdF = user.getId();
		userNameF = user.getName();
		Log.i(Config.TAG_INFO,
				"Connecting to chat websocket server as username = "
						+ user.getName());
		String urlSocket = Config.URL_WEBSOCKET
				+ URLEncoder.encode(String.valueOf(userIdF));
		Log.i(Config.TAG_INFO, "urlSocket -> " + urlSocket);
		if (userIdF != null) {
			this.setClient(new WebSocketClient(URI.create(urlSocket),
					new ChatServiceListener(this), extraHeaders));
			Log.i(Config.TAG_INFO,
					"**************************************************** "
							+ " CONECTANDO CON EL CLIENTE ........)"
							+ "****************************************************");

			client.connect();

			// userId = -33L;
		} else {
			Log.w(Config.TAG_WARNING,
					"Warning couldn't create a chat connection, no username was found on android device session "
							+ userIdF);
		}

		ChatIntentService.getMapListMessages().put(
				ChatIntentService.LIST_MESSAGES_SHARED,
				new ArrayList<Message>());

		/*
		 * inicializamos la sesion
		 */
		if (session == null) {
			session = Session.getSession(chatActivity);
		}
		ChatIntentService.isConnected = true;
	}

	/**
	 * only for test purposes
	 */
	public void showRuningMessages() {
		new Thread() {
			@Override
			public void run() {
				int i = 0;
				while (true) {
					i++;
					Log.i(Config.TAG_INFO,
							" the chat service is runing ....  numStarts: "
									+ numStarts + "  cycle: " + i + i);
				}
			}
		}.start();
	}

	public static boolean isConnected() {
		return isConnected;
	}

	public static void setConnected(boolean isConnected) {
		ChatIntentService.isConnected = isConnected;
	}

	/**
	 * 
	 * clase interna que gestiona, dentro del intent service los eventos
	 * lanzados por el web socket implementando los metodos del
	 * WebSocketClient.Listener, al realizarse un call back en onConnect,
	 * onMessage, onDisconnet o onError realizar un broadcast con los datos
	 * necesarios para que el ChatSocketListenerOnService que
	 * ChatBroadcastReceiver realice las operaciones correspondientes en la gui.
	 * 
	 */
	public class ChatServiceListener implements WebSocketClient.Listener {
		private ChatIntentService service;

		public ChatServiceListener() {

		}

		// public ChatServiceListener(ChatIntentService service) {
		// this.service = service;
		// }

		public ChatServiceListener(ChatIntentService service) {
			this.service = service;
		}

		@Override
		public void onConnect() {
			try {
				String userName = Session.getSession(chatActivity).getUser()
						.getName();
				Log.d("CHAT_INTENT_SERVICE", "- " + userName
						+ "  ON CONNECT ! ");
				isConnected = true;

			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		/**
		 * On receiving the message from web socket server
		 * */
		@Override
		public void onMessage(String message) {
			try {

				Log.i(Config.TAG_INFO,
						"ChatIntentService: On message !! (string)" + message);

				String userName = Session.getSession(chatActivity).getUser()
						.getName();

				Log.d("CHAT_INTENT_SERVICE", "- " + userName
						+ " HE RECIBIDO UN MENSAJE ! ");

				ChatSocketListenerOnService.parseMessage(message);

				/**
				 * do the notification on parse message !
				 */

				// TODO: TEST
				// showContactsConnected();
				// showMessagesMap();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		@Override
		public void onMessage(byte[] data) {

			try {
				String message = UtilsChat.bytesToHex(data);
				Log.i(Config.TAG_INFO,
						"ChatIntentService: On message !! (bytes)" + message);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		@Override
		public void onDisconnect(int code, String reason) {
			try {

				String userName = Session.getSession(chatActivity).getUser()
						.getName();
				Log.d("CHAT_INTENT_SERVICE", "- " + userName
						+ "  ON DICONNECT ! ");

				Log.d("CHAT_INTENT_SERVICE", " onDisconnect!! reason  "
						+ reason + " code " + code);

				try {

					client.disconnect();

					try {
						client.notifyAll();

					} catch (Exception e) {
						e.printStackTrace();
					}
				} catch (Throwable e) {
					e.printStackTrace();
					try {
						client.disconnect();

					} catch (Exception ex) {
						e.printStackTrace();
					}
				}

				isConnected = false;
				utilsChat.storeSessionId(null);

			} catch (Throwable e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onError(Exception error) {
			try {
				Log.i(Config.TAG_INFO,
						"onError!! error message: " + error.getMessage());
				try {
					// Log.i(Config.TAG_INFO, "onError!! error cause:  "
					// + error.getCause().getMessage());
					ChatIntentService.getClient().disconnect();

				} catch (Exception e) {
					e.printStackTrace();
				}

				// UtilMessage
				// .showErrorMessage(chatActivity.getApplicationContext(),
				// "Error Couldn't establish connection with the server\nTry Later");
				UtilActivity.enableGui(chatActivity.getViewRoot(), false);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}

	public static WebSocketClient getClient() {
		return client;
	}

	public static void setClient(WebSocketClient client) {
		ChatIntentService.client = client;
	}

	public static ChatActivity getActivity() {
		return chatActivity;
	}

	public static void setActivity(ChatActivity activity) {
		ChatIntentService.chatActivity = activity;
	}

	public static HashMap<Long, List<Message>> getMapListMessages() {
		return mapListMessages;
	}

	public static void setMapListMessages(
			HashMap<Long, List<Message>> mapListMessages) {
		ChatIntentService.mapListMessages = mapListMessages;
	}

	public static List<Contact> getListContacts() {
		return listContacts;
	}

	public static void setListContacts(List<Contact> listContacts) {
		ChatIntentService.listContacts = listContacts;
	}

	public static Long getSelectedContactId() {
		return selectedContactId;
	}

	public static void setSelectedContactId(Long selectedContactId) {
		ChatIntentService.selectedContactId = selectedContactId;
	}

	public static Session getSession() {
		return session;
	}

	public static void setSession(Session session) {
		ChatIntentService.session = session;
	}

	public UsersServ getUser() {
		return user;
	}

	public void setUser(UsersServ user) {
		this.user = user;
	}

	public static Long getUserIdF() {
		return userIdF;
	}

	public static void setUserIdF(Long userId) {
		ChatIntentService.userIdF = userId;
	}

	public static String getUserNameF() {
		return userNameF;
	}

	public static void setUserNameF(String userName) {
		ChatIntentService.userNameF = userName;
	}

	public void showMessagesMap() {
		Log.i(Config.TAG_INFO,
				"ChatIntentService - showMessagesMap() ...............");

		HashMap<Long, List<Message>> mapListMessages = ChatIntentService
				.getMapListMessages();
		for (Long key : mapListMessages.keySet()) {
			Log.i(Config.TAG_INFO, "messages key: " + key);

			int i = 0;
			for (Message messageN : mapListMessages.get(key)) {
				i++;
				Log.i(Config.TAG_INFO,
						"ChatIntentService - showMessagesMap() ->  " + i + ")"
								+ messageN);
			}
		}
	}

	/**
	 * muestra todos los contactos conectados
	 */
	public void showContactsConnected() {
		Log.i(Config.TAG_INFO,
				"ChatIntentService - showContactsConnected() ...............");
		final List<Contact> contacts = ChatIntentService.getListContacts();
		int i = 0;
		for (Contact contactN : contacts) {
			i++;
			Log.i(Config.TAG_INFO,
					"ChatIntentService - showContactsConnected() ->  " + i
							+ ")" + contactN);
		}
	}

	public static void udpateLocalization(ArrayList<String> data) {

		try {
			cityGeoLoc = data.get(0);
			contryGeoLoc = data.get(1);

			Log.i(Config.TAG_INFO,
					"ChatIntentService - udpateLocalization --  city    : "
							+ cityGeoLoc);
			Log.i(Config.TAG_INFO, "ChatIntentService - country : "
					+ contryGeoLoc);

		} catch (Exception e) {
			Log.i(Config.TAG_INFO, "Error updating Localization");
			e.printStackTrace();
		}

	}

	public static Integer getSelectedContactPos() {
		return selectedContactPos;
	}

	public static void setSelectedContactPos(Integer selectedContactPos) {
		ChatIntentService.selectedContactPos = selectedContactPos;
	}

	public static String getCityGeoLoc() {
		return cityGeoLoc;
	}

	public static void setCityGeoLoc(String cityGeoLoc) {
		ChatIntentService.cityGeoLoc = cityGeoLoc;
	}

	public static String getContryGeoLoc() {
		return contryGeoLoc;
	}

	public static void setContryGeoLoc(String contryGeoLoc) {
		contryGeoLoc = contryGeoLoc;
	}

	public static Integer getSelectedContactPosition() {
		return selectedContactPosition;
	}

	public static void setSelectedContactPosition(
			Integer selectedContactPosition) {
		ChatIntentService.selectedContactPosition = selectedContactPosition;
	}

	public static int getNumMessagesNotSeeing() {
		return numMessagesNotSeeing;
	}

	public static void setNumMessagesNotSeeing(int numMessagesNotSeeing) {
		ChatIntentService.numMessagesNotSeeing = numMessagesNotSeeing;
	}

}
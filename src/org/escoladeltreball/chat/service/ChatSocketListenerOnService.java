package org.escoladeltreball.chat.service;

import java.util.ArrayList;
import java.util.List;

import org.escoladeltreball.activities.ChatActivity;
import org.escoladeltreball.activities.UtilMessage;
import org.escoladeltreball.chat.R;
import org.escoladeltreball.friendshipRequests.FriendshipRequestsActivity;
import org.escoladeltreball.model.Contact;
import org.escoladeltreball.model.Message;
import org.escoladeltreball.model.Session;
import org.escoladeltreball.model.server.persistence.UsersServ;
import org.escoladeltreball.support.Config;
import org.escoladeltreball.support.UtilsChat;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class ChatSocketListenerOnService {

	private static UtilsChat utilsChat;
	private static ChatActivity chatActivity;
	private static NotificationManager mNotificationManager;
	public static final int NOTIFICATION_ID = 1;

	// private static ChatBroadcastReceiver receiver;

	public ChatSocketListenerOnService(ChatActivity chatActivity) {
		ChatSocketListenerOnService.chatActivity = chatActivity;
		// this.guiMessages = new GuiMessages(this.chatActivity);
		utilsChat = new UtilsChat(chatActivity.getApplicationContext());
	}

	/**
	 * inicia el intent service del chat
	 */
	public void start() {
		try {

			Intent msgIntent = new Intent(chatActivity, ChatActivity.class);
 
			chatActivity.startService(msgIntent);
 
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	/**
	 * Parsing the JSON message received from server The intent of message will
	 * be identified by JSON node 'flag'. flag = self, message belongs to the
	 * person. flag = new, a new person joined the conversation. flag = message,
	 * a new message received from server. flag = exit, somebody left the
	 * conversation.
	 * 
	 * @return returns a string containing a message for notification as it's
	 *         needed otherwise returns null
	 * 
	 *         the notifications are sent by ChatIntentService Class
	 * */
	public static String parseMessage(final String msg) {
		// Log.i("INFO", "parseMessage() msg = " + msg);
		// UtilMessage.showInfoMessage(chatActivity, " you're online ");
		JSONObject jObj = null;
		String notificationMsg = null;
		String tag = null;
		Log.i(Config.TAG_INFO, "parseMessage " + msg);
		try {
			try {
				jObj = new JSONObject(msg);

			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}

			try {

				tag = jObj.getString(UtilsChat.KEY_TAG);
			} catch (Exception e) {
				Log.w(Config.TAG_WARNING,
						"***************** WARINNG recibido un paquete sin TAG *********************");
				e.printStackTrace();
			}
			if (tag == null) {
				return null;
			}
			final UsersServ myUser = Session.getSession(chatActivity).getUser();
			//
			// // if flag is 'self', this JSON contains session id
			if (tag.equalsIgnoreCase(UtilsChat.TAG_SELF)) {
				String sessionId = jObj.getString(UtilsChat.KEY_SESSION_ID);
				Log.i("INFO", "our sessionId  = " + sessionId);
				// Save the session id in shared preferences
				utilsChat.storeSessionId(sessionId);

				Log.d(Config.TAG_DEBUG,
						"Your session id: " + utilsChat.getSessionId());

			} else if (tag.equalsIgnoreCase(UtilsChat.TAG_NEW)) {
				/**
				 * agregamos al nuevo contacto que que ha conectado
				 */
				final Long id = jObj.getLong(UtilsChat.KEY_SRC_ID);
				final String userName = jObj.getString(UtilsChat.KEY_SRC_NAME);
				Log.i(Config.TAG_INFO, "new Contact is online: " + userName
						+ " with id: " + id);
				String message = jObj.getString(UtilsChat.KEY_MESSAGE);
				Log.i(Config.TAG_INFO, "tag_new message " + message);
				Log.i(Config.TAG_INFO, "tag_new name " + userName);
				Log.i(Config.TAG_INFO, "tag_new id " + id);

				// mostramos el número de personas online cuando alguien se
				// conecta
				String onlineCount = jObj.getString(UtilsChat.KEY_ONLINE_COUNT);

				// actualizamos la lista de contactos conectados del chat
				Contact contact = new Contact();
				contact.setName(userName);
				contact.setId(id);

				int idImg = 0;

				try {
					idImg = Integer.parseInt(jObj
							.getString(UtilsChat.KEY_SRC_ID_IMG));
				} catch (Exception e) {

				}

				contact.setPhotoId(idImg);
				// buscamos la imagen de la base de datos
				Log.i(Config.TAG_INFO, "new Contact is online: " + userName
						+ " adding the new friend to chat contacts listview");

				/**
				 * 
				 * 
				 * agreagamos el contacto a la lista
				 */
				// chatActivity.appendContactConnected(contact);

				if (ChatIntentService.getSession().getUser().getId()
						.equals(contact.getId())) {
					Log.e("ChatActivity",
							"appendContactConnected(): ERROR ! el contacto a agregar es uno mismo !!!!");
				}
				if (ChatIntentService.getSession().getUser().getId()
						.equals(contact.getId()) == false
						&& ChatIntentService.getListContacts()
								.contains(contact) == false) {

					ChatIntentService.getListContacts().add(contact);
					// contactsListAdapter = new ContactsListAdapter(this,
					// ChatIntentService.getListContacts());
					//
					// listViewContactos.setAdapter(contactsListAdapter);
					// contactsListAdapter.notifyDataSetChanged();

					playBeep();
				} else {
					Log.i(Config.TAG_INFO,
							"from:  the contact " + contact.getName()
									+ " with id: " + contact.getId()
									+ " is already in list");
				}

			} else if (tag.equalsIgnoreCase(UtilsChat.TAG_MESSAGE)) {

				Log.i("chatSocketListener", "NEW MESSAGE ...........");
				// if the flag is 'message', new message received
				// Long fromUserId = ChatIntentService.getUserId();
				// final String userName = jObj
				// .getString(UtilsChat.KEY_SRC_NAME);
				// String message = jObj.getString("message");
				String message = jObj.getString(UtilsChat.KEY_MESSAGE);
				// String sessionId = jObj.getString("sessionId");
				String sessionId = jObj.getString(UtilsChat.KEY_SESSION_ID);
				String srcName = null;
				try {

					srcName = jObj.getString(UtilsChat.KEY_SRC_NAME);
				} catch (Exception e) {
					Log.w(Config.TAG_INFO, "el mensaje " + message
							+ " no tiene srcName");
					srcName = myUser.getName();
				}

				// String srcName =
				// jObj.getString(UtilsChat.KEY_SOURCE_NAME);
				Long targetId = jObj.getLong(UtilsChat.KEY_TARGET_ID);

				Long fromUserId = jObj.getLong(UtilsChat.KEY_SRC_ID);

				Log.i("TAG_INFO", "Yo id '" + myUser.getId()
						+ "' he Recibido mensaje de '" + fromUserId
						+ "' con target id =  '" + targetId + "'");

				final boolean isSelf = fromUserId.equals(myUser.getId());

				Log.i("TAG_INFO", "PARSE MESSAGE IS SELF = " + isSelf);

			 
				Log.e(Config.TAG_INFO,
						"My session id: " + utilsChat.getSessionId());
				Log.i("INFO", "message is self ? " + isSelf);
				Log.i(Config.TAG_INFO, "parse message - message - targetId: "
						+ targetId);

				Message newMsg = new Message(fromUserId, srcName, message,
						isSelf, targetId);

				/**
				 * si el el chat no esta visible mostramos una notificacion
				 */
				if (isSelf == false && chatActivity.isActive() == false) {
					ChatIntentService.setNumMessagesNotSeeing(ChatIntentService
							.getNumMessagesNotSeeing() + 1);
					sendNewMessageNotification("You have "
							+ ChatIntentService.getNumMessagesNotSeeing()
							+ " new messages not seeing");
				}

				/**
				 * 
				 * 
				 * agregamos el nuevo mensaje a la lista
				 * 
				 */
				chatActivity.appendMessageFromFriend(newMsg);

			} else if (tag.equalsIgnoreCase(UtilsChat.TAG_EXIT)) {
				// If the flag is 'exit', somebody left the conversation
				String idUser = jObj.getString(UtilsChat.KEY_SRC_ID);
				String message = jObj.getString(UtilsChat.KEY_MESSAGE);
				Log.i("INFO",
						"el usuario con ha dejado el chat !! id del usuario = "
								+ idUser + "message = " + message);

				// guiMessages.showToast(name + message);
				List<Contact> contactos = ChatIntentService.getListContacts();

				List<Contact> contactosTmp = new ArrayList<Contact>();
				for (Contact contactN : contactos) {
					if (contactN.getId().equals(idUser) == false) {
						contactosTmp.add(contactN);
					}
				}
				ChatIntentService.setListContacts(contactosTmp);

			} else if (tag.equalsIgnoreCase(UtilsChat.TAG_NOTIFICATION)) {
				/**
				 * muestra un mensaje de informacion que recibe del servidor
				 * usando un toast
				 */
				Log.i("ChatSocketListener",
						"YOU HAVE NEW NOTIFICATION ............");
				String notType = jObj
						.getString(UtilsChat.KEY_NOTIFICATION_TYPE);
				String message = jObj.getString(UtilsChat.KEY_MESSAGE);
				if (UtilsChat.NOT_ERROR.equals(notType)) {
					UtilMessage.showErrorMessage(chatActivity, message);
				} else if (UtilsChat.NOT_INFO.equals(notType)) {
					UtilMessage.showInfoMessage(chatActivity, message);
				} else {
					UtilMessage.showWarningMessage(chatActivity, message);
				}

			} else if (tag.equalsIgnoreCase(UtilsChat.TAG_UDPATE_PROFILE)) {
				try {

					final Long id = jObj.getLong(UtilsChat.KEY_SRC_ID);
					final String userName = jObj
							.getString(UtilsChat.KEY_SRC_NAME);
					int idImg = 0;

					idImg = Integer.parseInt(jObj
							.getString(UtilsChat.KEY_SRC_ID_IMG));
					// actualizamos la lista de contactos conectados del
					// chat
					Contact contact = new Contact();
					contact.setName(userName);
					contact.setId(id);
					contact.setPhotoId(idImg);

					if (ChatActivity.isActive()) {
						chatActivity.updateFriendProfile(contact);
					}
				} catch (Exception e) {
					e.printStackTrace();

				}

			} else if (tag.equalsIgnoreCase(UtilsChat.TAG_NEW_FRIENDS_REQ)) {
				Log.i("ChatSocketListener", "New friend request.... msg = "
						+ msg);
				final Long targetId = jObj.getLong(UtilsChat.KEY_TARGET_ID);
				if (myUser.getId().equals(targetId)) {
					final String reqNotmsg = jObj
							.getString(UtilsChat.KEY_MESSAGE);
					sendNewFriendRequestNotification(reqNotmsg);
				} else {
					Log.w("ChatSocketListener",
							" the new friend request notification is ignored.............");
				}
			} else {
				Log.e(Config.TAG_ERROR,
						"ERROR COULDN'T PARSE THE MESSAGE - THE TAG " + tag
								+ " ISN'T RECOGNIZED");
			}

		} catch (JSONException e) {
			e.printStackTrace();
			Log.e(Config.TAG_ERROR, "ERROR PARSING THE MESSAGE  " + msg
					+ " WITH TAG " + tag);

		}

		return notificationMsg;
		// } catch (Throwable e) {
		// e.printStackTrace();
		// }

	}

	private static void sendNewMessageNotification(String msg) {
		try {
			Log.i("ChatIntentService",
					"ChatIntentService - sendNotification(): msg = " + msg);
			mNotificationManager = (NotificationManager) chatActivity
					.getSystemService(Context.NOTIFICATION_SERVICE);

			PendingIntent contentIntent = PendingIntent.getActivity(
					chatActivity, 0, new Intent(chatActivity,
							ChatActivity.class), 0);

			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
					chatActivity)
					.setContentTitle(
							chatActivity.getResources().getString(
									R.string.app_name))
					.setStyle(new NotificationCompat.InboxStyle().addLine(msg))
					.setContentText(msg)
					.setAutoCancel(true)
					.setSmallIcon(R.drawable.message_notification_ico)
					.setLargeIcon(
							BitmapFactory.decodeResource(
									chatActivity.getResources(),
									R.drawable.ic_launcher));

			mBuilder.setContentIntent(contentIntent);
			mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
		} catch (Exception e) {
			Log.i("ChatIntentService",
					"ChatIntentService - sendNotification(): Exception! ");
			e.printStackTrace();
		}
	}

	private static void sendNewFriendRequestNotification(String msg) {
		try {
			Log.i("ChatIntentService",
					"ChatIntentService - sendNotification(): msg = " + msg);
			mNotificationManager = (NotificationManager) chatActivity
					.getSystemService(Context.NOTIFICATION_SERVICE);

			PendingIntent contentIntent = PendingIntent.getActivity(
					chatActivity, 0, new Intent(chatActivity,
							FriendshipRequestsActivity.class), 0);

			Log.i("ChatIntentService",
					"ChatIntentService - sendNotification(): contentIntent = "
							+ msg);

			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
					chatActivity)
					.setContentTitle(
							chatActivity.getResources().getString(
									R.string.app_name))
					.setStyle(new NotificationCompat.InboxStyle().addLine(msg))
					.setContentText(msg)
					.setAutoCancel(true)
					.setSmallIcon(R.drawable.friend_request)
					.setLargeIcon(
							BitmapFactory.decodeResource(
									chatActivity.getResources(),
									R.drawable.ic_launcher));

			// "enviamos" la Notificacion
			mBuilder.setContentIntent(contentIntent);
			mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
		} catch (Exception e) {
			Log.i("ChatIntentService",
					"ChatIntentService - sendNotification(): Exception! ");
			e.printStackTrace();
		}
	}

	/**
	 * Reproduce la notificacio por defecto del sistema
	 * */
	public static void playBeep() {

		try {
			Uri notification = RingtoneManager
					.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			Ringtone r = RingtoneManager.getRingtone(
					chatActivity.getApplicationContext(), notification);
			r.play();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// public void unregisterReceiver(Activity activity) {
	// try {
	// activity.unregisterReceiver(receiver);
	//
	// } catch (Throwable th) {
	// th.printStackTrace();
	// }
	// }

}

package org.escoladeltreball.profileClasses;

import org.escoladeltreball.activities.UtilActivity;
import org.escoladeltreball.chat.R;
import org.escoladeltreball.db4o.User;
import org.escoladeltreball.support.Config;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Activity que muestra todos los actuales datos del usuario friend que queramos
 * visualizar.
 * 
 * @author Emilio Amado 2015
 * 
 */

public class PerfilFriendActivity extends Activity {
	public static final String EDIT_MODE_NAME = "edit_name";
	public static final String EDIT_MODE_PASS = "edit_pass";
	public static final String EDIT_MODE_CITY = "edit_city";
	public static final String EDIT_MODE_COUNTRY = "edit_country";
	public static final String EDIT_MODE_DESCRIPTION = "edit_description";
	public static final String EDIT_MODE_PREFERENCES = "edit_preferences";

	public static final String EDIT_NAME_MESSAGE = "Edit name";
	public static final String EDIT_PASS_MESSAGE = "Edit pass";
	public static final String EDIT_CITY_MESSAGE = "Edit city";
	public static final String EDIT_COUNTRY_MESSAGE = "Edit country";
	public static final String EDIT_DESCRIPTION_MESSAGE = "Edit personal description";
	public static final String EDIT_PREFERENCES_MESSAGE = "Edit music preferences";
	public static final String EDIT_AVATAR_MESSAGE = "Change avatar";

	public static final int MAX_CHARS_ALLOWED_SMALL_SIZE = 30;
	public static final int MAX_CHARS_ALLOWED_BIGGER_SIZE = 500;

	public static final int CHECK_MODE_SMALL_SIZE = 0;
	public static final int CHECK_MODE_BIGGER_SIZE = 1;

	public static final String WARNING_NEW_LINE_MESSAGE = "Break line not allowed";
	public static final String WARNING_MAX_CHARS_MESSAGE_SMALL_SIZE = "Max, "
			+ MAX_CHARS_ALLOWED_SMALL_SIZE + " chars allowed";
	public static final String WARNING_MAX_CHARS_MESSAGE_BIGGER_SIZE = "Max, "
			+ MAX_CHARS_ALLOWED_BIGGER_SIZE + " chars allowed";
	public static final String WARNING_FIELD_EMPTY_MESSAGE = "Empty fields are not allowed";
	// public static final String WARNING_DATA_DO_NOT_MATCH =
	// "The user name or pass dont match";

	private Button btEditaImagen;
	private Button btEditaNombre;
	private Button btEditaPass;
	private Button btEditaCiudad;
	private Button btEditaPais;
	private Button btEditaDescripcion;
	private Button btEditaGustos;

	private TextView tvNombre;
	private TextView tvPass;
	private TextView tvCiudad;
	private TextView tvPais;
	private TextView tvDescripcionPersonal;
	private TextView tvGustosMusicales;

	private ImageView ivAvatar;
	private static User user;
	private TextView tvTitle;
	private static String profileTitle = "Friend Profile";
	private static Class<?> comesFrom;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_perfil_friend);

		// obtenemos las dimensiones de la pantalla
		Display display = getWindowManager().getDefaultDisplay();
		int displayWidth = display.getWidth();
		int maxWidthForTextView = (int) displayWidth / 2;

		tvCiudad = (TextView) findViewById(R.id.tvCiudadFriend);
		tvCiudad.setMaxWidth(maxWidthForTextView);
		tvCiudad.setMaxLines(1);

		tvNombre = (TextView) findViewById(R.id.tvNombreFriend);
		tvNombre.setMaxWidth(maxWidthForTextView);
		tvNombre.setMaxLines(1);

		tvPais = (TextView) findViewById(R.id.tvPaisFriend);
		tvPais.setMaxWidth(maxWidthForTextView);
		tvPais.setMaxLines(1);

		tvDescripcionPersonal = (TextView) findViewById(R.id.tvDescripcionPersonalFriend);
		tvGustosMusicales = (TextView) findViewById(R.id.tvGustosMusicalesFriend);

		ivAvatar = (ImageView) findViewById(R.id.ivAvatarFriend);

		Log.i(Config.TAG_INFO, "PerfilFriendActivity - onCreate() - user = "
				+ user);

		tvCiudad.setText(user.getCity());
		tvDescripcionPersonal.setText(user.getPersonalDescription());
		tvGustosMusicales.setText(user.getMusicPreferences());
		tvNombre.setText(user.getName());
		tvPais.setText(user.getCountry());

		int friendAvatar = user.getPhotoResource();

		UtilActivity
				.setProfileBackgroundFromIdImg(ivAvatar, friendAvatar, true);

		this.tvTitle = (TextView) findViewById(R.id.tvFriendProfileTitle);
		this.tvTitle.setText(profileTitle);

	}

	public static User getUser() {
		return user;
	}

	public static void setUser(User user) {
		PerfilFriendActivity.user = user;
	}

	@Override
	public void onBackPressed() {
		if (comesFrom != null) {
			Intent intent = new Intent(this, comesFrom);
			this.startActivity(intent);
			this.finish();
		} else {
			super.onBackPressed();
		}
	}

	public static String getProfileTitle() {
		return profileTitle;
	}

	public static void setProfileTitle(String profileTitle) {
		PerfilFriendActivity.profileTitle = profileTitle;
	}

	public static Class<?> getComesFrom() {
		return comesFrom;
	}

	public static void setComesFrom(Class<?> comesFrom) {
		PerfilFriendActivity.comesFrom = comesFrom;
	}
}

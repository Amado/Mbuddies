package org.escoladeltreball.profileClasses;

//import com.example.projectm13.R;
//import com.example.projectm13.R.id;
//import com.example.projectm13.R.layout;

import org.escoladeltreball.chat.R;
import org.escoladeltreball.db4o.Db4oDAOUser;
import org.escoladeltreball.db4o.Db4oHelper;
import org.escoladeltreball.db4o.User;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.db4o.ObjectContainer;

/**
 * Clase encargada de realizar la autentificación del usuario para proseguir ,
 * si la autentificación ha tenido éxito, con la edición de perfil, en cuyo caso
 * carga la Activity PerfilOwnerActivity.
 * 
 * 
 * @author Emilio Amado 2015
 *
 */
public class DisplayAutentificationDialog {

	private ObjectContainer oc;
	private Db4oHelper dbHelper;
	private Activity activity;
	private User user;

	/**
	 * Constructor.
	 * 
	 * @param activity
	 *            la Activity desde donde se instancia esta clase
	 */
	public DisplayAutentificationDialog(Activity activity) {
		this.activity = activity;
		try {
			dbHelper = new Db4oHelper(activity);
			oc = dbHelper.oc;
			user = Db4oDAOUser.getUserOwnerFromDB(oc);
		} catch (Exception e) {
			e.printStackTrace();
			// Db4oHelper.displayDbError(this, Db4oHelper.ERROR_DB);
		}
		displayOwnerAutentification();
	}

	/**
	 * Despliega en pantalla un AlertDialog que pide la autentificación del
	 * usuario para proseguir con la edición deperfil.
	 */
	public void displayOwnerAutentification() {

		activity.runOnUiThread(new Runnable() {
			public void run() {

				final AlertDialog.Builder alert = new AlertDialog.Builder(
						activity);

				LayoutInflater inflater = activity.getLayoutInflater();

				View dialogView = inflater.inflate(
						R.layout.autentificate_user_dialog_layout, null);

				final EditText etUserName = (EditText) dialogView
						.findViewById(R.id.etUserName);
				final EditText etPass = (EditText) dialogView
						.findViewById(R.id.etPass);

				alert.setView(dialogView);
				alert.setCancelable(false);
				alert.setTitle("Autentificate user");
				alert.setPositiveButton("Accept",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {

								String introducedName = ""
										+ etUserName.getText().toString()
												.trim();
								String introducedPass = ""
										+ etPass.getText().toString();

								// comprobamos que nombre y pass coinciden con
								// los datos en la db local
								if ((introducedPass.equals(user.getPass()))
										&& (introducedName.equals(user
												.getName()))) {

									// ==========================================
									// CARGAMOS LA ACTIVITY DE EDICIÓN DE PERFIL
									// ==========================================
									Intent perfilOwnerIntent = new Intent(
											activity, PerfilOwnerActivity.class);
									perfilOwnerIntent
											.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
									activity.startActivity(perfilOwnerIntent);
								} else {
									if ((introducedName.equals(""))
											|| (introducedPass.equals(""))) {
										displayWarning(PerfilOwnerActivity.WARNING_FIELD_EMPTY_MESSAGE);
										displayOwnerAutentification();
									} else {
										displayWarning(PerfilOwnerActivity.WARNING_DATA_DO_NOT_MATCH);
										displayOwnerAutentification();
									}
								}
							}
						});
				alert.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {

								// ====================================================
								// AQUI CARGAMOS LA ACTIVITY DE REPRODUCCION DE
								// MUSICA
								// ====================================================

								dialog.cancel();
							}
						});
				alert.show();
			}
		});
	}

	
	
	/**
	 * Muestra un Toast de advertencia, el mensaje es modificable.
	 * 
	 * @param activity
	 *            Activity donde se muestra el Toast
	 */
	public void displayWarning(String message) {
		Toast toast = Toast.makeText(activity, message, Toast.LENGTH_LONG);
		View v = toast.getView();
		v.setBackgroundColor(Color.rgb(255, 204, 0));
		toast.setGravity(Gravity.CENTER | Gravity.TOP, 0, 20);
		toast.show();
	}

}

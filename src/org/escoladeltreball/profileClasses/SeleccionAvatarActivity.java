package org.escoladeltreball.profileClasses;

import java.lang.reflect.Field;

import org.escoladeltreball.chat.R;
import org.escoladeltreball.support.Config;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;

//import com.db4o.ObjectContainer;
//import com.example.db4o.Db4oHelper;

/**
 * Activity que muestra todos los avatares posibles para que el usuario pueda
 * uar uno como imagen del perfil, esta Activity es lanzada por
 * PerfilOwnerActivity.
 * 
 * @author Emilio Amado 2015
 *
 */
@SuppressLint("NewApi")
public class SeleccionAvatarActivity extends Activity implements
		OnClickListener {

	private Button btAv0;
	private Button btAv1;
	private Button btAv2;
	private Button btAv3;
	private Button btAv4;
	private Button btAv5;
	private Button btAv6;
	private Button btAv7;
	private Button btAv8;
	private Button btAv9;
	private Button btAv10;
	private Button btAv11;
	private Button btAv12;
	private Button btAv13;
	private Button btAv14;
	private Button btAv15;
	// private Button btAv16;
	// private Button btAv17;
	// private Button btAv18;
	// private Button btAv19;
	// private Button btAv20;
	// private Button btAv21;
	// private Button btAv22;
	// private Button btAv23;
	// private Button btAv24;
	// private Button btAv25;
	// private Button btAv26;
	// private Button btAv27;
	// private Button btAv28;
	// private Button btAv29;
	// private Button btAv30;
	// private Button btAv31;
	// private Button btAv32;
	// private Button btAv33;
	// private Button btAv34;
	// private Button btAv35;
	// private Button btAv36;
	// private Button btAv37;
	// private Button btAv38;
	// private Button btAv39;
	// private Button btAv40;
	// private Button btAv41;
	// private Button btAv42;
	// private Button btAv43;
	// private Button btAv44;
	// private Button btAv45;
	// private Button btAv46;
	// private Button btAv47;
	// private Button btAv48;
	// private Button btAv49;
	// private Button btAv50;

	private Button swapButton;

	// private ObjectContainer oc;
	// private Db4oHelper dbHelper;

	// private static User owner;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_seleccion_avatar);

		try {
			// dbHelper = new Db4oHelper(this);
			// oc = dbHelper.oc;

			Log.i("SeleccionaAvatarActivity", "db open");

			btAv0 = (Button) findViewById(R.id.btAv0);
			btAv0.setOnClickListener(this);
			btAv0.setBackgroundResource(R.drawable.av0);

			btAv1 = (Button) findViewById(R.id.btAv1);
			btAv1.setOnClickListener(this);
			btAv1.setBackgroundResource(R.drawable.av1);

			btAv2 = (Button) findViewById(R.id.btAv2);
			btAv2.setOnClickListener(this);
			btAv2.setBackgroundResource(R.drawable.av2);

			btAv3 = (Button) findViewById(R.id.btAv3);
			btAv3.setOnClickListener(this);
			btAv3.setBackgroundResource(R.drawable.av3);

			btAv4 = (Button) findViewById(R.id.btAv4);
			btAv4.setOnClickListener(this);
			btAv4.setBackgroundResource(R.drawable.av4);

			btAv5 = (Button) findViewById(R.id.btAv5);
			btAv5.setOnClickListener(this);
			btAv5.setBackgroundResource(R.drawable.av5);

			btAv6 = (Button) findViewById(R.id.btAv6);
			btAv6.setOnClickListener(this);
			btAv6.setBackgroundResource(R.drawable.av6);

			btAv7 = (Button) findViewById(R.id.btAv7);
			btAv7.setOnClickListener(this);
			btAv7.setBackgroundResource(R.drawable.av7);

			btAv8 = (Button) findViewById(R.id.btAv8);
			btAv8.setOnClickListener(this);
			btAv8.setBackgroundResource(R.drawable.av8);

			btAv9 = (Button) findViewById(R.id.btAv9);
			btAv9.setOnClickListener(this);
			btAv9.setBackgroundResource(R.drawable.av9);

			btAv10 = (Button) findViewById(R.id.btAv10);
			btAv10.setOnClickListener(this);
			btAv10.setBackgroundResource(R.drawable.av10);

			btAv11 = (Button) findViewById(R.id.btAv11);
			btAv11.setOnClickListener(this);
			btAv11.setBackgroundResource(R.drawable.av11);

			btAv12 = (Button) findViewById(R.id.btAv12);
			btAv12.setOnClickListener(this);
			btAv12.setBackgroundResource(R.drawable.av12);

			btAv13 = (Button) findViewById(R.id.btAv13);
			btAv13.setOnClickListener(this);
			btAv13.setBackgroundResource(R.drawable.av13);

			btAv14 = (Button) findViewById(R.id.btAv14);
			btAv14.setOnClickListener(this);
			btAv14.setBackgroundResource(R.drawable.av14);

			// btAv15 = (Button) findViewById(R.id.btAv15);
			// btAv15.setOnClickListener(this);
			// btAv15.setBackgroundResource(R.drawable.av15);

			// btAv16 = (Button) findViewById(R.id.btAv16);
			// btAv16.setOnClickListener(this);
			// btAv16.setBackgroundResource(R.drawable.av16);
			//
			// btAv17 = (Button) findViewById(R.id.btAv17);
			// btAv17.setOnClickListener(this);
			// btAv17.setBackgroundResource(R.drawable.av17);
			//
			// btAv18 = (Button) findViewById(R.id.btAv18);
			// btAv18.setOnClickListener(this);
			// btAv18.setBackgroundResource(R.drawable.av18);
			//
			// btAv19 = (Button) findViewById(R.id.btAv19);
			// btAv19.setOnClickListener(this);
			// btAv19.setBackgroundResource(R.drawable.av19);
			//
			// btAv20 = (Button) findViewById(R.id.btAv20);
			// btAv20.setOnClickListener(this);
			// btAv20.setBackgroundResource(R.drawable.av20);
			//
			// btAv21 = (Button) findViewById(R.id.btAv21);
			// btAv21.setOnClickListener(this);
			// btAv21.setBackgroundResource(R.drawable.av21);
			//
			// btAv22 = (Button) findViewById(R.id.btAv22);
			// btAv22.setOnClickListener(this);
			// btAv22.setBackgroundResource(R.drawable.av22);
			//
			// btAv23 = (Button) findViewById(R.id.btAv23);
			// btAv23.setOnClickListener(this);
			// btAv23.setBackgroundResource(R.drawable.av23);
			// btAv24 = (Button) findViewById(R.id.btAv24);
			// btAv24.setOnClickListener(this);
			// btAv24.setBackgroundResource(R.drawable.av24);
			//
			// btAv25 = (Button) findViewById(R.id.btAv25);
			// btAv25.setOnClickListener(this);
			// btAv25.setBackgroundResource(R.drawable.av25);
			//
			// btAv26 = (Button) findViewById(R.id.btAv26);
			// btAv26.setOnClickListener(this);
			// btAv26.setBackgroundResource(R.drawable.av26);
			//
			// btAv27 = (Button) findViewById(R.id.btAv27);
			// btAv27.setOnClickListener(this);
			// btAv27.setBackgroundResource(R.drawable.av27);
			//
			// btAv28 = (Button) findViewById(R.id.btAv28);
			// btAv28.setOnClickListener(this);
			// btAv28.setBackgroundResource(R.drawable.av28);
			//
			// btAv29 = (Button) findViewById(R.id.btAv29);
			// btAv29.setOnClickListener(this);
			// btAv29.setBackgroundResource(R.drawable.av29);
			//
			// btAv30 = (Button) findViewById(R.id.btAv30);
			// btAv30.setOnClickListener(this);
			// btAv30.setBackgroundResource(R.drawable.av30);
			// btAv31 = (Button) findViewById(R.id.btAv31);
			// btAv31.setOnClickListener(this);
			// btAv31.setBackgroundResource(R.drawable.av31);
			//
			// btAv32 = (Button) findViewById(R.id.btAv32);
			// btAv32.setOnClickListener(this);
			// btAv32.setBackgroundResource(R.drawable.av32);
			//
			// btAv33 = (Button) findViewById(R.id.btAv33);
			// btAv33.setOnClickListener(this);
			// btAv33.setBackgroundResource(R.drawable.av33);
			//
			// btAv34 = (Button) findViewById(R.id.btAv34);
			// btAv34.setOnClickListener(this);
			// btAv34.setBackgroundResource(R.drawable.av34);
			//
			// btAv35 = (Button) findViewById(R.id.btAv35);
			// btAv35.setOnClickListener(this);
			// btAv35.setBackgroundResource(R.drawable.av35);
			//
			// btAv36 = (Button) findViewById(R.id.btAv36);
			// btAv36.setOnClickListener(this);
			// btAv36.setBackgroundResource(R.drawable.av36);
			//
			// btAv37 = (Button) findViewById(R.id.btAv37);
			// btAv37.setOnClickListener(this);
			// btAv37.setBackgroundResource(R.drawable.av37);
			//
			// btAv38 = (Button) findViewById(R.id.btAv38);
			// btAv38.setOnClickListener(this);
			// btAv38.setBackgroundResource(R.drawable.av38);
			//
			// btAv39 = (Button) findViewById(R.id.btAv39);
			// btAv39.setOnClickListener(this);
			// btAv39.setBackgroundResource(R.drawable.av39);
			//
			// btAv40 = (Button) findViewById(R.id.btAv40);
			// btAv40.setOnClickListener(this);
			// btAv40.setBackgroundResource(R.drawable.av40);

			// btAv41 = (Button) findViewById(R.id.btAv41);
			// btAv41.setOnClickListener(this);
			// btAv41.setBackgroundResource(R.drawable.av41);
			//
			// btAv42 = (Button) findViewById(R.id.btAv42);
			// btAv42.setOnClickListener(this);
			// btAv42.setBackgroundResource(R.drawable.av42);
			// btAv43 = (Button) findViewById(R.id.btAv43);
			// btAv43.setOnClickListener(this);
			// btAv43.setBackgroundResource(R.drawable.av43);
			//
			// btAv44 = (Button) findViewById(R.id.btAv44);
			// btAv44.setOnClickListener(this);
			// btAv44.setBackgroundResource(R.drawable.av44);
			//
			// btAv45 = (Button) findViewById(R.id.btAv45);
			// btAv45.setOnClickListener(this);
			// btAv45.setBackgroundResource(R.drawable.av45);
			//
			// btAv46 = (Button) findViewById(R.id.btAv46);
			// btAv46.setOnClickListener(this);
			// btAv46.setBackgroundResource(R.drawable.av46);
			//
			// btAv47 = (Button) findViewById(R.id.btAv47);
			// btAv47.setOnClickListener(this);
			// btAv47.setBackgroundResource(R.drawable.av47);
			//
			// btAv48 = (Button) findViewById(R.id.btAv48);
			// btAv48.setOnClickListener(this);
			// btAv48.setBackgroundResource(R.drawable.av48);
			//
			// btAv49 = (Button) findViewById(R.id.btAv49);
			// btAv49.setOnClickListener(this);
			// btAv49.setBackgroundResource(R.drawable.av49);
			//
			// btAv50 = (Button) findViewById(R.id.btAv50);
			// btAv50.setOnClickListener(this);
			// btAv50.setBackgroundResource(R.drawable.av50);

		} catch (Exception e) {
			e.printStackTrace();
			// Db4oHelper.displayDbError(this, Db4oHelper.ERROR_DB);
		}
	}

	@Override
	public void onClick(View v) {
		int vId = v.getId();

		if (vId == R.id.btAv0) {
			swapButton = btAv0;
			showEditAvatarDialog(vId);

		} else if (vId == R.id.btAv1) {
			swapButton = btAv1;
			showEditAvatarDialog(vId);

		} else if (vId == R.id.btAv2) {
			swapButton = btAv2;
			showEditAvatarDialog(vId);

		} else if (vId == R.id.btAv3) {
			swapButton = btAv3;
			showEditAvatarDialog(vId);

		} else if (vId == R.id.btAv4) {
			swapButton = btAv4;
			showEditAvatarDialog(vId);

		} else if (vId == R.id.btAv5) {
			swapButton = btAv5;
			showEditAvatarDialog(vId);

		} else if (vId == R.id.btAv6) {
			swapButton = btAv6;
			showEditAvatarDialog(vId);

		} else if (vId == R.id.btAv7) {
			swapButton = btAv7;
			showEditAvatarDialog(vId);

		} else if (vId == R.id.btAv8) {
			swapButton = btAv8;
			showEditAvatarDialog(vId);

		} else if (vId == R.id.btAv9) {
			swapButton = btAv9;
			showEditAvatarDialog(vId);

		} else if (vId == R.id.btAv10) {
			swapButton = btAv10;
			showEditAvatarDialog(vId);

		} else if (vId == R.id.btAv11) {
			swapButton = btAv11;
			showEditAvatarDialog(vId);

		} else if (vId == R.id.btAv12) {
			swapButton = btAv12;
			showEditAvatarDialog(vId);

		} else if (vId == R.id.btAv13) {
			swapButton = btAv13;
			showEditAvatarDialog(vId);

		} else if (vId == R.id.btAv14) {
			swapButton = btAv14;
			showEditAvatarDialog(vId);

		} else {
			swapButton = btAv15;
			showEditAvatarDialog(vId);
		}

		//
		// } else if (vId == R.id.btAv15) {
		// swapButton = btAv15;
		// showEditAvatarDialog(vId);
		// }
		//

		// } else if (vId == R.id.btAv16) {
		// swapButton = btAv16;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv17) {
		// swapButton = btAv17;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv18) {
		// swapButton = btAv18;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv19) {
		// swapButton = btAv19;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv20) {
		// swapButton = btAv20;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv21) {
		// swapButton = btAv21;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv22) {
		// swapButton = btAv22;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv23) {
		// swapButton = btAv23;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv24) {
		// swapButton = btAv24;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv25) {
		// swapButton = btAv25;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv26) {
		// swapButton = btAv26;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv27) {
		// swapButton = btAv27;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv28) {
		// swapButton = btAv28;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv29) {
		// swapButton = btAv29;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv30) {
		// swapButton = btAv30;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv31) {
		// swapButton = btAv31;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv32) {
		// swapButton = btAv32;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv33) {
		// swapButton = btAv33;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv34) {
		// swapButton = btAv34;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv35) {
		// swapButton = btAv35;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv36) {
		// swapButton = btAv36;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv37) {
		// swapButton = btAv37;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv38) {
		// swapButton = btAv38;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv39) {
		// swapButton = btAv39;
		// showEditAvatarDialog(vId);
		// } else if (vId == R.id.btAv40) {
		// swapButton = btAv40;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv41) {
		// swapButton = btAv41;
		// showEditAvatarDialog(vId);
		// } else if (vId == R.id.btAv42) {
		// swapButton = btAv2;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv43) {
		// swapButton = btAv43;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv44) {
		// swapButton = btAv44;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv45) {
		// swapButton = btAv45;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv46) {
		// swapButton = btAv46;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv47) {
		// swapButton = btAv47;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv48) {
		// swapButton = btAv48;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv49) {
		// swapButton = btAv49;
		// showEditAvatarDialog(vId);
		//
		// } else if (vId == R.id.btAv50) {
		// swapButton = btAv50;
		// showEditAvatarDialog(vId);
		// }
	}

	/**
	 * Despliega en pantalla un AlertDialog para confirmar la actualización del
	 * avatar.
	 * 
	 * @param vId
	 *            la id del res del Button que dispara el Dialog
	 */
	public void showEditAvatarDialog(final int vId) {

		try {
			final AlertDialog.Builder alert = new AlertDialog.Builder(this);

			// creamos un layout por código para ubicar la imagen del avatar
			// en el AlertDialog
			Drawable background = swapButton.getBackground();
			Button bt = new Button(this);
			bt.setBackgroundDrawable(background);
			RelativeLayout rL = new RelativeLayout(this);
			rL.addView(bt);
			RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) bt
					.getLayoutParams();
			layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT,
					RelativeLayout.TRUE);
			bt.setLayoutParams(layoutParams);
			alert.setView(rL);

			alert.setTitle(PerfilOwnerActivity.EDIT_AVATAR_MESSAGE);
			alert.setPositiveButton("Accept",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int whichButton) {

							for (Field f : R.drawable.class.getDeclaredFields()) {
								System.out.println(f.getName());
							}

							for (int avIdImgI = 0; avIdImgI <= Config.AVATAR_MAX_IMGS; avIdImgI++) {

								int btImg = 0;
								try {
									final String fieldName = Config.AVATAR_BOTON_BASE_NAME
											+ avIdImgI;

									// Log.i("INFO", "fieldName --- " +
									// fieldName);
									btImg = (Integer) R.id.class
											.getDeclaredField(fieldName).get(
													null);
								} catch (IllegalAccessException e1) {
									e1.printStackTrace();
								} catch (IllegalArgumentException e1) {
									e1.printStackTrace();
								} catch (NoSuchFieldException e1) {
									e1.printStackTrace();
								}

								if (vId == btImg) {
									Integer imgResource = null;
									try {
										final String fieldName = Config.AVATAR_IMG_BASE_NAME
												+ avIdImgI;

										try {
											imgResource = (Integer) R.drawable.class
													.getDeclaredField(fieldName)
													.get(null);
										} catch (IllegalAccessException e) {
											e.printStackTrace();
										} catch (NoSuchFieldException e) {
											e.printStackTrace();
										}

										if (imgResource != null) {
											Intent perfilUsuarioIntent = new Intent(
													getApplicationContext(),
													PerfilOwnerActivity.class);

											PerfilOwnerActivity
													.setSelectedAv(avIdImgI);
											PerfilOwnerActivity
													.setHasChangedProfile(true);
											

											SeleccionAvatarActivity.this
													.onBackPressed();
											SeleccionAvatarActivity.this
													.finish();
											// startActivity(perfilUsuarioIntent);
											// PerfilOwnerActivity
											// .setHasChangedProfile(true);
											// SeleccionAvatarActivity.this
											// .finish();
											break;
										}
									} catch (IllegalArgumentException e) {
										e.printStackTrace();
									}

								}

							}

						}
					});

			alert.setNegativeButton("Cancel",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int whichButton) {
							dialog.cancel();
						}
					});
			alert.show();

		} catch (Exception e) {
			e.printStackTrace();
			// Db4oHelper.displayDbError(this, Db4oHelper.ERROR_DB);
		}
	}

	/**
	 * actualiza el usuario en la session y envia los datos actualizados al
	 * servidor
	 */

	// public static User getOwner() {
	// return owner;
	// }
	//
	// public static void setOwner(User owner) {
	// SeleccionAvatarActivity.owner = owner;
	// }

	@Override
	public void onBackPressed() {
		// Intent perfilUsuarioIntent = new Intent(getApplicationContext(),
		// PerfilOwnerActivity.class);
		// startActivity(perfilUsuarioIntent);
		this.finish();
		super.onBackPressed();

	}

}
package org.escoladeltreball.profileClasses;

import org.escoladeltreball.activities.ActionActivity;
import org.escoladeltreball.activities.UtilActivity;
import org.escoladeltreball.activities.UtilMessage;
import org.escoladeltreball.chat.R;
import org.escoladeltreball.chat.service.ChatIntentService;
import org.escoladeltreball.db4o.Db4oDAOUser;
import org.escoladeltreball.db4o.Db4oHelper;
import org.escoladeltreball.db4o.User;
import org.escoladeltreball.manager.RequestManager;
import org.escoladeltreball.model.Session;
import org.escoladeltreball.model.server.persistence.ModelConverter;
import org.escoladeltreball.model.server.persistence.UsersServ;
import org.escoladeltreball.musicPlayer.MusicPlayerActivity;
import org.escoladeltreball.support.Config;
import org.escoladeltreball.support.Config.Validation;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.db4o.ObjectContainer;

/**
 * Activity que muestra todos los actuales datos del usuario owner, todos los
 * datos son modificables mediantes el uso de AlertDialog, a excepción del
 * avatar que se modifica con el lanzamiento de SeleccionAvatarActivity.
 * 
 * @author Emilio Amado 2015
 *
 */
public class PerfilOwnerActivity extends Activity implements OnClickListener {

	public static final String EDIT_MODE_NAME = "edit_name";
	public static final String EDIT_MODE_PASS = "edit_pass";
	public static final String EDIT_MODE_CITY = "edit_city";
	public static final String EDIT_MODE_COUNTRY = "edit_country";
	public static final String EDIT_MODE_DESCRIPTION = "edit_description";
	public static final String EDIT_MODE_PREFERENCES = "edit_preferences";

	public static final String EDIT_NAME_MESSAGE = "Edit name";
	public static final String EDIT_PASS_MESSAGE = "Edit pass";
	public static final String EDIT_CITY_MESSAGE = "Edit city";
	public static final String EDIT_COUNTRY_MESSAGE = "Edit country";
	public static final String EDIT_DESCRIPTION_MESSAGE = "Edit personal description";
	public static final String EDIT_PREFERENCES_MESSAGE = "Edit music preferences";
	public static final String EDIT_AVATAR_MESSAGE = "Change avatar";

	public static final int MAX_CHARS_ALLOWED_SMALL_SIZE = 30;
	public static final int MAX_CHARS_ALLOWED_BIGGER_SIZE = 500;

	public static final int CHECK_MODE_SMALL_SIZE = 0;
	public static final int CHECK_MODE_BIGGER_SIZE = 1;

	public static final String WARNING_NEW_LINE_MESSAGE = "Break line not allowed";
	public static final String WARNING_MAX_CHARS_MESSAGE_SMALL_SIZE = "Max, "
			+ MAX_CHARS_ALLOWED_SMALL_SIZE + " chars allowed";
	public static final String WARNING_MAX_CHARS_MESSAGE_BIGGER_SIZE = "Max, "
			+ MAX_CHARS_ALLOWED_BIGGER_SIZE + " chars allowed";
	public static final String WARNING_FIELD_EMPTY_MESSAGE = "Empty fields are not allowed";
	public static final String WARNING_DATA_DO_NOT_MATCH = "The user name or pass doesn't match";

	public static final String NAME_DATA = Db4oDAOUser.NAME_DATA;
	public static final String PASS_DATA = Db4oDAOUser.PASS_DATA;
	public static final String CITY_DATA = Db4oDAOUser.CITY_DATA;
	public static final String COUNTRY_DATA = Db4oDAOUser.COUNTRY_DATA;
	public static final String PERSONAL_DESCRIPTION_DATA = Db4oDAOUser.PERSONAL_DESCRIPTION_DATA;
	public static final String MUSIC_PREFERENCES_DATA = Db4oDAOUser.MUSIC_PREFERENCES_DATA;

	private ObjectContainer oc;
	private Db4oHelper dbHelper;

	private Button btEditaImagen;
	private Button btEditaNombre;
	private Button btEditaPass;
	private Button btEditaCiudad;
	private Button btEditaPais;
	private Button btEditaDescripcion;
	private Button btEditaGustos;

	private TextView tvNombre;
	private TextView tvPass;
	private TextView tvCiudad;
	private TextView tvPais;
	private TextView tvDescripcionPersonal;
	private TextView tvGustosMusicales;
	private ImageView ivAvatar;
	private User owner;
	private Button btSaveProfile;
	private static boolean confirmChanges = false;

	private static Class<?> previousActivity = MusicPlayerActivity.class;

	private static boolean hasChangedProfile = false;
	private static Integer selectedAv = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_perfil_owner);

		btEditaImagen = (Button) findViewById(R.id.btEditaImagen);
		btEditaImagen.setOnClickListener(this);
		btEditaImagen.setBackgroundResource(R.drawable.bt_lapiz_images);

		btEditaNombre = (Button) findViewById(R.id.btEditaNombre);
		btEditaNombre.setOnClickListener(this);
		btEditaNombre.setBackgroundResource(R.drawable.bt_lapiz_images);

		btEditaPass = (Button) findViewById(R.id.btEditaPass);
		btEditaPass.setOnClickListener(this);
		btEditaPass.setBackgroundResource(R.drawable.bt_lapiz_images);

		btEditaCiudad = (Button) findViewById(R.id.btEditaCiudad);
		btEditaCiudad.setOnClickListener(this);
		btEditaCiudad.setBackgroundResource(R.drawable.bt_lapiz_images);

		btEditaPais = (Button) findViewById(R.id.btEditaPais);
		btEditaPais.setOnClickListener(this);
		btEditaPais.setBackgroundResource(R.drawable.bt_lapiz_images);

		btEditaDescripcion = (Button) findViewById(R.id.btEditaDescripcion);
		btEditaDescripcion.setOnClickListener(this);
		btEditaDescripcion.setBackgroundResource(R.drawable.bt_lapiz_images);

		btEditaGustos = (Button) findViewById(R.id.btEditaGustos);
		btEditaGustos.setOnClickListener(this);
		btEditaGustos.setBackgroundResource(R.drawable.bt_lapiz_images);

		// obtenemos las dimensiones de la pantalla
		Display display = getWindowManager().getDefaultDisplay();
		int displayWidth = display.getWidth();
		int maxWidthForTextView = (int) displayWidth / 3;

		tvCiudad = (TextView) findViewById(R.id.tvCiudad);
		tvCiudad.setMaxWidth(maxWidthForTextView);
		tvCiudad.setMaxLines(1);

		tvNombre = (TextView) findViewById(R.id.tvNombre);
		tvNombre.setMaxWidth(maxWidthForTextView);
		tvNombre.setMaxLines(1);

		tvPais = (TextView) findViewById(R.id.tvPais);
		tvPais.setMaxWidth(maxWidthForTextView);
		tvPais.setMaxLines(1);

		tvPass = (TextView) findViewById(R.id.tvPass);
		tvPass.setMaxWidth(maxWidthForTextView);
		tvPass.setMaxLines(1);

		btSaveProfile = (Button) findViewById(R.id.btSaveUserProfile);

		btSaveProfile.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				PerfilOwnerActivity.this.saveProfile();
			}
		});

		tvDescripcionPersonal = (TextView) findViewById(R.id.tvDescripcionPersonalFriend);
		tvGustosMusicales = (TextView) findViewById(R.id.tvGustosMusicales);

		ivAvatar = (ImageView) findViewById(R.id.ivAvatar);

		refreshProfile();

	}

	public void refreshProfile() {
		Log.i("DADF", "REFRESH PROFILE............");
		selectedAv = null;
		hasChangedProfile = false;
		try {
			dbHelper = new Db4oHelper(this);
			oc = dbHelper.oc;

			// lo primero comprobamos si hay un owner
			// si no lo hay lo creamos y guardamos en la db local

			UsersServ userServ = Session.getSession(this).getUser();

			owner = ModelConverter.convertToLocalUser(userServ);
			// owner = Db4oDAOUser.getUserOwnerFromDB(oc);

			// damos los datos del owner a los campos en pantalla
			tvCiudad.setText(owner.getCity());
			tvDescripcionPersonal.setText(owner.getPersonalDescription());
			tvGustosMusicales.setText(owner.getMusicPreferences());
			tvNombre.setText(owner.getName());
			tvPais.setText(owner.getCountry());

			UtilActivity.setProfileBackgroundFromIdImg(ivAvatar,
					owner.getPhotoResource(), false);

		} catch (Exception e) {
			e.printStackTrace();
			// Db4oHelper.displayDbError(this, Db4oHelper.ERROR_DB);
		}
	}

	// @Override
	// protected void onPostResume() {
	// super.onResume();
	// if (selectedAv != null) {
	// UtilActivity.setProfileBackgroundFromIdImg(ivAvatar, selectedAv,
	// false);
	// owner.setPhotoResource(selectedAv);
	// confirmChanges = false;
	// selectedAv = null;
	// } else {
	// refreshProfile();
	// }
	// super.onPostResume();
	// }

	// @Override
	// protected void onPause() {
	// super.onResume();
	// if (selectedAv != null) {
	// UtilActivity.setProfileBackgroundFromIdImg(ivAvatar, selectedAv,
	// false);
	// owner.setPhotoResource(selectedAv);
	// confirmChanges = false;
	// selectedAv = null;
	// } else {
	// refreshProfile();
	// }
	// super.onPause();
	// }

	@Override
	public void onClick(View v) {

		int vId = v.getId();

		if (vId == R.id.btEditaCiudad) {
			showEditDialog(EDIT_MODE_CITY);
		} else if (vId == R.id.btEditaDescripcion) {
			showEditDialog(EDIT_MODE_DESCRIPTION);
		} else if (vId == R.id.btEditaGustos) {
			showEditDialog(EDIT_MODE_PREFERENCES);
		} else if (vId == R.id.btEditaNombre) {
			showEditDialog(EDIT_MODE_NAME);
		} else if (vId == R.id.btEditaPais) {
			showEditDialog(EDIT_MODE_COUNTRY);
		} else if (vId == R.id.btEditaPass) {
			showEditDialog(EDIT_MODE_PASS);
		} else {
			Intent seleccionAvatarIntent = new Intent(this,
					SeleccionAvatarActivity.class);
			// SeleccionAvatarActivity.setOwner(owner);
			// startActivity(seleccionAvatarIntent);
			startActivityForResult(seleccionAvatarIntent, 1);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (selectedAv != null) {
			UtilActivity.setProfileBackgroundFromIdImg(ivAvatar, selectedAv,
					false);
			owner.setPhotoResource(selectedAv);
			confirmChanges = false;
			selectedAv = null;
		} else {
			refreshProfile();
		}
	}

	// @Override
	// protected void onRestart() {
	// if (selectedAv != null) {
	// UtilActivity.setProfileBackgroundFromIdImg(ivAvatar, selectedAv,
	// false);
	// owner.setPhotoResource(selectedAv);
	// confirmChanges = false;
	// selectedAv = null;
	// } else {
	// refreshProfile();
	// }
	// super.onRestart();
	// }

	/**
	 * Despliega en pantalla un AlertDialog para modificar uno de los campo del
	 * prefil de usuario, dependiendo del modo de edición que entre como arg,
	 * por ejemplo, si se desea modificar el nombre se usará como arg el modo
	 * EDIT_MODE_NAME.
	 * 
	 * @param editMode
	 *            el modo de edición seleccionado
	 */
	public void showEditDialog(final String editMode) {

		try {
			String title = "";
			final EditText input = new EditText(this);
			input.setMaxLines(1);
			;

			if (editMode.equals(EDIT_MODE_CITY)) {
				title = EDIT_CITY_MESSAGE;
				input.setText(owner.getCity());
			} else if (editMode.equals(EDIT_MODE_COUNTRY)) {
				title = EDIT_COUNTRY_MESSAGE;
				input.setText(owner.getCountry());
			} else if (editMode.equals(EDIT_MODE_DESCRIPTION)) {
				title = EDIT_DESCRIPTION_MESSAGE;
				input.setText(owner.getPersonalDescription());
			} else if (editMode.equals(EDIT_MODE_NAME)) {
				title = EDIT_NAME_MESSAGE;
				input.setText(owner.getName());
			} else if (editMode.equals(EDIT_MODE_PASS)) {
				title = EDIT_PASS_MESSAGE;
			} else if (editMode.equals(EDIT_MODE_PREFERENCES)) {
				title = EDIT_PREFERENCES_MESSAGE;
				input.setText(owner.getMusicPreferences());
			}

			final AlertDialog.Builder alert = new AlertDialog.Builder(this);
			alert.setTitle(title);
			alert.setView(input);
			alert.setPositiveButton("Accept",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int whichButton) {
							String value = input.getText().toString().trim();

							// boolean isUpdated = false;

							if (value.equals("")) {
								displayWarning(WARNING_FIELD_EMPTY_MESSAGE);
								showEditDialog(editMode);
							} else {

								if (editMode.equals(EDIT_MODE_CITY)) {
									if (!checkNewData(value,
											CHECK_MODE_SMALL_SIZE)
											&& isDataDifferent(CITY_DATA, value)) {
										showEditDialog(editMode);
									}

									else {
										tvCiudad.setText(value);
										owner.setCity(value);
										hasChangedProfile = true;
									}

								} else if (editMode.equals(EDIT_MODE_COUNTRY)) {
									if (!checkNewData(value,
											CHECK_MODE_SMALL_SIZE)
											&& isDataDifferent(COUNTRY_DATA,
													value)) {
										showEditDialog(editMode);
									} else {
										tvPais.setText(value);
										owner.setCountry(value);
										hasChangedProfile = true;
									}

								} else if (editMode
										.equals(EDIT_MODE_DESCRIPTION)) {
									if (!checkNewData(value,
											CHECK_MODE_BIGGER_SIZE)

											&& isDataDifferent(
													PERSONAL_DESCRIPTION_DATA,
													value)) {
										showEditDialog(editMode);
										hasChangedProfile = true;
									}

									else {
										tvDescripcionPersonal.setText(value);
										owner.setPersonalDescription(value);
										hasChangedProfile = true;
									}

								} else if (editMode.equals(EDIT_MODE_NAME)) {
									if (validateName(value) == false) {
										showEditDialog(editMode);
									} else {

										/**
										 * evitamos que se cambie el nombre sin
										 * comunicarlo al servidor
										 */
										changeName(value, editMode);
									}

								} else if (editMode.equals(EDIT_MODE_PASS)) {

									if (validatePassword(value) == false) {
										showEditDialog(editMode);
										return;
									} else {
										/**
										 * evitamos que se cambie el password
										 * sin comunicarlo al servidor
										 */
										changePass(value, editMode);

									}

								} else if (editMode
										.equals(EDIT_MODE_PREFERENCES)) {

									if (!checkNewData(value,
											CHECK_MODE_BIGGER_SIZE)
											&& isDataDifferent(
													MUSIC_PREFERENCES_DATA,
													value)) {
										showEditDialog(editMode);
									} else {
										tvGustosMusicales.setText(value);
										owner.setMusicPreferences(value);
										hasChangedProfile = true;
									}
								}

							}

							// if (isUpdated == true) {
							// updateUser();
							// }
						}
						// Toast.makeText(getApplicationContext(), value,
						// Toast.LENGTH_SHORT).show();
						// }
					});
			alert.setNegativeButton("Cancel",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int whichButton) {
							dialog.cancel();
						}
					});
			alert.show();

		} catch (Exception e) {
			e.printStackTrace();
			// Db4oHelper.displayDbError(this, Db4oHelper.ERROR_DB);
		}
	}

	/**
	 * Actualiza el nombre de usario en la base de datos y en el servidor si no
	 * se puede actualizar en el servidor entonces no se cambia el nombre del
	 * usuario
	 * 
	 * @param value
	 * @param editMode
	 */

	public void changeName(final String value, final String editMode) {
		/**
		 * evitamos que se cambie el nombre sin comunicarlo al servidor
		 */
		final String finalValue = value;
		// *

		Log.i("INFO", "USERLOCAL = > " + owner);
		final User tmpUser = new User(finalValue, owner.getPass(),
				owner.getCity(), owner.getCountry(),
				owner.getPersonalDescription(), owner.getMusicPreferences(),
				owner.getPhotoResource(), true);
		tmpUser.setId(owner.getId());

		Log.i("INFO", "update User -> " + owner.getId());

		final ProgressDialog dialog = UtilMessage.showProgressDialog(this,
				"the user name is been update on server ... please wait");
		new Thread() {
			@Override
			public void run() {

				try {
					// owner.setName(finalValue);

					UsersServ servUser = convertToServUser(tmpUser);

					Log.i(Config.TAG_INFO, "tmpUser => " + tmpUser);

					boolean isUpdated = RequestManager.updateUser(servUser);
					if (isUpdated == true
							&& Db4oDAOUser.changeOwnerData(oc,
									Db4oDAOUser.NAME_DATA, finalValue)) {

						ActionActivity.notifyUpdateProfileOnChat(finalValue,
								String.valueOf(tmpUser.getId()),
								" updating the name of user ....",
								String.valueOf(tmpUser.getPhotoResource()),
								PerfilOwnerActivity.this);

						PerfilOwnerActivity.this.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								tvNombre.setText(finalValue);
								owner.setName(finalValue);
							}
						});

						updateUser(tmpUser);

					} else {
						showEditDialog(editMode);
					}
				} catch (Exception e) {
					Log.i(Config.TAG_INFO,
							"There is already a user with name !! ");

					PerfilOwnerActivity.this.runOnUiThread(new Runnable() {

						@Override
						public void run() {
							UtilMessage
									.showErrorMessage(
											PerfilOwnerActivity.this,
											"Couldn't update your user name "
													+ "at this moment, probably "
													+ "because your aren't connected to internet");
						}
					});
				} finally {
					UtilMessage.dismisProgressDialog(dialog);
				}
				PerfilOwnerActivity.this.runOnUiThread(new Runnable() {

					@Override
					public void run() {
						UtilMessage.showErrorMessage(PerfilOwnerActivity.this,
								"Your user name has been update");
					}
				});
			}
		}.start();
	}

	/**
	 * Actualiza la contraseña en la base de datos y en el servidor si no se
	 * puede actualizar en el servidor entonces no se cambia la contraseña
	 * 
	 * @param value
	 * @param editMode
	 */
	public void changePass(final String value, final String editMode) {
		final String finalValue = value;

		final User tmpUser = new User(owner.getName(), finalValue,
				owner.getCity(), owner.getCountry(),
				owner.getPersonalDescription(), owner.getMusicPreferences(),
				owner.getPhotoResource(), true);

		final ProgressDialog dialog = UtilMessage.showProgressDialog(this,
				"the user name is been update on server ... please wait");
		new Thread() {
			@Override
			public void run() {

				try {

					UsersServ servUser = convertToServUser(tmpUser);

					RequestManager.updateUser(servUser);

					Log.i(Config.TAG_INFO, "tmpUser => " + tmpUser);

					if (Db4oDAOUser.changeOwnerData(oc, Db4oDAOUser.PASS_DATA,
							finalValue)) {
						PerfilOwnerActivity.this.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								tvPass.setText(finalValue);
								owner.setPass(finalValue);
							}
						});
					} else {
						showEditDialog(editMode);
					}

				} catch (Exception e) {
					Log.i(Config.TAG_INFO,
							"There is already a user with name !! ");

					PerfilOwnerActivity.this.runOnUiThread(new Runnable() {

						@Override
						public void run() {
							UtilMessage
									.showErrorMessage(
											PerfilOwnerActivity.this,
											"Couldn't update your password "
													+ "at this moment, probably "
													+ "because your aren't connected to internet");
						}
					});
				} finally {
					UtilMessage.dismisProgressDialog(dialog);
				}

				PerfilOwnerActivity.this.runOnUiThread(new Runnable() {

					@Override
					public void run() {
						UtilMessage.showErrorMessage(PerfilOwnerActivity.this,
								"Your password has been update");
					}
				});
			}
		}.start();
	}

	public void saveProfile() {
		if (hasChangedProfile == false) {
			UtilMessage
					.showInfoMessage(this, "YOUR PROFILE IS ALREADY UPDATED");
			return;
		}

		UtilMessage.showInfoMessage(this, "UPDATING PROFILE ....");
		final ProgressDialog dialog = UtilMessage.showProgressDialog(this,
				"Updating profile on server... please wait");

		new Thread() {
			@Override
			public void run() {
				boolean updatedOkOnLocal = false;
				boolean updatedOkOnServer = false;
				Session session = null;
				try {

					UsersServ servUser = ModelConverter
							.convertToServerUser(owner);
					// Session session = Session
					// .getSession(PerfilOwnerActivity.this);
					session = Session.getSession(PerfilOwnerActivity.this);
					servUser.setId(session.getUser().getId());
					session.setUser(servUser);
					Session.saveSessionOnPreferences(PerfilOwnerActivity.this);
					// reseteamos el flag si el perfil se ha guardado en la
					// base
					// de datos local
					hasChangedProfile = false;
					updatedOkOnLocal = true;

					if (updatedOkOnLocal == true) {
						updatedOkOnServer = RequestManager.updateUser(servUser);

						try {
							ActionActivity
									.notifyUpdateProfileOnChat(
											owner.getName(),
											String.valueOf(owner.getId()),
											"Updating profile of user "
													+ owner.getId(),
											String.valueOf(owner
													.getPhotoResource()),
											PerfilOwnerActivity.this);

						} catch (Throwable th) {
							Log.e(Config.TAG_ERROR,
									"Error nofiying profile chang on chat");
							th.printStackTrace();
						}
						session.setUserToUpdate(!updatedOkOnServer);

						if (updatedOkOnServer == false) {
							throw new Exception(
									"The changes on user profile coulnd't been updated on Server");
						}

					}
				} catch (Exception e) {
					updatedOkOnServer = false;
					e.printStackTrace();

				} finally {
					// finalmente alamacenamos en memoria el estado del usuario
					// contenido en la sesion
					if (session != null) {
						Session.saveSessionOnPreferences(PerfilOwnerActivity.this);
					}
					if (session == null) {
						Log.e(Config.TAG_ERROR,
								"ERROR almacenando el estado del usuario updateOnLocal = "
										+ updatedOkOnLocal
										+ ", updatedOkOnServer="
										+ updatedOkOnServer);
					}
					UtilMessage.dismisProgressDialog(dialog);

				}
			}
		}.start();

	}

	public void updateUser(final User user) throws Exception {

		UsersServ servUser = ModelConverter.convertToServerUser(user);
		// Session session = Session
		// .getSession(PerfilOwnerActivity.this);
		Session session = Session.getSession(PerfilOwnerActivity.this);
		servUser.setId(session.getUser().getId());
		session.setUser(servUser);
		ChatIntentService.getSession().getUser().setName(user.getName());
		Session.saveSessionOnPreferences(PerfilOwnerActivity.this);

		try {
			RequestManager.updateUser(servUser);

		} catch (Exception e) {
			user.getSession().setUserToUpdate(true);
			Session.saveSessionOnPreferences(PerfilOwnerActivity.this);
			Log.i(Config.TAG_INFO, "error updating the user on database");
			// e.printStackTrace();
		}
	}

	public UsersServ convertToServUser(final User user) {

		UsersServ servUser = ModelConverter.convertToServerUser(user);
		// Session session = Session
		// .getSession(PerfilOwnerActivity.this);
		Session session = Session.getSession(PerfilOwnerActivity.this);
		servUser.setId(session.getUser().getId());
		session.setUser(servUser);
		return servUser;
		// Session.saveSessionOnPreferences(PerfilOwnerActivity.this);
		// RequestManager.updateUser(servUser);
	}

	/**
	 * Comprueba que el nuevo dato introducido por el usuario cumpla con las
	 * limitaciones de MAX_CHARS_ALLOWED y que no contenga saltos de línea.
	 * 
	 * @param value
	 *            la String a comprobar
	 * @return true si se supera el test
	 */
	public boolean checkNewData(String value, int checkMode) {

		if (checkMode == CHECK_MODE_SMALL_SIZE) {
			for (int i = 0; i < value.length(); i++) {
				char c = value.charAt(i);
				if (c == '\n') {
					displayWarning(WARNING_NEW_LINE_MESSAGE);
					return false;
				}
			}
			if (value.length() > MAX_CHARS_ALLOWED_SMALL_SIZE) {
				displayWarning(WARNING_MAX_CHARS_MESSAGE_SMALL_SIZE);
				return false;
			}
		} else if (checkMode == CHECK_MODE_BIGGER_SIZE) {
			if (value.length() > MAX_CHARS_ALLOWED_BIGGER_SIZE) {
				displayWarning(WARNING_MAX_CHARS_MESSAGE_BIGGER_SIZE);
				return false;
			}
		}
		return true;
	}

	public boolean validateName(String username) {

		boolean isValid = false;

		StringBuilder errorMsg = new StringBuilder();
		if (username == null || username.isEmpty()) {
			errorMsg.append("The name is empty\n");
		} else if (username.length() < Validation.USER_NAME_MIN_LENGTH
				|| username.length() > Validation.USER_NAME_MAX_LENGTH) {
			errorMsg.append(String.format(
					"The name has to be beetwen %d and %d%n" + " characters",
					Validation.USER_NAME_MIN_LENGTH,
					Validation.USER_NAME_MAX_LENGTH));
		}
		isValid = (errorMsg.toString().isEmpty() == true);
		Log.i("info", "validate login -> " + isValid);
		if (isValid == false) {
			UtilMessage.showErrorMessage(PerfilOwnerActivity.this,
					errorMsg.toString());
		}
		return isValid;
	}

	public boolean validatePassword(String password) {

		boolean isValid = false;

		StringBuilder errorMsg = new StringBuilder();

		if (password == null || password.isEmpty()) {
			errorMsg.append("The password is empty\n");
		} else if (password.length() < Validation.PASS_MIN_LENGTH
				|| password.length() > Validation.PASS_MAX_LENGTH) {
			errorMsg.append(String.format(
					"The password has to be beetwen %d and %d%n"
							+ " characters", Validation.PASS_MIN_LENGTH,
					Validation.PASS_MAX_LENGTH));
		}
		isValid = (errorMsg.toString().isEmpty() == true);
		Log.i("info", "validate login -> " + isValid);
		if (isValid == false) {
			UtilMessage.showErrorMessage(PerfilOwnerActivity.this,
					errorMsg.toString());
		}
		return isValid;
	}

	/**
	 * Muestra un Toast de advertencia, el mensaje es modificable.
	 * 
	 * @param activity
	 *            Activity donde se muestra el Toast
	 */
	public void displayWarning(String message) {
		Toast toast = Toast.makeText(this, message, Toast.LENGTH_LONG);
		View v = toast.getView();
		v.setBackgroundColor(Color.rgb(255, 204, 0));
		toast.setGravity(Gravity.CENTER | Gravity.TOP, 0, 20);
		toast.show();
	}

	@Override
	public void onBackPressed() {
		if (hasChangedProfile == true) {
			confirmChanges();
		} else {
			Intent mplayer = new Intent(this, MusicPlayerActivity.class);
			startActivity(mplayer);
		}

	}

	public boolean confirmChanges() {
		confirmChanges = false;

		if (hasChangedProfile == false) {
			confirmChanges = true;
			return confirmChanges;
		}
		new AlertDialog.Builder(this)
				.setIcon(android.R.drawable.ic_dialog_alert)
				.setTitle("You have unsaved changes")
				.setMessage("Are you sure you want to leave without save?")
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								PerfilOwnerActivity.this.finish();
							}

						}).setNegativeButton("No", null).show();
		return confirmChanges;
	}

	public static Class<?> getPreviousActivity() {
		return previousActivity;
	}

	public static void setPreviousActivity(Class<?> previousActivity) {
		PerfilOwnerActivity.previousActivity = previousActivity;
	}

	/**
	 * Dado un nuevo nombre, o email, o pass, o ciudad, o país, o preferencias
	 * musicales, o descripción personal, pero no la foto de usuario, cambia el
	 * dato del owner actual.
	 * 
	 * @param oc
	 *            el ObjectContainer de la db
	 * @param dataType
	 *            el tipo de dato a cambiar
	 * @param newData
	 *            el nuevo dato para el usuario
	 * @return
	 */
	public boolean isDataDifferent(String dataType, String newData) {

		if (dataType.equals(NAME_DATA)) {
			return this.tvNombre.getText().equals(newData) == false;
		} else if (dataType.equals(PASS_DATA)) {
			return this.tvPass.getText().equals(newData) == false;
		} else if (dataType.equals(CITY_DATA)) {
			return this.tvCiudad.getText().equals(newData) == false;
		} else if (dataType.equals(COUNTRY_DATA)) {
			return this.tvPais.getText().equals(newData) == false;

		} else if (dataType.equals(PERSONAL_DESCRIPTION_DATA)) {
			return this.tvDescripcionPersonal.getText().equals(newData) == false;

		} else if (dataType.equals(MUSIC_PREFERENCES_DATA)) {
			return this.tvGustosMusicales.getText().equals(newData) == false;

		}
		Log.w(Config.TAG_WARNING,
				" WARNING --- the value data type match any expected field");
		return false;

	}

	public static boolean isHasChangedProfile() {
		return hasChangedProfile;
	}

	public static void setHasChangedProfile(boolean hasChangedProfile) {
		PerfilOwnerActivity.hasChangedProfile = hasChangedProfile;
	}

	public static Integer getSelectedAv() {
		return selectedAv;
	}

	public static void setSelectedAv(Integer selectedAv) {
		PerfilOwnerActivity.selectedAv = selectedAv;
	}

	public static boolean isConfirmChanges() {
		return confirmChanges;
	}

	public static void setConfirmChanges(boolean confirmChanges) {
		PerfilOwnerActivity.confirmChanges = confirmChanges;
	}

}
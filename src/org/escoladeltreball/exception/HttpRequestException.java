package org.escoladeltreball.exception;

public class HttpRequestException extends Exception {

	private static final long serialVersionUID = 1L;

	public HttpRequestException(String message) {
		super(message);
	}

}

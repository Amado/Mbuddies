package org.escoladeltreball.support;

//import org.apache.commons.codec.binary.Base64;

import java.io.ByteArrayOutputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Base64;

/**
 * convierte un array de bytes en un string en base 64
 */
public class ImageConverter {

	/**
	 * Encodes the byte array into base64 string
	 * 
	 * @param imageByteArray
	 *            - byte array
	 * @return String a {@link java.lang.String}
	 */
	public static String encodeImage(byte[] imageByteArray) {
		// Log.i("INFO", Base64.class.getProtectionDomain().getCodeSource()
		// .getLocation().toString());
		// return Base64Prt.encode(imageByteArray);
		return Base64.encodeToString(imageByteArray, Base64.URL_SAFE);
	}

	public static String encodeImage(Bitmap photo) {
		// Log.i("INFO", Base64.class.getProtectionDomain().getCodeSource()
		// .getLocation().toString());

		ByteArrayOutputStream byteArrOutStream = new ByteArrayOutputStream();
		photo.compress(Bitmap.CompressFormat.JPEG, 100, byteArrOutStream);
		byte[] imageBytes = byteArrOutStream.toByteArray();
		String encodedImage = Base64
				.encodeToString(imageBytes, Base64.URL_SAFE);

		return encodedImage;
		// return Base64.encode(imageByteArray);
	}

	/**
	 * Decodes the base64 string into byte array
	 * 
	 * @param imageDataString
	 *            - a {@link java.lang.String}
	 * @return byte array
	 */
	public static byte[] decodeImage(String imageDataString) {
		// String encodedImage = Base64.encodeToString(input,
		// flags)(imageDataString , Base64.DEFAULT);
		// Log.i("INFO", Base64.class.getProtectionDomain().getCodeSource()
		// .getLocation().toString());
		return Base64.decode(imageDataString, Base64.URL_SAFE);
	}

	/**
	 * 
	 * @param bitmap
	 * @param size
	 * @return
	 */
	public static Bitmap scaleImage(Bitmap bitmap, int size) {
		Bitmap resizedBitmap = null;
		int originalWidth = bitmap.getWidth();
		int originalHeight = bitmap.getHeight();
		int newWidth = -1;
		int newHeight = -1;
		float multFactor = -1.0F;
		if (originalHeight > originalWidth) {
			newHeight = size;
			multFactor = (float) originalWidth / (float) originalHeight;
			newWidth = (int) (newHeight * multFactor);
		} else if (originalWidth > originalHeight) {
			newWidth = size;
			multFactor = (float) originalHeight / (float) originalWidth;
			newHeight = (int) (newWidth * multFactor);
		} else if (originalHeight == originalWidth) {
			newHeight = size;
			newWidth = size;
		}
		resizedBitmap = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight,
				false);
		return resizedBitmap;
	}

	/**
	 * 
	 * @param songImageBitmap
	 * @return
	 */
	public static byte[] getLowByteArrayFromBitmap(Bitmap songImageBitmap) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		// songImageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
		songImageBitmap = scaleImage(songImageBitmap, 150);
		songImageBitmap.compress(Bitmap.CompressFormat.JPEG, 45, stream);
		byte[] byteArray = stream.toByteArray();

		return byteArray;
	}

	/**
	 * Convierte un objeto Bitmap en un array de bytes.
	 * 
	 * @param songImageBitmap
	 *            el objeto Bitmap a convertir
	 * @return el array de bytes
	 */
	public static byte[] getByteArrayFromBitmap(Bitmap songImageBitmap) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		// songImageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
		// songImageBitmap = scaleImage(songImageBitmap, 400);
		songImageBitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream);
		byte[] byteArray = stream.toByteArray();

		return byteArray;
	}

	public static Bitmap getBitmpaFromByteArray(byte[] songImageBytes) {
		return BitmapFactory.decodeByteArray(songImageBytes, 0,
				songImageBytes.length);

	}

	public static Bitmap drawableToBitmap(Drawable drawable) {
		if (drawable instanceof BitmapDrawable) {
			return ((BitmapDrawable) drawable).getBitmap();
		}

		Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
				drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap);
		drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
		drawable.draw(canvas);

		return bitmap;
	}
}
package org.escoladeltreball.support;

import org.escoladeltreball.musicPlayer.MusicPlayerActivity;

import android.graphics.Color;

/**
 * clase de configuracion de la aplicacion
 */
public interface Config {

	// public static final String ERROR_SERVER_TYPE = "server error";
	/*
	 * establece cuando y como se ha de logear el usuario
	 */
	// siempre se ha de logear al abrir la aplicacion
	final static boolean OBLIGATE_LOGIN = true;
	// siempre se ha de logear conectandose al servidor (sin usar la sesion)
	// recomendable poner en true
	final static boolean ALWAYS_LOG_ONLINE = true;

	/*
	 * nombre base de las imagenes y los botones de los avatares -> (btAv1,
	 * btAv2, .... btAvn) donde n = AVATAR_MAX_IMGS
	 */
	final static String AVATAR_BOTON_BASE_NAME = "btAv",
			AVATAR_IMG_BASE_NAME = "av";
	final static int AVATAR_MAX_IMGS = 15;
	/*
	 * establece si se ha de ejecutar algun codigo que solo este pensado para
	 * realizar pruebas
	 */
	public static final boolean TEST_APP = false;
	/*
	 * nombre de las shared preferences usadas para almacenar los datos de la
	 * sesion y darles persistencia
	 */
	public static final String SHARED_PREF_NAME = "SHARED_PREF_NAME",
			SHARED_PREF_SESSION = "SHARED_PREF_SESSION",
			SHARED_PREF_SESSION_CHAT_MESSAGES = "SHARED_PREF_SESSION_FRIEND_REQUEST";
	/**
	 * <pre>
	 * Tipo de mensajes que puede devolver el servidor que informan de algun
	 * error en la petcion o si se ha resuelto correctamente
	 *   ( usado en el objeto StatusPackage que puede devolver el servidor )
	 * </pre>
	 */
	public static final String JSON_OB_TYPE_ERROR = "ERROR",
			JSON_OB_TYPE_SUCCESS = "SUCCESS";

	/*
	 * TAGS usados en el LogCat de Android
	 */
	public static final String TAG_INFO = "INFO", TAG_ERROR = "ERROR",
			TAG_DEBUG = "DEBUG", TAG_WARNING = "WARNING";

	/**
	 * <pre>
	 * -----------------------------------------------------------------------------------------------------
	 *  
	 *   Parametros de Configuracion de la Conexion con el Servidor
	 *   
	 * -----------------------------------------------------------------------------------------------------
	 * </pre>
	 */
	/**
	 * URLs del servidor
	 */

	/**
	 * LAN 1
	 */
	public static final String URL_WEBSOCKET = "ws://192.168.1.217:8080/WebServer/chat?idUser=";
	public static final String SERVER_URL = "http://192.168.1.217:8080/WebServer/HttpServlet/";

	/**
	 * OPENSHIFT
	 */
	// public static final String URL_WEBSOCKET =
	// "ws://glassfish4-musicfriends.rhcloud.com:8000/WebServer/chat?idUser=";
	// public static final String SERVER_URL =
	// "http://glassfish4-musicfriends.rhcloud.com:8000/WebServer/HttpServlet/";

	//

	/**
	 * constantes compartidas entre activities
	 */
	public static interface ConfigActivity {
		// acitivity que se inicia despues de realizar el login
		// final public Class<?> ACTIVITY_POS_LOGIN = ChatActivity.class;
		// final public Class<?> ACTIVITY_POS_LOGIN = SearchUsersActivity.class;
		public static final Class<?> ACTIVITY_POS_LOGIN = MusicPlayerActivity.class;
		public static final String PROFILE_TILE = "%s - Profile";
	}

	/**
	 * 
	 * variables de configuracion de la conexion cuando se realiza un peticion
	 * al servidor
	 */
	public static interface Connection {
		public static final String CHARACTER_ENCODING = "UTF-8";
		/**
		 * <pre>
		 * Maximun time to wait for server connection on post request  
		 *  -readTimeout: Sets the maximum time to wait for an input stream read to complete before giving up
		 *  -connectTimeout: Sets the maximum time in milliseconds to wait while connecting.
		 * </pre>
		 */
		public static final int readTimeout = 15000, connectTimeout = 15000;
	}

	/*
	 * l <SHARED VARIABLES BEETWEN SERVER AND CLIENT COMMUNICATION>
	 */
	/**
	 * variables con los parametros que se pueden parsar al servidor al realizar
	 * una peticion
	 */
	public static interface HttpArgs {

		public static final String VALUE_ALL = "all";
		public static final String VALUE_TRUE = "TRUE";
		public static final String VALUE_FALSE = "FALSE";
		/*
		 * Users attribures
		 */
		// public static final String latitude = "latitude";
		// public static final String longitude = "longitude";
		public static final String idUser = "idUser";
		public static final String username = "username";
		// public static final String mail = "mail";
		public static final String password = "password";
		public static final String city = "city";
		public static final String country = "country";
		public static final String personalDescription = "personalDescription";
		public static final String musicPreferences = "musicPreferences";
		public static final String photo = "photo";
		public static final String idPhoto = "idPhoto";
		//
		public static final String withSongObject = "withSongObject";
		// public static final String photoResource = "photoResource";

		/*
		 * favorites to identify a song y server db: the identify field instead
		 * of idFavorite
		 */
		// public static final String idFavorites = "idFavorites";
		public static final String favoritesName = "favoritesName";
		public static final String favoritesNewName = "favoritesNewName";
		public static final String favoritesIdentification = "favoritesIdentification";
		public static final String favoriteAndItsSongs = "favoriteAndItsSongs";
		public static final String favoritesList = "favoritesList";

		/*
		 * songs attributes to identify a song y server db: is necessary the
		 * userId and the songSrcPath
		 */
		// public static final String idSong = "idSong";
		public static final String songName = "songName";
		public static final String songAuthor = "songAuthor";
		public static final String songSrcPath = "songSrcPath";
		public static final String songPhotoEncoded = "songPhotoEncoded";
		/*
		 * users_friends
		 */
		public static final String idUserFriend = "idUserFriend";
		public static final String idUserOwner = "idUserOwner";
	}

	/**
	 * variables que establecen las peticiones que el servidor puede responder
	 */
	public static interface HttpActions {
		/*
		 * Users
		 */
		public static final String LOGIN = "login";
		public static final String SIGNUP = "signUp";
		public static final String UPDATE_USER_PROFILE = "updateProfile";
		public static final String REMOVE_USER = "removeUser";
		//
		public static final String FIND_ALL_USERS = "findAllUsers";
		public static final String FIND_USER = "findUser";
		public static final String FIND_USERS_BY_COUNTRY_AND_CITY = "findUsersByCountryAndCity";
		public static final String UPDATE_CURRENT_PLAY_SONG = "findCurrentPlaySong";
		public static final String FIND_DISTINCT_COUNTRIES_AND_CITIES = "findDistinctCountriesAndCities";
		public static final String FIND_DISTINCT_CITIES_FROM_COUNTRY = "findDistinctCitiesFromCountry";
		public static final String FIND_FRIENDS_FROM_USER = "findFriendsFromUsers";
		//
		public static final String TEST = "test"; // to debug
		public static final String CREATE_TEST_DATA = "createTestData";

		/*
		 * Friends (idenficiation of users by name instead of their id)
		 */
		public static final String SEND_FRIEND_REQUEST = "sendFriendRequest";
		public static final String RESPONSE_FRIEND_REQUEST = "responseFriendRequest";
		public static final String DISMIS_FRIEND_REQUEST = "dismisFriendRequest";
		public static final String FIND_FRIENDS_REQUEST = "findFriendsRequest";
		public static final String REMOVE_FRIEND = "removeFriend";
		public static final String ADD_FRIEND = "addFriend";

		/*
		 * Songs / Favorites
		 */
		public static final String UPDATE_SONG = "updateSong";
		public static final String ADD_SONG_TO_FAVORITE = "addSongToFavorite";
		public static final String REMOVE_SONG_FROM_FAVORITE = "removeSongFromFavorite";
		// ADD_OR_UPDATE_FAVORITE -> (only updates its name)
		public static final String ADD_OR_UPDATE_FAVORITE = "addUpdateFavorite";
		public static final String UPDATE_FAVORITE_AND_ITS_SONGS = "updateFavoriteAndItsSongs";
		public static final String COMMIT_FAVORITES = "commitFavorites";
		// REMOVE_FAVORITE -> (only removes the orphans songs)
		public static final String REMOVE_FAVORITE = "removeFavorite";

		// FIND_SONGS -> params: [ user | favorite ]
		public static final String FIND_SONGS = "findSongs";
		public static final String FIND_FAVORITES_FROM_USER = "findFavoritesFromUser";

	}

	/*
	 * </SHARED VARIABLES BEETWEN SERVER AND CLIENT COMMUNICATION>
	 */

	public static interface Validation {
		public static final int USER_NAME_MIN_LENGTH = 3,
				USER_NAME_MAX_LENGTH = 60;
		public static final int PASS_MIN_LENGTH = 6, PASS_MAX_LENGTH = 20;
	}

	public static interface Style {
		public static final int TOAST_INFO_COLOR = Color.parseColor("#A5D7D6");
		public static final int TOAST_ERROR_COLOR = Color.parseColor("#FF5F5A");
		public static final int TOAST_WARNING_COLOR = Color
				.parseColor("#FFE681");

	}
}
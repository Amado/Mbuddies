package org.escoladeltreball.support;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.escoladeltreball.manager.UpdateLocationListener;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

/**
 * Usando un network provider, obtiene la latitud y la longitud del dispositivo
 * y gestiona la localización exacta en cuanto a ciudad y país del dispositivo
 * con Geocoder.
 * 
 * @author Emilio Amado 2015
 * 
 */
public class FindMyAddress extends Thread implements LocationListener {

	private Activity activity;
	private UpdateLocationListener updatelocationListener;
	private LocationManager locationManager;
	private Location location;
	private Location nwLocation;

	// distancia mínima para que se actualize la posicion 10 km
	private static final long MIN_DISTANCE_FOR_UPDATE = 10000;
	// tiempo mínimo para que se actualice la posicion 3 horas
	private static final long MIN_TIME_FOR_UPDATE = 1000 * 60 * 60 * 3;

	private ArrayList<String> data = new ArrayList<String>();

	/**
	 * Constructor.
	 * 
	 * @param la
	 *            Activity de la app
	 */
	public FindMyAddress(Activity activity,
			UpdateLocationListener locationListener) {
		this.activity = activity;
		this.updatelocationListener = locationListener;
	}

	/**
	 * Es necesario llamar a Looper.prepare(), para realizar las acciones fuera
	 * del Thread de UI, una vez hecho esto, iniciamos las operaciones en un
	 * nuevo Thread, ATENCIÓN, ASYNKTASK NO FUNCIONA AQUÍ.
	 * 
	 * @author Emilio Amado 2015
	 * 
	 */
	public void run() {
		try {
			Looper.prepare();
			initializeLocation();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Inicializa los servicios para la geolocalización del dispositivo.
	 */
	public void initializeLocation() {

		Log.i(Config.TAG_INFO, "FindMyAdress initializeLocation() .....");
		// instanciamos un LocationManager
		locationManager = (LocationManager) activity
				.getSystemService(activity.LOCATION_SERVICE);

		// obtenemos las coordenadas del dispositivo
		nwLocation = getLocation(LocationManager.NETWORK_PROVIDER);

		if (nwLocation == null) {
			Log.w(Config.TAG_WARNING,
					"FindMyAdress initializeLocation()  WARNING ---  nwLocation = "
							+ nwLocation
							+ "\nNo se han podido obtener las coordenadas del dispositivo\n"
							+ "probablemente porque no tiene habilitado el GPS");
		} else {
			// obtenemos la localización
			getCityAndCountry(nwLocation);
		}
	}

	/**
	 * Dado un provider , en este caso network provider devuelve la localización
	 * del dispositivo en coordenadas.
	 * 
	 * @param provider
	 *            el provider para obtener la localización
	 * @return objeto Location con la localización del dispositivo
	 */
	public Location getLocation(String provider) {
		if (locationManager.isProviderEnabled(provider)) {
			locationManager.requestLocationUpdates(provider,
					MIN_TIME_FOR_UPDATE, MIN_DISTANCE_FOR_UPDATE, this);
			if (locationManager != null) {
				location = locationManager.getLastKnownLocation(provider);
				return location;
			}
		}
		return null;
	}

	/**
	 * Dado una instancia a Location obtiene la localización del dispositivo.
	 * 
	 * @param l
	 *            la instancia a Location
	 */
	public void getCityAndCountry(Location l) {
		Log.e("INFO", "FindMyAdress - getCityAndCountry() location = " + l);
		// si hemos podido obtener as coordenadas
		// if (l != null) {
		try {

			setGeocoder(l);

			ArrayList<String> data = getData();

			boolean goOn = true;

			while (goOn) {
				data = getData();
				if (data.size() == 2) {
					goOn = false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		String city = null;
		String country = null;

		if (data.get(0) != null) {
			city = data.get(0);
		}
		if (data.get(1) != null) {
			country = data.get(1);
		}
		
		FindMyAddress.this.updatelocationListener.updateLocalization(city,
				country);
		// ChatIntentService.udpateLocalization
		// MainActivity ma = new LoginActivity();
		// Lon.updateData(data);

		// si no se han podido obtener las coordenadas abrimos la ventana
		// } else {
		// showSettingsAlert();
		// }
	}

	/**
	 * Realiza una instancia del objeto geocoder para obtener la localización
	 * del dispositivo.
	 * 
	 * @param location
	 *            objeto Location con la latitud y la longitud
	 */
	public void setGeocoder(Location location) {
		Log.e("INFO", "FindMyAdress - setGeocoder()");
		Geocoder geocoder = new Geocoder(activity, Locale.getDefault());
		double latitude = location.getLatitude();
		double longitude = location.getLongitude();

		try {

			List<Address> addressList = geocoder.getFromLocation(latitude,
					longitude, 1);

			if (addressList != null && addressList.size() > 0) {

				Address address = addressList.get(0);
				data.add("" + address.getLocality());
				data.add("" + address.getCountryName());
			}

		} catch (IOException e) {
			Log.e("", "Unable to parse response from server at "
					+ "android.location.Geocoder.getFromLocation"
					+ "(Geocoder.java:136)");

			new Thread() {
				public void run() {
					activity.runOnUiThread(new Runnable() {
						public void run() {
							Toast.makeText(
									activity,
									"impossible to find location "
											+ "please try it later",
									Toast.LENGTH_LONG).show();
						}
					});
				}
			}.start();
			return;
		}

		new Thread() {
			public void run() {
				activity.runOnUiThread(new Runnable() {
					public void run() {

						String city = null;
						String country = null;

						if (data.get(0) != null) {
							city = data.get(0);
						}
						if (data.get(1) != null) {
							country = data.get(1);
						}

						FindMyAddress.this.updatelocationListener
								.updateLocalization(city, country);

						Log.i(Config.TAG_INFO, "FindMyAdress, country: "
								+ country + ", city = " + city);
						// Toast.makeText(
						// activity,
						// "location found at :\n" + "city : " + city
						// + "\n" + "country : " + country,
						// Toast.LENGTH_LONG).show();
					}
				});
			}
		}.start();
	}

	public ArrayList<String> getData() {
		return data;
	}

	/**
	 * Abre una ventana emergente que permite configurar los
	 * "servicios de ubicación" (provider settings) del dispositivo.
	 */
	public void showSettingsAlert() {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);

		alertDialog.setTitle("NETWORK SETTINGS");

		alertDialog
				.setMessage("NETWORK is not enabled! Want to go to settings menu?");

		alertDialog.setPositiveButton("Settings",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(
								Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						activity.startActivity(intent);
					}
				});

		alertDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		alertDialog.show();
	}

	/*
	 * Método propio de LocationListener, se dispara al cambiar de localización
	 * el dispositivo.
	 */
	@Override
	public void onLocationChanged(Location arg0) {
		getCityAndCountry(arg0);
	}

	@Override
	public void onProviderDisabled(String arg0) {

	}

	@Override
	public void onProviderEnabled(String arg0) {

	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {

	}

}

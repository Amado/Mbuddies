package org.escoladeltreball.support;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HttpsURLConnection;

import org.apache.http.HttpException;
import org.escoladeltreball.db4o.Db4oHelper;
import org.escoladeltreball.exception.ValidationException;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public final class Utils {
	public static final String ERROR_LOCAL_DB_TYPE = "local error";
	public static final String ERROR_SERVER_TYPE = "server error";

	public static byte[] getBytesArrayFromBitMap(Bitmap bmp) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
		byte[] byteArray = stream.toByteArray();
		return byteArray;

	}

	public static Bitmap getBitmapFromBytes(byte[] byteArray) {
		Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0,
				byteArray.length);
		return bmp;
	}

	public static Class<?> deSerializeFromJson(String jsonObject, Class<?> clase) {
		Class<?> paquete = null;
		try {

			Gson gson = new GsonBuilder().disableHtmlEscaping()
					.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
					.setPrettyPrinting().serializeNulls().create();

			paquete = (Class<?>) gson.fromJson(jsonObject, clase);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Log.i("info", "paquete.class = " + paquete.getClass());
		Log.i("info", "paquete.toString = " + paquete.toString());
		return paquete;
	}

	public static String createUrl2(final String action, final String[] params,
			final String[] values) throws UnsupportedEncodingException,
			ValidationException {

		String result = null;
		try {
			if (params == null || values == null) {
				result = Config.SERVER_URL + action;
			} else {
				if (params.length != values.length) {
					throw new ValidationException(
							"Exception creating url the number of params and values must be the same");
				}
				StringBuilder paramsValues = new StringBuilder("?");
				for (int i = 0; i < values.length; i++) {
					paramsValues
							.append(params[i]
									+ "="
									+ URLEncoder
											.encode(values[i],
													Config.Connection.CHARACTER_ENCODING)
									+ "&");
				}

				// paramsValues.delete(paramsValues.capacity() - 2,
				// paramsValues.capacity()-1);
				// result = URLEncoder.encode(paramsValues.toString(),
				// Config.CHARACTER_ENCODING);
			}
			int a = 0;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			Log.i("INFO", "result = " + result);
		}

		// String requestUrlStr = String.format(SERVER_URL + HttpActions.LOGIN
		// + "?%s=%s&%s=%s&%s=%s&%s=%s&%s=%s", HttpArgs.usernameArg,
		// username, HttpArgs.mailArg, mail, HttpArgs.passwordArg,
		// password, HttpArgs.city, city, HttpArgs.country, country);

		return result;
	}

	public static String createUrl(final String action, final String[] params,
			final String[] values) throws UnsupportedEncodingException,
			ValidationException {

		String result = null;
		try {
			if (params == null || values == null) {
				result = Config.SERVER_URL + action;
			} else {
				if (params.length != values.length) {
					throw new ValidationException(
							"Exception creating url the number of params and values must be the same");
				}
				StringBuilder paramsValues = new StringBuilder("?");
				for (int i = 0; i < values.length; i++) {
					paramsValues
							.append(URLEncoder.encode(params[i],
									Config.Connection.CHARACTER_ENCODING)
									+ "="
									+ URLEncoder
											.encode(values[i],
													Config.Connection.CHARACTER_ENCODING)
									+ "&");

					// paramsValues.append(params[i] + "="
					// + URLEncoder.encode(values[i], CHARACTER_ENCODING)
					// + "&");
				}

				paramsValues = paramsValues.delete(paramsValues.length() - 1,
						paramsValues.length());

				result = Config.SERVER_URL + action + paramsValues.toString();
				System.out.println("before encode");
				System.out.println("result = " + result);

			}
			int a = 0;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.out.println("result = " + result);
		}

		return result;
	}

	public String performPostCall(String requestURL,
			HashMap<String, String> postDataParams) {

		URL url;
		String response = "";
		try {
			url = new URL(requestURL);

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setReadTimeout(15000);
			conn.setConnectTimeout(15000);
			conn.setRequestMethod("POST");
			conn.setDoInput(true);
			conn.setDoOutput(true);

			OutputStream os = conn.getOutputStream();
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
					os, Config.Connection.CHARACTER_ENCODING));
			writer.write(getPostData(postDataParams));

			writer.flush();
			writer.close();
			os.close();
			int responseCode = conn.getResponseCode();

			if (responseCode == HttpsURLConnection.HTTP_OK) {
				String line;
				BufferedReader br = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
				while ((line = br.readLine()) != null) {
					response += line;
				}
			} else {
				response = "";

				throw new HttpException(responseCode + "");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}

	private String getPostData(HashMap<String, String> params)
			throws UnsupportedEncodingException {
		StringBuilder result = new StringBuilder();
		boolean first = true;
		for (Map.Entry<String, String> entry : params.entrySet()) {
			if (first)
				first = false;
			else
				result.append("&");

			result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
			result.append("=");
			result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
		}

		return result.toString();
	}

	/**
	 * Inicia un temporizador para cerrar la app si hubiese algún error en la db
	 * local o en el servidor , para evitar que la app se quede colgada.
	 * 
	 * @param errorType
	 *            si el error es de db local o del server
	 * @author Emilio Amado 2015
	 */
	public void executeCountDownToShutDown(final String errorType) {
		Thread db4oServerThread = new Thread(new Runnable() {
			@Override
			public void run() {
				if (errorType == ERROR_LOCAL_DB_TYPE) {
					Db4oHelper
							.displayDbError(Db4oHelper.ERROR_LOCAL_APP_WILL_CLOSE);
				} else if (errorType == ERROR_SERVER_TYPE) {
					Db4oHelper
							.displayDbError(Db4oHelper.ERROR_SERVER_APP_WILL_CLOSE);
				}
			}
		});
		Thread gpxClientThread = new Thread(new Runnable() {
			@Override
			public void run() {
				android.os.Process.killProcess(android.os.Process.myPid());
			}
		});
		// gestionamos la ejecucion de los hilos
		ScheduledExecutorService ses = Executors.newScheduledThreadPool(3);
		ses.schedule(db4oServerThread, 0, TimeUnit.SECONDS);// ejecutamos el
															// servidor
															// inmediatamente
		ses.schedule(gpxClientThread, 10, TimeUnit.SECONDS);// ejecutamos el
															// cliente gpx
	}

	
}

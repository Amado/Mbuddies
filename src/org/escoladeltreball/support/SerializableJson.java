package org.escoladeltreball.support;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class SerializableJson<T extends Object> {

	public String serializeToJson() {
		// .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
		Gson gson = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting()
				.serializeNulls().create();

		String objectJson = gson.toJson(this);
		// System.out.println(objectJson);
		return objectJson;
	}

	@SuppressWarnings("unchecked")
	public SerializableJson<T> deSerializeFromJson(String jsonObject) {
		SerializableJson<T> paquete = null;
		try {

			// .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
			Gson gson = new GsonBuilder().disableHtmlEscaping()
					.setPrettyPrinting().serializeNulls().create();

			// paquete = gson.fromJson(jsonObject, clase.getClass());
			paquete = gson.fromJson(jsonObject, this.getClass());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return paquete;
	}

}

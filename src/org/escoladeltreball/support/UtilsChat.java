package org.escoladeltreball.support;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class UtilsChat {
	private final static char[] hexArray = "0123456789ABCDEF".toCharArray();
	private Context context;
	private SharedPreferences sharedPref;

	// JSON flags to identify the kind of JSON response
	public static final String TAG_SELF = "self", TAG_NEW = "new",
			TAG_MESSAGE = "message", TAG_EXIT = "exit";
	/* 
	 */
	// private static final String KEY_SHARED_PREF = "ANDROID_WEB_CHAT";
	private static final String KEY_SHARED_PREF = "MBUDDIES_CHAT";
	private static final int KEY_MODE_PRIVATE = 0;
	public static final String KEY_TAG = "flag";
	//

	public static final String
	// flags
			FLAG_NEW = "new",
			FLAG_SELF = "self",
			TAG_UDPATE_PROFILE = "update_profile",
			FLAG_NEW_FRIENDS_REQ = "newFriendsReq",
			FLAG_MESSAGE = "message",
			FLAG_EXIT = "exit",
			TAG_NOTIFICATION = "notification",
			FLAG_MESSAGE_TO_TARGET = "messageToTarget",
			FLAG_MESSAGE_TO_ALL_FRIENDS = "messageAllFriends",
			TAG_NEW_FRIENDS_REQ = "newFriendsReq",
			// keys
			KEY_SESSION_ID = "sessionId",
			KEY_FLAG = "flag",
			KEY_MESSAGE = "message",
			KEY_TARGET_ID = "targetId",
			// KEY_SOURCE_ID = "sourceId",
			KEY_SRC_ID = "srcId",
			KEY_ONLINE_COUNT = "onlineCount",
			KEY_SRC_ID_IMG = "srcIdImg",
			KEY_NUM_FRIENDS_REQ = "numFriendsReq",
			KEY_SRC_NAME = "srcName",
			KEY_TARGET_NAME = "targetName",
			// Notifications.......................
			KEY_TYPE_NOT = "typeNotification",
			NOTIFICATION_NEW_FRIEND_REQUEST = "typeNotNewFriendRequest";
	//
	//
	//
	public static String KEY_NOTIFICATION_TYPE = "NOTIFICATION_TYPE";
	public static String NOT_WARNING = "NOT_WARNING", NOT_ERROR = "NOT_ERROR",
			NOT_INFO = "NOT_ERROR";

	public UtilsChat(Context context) {
		this.context = context;
		sharedPref = this.context.getSharedPreferences(KEY_SHARED_PREF,
				KEY_MODE_PRIVATE);
	}

	public void storeSessionId(String sessionId) {
		Editor editor = sharedPref.edit();
		editor.putString(KEY_SESSION_ID, sessionId);
		editor.commit();
	}

	public String getSessionId() {
		String sessionId = sharedPref.getString(KEY_SESSION_ID, null);
		Log.i("INFO", "SESSION ID = " + sessionId);
		return sessionId;
	}

	public String getSendMessageToTarget(String message, String sourceIdUser,
			String srcUserName, String targetIdUser) {
		// , Integer srcIdImg) {
		String json = null;

		try {
			JSONObject jObj = new JSONObject();
			jObj.put(KEY_TAG, FLAG_MESSAGE);
			jObj.put(KEY_SESSION_ID, getSessionId());
			jObj.put(KEY_MESSAGE, message);
			jObj.put(KEY_SRC_ID, sourceIdUser);
			jObj.put(KEY_TARGET_ID, targetIdUser);
			jObj.put(KEY_SRC_NAME, srcUserName);
			// jObj.put(KEY_SRC_ID_IMG, srcIdImg);

			json = jObj.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}

	/**
	 * Json message to notify to all friends about new contact is connected
	 * */
	public String getUpdateProfile(String srcName, String srcUserId,
			String message, String idImage) {
		String json = null;

		try {

			JSONObject jObj = new JSONObject();
			jObj.put(KEY_TAG, TAG_UDPATE_PROFILE);
			jObj.put(KEY_SRC_NAME, srcName);
			jObj.put(KEY_SRC_ID, srcUserId);
			jObj.put(KEY_SRC_ID_IMG, idImage);
			jObj.put(KEY_MESSAGE, message);
			json = jObj.toString();

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return json;
	}

	/**
	 * Json message to notify to all friends about new contact is connected
	 * */
	public String getNotificationNewFriendRequest(String srcName,
			String srcUserId, String message, String idImage) {
		String json = null;

		try {

			JSONObject jObj = new JSONObject();
			jObj.put(KEY_TAG, TAG_NOTIFICATION);
			jObj.put(KEY_TYPE_NOT, NOTIFICATION_NEW_FRIEND_REQUEST);
			jObj.put(KEY_SRC_NAME, srcName);
			jObj.put(KEY_SRC_ID, srcUserId);
			jObj.put(KEY_SRC_ID_IMG, idImage);
			jObj.put(KEY_MESSAGE, message);
			json = jObj.toString();

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return json;
	}

	 

	public static String bytesToHex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}

}
